<!DOCTYPE html>
<html lang="en" ng-app="appFormCreateUser">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/forms/createuser/loader.js"></script>	
		
	</head>
	
	<body style="display:none;font-size:13px;overflow-y:auto;" ng-controller="Forms" id="viewport">
		
		<div id="log_form" ng-controller="Webix">
		</div>
		
	</body>

</html>
