<!DOCTYPE html>
<html lang="en" app="/forms/setroles/form.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/forms/setroles/loader.js"></script>	
		
	</head>
	
	<body ng-controller="Dhtmlx" class="fill" otype="view" oid="{{ $id }}" id="viewport-{{ $id }}">
		
	</body>

</html>
