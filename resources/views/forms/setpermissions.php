<!DOCTYPE html>
<html lang="en" app="/forms/setpermissions/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/forms/setpermissions/loader.js"></script>	
		
		<style>
			
			.tree-multiselect{
				font-family:arial !important;
				
			}
			
			div.tree-multiselect div.title {
				background-color: transparent !important;
				color: #3B3B3B !important;
				border-bottom-style:solid !important;
				border-color:#DBDBDB !important;
				border-bottom-width:1px !important;
			}
			
			div.tree-multiselect {
				margin-top:10px;
				color: #666 !important;
				border:0px solid #D8D8D8 !important;
			}
			
			div.tree-multiselect>div.selections {
				
				border-right:0px !important;
				
			}
			
        </style>
		
	</head>
	
	<body style="display:none;font-size:13px;overflow-y:auto;" id="viewport">
		
		<div ng-controller="Webix">
			
			<div id="formAdd"></div>
			
			<div>
				<select id="permissions" multiple="multiple">
				
				</select>		
			</div>
			
			<div id="formRemove"></div>
			
		</div>
		
	</body>

</html>
