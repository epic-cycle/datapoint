<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Data Point</title>
		
		<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
		
	</head>
	
	<body style="padding-top:3px;">
		
		<div class="container">  
			<div class="row">  
				<div class="center-block">  
					<div class="span4 alert alert-danger" style="text-align:center;">
						You are not authorized for this request.<br/><br/>
						<span style="color:gray">
							{{ $request->method()}} : {{ $request->getPathInfo()}}
						</span>
					</div>
				</div>  
			</div>  
		</div>  
		
	</body>
	
</html>
