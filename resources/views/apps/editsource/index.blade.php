<!DOCTYPE html>
<html lang="en" app="/apps/editsource/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/editsource/loader.js"></script>
		
	</head>
	
	<body ng-controller="Objects">
		
		<div ng-controller="Editsource" id="editor" otype="source" oid="{{ $id }}" class="fill">
			
		</div>
		
	</body>

</html>
