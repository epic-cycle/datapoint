<!DOCTYPE html>
<html lang="en" ng-app="appEditView">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/datatables/loader.js"></script>	
		<link href="{{ asset('/css/datatables.css') }}" rel="stylesheet">
		
	</head>
	
	<body ng-controller="Objects" style="display:none;padding:20px;">
		
		<div ng-controller="Datatables" id="viewport" class="fill">
			
			<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" datatable="ng" class="row-border hover">
			
			</table>
			
		</div>
		
	</body>

</html>
