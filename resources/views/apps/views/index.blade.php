<!DOCTYPE html>
<html lang="en" app="/apps/views/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/deps/main.js"></script>
		<script type="text/javascript" src="/apps/views/loader.js"></script>		
		<link href="/apps/views/app.css" rel="stylesheet">
		
		
		<!--		
                <script src="/dist/views.js" type="text/javascript"></script>
                <link href="/dist/views.css" rel="stylesheet">
		-->
		
		<link href='https://fonts.googleapis.com/css?family=Product+Sans' rel='stylesheet' type='text/css'>
		
		<style>
			.grid-stack-item-content, .placeholder-content{
			    left : {!! $space !!}px !important;
			    top : {!! $space !!}px !important;
			    right : 0px !important;
			    margin-bottom : -1px !important;
			    
			}
			
			.dhx_cell_cont_wins{
				background-color : {!! $background !!} !important;
			}
			
			.dhx_cell_wins{
				background-color : {!! $background !!} !important;
			}			
			
		</style>
		
	</head>
	
	<body ng-controller="Components">
		
		<script type="text/javascript">
			
			var view = '{!! $view !!}';
			view = JSON.parse(view);
			
		</script>
		
		<div ng-controller="Gridstack" viewid="{{ $id }}" class="grid-stack"></div>
		
		<div class="tooltip_templates" ng-controller="Views">
			<span id="tooltip_content" style="z-index:100000000000000000">
				
				<span style="cursor:pointer" ng-click="componentSettings()">Settings</span>
				
			</span>
		</div>
		
	</body>

</html>
