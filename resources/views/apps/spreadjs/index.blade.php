<!DOCTYPE html>
<html lang="en" app="/apps/spreadjs/app.json">

	<head>
		
		<title>app/spreadjs</title>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/spreadjs/loader.js"></script>	
		
	</head>
	
	<body ng-controller="Spreadjs" id="spreadjs">
		
		<div id="content">
		
		</div>
		
	</body>

</html>
