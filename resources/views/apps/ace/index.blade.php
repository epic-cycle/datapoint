<!DOCTYPE html>
<html lang="en" app="/apps/ace/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/ace/loader.js"></script>	
		
		<style type="text/css" media="screen">
		    #editor { 
		        position: absolute;
		        top: 0;
		        right: 0;
		        bottom: 0;
		        left: 0;
		        font-size:24px;
		    }

			.ace_gutter-cell  {
				color: #A1A1A1 !important;
			}		    
		</style>
		
	</head>
	
	<body id="viewport" class="fill">
		
		<div ng-controller="Ace" id="editor"></div>
		
	</body>

</html>
