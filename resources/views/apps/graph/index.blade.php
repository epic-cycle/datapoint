<!DOCTYPE html>
<html lang="en" app="/apps/graph/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/graph/loader.js"></script>
		
<style>

.bar {
  fill: steelblue;
}

.bar:hover {
  fill: brown;
}

.axis--x path {
  display: none;
}

body {
	
	cursor:pointer;
	height:100%;
	width:100%;
	margin:0px;

}

.fill {

	position: absolute;
	top: 0px;
	bottom: 0px;
	left:0px;
	width: 100%;
	display:table;
	height:100%;
	
}

.graph-description-container {
	
    height:100%;
    display:table-cell;
    vertical-align:middle;
    text-align:center;	
	
}

.graph-description-image {
	
	height:25%;
    opacity: 0.5;
    filter: alpha(opacity=50);
	
}

.graph-description-text {

	display: inline-block;
	width:70%;
	color:#898989;
	font-family:arial;
	
}

</style>

	</head>
	
	<body>
		
		<div ng-controller="Graph" id="graph" class="fill">
		
		</div>
		
	</body>

</html>
