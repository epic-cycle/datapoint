<!DOCTYPE html>
<html lang="en">

	<head>
		
		<title>Data Point</title>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
          	<script src="/dist/main.js" type="text/javascript"></script>
          	<link href="/dist/main.css" rel="stylesheet">
		
	</head>
	
	<body>
		
		<div id="toolbar" style="display:none;">
		
		</div>
		
		
		<div id="playmenu" style="width:200px;background-color:#e0ded9;position:absolute;">
			
			<div id="playmenu-content" style="display:none;width:100%;height:100%;text-align:center;">
				
				<div style="display:table;height:100%;">
					
					<div style="display:table-cell;height:100%;vertical-align:middle;">
						
						<img style="height:128px;" src="/resources/shared/imgs/forward-128px.png"/>
						<img style="height:128px;margin-top:20px;" src="/resources/shared/imgs/backward-512px.png"/>
						
					</div>
					
				</div>
				
			</div>
			
		</div>			
		
		<div id="viewport" class="container fill" style="width:100%;background: url('/resources/shared/imgs/bg-login.png') repeat #1b1b1b;">  
		
		</div>
		
	</body>
	
</html>
