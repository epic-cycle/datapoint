<!DOCTYPE html>
<html lang="en" app="/apps/editview/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/editview/loader.js"></script>	
		
	</head>
	
	<body ng-controller="Editview" otype="view" oid="{{ $id }}" id="viewport-{{ $id }}">
		
	</body>

</html>
