<!DOCTYPE html>
<html lang="en" app="/apps/roles/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/roles/loader.js"></script>	
		
	</head>
	
	<body ng-controller="Roles" id="viewport" class="fill">
		
	</body>

</html>
