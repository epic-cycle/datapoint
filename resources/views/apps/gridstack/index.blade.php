<!DOCTYPE html>
<html lang="en" app="/apps/gridstack/app.json">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/deps/main.js"></script>
		<script type="text/javascript" src="/apps/gridstack/loader.js"></script>	
		<link href="{{ asset('/css/app.css') }}" rel="stylesheet">
	</head>
	
	<body class="darklue" style="margin-top:1px;display:none;padding-left:1px;padding-right:1px;">
		
		<div ng-controller="Gridstack" class="grid-stack" viewid="{{ $id }}" data-gs-width="12" data-gs-animate="yes">
		
		</div>
		
	</body>

</html>

