<!DOCTYPE html>
<html lang="en" app="/apps/editmodel/app.json">

	<head>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/apps/editmodel/loader.js"></script>
		
	</head>
	
	<body>
		
		<div ng-controller="Editmodel" id="editor" otype="model" oid="{{ $id }}" class="fill">
			
		</div>
		
	</body>

</html>
