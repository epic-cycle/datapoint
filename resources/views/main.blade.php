<!DOCTYPE html>
<html lang="en">

	<head>
		
		<title>Data Point</title>
		
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

         	<link href="/apps/main/app.css" rel="stylesheet">
		<script type="text/javascript" src="/vendor/ljs/l.min.js"></script>
		<script type="text/javascript" src="/deps/main.js"></script>
		<script type="text/javascript" src="/apps/main/loader.js"></script>         	
		
	</head>
	
	<body style="background-color:white;">

		<nav class="navbar navbar-default navbar-fixed-top" style="z-index:1;background-color:#ebebeb;border-bottom-style:solid;border-width:1px;border-color:#D6D6D6">
			
			<div class="container-fluid">
				<div class="navbar-header">
					
					<div class="btn-group" style="position:absolute;left:0px;">
						
						<div style="display:table-cell;">
						
							<button style="background-color:transparent;" class="btn btn-secondary btn-lg dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<b>Data Point</b>
							</button>
							
							<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
								<li role="presentation"><a ng-click="playviews()" role="menuitem" href="#">Play views</a></li>
								<li role="presentation" class="divider"></li>
								<li role="presentation"><a role="menuitem" href="#">Logout</a></li>
							</ul>
							
						</div>
						
						<div id="toolbar" style="display:table-cell;">						
						</div>
						
					</div>
				</div>
				
				<div>
					
					<div id="menu">
						<ul>
							
							<?php
								
								function menuhtml($projects) {
									
									$string = "";
									
									foreach ($projects as $i => $project) {
										
										//$left = $project["depth"]*20+7;
										if(count($project["rows"])>0){
											
											if($project["depth"] == 1) $x = "go-down"; else $x="fly-left";
											
											$string .= '
												<li class="'.$x.'">
													<a href="#">'.$project['name'].'</a>
													<label for="fof-services-seo" class="toggle-sub" onclick="">&#9658;</label>
													<input type="checkbox" id="fof-services-seo" class="sub-nav-check" />
											';
											
											$string .= '
														<ul id="fof-sub" class="sub-nav">
											';
												
											$string .= menuhtml($project['rows']);
												
											$string .= '
														</ul>
											';
											
											$string .= '
												</li>
											';
											
										} else {
											
											// if view is not found then do not include
											
											if(file_exists(env('VERSION_PATH').'/view/'.$project["id"])) {
											
												$string .= '
													<li>
														<a ng-click="load(\'app/views/'.$project["id"].'\',\'View : '.$project['name'].'\')" href="#">'.$project['name'].'</a>
													</li>
												';
											
											}
											
										}
									}
									
									return $string;
								}
								
								$menuhtml = menuhtml($items);
								
								echo $menuhtml;
								
							?>
							
							<li class="go-down">
								
								<a href="#">Settings</a>
								<label for="fof-services-seo" class="toggle-sub" onclick="">►</label>
								<input type="checkbox" id="fof-services-seo" class="sub-nav-check">	
								
								<ul id="fof-sub" class="sub-nav">
									
									<li>
										<a ng-click="toggleToolbar()" href="#">Toolbar </a>
									</li>
									
									<li>
										<a ng-click="toggleHeader()" href="#">Header </a>
									</li>
									
								</ul>
									
							</li>
							
						</ul>
					</div>
				</div>
			</div>
		</nav>
		
		@yield('content')
		
		<div id="footer"></div>
		
		<div id="notes" style="top:0px;position:absolute;z-index:99999999999999999999999999999999999999999;display:none;">
		
		</div>	
		
	</body>
</html>
