var util = require('util');
var gulp = require('gulp');
var untar = require('gulp-untar');
var gunzip= require('gulp-gunzip');
var bower = require('gulp-bower');
var gulpSqlite3 = require('gulp-sqlite3');
var concat = require('gulp-concat');
var fs = require("fs");
var es = require("event-stream");
var replace = require('gulp-replace');
var git = require('gulp-git');
var shell = require('gulp-shell')
var clean = require('gulp-clean');
var args = require('yargs').argv;

global_deps = {};

ljs = {
	
	addAliases : function(a){
		
		Object.assign(deps,a);
		return this;
		
	},
	
	load : function(){
		
		for(var i in arguments){
			
			for(var a in deps[arguments[i]]){
				
				var path = "./public/" + deps[arguments[i]][a];
				path = path.split("?")[0];
				
				if(path.indexOf(".js") !== -1) {
					
					js.push(path);
					
				} 
				else if(path.indexOf(".css") !== -1) {
					
					css.push(path);
					
				} 
				
			}
		}
		
		return this;
		
	}

};

gulp.task('mods', ['bower'], function(cb) {
	
	return gulp.src('./mods/**/*')
	.pipe(gulp.dest('./'),cb);
	
});

gulp.task('dhtmlx', function (cb) {
	
	return gulp.src('./proprietary/dhtmlx.tar.gz')
	.pipe(gunzip())
	.pipe(untar())
	.pipe(gulp.dest('./public/vendor/proprietary/'),cb);
      
});

gulp.task('webix', function (cb) {

	return gulp.src('./proprietary/webix.tar.gz')
	.pipe(gunzip())
	.pipe(untar())
	.pipe(gulp.dest('./public/vendor/proprietary/'),cb);

});

gulp.task('wijmo', function (cb) {
	
	return gulp.src('./proprietary/wijmo.tar.gz')
	.pipe(gunzip())
	.pipe(untar())
	.pipe(gulp.dest('./public/vendor/proprietary/'),cb);
	
});

gulp.task('bower', function(cb) {
	
	return bower({interactive: true},cb);
    
});

gulp.task('db', function(cb) {
	
	return gulpSqlite3('storage/db/datapoint.db',cb);
    
});

gulp.task('bundle-views', ['deps'], function () {

	deps = {}, Object.assign(deps, global_deps), js = [], css = [], ljs = Object.assign({}, ljs);
	
	require('./public/apps/views/loader.js');

	return es.concat(
		
		gulp.src(css)
		.pipe(concat("views.css"))
		.pipe(gulp.dest("./public/dist/")),
		
		gulp.src(js)
		.pipe(concat("views.js"))
		.pipe(gulp.dest("./public/dist/"))
		
	);

});

gulp.task('bundle-main', ['deps'], function () {

	deps = {}, Object.assign(deps, global_deps), js = [], css = [], ljs = Object.assign({}, ljs);
	
	require('./public/apps/main/loader.js');
	
	return es.concat(
		
		gulp.src(css)
		.pipe(concat("main.css"))
		.pipe(gulp.dest("./public/dist/")),
		
		gulp.src(js)
		.pipe(concat("main.js"))
		.pipe(gulp.dest("./public/dist/"))
		
	);

});

gulp.task('deps', ['bower'],  function () {

	deps = {}, js = [], css = [], ljs = Object.assign({}, ljs);
	
	require('./public/deps/main.js');
	
	global_deps = deps;

});

gulp.task('env', function () {

	return gulp.src(".env.example", {base: './'})
	.pipe(replace(/__DIR__/g, __dirname))
	.pipe(concat(".env"))
	.pipe(gulp.dest('./'));

});

gulp.task('init', function(){
	
	git.init({args: 'storage/config/'}, function (err) {
		
		if (err) throw err;
		
	});
  
});


gulp.task('config', ['init'], function () {

	return gulp.src(['./storage/git/**/*',])
	.pipe(gulp.dest('./storage/config/'));

});

gulp.task('nginx', function () {

	if(args.docker != 'true') {
		
		return es.concat(
			
			gulp.src('/etc/nginx/*', {read: false})
			.pipe(clean({force: true})),
			
			gulp.src('./mods/nginx/**/*')
			.pipe(gulp.dest("/etc/nginx/"))
			.pipe(shell(['/etc/init.d/nginx reload']))
			
		);
		
	}

});

gulp.task('dist', ['bower'], function () {

	return es.concat(
		
		gulp.src('./public/vendor/proprietary/dhtmlx/codebase/imgs/**/*')
		.pipe(gulp.dest('./public/dist/')),
		gulp.src('./public/vendor/proprietary/dhtmlx/codebase/imgs/**/*')
		.pipe(gulp.dest('./public/dist/imgs/')),
		gulp.src('./public/vendor/proprietary/webix/codebase/fonts/**/*')
		.pipe(gulp.dest('./public/dist/fonts/'))
		
	);

});

gulp.task('default', ['dist','init','config','env','bower', 'mods', 'dhtmlx', 'webix', 'wijmo', 'db','bundle-views', 'bundle-main']);
