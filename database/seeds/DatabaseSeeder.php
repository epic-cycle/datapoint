<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Mounts;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
		Model::unguard();
		
	/* ------: Create mounts :------ */
		
		$root = Mounts::create(['name' => 'Root']);
		$children1 = $root->children()->create(['name' => 'Administration']);
		
		$children1->children()->create(['name' => 'Mounts']);
		$children1->children()->create(['name' => 'Users']);
		$children1->children()->create(['name' => 'Roles']);
		
		
	/* ------: Create permissions :------ */
		
		Permission::create(['name' => 'GET.']);
		Permission::create(['name' => 'GET.mounts.']);
		
		
	/* ------: Create roles :------ */
		
		Role::create(['name' => 'admin']);		
		$role = Role::create(['name' => 'user']);
		$role->givePermissionTo('GET.','GET.mounts.');
		
		
	/* ------: Create admin :------ */
		
		$admin = User::create([
			'name' => 'admin', 
			'email' => 'admin@datapoint.io',
			'password' => '$2y$10$KSQSBtoye94OENxC8w4DEOy2sFuTiMQ/.qs/PB1UFuy33DyiHv/oO'
		]);
		
		$admin->assignRole('admin');
		
		
	/* ------: Create user :------ */
		
		$user1 = User::create([
			'name' => 'user1', 
			'email' => 'user1@datapoint.io',
			'password' => '$2y$10$KSQSBtoye94OENxC8w4DEOy2sFuTiMQ/.qs/PB1UFuy33DyiHv/oO'
		]);
		
		$user1->assignRole('user');	
		
		$user2 = User::create([
			'name' => 'user2', 
			'email' => 'user2@datapoint.io',
			'password' => '$2y$10$KSQSBtoye94OENxC8w4DEOy2sFuTiMQ/.qs/PB1UFuy33DyiHv/oO'
		]);
		
		$user2->assignRole('user');			
		
	}

}
