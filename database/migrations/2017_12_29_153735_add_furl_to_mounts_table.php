<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFurlToMountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mounts', function (Blueprint $table) {
		        	
              $table->char('furl', 128)->nullable($value = true)->unique();
              
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mounts', function (Blueprint $table) {
            
            $table->dropColumn('furl');
            
        });
    }
}
