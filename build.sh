#!/bin/sh

nodejs -v
npm -v
composer config --list

composer install

npm install
npm install yargs
npm install gulp
npm install gulp-untar
npm install gulp-gunzip
npm install gulp-bower
npm install gulp-sqlite3
npm install gulp-shell
npm install gulp-concat
npm install gulp-replace
npm install gulp-git
npm install gulp-shell
npm install gulp-clean	

gulp --docker true

yes | php artisan migrate:refresh --force --seed