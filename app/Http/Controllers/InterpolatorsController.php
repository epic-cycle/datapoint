<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Versions;
use App\Models\Interpolators;
use App\Models\Mounts;
use Auth;


/**
 *	Controller to operate with interpolators
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class InterpolatorsController extends Controller {

	public static function test($test, $param){
		
		if(!$test) response()->json($param.' not found', 404)->send();
		
		return true;
		
	}

	public function get($id, $recursive = false) {
		
		$ids = explode(",", $id);
		
		foreach($ids as $id){
			
			$model = Versions::get(
				
				[ "id" => $id, "type" => "model" ], 
				Auth::User()->id
				
			);
			
			if($recursive) {
				
				$response['interpolators'] = Interpolators::extractInterpolators($model['data']['query']);
				$response['datasources'] = $model['data']['datasource']['id'];
				$response['model']['id'] = $id;
				$response['model']['name'] = Mounts::where('id', '=', $id)->first()->name;
				
			} else {
				
				$this->test($model, $id);
				
				$response[$id] = Interpolators::extractInterpolators($model['data']['query']);
				
			}
			
		}
		
		return $response;
		
	}
 
 	public function recursive($datasources){
		
		if(!is_array($datasources)) $datasources = explode(" ",$datasources);
		
		foreach($datasources as $datasource){
			
			if(is_numeric($datasource)) {
				
				$result = $this->get($datasource, true);
				
				$response[] = $result;	
				
				$response = array_merge($response, $this->recursive($result['datasources']));
					
				if($result['datasources'] === NULL) {
					
					array_pop($response);
					
				}
				
			} else {
				
				if(!isset($response)) $response = array();
				
			}
			
		}
		
		return $response;
		
 	}
 
	public function pipe($id) {
		
		$result = $this->get($id, true);
		
		$response[] = $result;
		$response = array_merge($response, $this->recursive($result['datasources']));
		
		$a = array();
		
		foreach($response as $key => $value){
			
			$a[$value['model']['id']] = $value;
			
		}
		
		return $a;
		
	}

}

//EOF