<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Versions;
use File;
use App\Http\Controllers\Git\git;

/**
 *	Controller to operate with items
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class ItemsController extends Controller {

    public function index($id, Request $request) {
		
		$subject = $request->target;
		
		switch($subject['auditory']) {
			
			case 'User':
				
				$path = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/*.json';
				
			break;
			
			case 'All':
				
				$path = env('VERSION_PATH').'/component/'.$id.'/All/*.json';
				
			break;
			
		}
		
		$result = glob($path); 
		
		if(empty($result)) {
			
			$response = array();
			
		} else {
			
			foreach($result as $path) {
				
				$file = Array(
					'id' => basename($path, '.json'),
					'name' => base64_decode(basename($path, '.json')),
					'updated' => filemtime($path)
				);
				
				$response["data"][] = $file;
				
			}	
			
		}
		
		return $response;
		
    }	

    public function save($id, Request $request) {
		
		$item = $request->item;

		$subject = $request->target;
		
		switch($subject['auditory']) {
			
			case 'User':
				
				if(empty($item)) {
					
					$dir = env('VERSION_PATH').'/component/'.$id.'/User/';
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'.json';
					
				} else {
					
					$dir = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id;
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$request->item.'.json';
				
				}
				
			break;
			
			case 'All':
				
				$dir = env('VERSION_PATH').'/component/'.$id.'/All/';
				$file = env('VERSION_PATH').'/component/'.$id.'/All/'.$request->item.'.json';
				
			break;
			
			default:
				
				if(empty($item)) {
					
					$dir = env('VERSION_PATH').'/component/'.$id.'/User/';
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'.json';
					
				} else {
					
					$dir = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id;
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$request->item.'.json';
				
				}
				
			break;
			
		}


		if(is_array($request->data)) {
			
			$data = json_encode($request->data);
			
		} else {
			
			$data = $request->data;
			
		}

		
		File::makeDirectory($dir, $mode = 0777, true, true);
		
		
		File::put($file, $data);





		// Init Git repo

		if(!File::exists(env('VERSION_PATH').'/.git/')) {
			
			$repo = git::create(env('VERSION_PATH'));
			
		} else {
			
			$repo = new git(env('VERSION_PATH'));	
			
		}
		
		
		// Commit

		$repo->git('add '.$file);
		$gitresponse = $repo->git('git commit --allow-empty -m "'.uniqid().'"');
		
		return $request->item;

    }	
    
    public function get($id, Request $request) {
		
		$item = $request->item;
		
		$subject = $request->target;
		
		switch($subject['auditory']) {
			
			case 'User':
				
				if(empty($item)) {
					
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'.json';
					
				} else {
					
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$request->item.'.json';
					
				}
				
			break;
			
			case 'All':
				
				$file = env('VERSION_PATH').'/component/'.$id.'/All/'.$request->item.'.json';
				
			break;
			
			default:
				
				if(empty($item)) {
					
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'.json';
					
				} else {
					
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$request->item.'.json';
					
				}
				
			break;
			
		}


		
		if(File::exists($file)) {
			
		    $response['data'] = File::get($file);
		    
		} else {
			
			$response['data'] = '';	
			
		}
		
		return $response;
		
    }    

    public function add($id, Request $request) {
		
		$name = microtime(true);
		$itemid = base64_encode($name);
		
		$subject = $request->target;
		
		switch($subject['auditory']) {
			
			case 'User':
				
				$dir = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id;
				$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$itemid.'.json';
				
			break;
			
			case 'All':
				
				$dir = env('VERSION_PATH').'/component/'.$id.'/All/';
				$file = env('VERSION_PATH').'/component/'.$id.'/All/'.$itemid.'.json';
				
			break;
			
		}
		

		
		File::makeDirectory($dir, $mode = 0777, true, true);
		File::put($file, '');
		
		
		
		
		// Init Git repo

		if(!File::exists(env('VERSION_PATH').'/.git/')) {
			
		    $repo = git::create(env('VERSION_PATH'));
		    
		} else {
			
			$repo = new git(env('VERSION_PATH'));	
			
		}
		
		
		// Commit

		$repo->git('add '.$file);
		$gitresponse = $repo->git('git commit -m "'.uniqid().'"');
		
		
		
		
		$response['data']['item'] = Array(
			'id' => $itemid,
			'name' => $name,
			'updated' => date('Y-m-d H:i:s', $name)
		);
		
		return $response;
		
    }   

    public function delete($id, Request $request) {
		
		$item = $request->item;
		
		if(empty($item)){
			
			return 0;
			
		} else {
			
			$subject = $request->target;
			
			switch($subject['auditory']) {
				
				case 'User':
					
					$file = env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$item.'.json';
					
				break;
				
				case 'All':
					
					$file = env('VERSION_PATH').'/component/'.$id.'/All/'.$item.'.json';
					
				break;
				
			}
			
			
			
			File::delete($file);
			
			// Init Git repo
	
			if(!File::exists(env('VERSION_PATH').'/.git/')) {
				
			    $repo = git::create(env('VERSION_PATH'));
			    
			} else {
				
				$repo = new git(env('VERSION_PATH'));	
				
			}
			
			
			// Commit
			
			$repo->git('add '.$file);
			$gitresponse = $repo->git('git commit -m "'.uniqid().'"');
			
			
			return 1;			
			
		}
		
    }   

	public function update($id, Request $request) {
		
		$response['data'] = null;
		
		$itemid = Array ( 
			'old' => $request->item,
			'new' => base64_encode($request->name)
		);
		
		if(empty($itemid['old']) || empty($itemid['new']) || $itemid['old'] == $itemid['new']){
			
			return Response::json([
			    'message' => "Error 5555"
			], 500);
			
		} else {

			$subject = $request->target;
			
			switch($subject['auditory']) {
				
				case 'User':
					
					$file = Array (
						'old' => env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$itemid['old'].'.json',
						'new' => env('VERSION_PATH').'/component/'.$id.'/User/'.$request->user()->id.'/'.$itemid['new'].'.json'
					);
					
				break;
				
				case 'All':
					
					$file = Array (
						'old' => env('VERSION_PATH').'/component/'.$id.'/All/'.$itemid['old'].'.json',
						'new' => env('VERSION_PATH').'/component/'.$id.'/All/'.$itemid['new'].'.json'
					);
					
				break;
				
			}
			
			if (!File::move($file['old'], $file['new'])) {
				
				return Response::json([
				    'message' => "Can not rename file"
				], 500);
				
			} else {
				
				$response['data']['itemid'] = $itemid;
				
				return $response;
				
			}
			
		}		
		
	}

}

//EOF