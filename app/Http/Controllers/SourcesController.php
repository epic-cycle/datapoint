<?php namespace App\Http\Controllers;



/* ----------------------------- */

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Git\PHPGit_Repository;
use Illuminate\Http\Request;
use DB;
use File;
use App\Models\Versions;
use App\Models\Sources;
use Config;
use ClickHouseDB;


/* ----------------------------- */

class SourcesController extends Controller {

/* ----------------------------- */

	private function response($request)
	{
		
		$callers = debug_backtrace();
		
		$response['controller'] = get_class($this);	
		$response['method'] = $callers[1]['function'];	
		$response['reqid'] = $request['reqid'];		
		
		return $response;
		
	}

/* ----------------------------- */

	public function get($id, Request $request)
	{
		
		//$response['data'] = Sources::get($id);
		return "lalala";
		
	}

/* ----------------------------- */

    public function index(Request $request)
    {
		
		$response['data'] = Sources::getAllActive();
		return $response;
		
    }	

/* ----------------------------- */

	public function test(Request $request)
	{
		
		$data = $request->all();
		
	/* ----------------------------- */
		
		// Set database connection settings
		
		$db = [
			'driver'	=> isset($data['config']['driver']) ? $data['config']['driver'] : '',
			'host'		=> isset($data['config']['host']) ? $data['config']['host'] : '',
			'port'		=> isset($data['config']['port']) ? $data['config']['port'] : '',
			'database'	=> isset($data['config']['database']) ? $data['config']['database'] : '',
			'username'	=> isset($data['config']['username']) ? $data['config']['username'] : '',
			'password'	=> isset($data['config']['password']) ? $data['config']['password'] : '',
			'charset'	=> isset($data['config']['charset']) ? $data['config']['charset'] : '',
			'collation'	=> isset($data['config']['collation']) ? $data['config']['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($data['config']['schema']) ? $data['config']['schema'] : '',
		];
		
	/* ----------------------------- */
		
		switch($db['driver']) {
			
			case 'mysql':
			case 'pgsql':
				
				// Set database connection settings
				
				Config::set('database.connections.test', $db);
				
				// Execute query on database
				
				$result = DB::connection('test')->select("SELECT 'pass' as test");
				
			break;
			
			
			case 'clickhouse':
				
				require_once 'Clickhouse/include.php';				
				
				$conn = new ClickHouseDB\Client($db);
				$conn->database($db['database']);
				$conn->setReadOnlyUser(true);
				$conn->setConnectTimeOut(5);				
				$result = $conn->select("SELECT 'pass' as test")->rows();
				
			break;			
			
		}
		
	/* ----------------------------- */
		
		// Create response
		
		$response["server"] = $this->response($request);
		$response['data'] = $result;
		
	/* ----------------------------- */
		
		return $response;
		
	}
	
/* ----------------------------- */

}

//EOF