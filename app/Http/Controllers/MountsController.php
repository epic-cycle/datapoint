<?php namespace App\Http\Controllers;

use App\Models\Mounts;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Eloquent;
use App\Models\Views;

/**
 *	Controller to operate with mounts
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class MountsController extends Controller {

	public function view(Request $request)
	{
		
		//$mounts = Mounts::all();
		$mounts = Mounts::root()->getDescendants()->toHierarchy();
		
		return view('apps/main/index')->with('items', $mounts);

	}

	public function index(Request $request)
	{

		$mounts = Mounts::all();
		//$mounts = Mounts::root()->getDescendants();
		
		$response["rows"] = $mounts->toHierarchy();
		
		return $response;
		
	}
	
	public function create(Request $request)
	{
		
	/* ----------------------------- */
		
		// Create new mount and save to database
		
		$mount = Mounts::create(['name' => $request->get('name')]);
		$mount->parent_id = $request->get('parent');
		$mount->save();
		
	/* ----------------------------- */
		
		// Create response
		
		$response = array('id' => $mount->id);
		
	/* ----------------------------- */
		
		return $response;
		
	}

    public function update(Request $request)
    {
		
	/* ----------------------------- */
	    
	    // Update mount properties in database

	    foreach($request->data as $key => $update) {
	    	if($update['value'] == "") $update['value'] = NULL;
	    	Mounts::where('id', $request->id)->update(array($update['field'] => $update['value']));
	    }


	/* ----------------------------- */
	    
	    return $request->id;

    }	
	
    public function delete($id)
    {
		
	/* ----------------------------- */		
		
		$node = Mounts::where('id', '=', $id)->first();
		$node->delete();
		
	/* ----------------------------- */		
		
		return $node->id;
    }	
    
    public function get($id)
    {
		
	/* ----------------------------- */		
		
		$ids = explode(",", $id);
		
		if(count($ids) === 1){
			
			$node = Mounts::where('id', '=', $ids[0])->first();
			
		} else {
			
			foreach($ids as $id){
				
				$node[] = Mounts::where('id', '=', $id)->first();	
				
			}
			
		}

	/* ----------------------------- */		
		
		return $node;
    }
    
    public function setparent($id, Request $request)
    {
		
		foreach($request->data as $key => $item) {
			
			$mounts = explode(',', $item['id']);
			
			foreach($mounts as $mountid) {
				
				Mounts::where('id', $mountid)->update(array('parent_id' => $item['parent']));
				
			}		
			
		}
		
		return 1;
    }       

}

//EOF