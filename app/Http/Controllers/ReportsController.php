<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Reports;

/**
 *	Controller to operate with items
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class ReportsController extends Controller {

	public function get($id, Request $request) {
		
		// array = data/select/12 {POST DATA}
		// return (depends on how report is configured)
		// could be : json, excel, html
		// if report called from api then sent it back to client
		// if called from scheduled job then use configured target (email, storage etc.)
		// but no
		// every report is called from api (job also)
		// difference is that some parameters should be passed
		// parameter specifies (return results or send somwehre results)
		
		// datapoint.io/reports/myReport/ (will be sent data to client in default format)
		// datapoint.io/reports/myReport/json (will be sent data to client in json)
		// datapoint.io/reports/myReport/excel (will be sent data to client in excel)
		// datapoint.io/reports/myReport/html (will be sent data to client in html)
		
		// datapoint.io/reports/myReport/send (will be send/stored data to default target and default format)
		// datapoint.io/reports/myReport/send/email/json (will be send data to email in json)
		// datapoint.io/reports/myReport/send/gdrive/excel (will be stored data to gdrive in excel)
		// datapoint.io/reports/myReport/send/gdrive/html (will be stored data to gdrive in html)

		
		// get model id
		// make request using this id and post data
		// like Data.select -> /data/12
		$response = Reports::get($id);
		
		return $response;
		
	}
	
	public function store($id, Request $request) {
		
		$data = $request->all();
		
		$response = Reports::save($id, $data);
		
		return $response;
		
	}	

}

//EOF