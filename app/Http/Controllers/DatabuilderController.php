<?php namespace App\Http\Controllers;

/* ----------------------------- */

use App\Http\Requesrots;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Git\PHPGit_Repository;
use Illuminate\Http\Request;
use DB;
use File;
use App\Models\Versions;
use App\Models\Sources;
use App\Models\Data;
use Config;
use ClickHouseDB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;


/* ----------------------------- */

class DatabuilderController extends Controller {

/* ----------------------------- */

	public function testScript($id, Request $request){
		
		$data = $request->all();
		
		$params = array(
			
			'query' => $data['data']['request']['query'],
			'mapping' => $data['data']['request']['mapping'],
			'interpolators' => $data['data']['request']['interpolators'],
			'parameters' => $data['data']['request']['parameters'],
			'test' => true,
			// TODO : take parameters from model if not passed (if this is test)
			// initiator is browser, if pipe then eveyrhint gshould be taken from model
			// or everything absolutely taken from model but execute should do a save (not good i think)
			
		);
		
		$response['data'] = Data::rscript($params);
		
		return $response;
		
	}

/* ----------------------------- */

	public function runSQL($id, Request $request){
		
		$data = $request->all();
		
	/* ----------------------------- */
		
		// Get connection settings
		
		$object = [
			
			"id" =>	$id, //$data['data']['connection']['id'],
			"type" => "source",
			"target" => ["auditory" => "All"]
			
		];
		
		$result = Versions::get($object, $request->userid);
		$source = json_decode($result['data']['datasource'], true);
		
	/* ----------------------------- */
		
		// Set query
		
		$query = $data['data']['request']['query'];
		
	/* ----------------------------- */
		
		// Set database connection settings
		
		$db = [
			'driver'	=> isset($source['driver']) ? $source['driver'] : '',
			'host'		=> isset($source['host']) ? $source['host'] : '',
			'port'		=> isset($source['port']) ? $source['port'] : '',
			'database'	=> isset($source['database']) ? $source['database'] : '',
			'username'	=> isset($source['username']) ? $source['username'] : '',
			'password'	=> isset($source['password']) ? $source['password'] : '',
			'charset'	=> isset($source['charset']) ? $source['charset'] : '',
			'collation'	=> isset($source['collation']) ? $source['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($source['schema']) ? $source['schema'] : '',
		];
		
	/* ----------------------------- */
		
		switch($db['driver']) {
			
			case 'mysql':
			case 'pgsql':
				
			/* ----------------------------- */
				
				// Set database connection settings
				
				Config::set('database.connections.sample', $db);
				
			/* ----------------------------- */
				
				// Execute query on database
				
				$result = DB::connection('sample')->select($query);
				
			break;
			
			
			case 'clickhouse':
				
				require_once 'Clickhouse/include.php';				
				
				date_default_timezone_set('Europe/Riga');
				$time = microtime(true);
				
				$conn = new ClickHouseDB\Client($db);
				$conn->database($db['database']);
				$conn->setTimeout(600);
				$conn->setReadOnlyUser(true);
				$conn->setConnectTimeOut(5);
				$dbresult = $conn->select($query);
				$result['data'] = $dbresult->rows();
				$result['statistics'] = $dbresult->statistics();
				$result['totals'] = $dbresult->totals();
				$result['totalTimeRequest'] = $dbresult->totalTimeRequest();
				$result['responseInfo'] = $dbresult->responseInfo();
				$result['duration'] = round(microtime(true) - $time,3)*1000;
				
			break;			
			
		}
		
	/* ----------------------------- */
		
		return $result;
		
	}
	
/* ----------------------------- */

	public function testSQL($id, Request $request){
		
		$data = $request->all();
		
	/* ----------------------------- */
		
		// Get connection settings
		
		$object = [
			
			"id" =>	$id, //$data['data']['connection']['id'],
			"type" => "source",
			"target" => ["auditory" => "All"]
			
		];
		
		$result = Versions::get($object, $request->userid);
		$source = json_decode($result['data']['datasource'], true);
		
		
	/* ----------------------------- */
		
		// Set query
		
		$query = "SELECT * FROM (".$data['data']['request']['query'].") AS sample LIMIT 1";
		
		
	/* ----------------------------- */
		
		// Set database connection settings
		
		$db = [
			'driver'	=> isset($source['driver']) ? $source['driver'] : '',
			'host'		=> isset($source['host']) ? $source['host'] : '',
			'port'		=> isset($source['port']) ? $source['port'] : '',
			'database'	=> isset($source['database']) ? $source['database'] : '',
			'username'	=> isset($source['username']) ? $source['username'] : '',
			'password'	=> isset($source['password']) ? $source['password'] : '',
			'charset'	=> isset($source['charset']) ? $source['charset'] : '',
			'collation'	=> isset($source['collation']) ? $source['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($source['schema']) ? $source['schema'] : '',
		];
		
	/* ---- Interpolation ---- */
		
		preg_match_all('/{{(.*)}}/U', $query, $interpolators);
		
		foreach($interpolators[1] as $index => $key) {
			
			
			if(isset($data['data']['request']['interpolators'][$key])) {
				
				$query = preg_replace("/".$interpolators[0][$index]."/U", $data['data']['request']['interpolators'][$key], $query);
				
			} else {
				
				$query = preg_replace("/".$interpolators[0][$index]."/U", '', $query);
				
			}
			
		}
		
	/* ----------------------------- */
		
		switch($db['driver']) {
			
			case 'mysql':
			case 'pgsql':
			
			/* ----------------------------- */
				
				// Set database connection settings
				
				Config::set('database.connections.sample', $db);
				
			/* ----------------------------- */
				
				// Execute query on database
				
				$result = DB::connection('sample')->select($query);
				
			break;
			
			
			case 'clickhouse':
				
				require_once 'Clickhouse/include.php';				
				
				$conn = new ClickHouseDB\Client($db);
				$conn->database($db['database']);
				$conn->setReadOnlyUser(true);
				$conn->setConnectTimeOut(5);				
				$result = $conn->select($query)->rows();
				
			break;			
			
		}
		
	/* ----------------------------- */
		
		// Create response
		
		$result = json_decode(json_encode($result), true);
		
		if(empty($result)) {
			
			$response['data'] = Array();
			
		} else {
			
			if(isset($data['data']['request']['pipe'])) {
				
				return $result;
				
			} else {
			
				foreach($result[0] as $key=>$value){
					$response['data'][$key]['Field name'] = $key;
					$response['data'][$key]['Sample value'] = $value;
				}
			
			}
			
		}
		
	/* ----------------------------- */
		
		return $response;
		
	}
	
/* ----------------------------- */

	public function prettysql(Request $request){
		$params = $request->all();
		
		if(isset($_GET['options'])) {
			$options = json_decode($_GET['options'], true);
			
			foreach($options as $option) {
				$params["sql"] = str_replace("{".$option["id"]."}", $option["value"], $params["sql"]);
			}
		}
		
		return SqlFormatter::format($params["sql"]);
		
	}

/* ----------------------------- */

	public function getTables(Request $request){
		
	}

/* ----------------------------- */

	public function getFields($id, Request $request){
		
		$object = [
			
			"id" =>	$id,
			"type" => "source",
			"target" => ["auditory" => "All"]
			
		];
		
		$source = Versions::get($object, $request->userid);
		
		$source = json_decode($source['data']['datasource'], true);
		
		$db = [
			'driver'	=> isset($source['driver']) ? $source['driver'] : '',
			'host'		=> isset($source['host']) ? $source['host'] : '',
			'port'		=> isset($source['port']) ? $source['port'] : '',
			'database'	=> isset($source['database']) ? $source['database'] : '',
			'username'	=> isset($source['username']) ? $source['username'] : '',
			'password'	=> isset($source['password']) ? $source['password'] : '',
			'charset'	=> isset($source['charset']) ? $source['charset'] : '',
			'collation'	=> isset($source['collation']) ? $source['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($source['schema']) ? $source['schema'] : '',
		];		
		
		$result = Array();
		
		switch($db['driver']) {
			
			case 'pgsql':
			case 'mysql':
				
				Config::set('database.connections.sample', $db);
				
				if($request->table == ":all") {
					
					$result = DB::connection('sample')->select('select column_name, data_type from information_schema.columns ORDER BY column_name ASC;');
					
				} else if(is_array($request->table)) {
					
					foreach ($request->table as $table) {
						
						$fields = DB::connection('sample')->select('select column_name, data_type from information_schema.columns where table_name = \''.$table.'\' ORDER BY column_name ASC;');
					    $result = array_merge($result, $fields);
					    
					}					
					
				} else {
					
					//$result = DB::connection('sample')->getSchemaBuilder()->getColumnListing('netent_transactions');
					$result = DB::connection('sample')->select('select column_name, data_type from information_schema.columns where table_name = \''.$request->table.'\' ORDER BY column_name ASC;');
					
				}
				
				$result = json_decode(json_encode($result), true);
				
			break;
			
			
			case 'clickhouse':
				
				require_once 'Clickhouse/include.php';				
				
				$conn = new ClickHouseDB\Client($db);
				$conn->database($db['database']);
				$conn->setReadOnlyUser(true);
				$conn->setConnectTimeOut(5);
				
				if($request->table == ":all") {
					
					$dbresult = $conn->select('SELECT table, name AS column_name, type AS data_type FROM system.columns WHERE database = \''.$db['database'].'\'')->rows();    
					
					foreach ($dbresult as $field) {
						
						$result['data'][$field['table']][] = $field['column_name'];
						
					}
					
				} else if(is_array($request->table)) {
					
					foreach ($request->table as $table) {
						
						$fields = $conn->select('SELECT name AS column_name, type AS data_type FROM system.columns WHERE table=\''.$table.'\'')->rows();    
					    $result = array_merge($result, $fields);
					}
					
				} else {
					
					$result = $conn->select('SELECT name AS column_name, type AS data_type FROM system.columns WHERE table=\''.$request->table.'\'')->rows();
					
				}
				
			break;
			
		}
		
		return $result;		
		
	}

/* ----------------------------- */

	public function getSchema($id, Request $request){
		
		$object = [
			
			"id" =>	$id,
			"type" => "source",
			"target" => ["auditory" => "All"]
			
		];
		
		$result = Versions::get($object, $request->userid);
		$source = json_decode($result['data']['datasource'], true);
		$db = [
			'driver'	=> isset($source['driver']) ? $source['driver'] : '',
			'host'		=> isset($source['host']) ? $source['host'] : '',
			'port'		=> isset($source['port']) ? $source['port'] : '',
			'database'	=> isset($source['database']) ? $source['database'] : '',
			'username'	=> isset($source['username']) ? $source['username'] : '',
			'password'	=> isset($source['password']) ? $source['password'] : '',
			'charset'	=> isset($source['charset']) ? $source['charset'] : '',
			'collation'	=> isset($source['collation']) ? $source['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($source['schema']) ? $source['schema'] : '',
		];		
		
		switch($db['driver']) {
			
			case 'pgsql':
			case 'mysql':
				
				Config::set('database.connections.sample', $db);
				$result = DB::connection('sample')->select('SELECT tablename, schemaname FROM pg_tables ORDER BY tablename ASC');
				$result = json_decode(json_encode($result), true);				
				
			break;
			
			
			case 'clickhouse':
				
				require_once 'Clickhouse/include.php';				
				
				$conn = new ClickHouseDB\Client($db);
				$conn->database($db['database']);
				$conn->setReadOnlyUser(true);
				$conn->setConnectTimeOut(5);				
				$result = $conn->showTables();
				
			break;
			
		}
		
		return $result;
		
	}	
	
}

//EOF