<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Models;

/**
 *	Controller to operate with Views
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class ModelsController extends Controller {

    public function index(Request $request)
    {
		
		$response['data'] = Models::getAllActive();
		return $response;
		
    }	

}

//EOF