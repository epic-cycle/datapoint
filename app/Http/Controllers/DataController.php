<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Mounts;
use App\Models\Versions;
use App\Models\Data;
use App\Models\Interpolators;
use Auth;
use DB;
use Config;
use Cache;
use SSH;
use ClickHouseDB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use File;

/**
 *	Controller to get data from source using prepared data model
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class DataController extends Controller {

	private function getModel($id) {
		
		$model = Versions::get(
			
			[ "id" => $id, "type" => "model" ], 
			Auth::User()->id
			
		);
		
		if(!$model) response()->json('Data model '.$id.' not found', 503)->send();
		
		return $model;
		
	}

	private function getSource($model) {
		
		$source = Versions::get(
			
			[ "id" => $model['data']['datasource']['id'], "type" => "source" ], 
			Auth::User()->id
			
		);
		
		return $source;
		
	}

	private function setConnection($source) {
		
		$source = json_decode($source['data']['datasource'], true);
				
		$db = [
			'driver'	=> isset($source['driver']) ? $source['driver'] : '',
			'host'		=> isset($source['host']) ? $source['host'] : '',
			'port'		=> isset($source['port']) ? $source['port'] : '',
			'database'	=> isset($source['database']) ? $source['database'] : '',
			'username'	=> isset($source['username']) ? $source['username'] : '',
			'password'	=> isset($source['password']) ? $source['password'] : '',
			'charset'	=> isset($source['charset']) ? $source['charset'] : '',
			'collation'	=> isset($source['collation']) ? $source['collation'] : '',
			'prefix'	=> '',
			'schema'	=> isset($source['schema']) ? $source['schema'] : ''
		];
		
		return $db;
		
	}

	private function convertFurlToID($id){
		
		if(!is_numeric($id)) {
			
			$mount = Mounts::where('furl', $id)->first();
			
			if($mount && is_numeric($mount->id)){
				
				$id = $mount->id;
				
			} else {
				
				response()->json('Undefined method '.$id, 503)->send();
				
			}
			
		}
		
		return $id;
		
	}

	private function doValidation($data){
		
		$data = [
			
			'interpolators'	=> isset($data['interpolators']) ? $data['interpolators'] : array(),
			'parameters'	=> isset($data['parameters']) ? $data['parameters'] : array(),
			'filter'	=> isset($data['filter']) ? $data['filter'] : array()
		];
		
		return $data;
		
	}

	public function select($id, Request $request) {
		
		$data = $this->doValidation($request->all());
		
		$id = $this->convertFurlToID($id);
		
		$model = $this->getModel($id);
		
		switch($model['data']['editor']) {
			
			case 'rscript':
				
				$params = array(
					
					'query' => $model['data']['query'], 
					'mapping' => $model['data']['mapping'], 
					'interpolators' => $data['interpolators'],
					'parameters' => $data['parameters']
					
				);
				
				$response = Data::rscript($params);
				
				return $response;
				
			break;
			
			default:
				
				$source = $this->getSource($model);
				
				$db = $this->setConnection($source);
				
				// TODO : do not check editor!!
				
				if($model['data']['editor'] === 'modeldesigner'){
					
					$descr = array(
					    0 => array(
					        'pipe',
					        'r'
					    ) ,
					    1 => array(
					        'pipe',
					        'w'
					    ) ,
					    2 => array(
					        'pipe',
					        'w'
					    )
					);
					$pipes = array();
					$result = '';
					
					$params = Array(
						"filter" => $request->filter,
						"daterange" => $request->daterange,
						"dimensions" => $request->dimensions,
						"limit" => $request->limit
					);
					
					$process = proc_open('node ../app/Nodejs/Jsontosql.js barchart '.$db['driver'].' '.base64_encode(json_encode($params)), $descr, $pipes);
					
					if (is_resource($process)) {
						
					    while ($f = fgets($pipes[1])) {
					        $result[] = $f;
					    }
					    fclose($pipes[1]);
					    while ($f = fgets($pipes[2])) {
					        $result[] = $f;
					    }
					    fclose($pipes[2]);
					    proc_close($process);
					    
					}
		
					Log::useFiles(base_path() . '/storage/logs/queries.log', 'info');
					Log::info($result[0]);	
					Log::info(json_encode($request->dimensions));
					
					//return $result[0];
					$query = $result[0];
					$query2 = $result[0];
					
				} else {
					
					// all under this should be to different classes
					// classes by model type (sql, nosql etc.)
					
				/* ----------------------------- */
					
					// Query conditions
					
					if(!isset($data['filter']['query']['sql'])) $data['filter']['query']['sql'] = "";
					
					$limit = $request->limit;
					$offset = ($request->page - 1) * $limit;
					$orderby = $request->orderby;
					$ordervector = $request->ordervector;
					$fields = "";
					$daterange = "";
					
				/* ----------------------------- */
					
					// Check which fields are allowed to show in grid
					
					$skipsettings = false;
					
					if(isset($model['data']["skipsettings"])) {
						
						if($model['data']["skipsettings"] == "true")	 {
							$fields = "*";
							$skipsettings = true;
						}
						
					} 
					
					if(!$skipsettings) {
						
						foreach (json_decode($model['data']['settings'], true) as $key => $field) {
							
							// TODO : eliminate backward compatibility
							
							// Remove "_" -> Convert from hex to string -> remove ":"
							// "_" to disable object sorting, ":" for split table:field
							
							// Backward compatibility
							
							$fromhex = false;
							$hex = explode("_", $key);
							
							if(is_array($hex) && sizeof($hex) == 2){
								
								$str = $this->hextostr($hex[1]);
								$keystr = explode(":", $str);
								if(is_array($keystr) && sizeof($keystr) == 2) {
									
									$fromhex = true;
									
								}
								
							}
						
							//No backward comptatibiliyy
							//$key_str = explode(":", $this->hextostr(explode("_", $key)[1]))[1];
							
							if($fromhex) {
								
								$key_str = $keystr[1];
								
							} else {
								
								$key_str = $key;
								
							}
							
							if($field['Value'] === "1") $fields .= $key_str.',';
							
						}
						
					}
					
				/* ----------------------------- */
					
					// Check conditions
					
					// need to check if received query is okay against defined model
					
					if(isset($request->daterange['startDate']) && isset($request->daterange['endDate'])) {
						// get alias from daterange object and then this alias to daterange criteria
						if($data['filter']['query']['sql'] == "") {
							$daterange = "( ".$request->daterange['field']." BETWEEN '".$request->daterange['startDate']."' AND '".$request->daterange['endDate']."') ";					
						} else {
							$daterange = "( ".$request->daterange['field']." BETWEEN '".$request->daterange['startDate']."' AND '".$request->daterange['endDate']."') AND ";
						}
						
					}
					
					if($daterange == "" && $data['filter']['query']['sql'] == "") $where = ""; else $where = "WHERE";
					if($orderby == "") $key_orderby = ""; else $key_orderby = "ORDER BY";
					if($limit == "") {
						$key_limit = "";
						$key_offset = ""; 
						$key_offset2 = ""; 
					}
					else if ($offset > 0){
						$key_limit = "LIMIT";
						$key_offset = "OFFSET ".$offset;
						$key_offset2 = $offset.",";
					} 
					else {
						$key_limit = "LIMIT";
						$key_offset = "";				
						$key_offset2 = "0,";	
					}
					
					if($data['filter']['query']['sql'] != "") $data['filter']['query']['sql'] = "(".$data['filter']['query']['sql'].")";
					
					
				/* ----------------------------- */
					
					// Finalize query
					
					$query2 = "
						SELECT ".rtrim($fields, ",")." 
						FROM 
						(
							".$model['data']['query']."
						) AS result 
						".$where." 
						".$daterange." 
						".$data['filter']['query']['sql']." 
						
						
						".$key_orderby." 
						".$orderby." 
						".$ordervector." 
						".$key_limit." 
						".$key_offset2."
						".$limit;
					
					$query = "
						SELECT ".rtrim($fields, ",")." 
						FROM 
						(
							".$model['data']['query']."
						) AS result 
						".$where." 
						".$daterange." 
						".$data['filter']['query']['sql']." 
						".$key_orderby." 
						".$orderby." 
						".$ordervector." 
						".$key_offset." 
						".$key_limit." 
						".$limit."
					";			
				}
		
			// if you can not replaace all intepolators thn throw exception
		
			/* ---- Interpolation ---- */
				
				$query = Interpolators::doInterpolation($query, $data['interpolators']);
				$query2 = Interpolators::doInterpolation($query2, $data['interpolators']);
				
			/* ----------------------------- */
				
				// Set database connection settings
				
				Config::set('database.connections.dynamic', $this->setConnection($source));
				
			/* ----------------------------- */
				
				// Check caching and execute query
				
				if($model['data']['caching']['enabled'] == "1") {
					
					if (Cache::has(md5($query))) {
						
						$time = microtime(true);
						$result = Cache::get(md5($query)); 
						$result['duration'] = round(microtime(true) - $time,3)*1000;
						return $result;
						
					} else {
						
						$result = Cache::remember(md5($query), $model['data']['caching']['expiration'], function() use ($query, $query2, $db)
						{
							date_default_timezone_set('Europe/Riga');
							$time = microtime(true);
							
							switch($db['driver']) {
								
								case 'mysql':
								case 'pgsql':
									
									$result['data'] = DB::connection('dynamic')->select($query);
									
								break;
								
								
								case 'clickhouse':
									
									require_once 'Clickhouse/include.php';				
									
									$conn = new ClickHouseDB\Client($db);
									$conn->database($db['database']);
									$conn->setTimeout(600);
									$conn->setReadOnlyUser(true);
									$conn->setConnectTimeOut(5);							
									$result['data'] = $conn->select($query2)->rows();
									$result['query'] = $query2;
									
								break;
								
							}					
							
							$result['duration'] = round(microtime(true) - $time,3)*1000;
							$result['cached'] = date("Y-m-d H:i:s");
							return $result;
							
						});			
						
						unset($result['cached']);
						
					}
					
				} else {
					
					$time = microtime(true);
					
					switch($db['driver']) {
						
						case 'mysql':
						case 'pgsql':
							
							$result['data'] = DB::connection('dynamic')->select($query);
							
						break;
						
						
						case 'clickhouse':
							
							require_once 'Clickhouse/include.php';				
							
							$conn = new ClickHouseDB\Client($db);
							$conn->database($db['database']);
							$conn->setTimeout(600);
							$conn->setReadOnlyUser(true);
							$conn->setConnectTimeOut(5);					
							$result['data'] = $conn->select($query2)->rows();
							$result['query'] = $query2;
							
						break;			
						
					}
					
					$result['duration'] = round(microtime(true) - $time,3)*1000;
					
				}
				
			break;
			
		}
		
		return $result;
		
	}	
    
    public function count($id, Request $request) {

	/* ----------------------------- */
		
		$model = $this->getModel($id);
		$source = $this->getSource($model); 
		
	/* ----------------------------- */
		
		$daterange = "";
		$filter = "";
		
	/* ----------------------------- */
		
		if(isset($request->filter['query']['sql'])) {
			
			$filter = $request->filter['query']['sql'];
			
		}
		
	/* ----------------------------- */
		
		if(isset($request->daterange['startDate']) && isset($request->daterange['endDate'])) {
			
			if($filter == "") {
				
				$daterange = "( ".$request->daterange['field']." BETWEEN '".$request->daterange['startDate']."' AND '".$request->daterange['endDate']."') ";
				
			} else {
			
				$daterange = "( ".$request->daterange['field']." BETWEEN '".$request->daterange['startDate']."' AND '".$request->daterange['endDate']."') AND ";
				
			}
		}
		
		if($daterange == "" && $filter == "") $where = ""; else $where = "WHERE";		
		
		
		
	/* ----------------------------- */
		
		$query = "SELECT COUNT(*) OVER() FROM (".$model['data']['query'].") AS result ".$where." ".$daterange." ".$filter." LIMIT 1";
		$query2 = "SELECT COUNT(*) AS count FROM (".$model['data']['query'].") AS result ".$where." ".$daterange." ".$filter." LIMIT 1";
		
	/* ----------------------------- */
		
		$db = $this->setConnection($source);
		
		Config::set('database.connections.dynamic', $this->setConnection($source));
		
		if($model['data']['caching']['enabled'] == "1") {		
			
			if (Cache::has(md5($query))) {
				
				return Cache::get(md5($query));	
				
			} else {
				
				$result = Cache::remember(md5($query), $model['data']['caching']['expiration'], function() use ($query)
				{
				    return DB::connection('dynamic')->select($query);
				});			
				
			}
		} else {
			
			switch($db['driver']) {
				
				case 'mysql':
				case 'pgsql':
					
					return DB::connection('dynamic')->select($query);
					
				break;
				
				
				case 'clickhouse':
					
					require_once 'Clickhouse/include.php';				
					
					$conn = new ClickHouseDB\Client($db);
					$conn->database($db['database']);
					$conn->setTimeout(600);
					$conn->setReadOnlyUser(true);
					$conn->setConnectTimeOut(5);					
					return $conn->select($query2)->rows();
					
				break;			
				
			}	
			
		}
		
		return [];
		    	
    }
    
    private function hextostr($hex) {
		
		$string = "";
		$hexes = str_split($hex,4);
		for($i=0; $i<sizeof($hexes);$i++){
		
			$a = hexdec($hexes[$i]);	
			$b = chr($a);
			$string .= $b;
		}
		
		return $string;
		
	}    

}

//EOF