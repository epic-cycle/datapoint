<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 *	Controller to operate with Roles
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class RolesController extends Controller {

    public function index(Request $request)
    {
		
		$response['data'] = Role::all();
		
		return $response;
		
    }
    
    public function create(Request $request)
    {
		
       	$role = new Role;
        $role->name = $request->name;
        $role->save();
		
		$response = $role;
		
		return $response;
		
    }	    
    
    public function delete(Request $request)
    {
		
		$data = $request->all();
		
  		$response = Role::whereIn('id', $data['ids'])->delete(); 
		
		return $response;
		
    }    

	public function setPermissions($id, Request $request)
    {
		
		$data = $request->all();
		
		$ids = explode(",", $id);
		
		if(count($ids)>0 && count($data['permissions']>0)){
			
			foreach($ids as $id){
				
				$role = Role::findOrFail($id);
				
				foreach($data['permissions'] as $permission){
					
					if (Permission::where('name', $permission)->get()->isEmpty()) {
						
						Permission::create(['name' => $permission]);
						
					};	
					
					if($role->hasPermissionTo($permission) === false) {
					
						$role->givePermissionTo($permission);
						
					}
					
				}
				
			}
			
			$response = 1;
			
		} else {
			
			$response = 0;
			
		}
		
		return $response;
		
    }   

    public function getPermissions($id, Request $request)
    {
		
		$ids = explode(",", $id);
		
		if(count($ids)>0){
			
			foreach($ids as $id){
				
				$role = Role::findOrFail($id);
				$response[$role->id] = $role->permissions;
				
			}
			
		}
		    		
		return $response;
		
    }

    public function removePermissions($id, Request $request)
    {
    	
		$data = $request->all();
		
		$ids = explode(",", $id);
		
		if(count($ids)>0 && count($data['permissions']>0)){
			
			foreach($ids as $id){
				
				$role = Role::findOrFail($id);
				
				foreach($data['permissions'] as $permission){
					
					if (Permission::where('name', $permission)->get()->isEmpty() === false) {
						
						if($role->hasPermissionTo($permission) === true) {
							
							$role->revokePermissionTo($permission);
							
						}
						
					}
					
				}
				
			}
			
			$response = 1;
			
		} else {
			
			$response = 0;
			
		}
		
		return $response;
    	
    }
    
    public function postIndex(Request $request)
    {
		
		$response['data'] = Role::all();
		
		return $response;
		
    }	  
    
    public function postAssigned(Request $request)
    {
		
		$response['data'] = Role::all();
		
		return $response;
		
    }	      

}

//EOF