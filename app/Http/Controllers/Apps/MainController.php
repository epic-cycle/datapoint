<?php namespace App\Http\Controllers\Apps;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use App\Models\Versions;
use Cookie;

class MainController extends Controller {

	public function __construct()
	{
		
		$this->middleware('auth');
		
	}

	public function getXxx()
	{
		
		return 1;
		
	}

	public function acl()
	{
		
		return view('apps/acl/index');
		
	}

	public function users()
	{
		
		return view('apps/users/index');
		
	}

	public function ace()
	{
		
		return view('apps/ace/index');
		
	}

	public function roles()
	{
		
		return view('apps/roles/index');
		
	}
	
	public function editview($id)
	{
		
		return view('apps/editview/index')->with('id', $id);
		
	}	

	public function editmodel($id)
	{
		
		return view('apps/editmodel/index')->with('id', $id);
		
	}	

	public function editsource($id)
	{
		
		return view('apps/editsource/index')->with('id', $id);
		
	}
	
	public function gridstack($id)
	{
		
		return view('apps/gridstack/index')->with('id', $id);
		
	}
	
	public function spreadjs()
	{
		
		return view('apps/spreadjs/index');
		
	}	

	public function graph()
	{
		
		return view('apps/graph/index');
		
	}

	public function modelbrowser()
	{
		
		return view('apps/modelbrowser/index');
		
	}
	
	public function datatables()
	{
		
		return view('apps/datatables/index');
		
	}	

	public function form($id)
	{
		
		return view('forms/'.$id)->with('id', $id);
		
	}	
	
	public function playlist($id)
	{
		
		Cookie::queue('playlist', '1');
		return view('apps/playlist/index');
		
	}
	
	public function views($id, Request $request)
	{
                // TODO : develop something better for space and background
                
         	$space = $request->space;
         	
         	if($space == "10") {
         		
         		$background = "transparent";
         		
         	} else {
         		
         		$background = "white";
         		
         	}
		
		$view = Versions::findVersion($id, 'view', $request->user()->id);
		$view = json_decode(json_encode($view['data']), true);
		
		return view('apps/views/index')->with([ "view" => $view, "id" => $id, "space" => $space, "background" => $background]);
		
	}

}
