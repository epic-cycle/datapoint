<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;
use Spatie\Permission\Models\Permission;

/**
 *	Controller to operate with Users
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class UsersController extends Controller {

    public function get($id, Request $request)
    {
		
		$response['data'] = User::find($id);
		
		return $response;
		
    }	
    
    public function index(Request $request)
    {
		
		$response['data'] = User::all();
		
		return $response;
		
    }
    
    public function delete(Request $request)
    {
		
		$data = $request->all();
		
  		$response = User::whereIn('id', $data['ids'])->delete(); 
		
		return $response;
		
    }
    
    public function create(Request $request)
    {
		
       	$user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
		
		$response = $user;
		
		return $response;
		
    }    

    public function getPermissions($id, Request $request)
    {
		
		$ids = explode(",", $id);
		
		if(count($ids)>0){
			
			foreach($ids as $id){
				
				$user = User::findOrFail($id);
				$response[$user->id] = $user->permissions;
				
			}
			
		}
		    		
		return $response;
		
    }
    
    public function getRoles($id, Request $request)
    {
		
		$ids = explode(",", $id);
		
		if(count($ids)>0){
			
			foreach($ids as $id){
				
				$user = User::findOrFail($id);
				$response[$user->id] = $user->roles;
				
			}
			
		}
		    		
		return $response;
		
    }    
    
    public function setRoles($id, Request $request)
    {
		
		$data = $request->all();
		
		// Roles
		
		$roles = Array();
		
		foreach($data['roles'] as $roleid){
			
			array_push($roles, Role::findOrFail($roleid)->name);
			
		}
		
		// Users
		
		$ids = explode(",", $id);
		
		foreach($ids as $id){
			
			$user = User::findOrFail($id);
			
			$user->syncRoles($roles);
			
		}
		
		
		return 1;
		
    }    
    
    
    public function setPermissions($id, Request $request)
    {
		
		$data = $request->all();
		
		$ids = explode(",", $id);
		
		if(count($ids)>0 && count($data['permissions']>0)){
			
			foreach($ids as $id){
				
				$user = User::findOrFail($id);
				
				foreach($data['permissions'] as $permission){
					
					if (Permission::where('name', $permission)->get()->isEmpty()) {
						
						Permission::create(['name' => $permission]);
						
					};	
					
					if($user->hasPermissionTo($permission) === false) {
					
						$user->givePermissionTo($permission);
						
					}
					
				}
				
			}
			
			$response = 1;
			
		} else {
			
			$response = 0;
			
		}
		
		return $response;
		
    }    
    
    public function removePermissions($id, Request $request)
    {
    	
		$data = $request->all();
		
		$ids = explode(",", $id);
		
		if(count($ids)>0 && count($data['permissions']>0)){
			
			foreach($ids as $id){
				
				$user = User::findOrFail($id);
				
				foreach($data['permissions'] as $permission){
					
					if (Permission::where('name', $permission)->get()->isEmpty() === false) {
						
						if($user->hasPermissionTo($permission) === true) {
							
							$user->revokePermissionTo($permission);
							
						}
						
					}
					
				}
				
			}
			
			$response = 1;
			
		} else {
			
			$response = 0;
			
		}
		
		return $response;
    	
    }

}

//EOF