<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Versions;

/**
 *	Controller to operate with objects
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

class ObjectsController extends Controller {

    public function save($id, Request $request)
    {
		
		$object = $request->all();
		
		$response = Versions::save($object, $request->user()->id);
		
		return $response;
		
    }	

    public function get($id, Request $request)
    {
		
		$object = $request->all();
		
		$response['data'] = Versions::get($object, $request->user()->id);
		
		return $response;
		
    }	
    
    public function roles($id, Request $request)
    {
		
		$object = $request->all();
		
		$response['data'] = Versions::roles($object, $request->user()->id);
		
		return $response;
		
    }	  
    
    public function users($id, Request $request)
    {
		
		$object = $request->all();
		
		$response['data'] = Versions::users($object, $request->user()->id);
		
		return $response;
		
    }	 
    
    public function versions($id, Request $request)
    {
		
		$response['data'] = Versions::getVersions($id, $request);
		
		return $response;
		
    }    

    public function revert($id, Request $request)
    {    
		    	
		$response['data'] = Versions::revert($id, $request);
		    	
		return $response;
		
	}

}

//EOF