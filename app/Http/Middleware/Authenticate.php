<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Models\Authorization;
use Auth;
use App\User;
use Cookie;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		
		if ($this->auth->guest())
		{
			if ($request->ajax())
			{
				
				if(Cookie::get('playlist') == 1){
					$user = User::find(1); // set gest id
					Auth::setUser($user);
					return $next($request);
				};
				
				return response('Unauthorized.', 401);
				
			}
			else
			{
				
				if(isset($_SERVER['HTTP_POSTMAN_TOKEN']) || isset($_SERVER['HTTP_DATAPOINT'])){
					$user = User::find(1); // set gest id
					Auth::setUser($user);
					return $next($request);					
				}
				
				else if(strpos($request->url(),'playlist')>0) {
					return $next($request);
				}
				
				else if(Cookie::get('playlist') == 1){
					$user = User::find(1); // set gest id
					Auth::setUser($user);
					return $next($request);
				};
				
				return redirect()->guest('auth/login');
				
			}
		}
		
		if(Authorization::check($request) === true) {
			
			return $next($request);
			
		} else {
			
			if ($request->ajax())
			{
				
				return response('Unauthorized.', 401);
				
			}
			else
			{
				
				return view('auth/401')->with("request", $request);
				
			}
			
		}
		
	}

}
