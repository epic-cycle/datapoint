<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'auth'], function () {
	
	// Old
	
	Route::get('/', 'MountsController@view');
	Route::get('/home', 'MountsController@view');

	//Route::resource('mounts', 'MountsController');

	Route::get('app/acl/', 'Apps\MainController@acl');
	Route::get('app/ace/', 'Apps\MainController@ace');
	Route::get('app/editview/{id}', 'Apps\MainController@editview');
	Route::get('app/modelbrowser/', 'Apps\MainController@modelbrowser');
	Route::get('app/editmodel/{id}', 'Apps\MainController@editmodel');
	Route::get('app/editsource/{id}', 'Apps\MainController@editsource');
	Route::get('app/gridstack/{id}', 'Apps\MainController@gridstack');
	Route::get('app/views/{id}', 'Apps\MainController@views');
	Route::get('app/spreadjs', 'Apps\MainController@spreadjs');
	Route::get('app/playlist/{id}', 'Apps\MainController@playlist');	
	Route::get('app/datatables/', 'Apps\MainController@datatables');		

	Route::get('form/{id}', 'Apps\MainController@form');

	//Route::controller('roles/', 'RolesController');
	Route::controller('views/{id}', 'ViewsController');

	// New
	
	//Route::controller('users/', 'UsersController');

	Route::post('data/select/{id}', 'DataController@select');	
	Route::post('data/count/{id}', 'DataController@count');	

	Route::post('databuilder/prettysql', 'DatabuilderController@prettysql');
	Route::post('databuilder/prettysql/{id}', 'DatabuilderController@prettysql');
	Route::post('databuilder/test/sql/{id}', 'DatabuilderController@testSQL');
	Route::post('databuilder/test/script/{id}', 'DatabuilderController@testScript');	
	Route::post('databuilder/run/sql/{id}', 'DatabuilderController@runSQL');
	Route::get('databuilder/schema/{id}', 'DatabuilderController@getSchema');
	Route::post('databuilder/fields/{id}', 'DatabuilderController@getFields');

	Route::get('sources/{id}', 'SourcesController@get');
	Route::get('sources', 'SourcesController@index');
	Route::post('sources/test', 'SourcesController@test');

	Route::get('app/graph/', 'Apps\MainController@graph');

	Route::get('app/roles/', 'Apps\MainController@roles');
	Route::get('app/users/', 'Apps\MainController@users');

	Route::get('models', 'ModelsController@index');

	Route::get('views', 'ViewsController@index');
	Route::get('views/{id}', 'ViewsController@get');

	Route::get('roles', 'RolesController@index');
	Route::get('roles/permissions/{id}', 'RolesController@getPermissions');	
	Route::post('roles', 'RolesController@create');
	Route::delete('roles', 'RolesController@delete');
	Route::put('roles/permissions/{id}', 'RolesController@setPermissions');
	Route::delete('roles/permissions/{id}', 'RolesController@removePermissions');

	Route::put('mounts/{id}', 'MountsController@update');
	Route::put('mounts/setparent/{id}', 'MountsController@setparent');
	Route::post('mounts', 'MountsController@create');
	Route::get('mounts', 'MountsController@index');
	Route::get('mounts/{id}', 'MountsController@get');	
	Route::delete('mounts/{id}', 'MountsController@delete');

	Route::post('objects/{id}', 'ObjectsController@get');
	Route::post('objects/users/{id}', 'ObjectsController@users');
	Route::post('objects/roles/{id}', 'ObjectsController@roles');
	Route::post('objects/save/{id}', 'ObjectsController@save');
	Route::post('objects/revert/{id}', 'ObjectsController@revert');
	Route::post('objects/versions/{id}', 'ObjectsController@versions');
	
	Route::post('items/index/{id}', 'ItemsController@index');
	Route::put('items/{id}', 'ItemsController@save');
	Route::post('items/get/{id}', 'ItemsController@get');
	Route::post('items/add/{id}', 'ItemsController@add');	
	Route::post('items/update/{id}', 'ItemsController@update');		
	Route::delete('items/{id}', 'ItemsController@delete');
	
	Route::get('users', 'UsersController@index');
	Route::get('users/{id}', 'UsersController@get');
	Route::get('users/permissions/{id}', 'UsersController@getPermissions');	
	Route::get('users/roles/{id}', 'UsersController@getRoles');		
	Route::put('users/roles/set/{id}', 'UsersController@setRoles');	
	Route::post('users', 'UsersController@create');
	Route::put('users/permissions/{id}', 'UsersController@setPermissions');
	Route::delete('users/permissions/{id}', 'UsersController@removePermissions');
	Route::delete('users', 'UsersController@delete');

	Route::get('interpolators/{id}', 'InterpolatorsController@get');
	Route::get('interpolators/pipe/{id}', 'InterpolatorsController@pipe');


	Route::get('report/{id}', 'ReportsController@get');
	Route::put('report/{id}', 'ReportsController@store');

});

Route::controllers([
	
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	
]);	

