var jsonSql = require(__dirname + '/../../node_modules/json-sql')();
var fs = require('fs');

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};

var graph = process.argv[2];
var driver = process.argv[3];
var params = JSON.parse(Base64.decode(process.argv[4]));

var filter = null;
var daterange = null;

try {
	
	filter = typeof params.filter == "object" ? params.filter : JSON.parse(params.filter);
	
	if(filter.filterrecords === false && filter.filteraggregates === false ){
		
		filter = null;
		
	} 
	
} catch (error) {

	filter = null;
	
}

try {
	
	daterange = typeof params.daterange == "object" ? params.daterange : JSON.parse(params.daterange);	
	
} catch (error) {
	
	daterange = null;
	
}

try {
	
	var dimensions = typeof params.dimensions == "object" ? params.dimensions : JSON.parse(params.dimensions);		

} catch(error){}

try {

	var limit = typeof params.limit == "object" ? params.limit : JSON.parse(params.limit);		
	
} catch(error){}

//var filter = JSON.parse(process.argv[4]);
//var dimensions = JSON.parse(process.argv[3]);

var structure = [];
var queries = [];
//console.log(JSON.stringify(params));return;
switch(graph){
	
	case "barchart":
		
		var fields = [];			
		var group = [];
		var table = String;
		
		var fields2 = null;
		var group2 = null;
		
		dimensions[1].forEach(function(value) {
			
			group.push(value.key);
			
			fields.push(
				{
					name: value.key, 
					alias: '`' + value.key + '`'
				}
			);
			
			if(value.aggregation !== undefined && value.aggregation !== "Value") {
				
				fields2 = [];
				group2 = [];
				
			}
			
		});	
		
		if(fields2 !== null && group2 !== null){
			
			dimensions[1].forEach(function(value) {
				
				if(value.aggregation === undefined || value.aggregation == "Value") {
					
					group2.push(value.key);
					
					fields2.push(
						{
							name: value.key, 
							alias: '`' + value.key + '`'
						}
					);					
					
				} else if (value.aggregation == "count") {
					
					fields2.push(
						{
							func: {
								name: value.aggregation,
							},
							alias : '`' + value.key +"__" + value.aggregation + '`'
						}					
					);						
					
				} else {
					
					fields2.push(
						{
							func: {
								name: value.aggregation,
								args: [
									{
										field: value.key
									}
								]
							},
							alias : '`' + value.key +"__" + value.aggregation + '`'
						}					
					);				
					
				}
				
			});
			
		}
		
		dimensions[0].forEach(function(value) {
			
			table = value.table;
			
			if(value.aggregation === undefined || value.aggregation == "Value") {
				
				fields.push({name: value.key, alias: '`' + value.key + '`'});
				
			} else {
				
				if(value.aggregation === "count") {
					
					fields.push(
						{
							func: {
								name: value.aggregation,
							},
							alias : '`' + value.key +"__" + value.aggregation + '`'
						}					
					);
					
				} else {
					
					fields.push(
						{
							func: {
								name: value.aggregation,
								args: [
									{
										field: value.key
									}
								]
							},
							alias : '`' + value.key +"__" + value.aggregation + '`'
						}					
					);
					
				}
				
			}
			
		});				
		
		
		if(dimensions[0].length >0){
			
			if(filter !== null && filter.filteraggregates === true) {
				
				group.push("$having");
				
			}
			
			structure.group = group;
			
		}
		
		structure.type = "select";
		structure.table = table;
		structure.fields = fields;
		
		
		// Limit
		
		if(limit > 0) structure.limit = limit;
		
		
		// Sort
		
		structure.sort = {};
		
		dimensions[2].forEach(function(value) {
			
			switch(value.ordering) {
			
				case "asc":
					
					structure.sort[value.key] = 1;
					
				break;
				
				case "desc":
					
					structure.sort[value.key] = -1;
					
				break;				
				
			}
			
		});
		
		
		// Filter
		
		if((filter !== null && filter.filterrecords === true) || daterange !== null) {
			
			structure.condition = {replace: "true"};
			
		} else {
			
			structure.condition = {};
			
		}
		
	break;
	
}




//structures.forEach(function(query) {

	//var sql = jsonSql.build(structure);
	
	var constructor = {
		
		query : structure
		
	};
	
	if(fields2 !== null){
		
		constructor.fields = fields2;
		
	}
	
	if(group2 !== null){
		
		constructor.group = group2;
		
	}	
	
	var sql = jsonSql.build(constructor);

	queries.push(sql.query);

	// compatibility with clickhouse
	
	switch(driver){
		
		case "pgsql":
			
			
		break;
		
		
		case "clickhouse":
			
			sql.query = sql.query.slice(0, -1); // remove semicol
			sql.query = sql.query.replace(/"/g, ""); // remove double quotes
			
			
			// WHERE
			
			if(filter !== null && daterange === null) {
				
				sql.query = sql.query.replace(/replace = \$p1/g, filter.query.sql);
				
			}
			
			else if (daterange !== null && (filter !== null && filter.filterrecords === true)) {
				
				sql.query = sql.query.replace(/replace = \$p1/g, "(" + filter.query.sql + ") AND (" + daterange.field + " BETWEEN '" + daterange.startDate + "' AND '" + daterange.endDate + "')") ;							
				
			}
			
			else if(daterange !== null && (filter === null || filter.filterrecords !== true)) {
				
				sql.query = sql.query.replace(/replace = \$p1/g, "(" + daterange.field + " BETWEEN '" + daterange.startDate + "' AND '" + daterange.endDate + "')") ;							
				
			}
			
			
			// HAVING
			
			if(filter !== null && filter.filteraggregates === true) {
				
				sql.query = sql.query.replace(/, \$having/g, " HAVING " + filter.having.sql);
				
			}
			
			sql.query = sql.query.replace(/IN\(/g, "GLOBAL IN(");
			sql.query = sql.query.replace(/NOT GLOBAL IN\(/g, "GLOBAL NOT IN("); // TODO : workaround
			
		break;
		
	}

	console.log(sql.query);

	
//});


/*
var sql = jsonSql.build({
    type: 'select',
    table: 'users',
    alias: 'u',
    fields: [
	{
    	    func: {
        	name: 'sum',
        	args: [
		    {
			field: 'a'
		    }
		]
    	    }
	},
	{
	    func: {
		name: 'count',
		args: [
		    {
			func: {
			    name: 'cast',
        		    args: [
				{
				    query: {type: 'select', table: 'table'}
				}
			    ]
			}
		    }
		],
    	    },
	    alias: 'alias'
	},
    ],
    condition: {"u.name": 'Max', "u.id": 6}
});

console.log(sql.query);
console.log(sql.values)
*/
