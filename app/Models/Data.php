<?php namespace App\Models;

use File;
use DB;
use Config;
use Cache;
use ClickHouseDB;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\Models\Models;
use App\Models\Interpolators;

class Data {
	
	public static function runProcess($command){
		
		$process = new Process($command);
		$process->run();
		
		if (!$process->isSuccessful()) {
			
			throw new ProcessFailedException($process);
			
		}
		
		$response = $process->getOutput();
		
		return $response;
		
	}
	
	public static function rscript($params) {
		
	/* ---- Arguments ---- */
		
		$query = $params['query'];
		$interpolators = $params['interpolators'];
		$mapping = $params['mapping'];
		$parameters = $params['parameters'];
		
	/* ---- Convert paramaters as interpolators ---- */	
		
		if(is_array($parameters)){
			
			$aa['interpolators'] = array();
			
			foreach($parameters as $key=>$value){
				
				$x = explode('.', $key);
				
				if(isset($x[1])) {
					$aa['interpolators'][$x[1]] = $value;
				} else {
					$aa['interpolators'][$x[0]] = $value;	
				}
				
			}
			
		}
		
	/* ---- Interpolation ---- */
		
		$query = Interpolators::doInterpolation($query, $interpolators);
		
	/* ---- Create R Script ---- */
		
		$sid = uniqid();
		
		$header = '';
		$footer = '';
		$header .= "library(jsonlite)\n";
		
		foreach ($mapping as $id => $variable) {
			
			$ch = curl_init();
			
			
			
			if(isset($params['test']) && $params['test'] === true) {
				
				$model = Models::getModel($id);
				
				$post['data']['request']['query'] = isset($model['data']['query']) ? $model['data']['query'] : array();
				$post['data']['request']['mapping'] = isset($model['data']['mapping']) ? $model['data']['mapping'] : array();
				
				if(isset($aa['interpolators'])) {
					
					$post['data']['request']['interpolators'] = $aa['interpolators'];
					
				}else {
					
					$post['data']['request']['interpolators'] = isset($model['data']['interpolators']) ? $model['data']['interpolators'] : array();
					
				}
				$post['data']['request']['parameters'] = isset($model['data']['parameters']) ? $model['data']['parameters'] : array();
				$post['data']['request']['pipe'] = true;
				
				if(isset($aa)) {
				
					$body = json_encode(array_merge($aa,$post));
					
				} else {
					
					$body = json_encode($post);
					
				}
				
				switch($model['data']['editor']){
					
					case "rscript":
						
						$url = "http://".$_SERVER['HTTP_HOST']."/databuilder/test/script/".$id;
						
					break;
					
					case "sqlquery":
						
						$url = "http://".$_SERVER['HTTP_HOST']."/databuilder/test/sql/".$model['data']['datasource']['id'];
						
					break;
					
				}
				
			} else {
				
				$url = "http://".$_SERVER['HTTP_HOST']."/data/select/".$id;

				$body = json_encode($aa);
			
			}
			
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			    'DATAPOINT: RSCRIPT',
			    'Content-Type:application/json'
			    ));
			$server_output = curl_exec($ch);
			//return $server_output;
			//return curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$server_output = json_decode($server_output,true);
			
		
			if(isset($server_output['data'])){
				if(gettype($server_output['data']) != 'array'){
					$server_output = json_decode($server_output['data']);
				} else {
					$server_output = $server_output['data'];	
				}
			}
			
			$server_output = json_encode($server_output,true);
			
			curl_close ($ch);
			
			$header .= $variable.' <- fromJSON("'.addslashes($server_output).'")'."\n";
			
		}
		
	/* ---- Execution ---- */
		
		file_put_contents('/tmp/'.$sid.'.r', $header.$query."\n".$footer);
		$response = Data::runProcess('R --slave --save < /tmp/'.$sid.'.r');
		File::delete('/tmp/'.$sid.'.r');
		
	/* ---- Response ---- */
		
		return $response;
		
	}
	
	public static function sqlquery($id, Request $request) {
		
	}	
	
}