<?php namespace App\Models;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class Authorization {

	public static function check($request)
	{
		
		$user = $request->user();
		
		if($user->hasRole('admin')) {
			
			return true;
			
		} else {
			
			$segments = '';
			
			foreach($request->segments() as $segment){
				
				$segments = $segments.'.'.$segment;
				
			}
			
			if($segments === '') {
				
				return true;
				
			} else {
				
				$all = substr($segments, 0, strrpos($segments, ".")).".*";
				
			}
			
			if (!Permission::where('name', $request->method().$all)->get()->isEmpty()) {
				
				if (
					
					$user->hasPermissionTo(
						
						$request->method().$all
						
					)				
					
				) {
					
					return true;
					
				} 
				
			}
			
			
			if (Permission::where('name', $request->method().$segments)->get()->isEmpty()) {
				
				return false;
				
			}
			
			else if (
				
				$user->hasPermissionTo(
					
					$request->method().$segments
					
				)
				
			) {
				
				return true;
				
			} else {
				
				return false;
				
			}
			
		}
		
		return false;
		
	}
}
