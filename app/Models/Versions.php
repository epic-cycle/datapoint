<?php namespace App\Models;

use App\Http\Controllers\Git\git;
use File;
use DB;
use App\User;
use App\Role;

// @link http://laravel.com/api/5.0/Illuminate/Filesystem/Filesystem.html

class Versions {

	private static function dir($object)
	{
		
		if(is_array($object['id'])) {
			
			$dir = Array();
			
			foreach($object['id'] as $value){
				
				$dir[] = env('VERSION_PATH').'/'.$object['type'].'/'.$value.'/'.$object['target']['auditory'].'/';	
				
			}
			
		} else {
			
			$dir = env('VERSION_PATH').'/'.$object['type'].'/'.$object['id'].'/'.$object['target']['auditory'].'/';
			
		}		
		
		return $dir;
		
	}

	private static function subject($object, $userid = null)
	{
		
		if(strtolower($object['target']["auditory"]) === 'all') {
			
			$subject = 'default';
			
		}
		else {
			
			if(empty($object['target']['id'])) {
				
				$subject = $userid;
			
			} else {
			
				$subject = $object['target']['id'];
				
			}
		}
		
		return $subject;
		
	}

	public static function get($object, $userid)
	{	
		
		if(!isset($object['target']["auditory"])) {
			
			return Versions::findVersion($object['id'], $object['type'], $userid);
			
		}
		
		// Set subject
		
		$subject = Versions::subject($object);
		
		// Set path
		
		$dir = Versions::dir($object);
		$file = $subject.'.json';

		if(is_array($dir)) {
			
			foreach($dir as $value){
				
				if (File::exists($value.$file)) {
					
					$response['data'][] = json_decode(File::get($value.$file), true);
					
				} else {
					
					//$response['data'] = 0;
					$response['data'][] = null;
					
				}
				
			}
			
		} else {
			
			if (File::exists($dir.$file)) {
				
				$response = json_decode(File::get($dir.$file), true);
				
			} else {
				
				//$response['data'] = 0;
				$response['data'] = null;
				
			}
			
		}
		
		return $response;
	}

	public static function save($object, $userid)
	{	

	/* ----------------------------- */
		
		// Define valid values
		
		$valid['object_types'] = Array("component", "model", "view", 'source');
		$valid['auditory_types'] = Array("all", "role", "user",);
		
	/* ----------------------------- */
		
		// Validate request
		
		$allow = true;
		
		/*
		if
		(
			isset($object['id']) &&			
			isset($object['type']) &&
			isset($object['data']) &&
			isset($object['target']['auditory']) &&
			isset($object['target']['id'])
		) 
		{ 
			if
			(
				in_array($object['type'], $valid['object_types']) &&
				in_array($object['target']['auditory'], $valid['auditory_types'])
			) 
			{
				$allow = true;
			}
		} 
		*/
		
	/* ----------------------------- */

		// Check
		
		if(!isset($object['target']["auditory"])) {
			
			$object['target']["auditory"] = "User";
			$object['target']["id"] = $userid;
			
		}

		// Set subject
	    
	    $subject = Versions::subject($object);
	    
	    // Set path
	    
	    $dir = Versions::dir($object);
	    $file = $subject.'.json';

	    // Create directory
	    
		if(!File::exists($dir)) {
		    if(File::makeDirectory($dir, 0775, true));
		}	    
		
		// Save contents to file

		File::put($dir.$file, json_encode($object));


		// Init Git repo

		if(!File::exists(env('VERSION_PATH').'/.git/')) {
		    $repo = git::create(env('VERSION_PATH'));
		} else {
			$repo = new git(env('VERSION_PATH'));	
		}
		
		
		// Commit

		$repo->git('add '.env('VERSION_PATH').'/'.$object["type"].'/'.$object["id"].'/'.$object['target']["auditory"].'/'.$file);
		$gitresponse = $repo->git('git -c user.name=datapoint -c user.email=datapoint@datapoint.io commit -m "'.uniqid().'" 2>&1');

		$commit = explode(PHP_EOL, $gitresponse);
		$commit = explode(" ", $commit[0]);
		$commit = str_replace("]", null, $commit[1]);

	/* ----------------------------- */

		// Create response
		
		$response['status'] = $gitresponse;
		$response['commit'] = $commit;

	/* ----------------------------- */

	    return $response;		
	}

	public static function roles($object, $userid)
	{
		
		$result = File::glob(env('VERSION_PATH').'/'.$object['type'].'/'.$object['id'].'/Role/*');
		
		if(count($result) > 0) {
			
			$ids["string"] = '';
			
			foreach($result as $path) {
				$id = basename($path, '.json');
				$ids["array"][] = $id;
				$ids["string"] .= $id.',';
			}
			
			$ids["string"] = rtrim($ids["string"], ',');
			
			$sql = 'SELECT id, name FROM roles WHERE id IN ('.$ids["string"].') ORDER BY name ASC';
			$response = DB::connection('sqlite')->select($sql);
			
		} else {
			
			$response = array();
			
		}
		
		return $response;
	}

	public static function users($object, $userid)
	{
		$result = File::glob(env('VERSION_PATH').'/'.$object['type'].'/'.$object['id'].'/User/*');
		
		
		if(count($result) > 0) {
		
			$ids["string"] = '';
			
			foreach($result as $path) {
				$id = basename($path, '.json');
				$ids["array"][] = $id;
				$ids["string"] .= $id.',';
			}
			
			$ids["string"] = rtrim($ids["string"], ',');
	
			$sql = 'SELECT id, name, email FROM users WHERE id IN ('.$ids["string"].')';
			$response = DB::connection('sqlite')->select($sql);
			
		} else {
			
			$response = array();
				
		}
		
		return $response;
	}
	
	public static function findVersion($id, $type, $userid)
	{	
		
		$response = null;
		
		$files[] = env('VERSION_PATH').'/'.$type.'/'.$id.'/User/'.$userid.'.json';
		
		foreach(User::find($userid)->roles as $role) {
			$files[] = env('VERSION_PATH').'/'.$type.'/'.$id.'/Role/'.$role->id.'.json';	
		}
		
		$files[] = env('VERSION_PATH').'/'.$type.'/'.$id.'/All/default.json';
		
		foreach($files as $file) {
			
			if (File::exists($file)) {
				$response = json_decode(File::get($file), true);
				break;
			}
			
		}		
		
		return $response;
		
	}	
	
	public static function getVersions($id, $request)
	{	
		
		$type = $request->type;
		$item = $request->item;
		$target = $request->target;
		
		$object['target'] = $target;
		
		$subject = Versions::subject($object, $request->user()->id);
		
		$repo = new git(env('VERSION_PATH'));
		
		if(empty($item)) {
		
			$path = $type.'/'.$id.'/'.$target['auditory'].'/'.$subject.'.json';
			
		} else {
			
			$path = $type.'/'.$id.'/'.$target['auditory'].'/'.$subject.'/'.$item.'.json';
			
		}
		
		$gitresponse = $repo->git('git log --pretty=format:\'{"date":"%ct","hash":"%H"}\' -- '.$path);
		
		$response = explode(PHP_EOL, $gitresponse);
		
		return $response;
		
	}
	
	public static function revert($id, $request)
	{	
		
		/*
		
		$version = $request->version;
		$item = $request->item;
	
		$object = Array(
			
			'target' => $request->target,
			'type' => $request->type,
			'id' => $id
			
		);
		
		$filepath = Versions::dir($object).Versions::subject($object, $request->user()->id).'.json';
		*/
		
		$version = $request->version;
		$type = $request->type;
		$item = $request->item;
		$target = $request->target;
		
		$object['target'] = $target;
		$object['id'] = $id;
		
		$subject = Versions::subject($object, $request->user()->id);
	
		if(empty($item)) {
		
			$filepath = env('VERSION_PATH').'/'.$type.'/'.$id.'/'.$target['auditory'].'/'.$subject.'.json';
			
		} else {
			
			$filepath = env('VERSION_PATH').'/'.$type.'/'.$id.'/'.$target['auditory'].'/'.$subject.'/'.$item.'.json';
			
		}
		
		$repo = new git(env('VERSION_PATH'));
	
		$gitresponse = $repo->git('git checkout '.$version.' -- '.$filepath);
		
		if (File::exists($filepath)) {
			
			$data = File::get($filepath);
			$json = json_decode($data, true);
			
			if (json_last_error() === JSON_ERROR_NONE) {
				
			    $response = $json;
			    
			} else {
				
				$response = $data;
				
			}
			
		} else {
			
			$response = '';
			
		}
		
		return $response;
		
	}		
	
}
