<?php namespace App\Models;

use App\Http\Controllers\Git\git;
use File;
use DB;

class Sources {

	public static function getAllActive()
	{	
		$result = File::glob(env('VERSION_PATH').'/source/*');
		
		if(count($result) > 0) {
			
			$ids["string"] = '';
			
			foreach($result as $path) {
				$id = basename($path);
				$ids["array"][] = $id;
				$ids["string"] .= $id.',';
			}
			
			$ids["string"] = rtrim($ids["string"], ',');
			
			$sql = 'SELECT id, name FROM mounts WHERE id IN ('.$ids["string"].') ORDER BY name ASC';
			$response = DB::connection('sqlite')->select($sql);
			
		} else {
			
			$response = array();
			
		}		
		
		return $response;
	}

}
