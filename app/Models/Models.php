<?php namespace App\Models;

use App\Http\Controllers\Git\git;
use File;
use DB;
use App\Models\Versions;
use Auth;

class Models {

	public static function getAllActive(){	
		$result = File::glob(env('VERSION_PATH').'/model/*');
		
		if(count($result) > 0) {
			
			$ids["string"] = '';
			
			foreach($result as $path) {
				$id = basename($path);
				$ids["array"][] = $id;
				$ids["string"] .= $id.',';
			}
			
			$ids["string"] = rtrim($ids["string"], ',');
			
			$sql = 'SELECT id, name FROM mounts WHERE id IN ('.$ids["string"].') ORDER BY name ASC';
			$response = DB::connection('sqlite')->select($sql);
			
		} else {
			
			$response = array();
			
		}		
		
		return $response;
	}
	
	public static function getModel($id){
		
		$model = Versions::get(
			
			[ "id" => $id, "type" => "model" ], 
			Auth::User()->id
			
		);
		
		if(!$model) response()->json('Data model '.$id.' not found', 503)->send();
		
		return $model;
		
	}

}
