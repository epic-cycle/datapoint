<?php namespace App\Models;

use App\Http\Controllers\Git\git;
use File;
use Auth;

class Reports {

	public static function save($id, $data){
		
		if($data['cmp'] != '') {
			
			// if dir reports does not exists then create it
			// save report in config/reports/$id/$cmp.json
			// commit (will be available versions)
			
			$dir = env('VERSION_PATH').'/report/'.$id."/";
			$file = $dir.$data['cmp'].'.json';
			$lock = env('VERSION_PATH').'/commit.lock';
			
			if(!File::exists($dir)) {
				
				File::makeDirectory($dir, 0775, true);
				
			}	
			
			// Save contents to file
			
			File::put($file, json_encode($data));
			
			
			// Init Git repo
			
			if(!File::exists(env('VERSION_PATH').'/.git/')) {
				
				$repo = git::create(env('VERSION_PATH'));
				
			} else {
				
				$repo = new git(env('VERSION_PATH'));	
				
			}
			
			
			// Wait for lock and commit
			
			$timeout = 0;
			
			while(true) {
				
				if(!File::exists($lock)) {
					
					File::put($lock, '');
					$repo->git('git add '.$file);
					$gitresponse = $repo->git('git -c user.name=datapoint -c user.email=datapoint@datapoint.io commit '.$file.' --allow-empty -m "'.uniqid().'" 2>&1');
					File::delete($lock);
					break;
					
				} else {
					
					$timeout++;
					usleep(100000);
					
				}
				
				if($timeout > 5) break;
				
			}
			
			$commit = explode(PHP_EOL, $gitresponse);
			$commit = explode(" ", $commit[0]);
			$commit = str_replace("]", null, $commit[1]);			
			
			return $commit;
			
		}
		
		return 1;
		
	}

	public static function get($id) {
		
		$dir = env('VERSION_PATH').'/report/'.$id."/";
		
		//$components = File::glob($dir.'/view.json');		
		
		$contents = File::get($dir.'/view.json');
		
		$data = json_decode($contents,true);
		
		// This is just an examle
		
		// get view
		
		// and find here moel browser
		
		// then get config of cmp-[modelbrowser-id]
		
		foreach($data['view'] as $key => $view) {
			
			if($view['component'] == 'modelbrowser') {
				
				$modelbrowser = $view;
				break;
			}
			
		}
		
		$settings = json_decode(File::get($dir.'/'.$modelbrowser['id'].'.json'),true);
		
		$aa['limit'] = $settings['model']['limit'];
		
		for($i = 0; $i <= 2; $i++) {
			
			if(!isset($settings['model']['dimensions'][$i])) {
				
				$settings['model']['dimensions'][$i] = Array();
				
			}
			
		}
		
		$aa['dimensions'] = $settings['model']['dimensions'];
		$aa['filter'] = $settings['model']['brokers']['filter']['filter'];
		$aa['daterange'] = $settings['model']['brokers']['daterange'];
		$aa['id'] = $modelbrowser['settings']['model'];
		
		$url = "http://".$_SERVER['HTTP_HOST']."/data/select/".$modelbrowser['settings']['model'];
		
		$body = json_encode($aa, true);
			
		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'DATAPOINT: RSCRIPT',
		    'Content-Type:application/json'
		    ));
		$server_output = curl_exec($ch);
		$server_output = json_decode($server_output,true);
			
		curl_close ($ch);		

		//echo Reports::jsonToDebug(json_encode($server_output['data'],true));
		return $server_output['data'];
		
	}
	
public static function jsonToDebug($jsonText = '')
{
    $arr = json_decode($jsonText, true);
    $html = "";
    if ($arr && is_array($arr)) {
        $html .= self::_arrayToHtmlTableRecursive($arr);
    }
    return $html;
}

private static function _arrayToHtmlTableRecursive($arr) {
    $str = "<table><tbody>";
    foreach ($arr as $key => $val) {
        $str .= "<tr>";
        $str .= "<td>$key</td>";
        $str .= "<td>";
        if (is_array($val)) {
            if (!empty($val)) {
                $str .= self::_arrayToHtmlTableRecursive($val);
            }
        } else {
            $str .= "<strong>$val</strong>";
        }
        $str .= "</td></tr>";
    }
    $str .= "</tbody></table>";

    return $str;
}	

}
