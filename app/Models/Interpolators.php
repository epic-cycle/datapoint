<?php namespace App\Models;

class Interpolators {

	public static function extractInterpolators($query, $matches = false){
		
		preg_match_all('/{{(.*)}}/U', $query, $interpolations);		
		
		if($matches) {
			
			return $interpolations;
			
		} else {
			
			return $interpolations[1];
			
		}
		
	}

	public static function doInterpolation($query, $interpolators) {
		
		$interpolations = Interpolators::extractInterpolators($query, true);
		
		foreach($interpolations[1] as $index => $key) {
			
			if(isset($interpolators[$key])) {
				
				$query = preg_replace("/".$interpolations[0][$index]."/U", $interpolators[$key], $query);
				
			} else {
				
				response()->json($key.' is empty', 503)->send();
				
			}
			
		}
		
		return $query;
		
	}
	
}
