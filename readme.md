## Default login

Admin

* E-Mail Address = admin@datapoint.io
* Password = admin123

User

* E-Mail Address = user@datapoint.io
* Password = admin123


## Running Datapoint via Docker

#### Non-persistent storage

```
docker run -d -p [your.local.port]:80 registry.optibet.lv/sites/datapoint/datapoint-base:0.0.1
```

Open http://127.0.0.1:[your.local.port]

#### Persistent storage

```
docker run -d -p [your.local.port]:80 \
-v <your/local/path/to/store/configs>:/app/www/datapoint/storage/config/
-v <your/local/path/to/sotre/database>:/app/www/datapoint/storage/db/
registry.optibet.lv/sites/datapoint/datapoint-base:0.0.1
```

Open http://127.0.0.1:[your.local.port]

## Setup from scratch on Ubuntu 16.x


#### Nodejs 6.x and NPM 3.x

```
curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
bash nodesource_setup.sh
apt-get install nodejs
```

#### PHP 5.6.x

```
apt-get purge `dpkg -l | grep php| awk '{print $2}' |tr "\n" " "`
add-apt-repository ppa:ondrej/php
apt-get update
apt-get install php5.6 php5.6-mbstring php5.6-dom php5.6-sqlite php5.6-mcrypt php5.6-fpm php5.6-curl php5.6-pgsql
```

#### Nginx

```
apt-get install nginx
```

#### Get source
```
cd /var/www/
git clone https://git.optibet.lv/sites/datapoint.git
```

#### Change working directory

```
cd datapoint\
```

#### Install back-end dependencies

```
apt-get update
apt-get install curl git
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
composer install
```

#### Node modules

```
npm install
npm install yargs
```

#### Bower

```
npm install -g bower
```

#### Gulp
```
npm install -g gulp
```

#### Gulp plugins
```
npm install gulp
npm install gulp-untar
npm install gulp-gunzip
npm install gulp-bower
npm install gulp-sqlite3
npm install gulp-shell
npm install gulp-concat
npm install gulp-replace
npm install gulp-git
npm install gulp-shell
npm install gulp-clean
```



#### Install front-end dependencies

```
gulp
```

#### Setup database

```
yes | php artisan migrate:refresh --force --seed
```

#### Permissions

```
chown -R www-data:www-data ../datapoint
```


