pipeline {

	agent { label 'rancher-build-dc0-node0' }

	environment {
		
		THIS_STACK = "datapoint"
		
	}

	post {
		
		always {
			
			cleanWs()
			
		}
		
	}  
	
	stages {
		
		stage('PREPARE') {
			
			steps{
				
				script {
					
					dir('infrastructure/stack/' + env.THIS_STACK+ '/' + env.THIS_STACK + '-builder/') {
						
						sh """
						docker build --no-cache=false -t ${env.THIS_STACK}/builder .
						docker tag ${env.THIS_STACK}/builder ${env.THIS_STACK}/builder:${env.gitlabMergeRequestLastCommit}
						"""
						
					}
					
				}
				
			}		
			
		}
		
		stage('BUILD') {
			
			steps{
				
				script {
					
					sh """
					chmod +x build.sh
					
					mkdir -p /root/.cache/bower
					mkdir -p /root/.composer/cache
					mkdir -p /root/.npm
					
					docker run -i -ephemeral \
					-v /root/.composer/cache:/root/.composer/cache \
					-v /root/.cache/bower:/root/.cache/bower \
					-v /root/.npm:/root/.npm \
					-v \$(pwd):/source ${env.THIS_STACK}/builder:${env.gitlabMergeRequestLastCommit} \
					/source/build.sh
					
					docker rmi ${env.THIS_STACK}/builder:${env.gitlabMergeRequestLastCommit}
					"""
					
				}
				
			}
			
		}
		
		stage('TEST') {
			
			steps{
				
				script {
					
					
				}
				
			}
			
		}		
		
		stage('ARCHIVE') {
			
			steps{
				
				script {
					
					sh """
					VERSION_LAST=`git describe --abbrev=0 --tags | grep -Eo '[+-]?[0-9]+([.][0-9]+)?'`
					VERSION_NEW=`echo \$VERSION_LAST | awk -F. -v OFS=. 'NF==1{print ++\$NF}; NF>1{if(length(\$NF+1)>length(\$NF))\$(NF-1)++; \$NF=sprintf(\"%0*d\", length(\$NF), (\$NF+1)%(10^length(\$NF))); print}'`
					
					mkdir "v\$VERSION_NEW"
					tar -zcvf "v\$VERSION_NEW/${env.THIS_STACK}-v\$VERSION_NEW.tar.gz" . --exclude-vcs --exclude "v\$VERSION_NEW"
					
					ssh root@${ARTIFACT_STORAGE_IP} 'mkdir -p ${ARTIFACT_STORAGE_DIR}/${env.THIS_STACK}/'
					scp v\$VERSION_NEW/${env.THIS_STACK}-v\$VERSION_NEW.tar.gz root@${ARTIFACT_STORAGE_IP}:${ARTIFACT_STORAGE_DIR}/${env.THIS_STACK}/${env.THIS_STACK}-v\$VERSION_NEW.tar.gz
					
					rm -rf "v\$VERSION_NEW"					
					"""					
					
				}
				
			}		
			
		}		
		
		stage('PUBLISH') {
			
			steps{
				
				script {
					
					sh """
					VERSION_LAST=`git describe --abbrev=0 --tags | grep -Eo '[+-]?[0-9]+([.][0-9]+)?'`
					VERSION_NEW=`echo \$VERSION_LAST | awk -F. -v OFS=. 'NF==1{print ++\$NF}; NF>1{if(length(\$NF+1)>length(\$NF))\$(NF-1)++; \$NF=sprintf(\"%0*d\", length(\$NF), (\$NF+1)%(10^length(\$NF))); print}'`
										
					git tag v\$VERSION_NEW
					git push origin v\$VERSION_NEW
					"""
					
				}
				
			}
			
		}		
		
	}

}