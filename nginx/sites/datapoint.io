server {
    server_name datapoint.io;
    listen 80;

    root /var/www/datapoint/public;

    location / {
      index index.php;
      try_files $uri $uri/ /index.php?$args;
      autoindex on;
    }

    location ~ (\.php$) {
      fastcgi_split_path_info ^(.+\.php|json)(/.+)$;
      fastcgi_pass unix://run/php/php5.6-fpm.sock;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      fastcgi_param PHP_VALUE "error_log=/var/log/php/${server_name}_errors.log";
      fastcgi_read_timeout 60;
      include fastcgi_params;
    }

    allow all;

}
