
// Loading resources

ljs

	.addAliases({

	// Application

	    vendors	:
					    [
					    	"/deps/main.js",
					    ],

	    controllers	:
					    [
					    	"/js/controllers/Objects.js",
							"/js/controllers/Components.js",
							"/apps/gridstack/controllers/Gridstack.js",	
							"/apps/editview/controllers/Editview.js",							
					    ],
						
	    services	:
					    [
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",
							"/js/services/Report.js"
					    ],									
			    
	    app			:
					    [
							"/apps/editview/app.css?" + Date.now(),
							"/apps/init.js?" + Date.now()
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'dhtmlx',
		'chance',
		'angular',
		'momentjs',
		
		/* ---: Local :--- */
		
		'controllers',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){

			$('body').fadeIn(500);
		
		}	
	)
;

// EOF
