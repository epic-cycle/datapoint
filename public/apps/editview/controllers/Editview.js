/**
 *	Module for controller "Editview"
 * 
 *  @doc	module
 * 	@name	controller.Editview
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Editview', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Editview
	 * 
	 *	@doc	controller
	 * 	@name	Users
	 *	@duty	Rolands Strickis
	 *	@link	
	*/
	

	.controller('Editview', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, $location, Object) {
		
	/* ------: Extend controller :------ */		
		
		$controller('Components', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		$controller('Objects', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});

	/* ------: Data model :------ */
		
		$scope.model =
		{
			id : $attrs.oid,
			type : $attrs.otype,
			target : {
				auditory : "All", 
				id : undefined
			}
		};		

	/* ------: Linked applications :------ */	

		$scope.apps = {
			
			gridstack : null
			
		};
		
		
	/* ------: Calls from child :------ */
		
		window.controller = { editview : $scope };
		
		
	/* ------: Attributes :------ */
		
		$attrs.window = {
			toolbar : true,
			conf: true,
			viewport : $attrs.id,
			guid : chance.guid(),
		};
		
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		$scope.layout = $scope.window.attachLayout("1C");
		$scope.layout.cells("a").setText("Layout");
		$scope.layout.cells("a").attachURL("/app/gridstack/" + $attrs.oid);
		
	/* ------: Link presentation :------ */
		
		// OLD
		$scope.gridstack = $scope.layout.cells("a").getFrame().contentWindow;
		
		// NEW
		$scope.layout.attachEvent("onContentLoaded", function(id){
			
			if(id === "a") {
				
				setTimeout(function(){ 
					
					$scope.apps.gridstack = $scope.layout.cells("a").getFrame().contentWindow.angular.element(".grid-stack").scope();	
					
					Object.get({
						
						id : $($scope.layout.cells("a").getFrame()).contents().find(".grid-stack").attr("viewid"),
						type : "view",
						target : {
							auditory : "All", 
							id : null				
						}
						
					})
					
					.success(function(response) {
						
						$scope.object = response;
						
					})
					
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to get view : " + response,
							expire: 5000
						});
						
					});	
					
					//$scope.apps.gridstack.initGridstack($scope.object);					
					
				}, 100);
				
				
			}
			
		});
		
		
	/* ------: Object change :------ */
		
		$scope.$watch('object', function(object){
			
			if(object !== undefined) {
				
				$scope.apps.gridstack.initGridstack($scope.object.data);
				
			}
			
		});
		
		
	/* ------: Attach Statusbar to Layout :------ */
		
		$scope.statusbar = $scope.layout.cells("a").attachStatusBar({
		    text:   "Statusbar",
		    height: 35
		});
		
		
	/* ------: Get components :------ */
		
		$.getJSON("/js/components/main.json?" + Date.now(), function(data) {
			
			var components = [];
					
			$.each(data.components, function(index) {
				components.push(data.components[index].id);
			});
			
		/* ------: Toolbar configuration :------ */
			
			// Define configuration
			
			$scope.conf.toolbar.private = [
				{
					id: "addcomponent", type: "buttonSelect", text: "Add component..", openAll: true, img: "add-18px.png",
					selected: "type-all",
					options: data.components
				},
				{ 
					id: "test", type: "button", img: "test-view-32px.png", text: "Test view"
				},					
				{ 
					id: "save", type: "button", img: "save-18px.png", text: "Save changes"
				},
				{ 
					id: "close", type: "button", img: "close-18px.png", text: "Close"
				}
			];
			
			// Load configuration
			
			$scope.toolbar.loadStruct($scope.conf.toolbar.private);
			
			// Attach events
			
			$scope.toolbar.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						$scope.close();
						
					break;
					
					
					case "test":
						
						window.open("/app/views/" + $attrs.oid + "?" + chance.guid(), '_blank');
						
					break;
					
					
					case "save":
						
						var target = { 
							auditory : $scope.target.auditory, 
							id : $scope.target.id
						};
						
						$scope.apps.gridstack.saveGridstack(target);
						
					break;
					
					
					default:
						
						if(components.includes(id) === true) {
							$scope.gridstack.addComponent(id);
						}
						
					break;
					
				}
			});
			
		})
		
		
		/* ------: On error :------ */
		
		.error(function(response, status) {
			
			dhtmlx.message({
				type: "error",
				text: "Get components : " + status,
				expire: 5000
			});
			
		});
		

	/* ------: Process form :------ */
		
		$scope.processForm = function (data){
			
			$scope.modal.callback = null;
			
			switch ($scope.modal.form) {
				
				case "setlabel":
					
					$scope.gridstack.widgets[$scope.modal.widgetid].find("#label-" + $scope.modal.widgetid).html(data.name);
					$scope.gridstack.widgets[$scope.modal.widgetid].attr('data-label', data.name);
					
					$scope.modal.window.window.close();					
					
				break;
				
				
				case "componentsettings":
					
					$scope.gridstack.widgets[$scope.modal.widgetid].attr('data-settings', JSON.stringify(data));
					$scope.modal.window.window.close();	
					
				break;
				
				
				case "configureview":
					
					$scope.gridstack.widgets[$scope.modal.widgetid].attr('data-view', JSON.stringify(data));
					$scope.modal.window.window.close();	
					
				break;				
				
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Business logic for this form does not implemented!",
						expire: 5000
					});						
					
				break;
				
			}
			
		};

		$scope.componentView = function(id){
			
			$scope.modal = $scope.createModal({
				
				title : "Configure view",
				form : "configureview",
				dim : { width:500, height:400 }
				
			});
			
			$scope.modal.widgetid = id;
			
		};
		
		
		$scope.setLabel = function(id){
			
			$scope.modal = $scope.createModal({
				
				title : "Set label",
				form : "setlabel",
				dim : {width:500, height:200}
				
			});
			
			$scope.modal.widgetid = id;
			
		};
		
		$scope.componentSettings = function(id){
			
			var component = $scope.gridstack.widgets[id].attr('data-component');
			
			switch(component) {
				
				case "graph":
					
					dim = {width:850, height:700};
					
				break;
				
				
				default:
					
					dim = {width:700, height:600}; // TODO : calculate dynamic n% of h/w
					
				break;
				
			}
			
			$scope.modal = $scope.createModal({
				
				title : "Settings",
				form : "componentsettings",
				dim : dim
				
			});
			
			$scope.modal.widgetid = id;
			$scope.modal.component = component;
			
		};		
		
	/* ------: Compose request :------ */	
		
		$scope.request = function(){
			
			var request = 
			{
				id : $attrs.oid,
				type : $attrs.otype,
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				}
			};
			
			return request;
			
		};
		
	});
	

// EOF