/**
 *	Module for controller "Ace"
 * 
 *  @doc	module
 * 	@name	controller.Ace
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Ace', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with Data sources
	 * 
	 *	@doc	controller
	 * 	@name	Ace
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Ace :------ */

	.controller('Ace', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, $location) {
		
		if($scope.configuration === undefined) {
			
			$scope.configuration = {
				
				editor : {
					
					mode : "pgsql",
					theme : "ambiance"
					
				},
				
				autocomplete : {
				
				}	
				
			};	
			
		}
		
		$scope.completers = [
		
			// Comment below if you do not want a buch of rarely used keywords
			
			//langTools.keyWordCompleter,
			//langTools.snippetCompleter
			
		];
		
		$.each($scope.configuration.autocomplete, function( key, value ) {
			
			if(["schemas", "databases", "tables", "columns", "views", "keywords", "functions", "categories"].includes(key)) {
				
				$scope.configuration.autocomplete[key].fn = {
					
					getCompletions: function(editor, session, pos, prefix, callback) {
						
						var wordList = $scope.configuration.autocomplete[key].wordList.sort();
						
						callback(null, wordList.map(function(word) {
							
							return {
								
								caption: word,
								value: word,
								meta: $scope.configuration.autocomplete[key].meta
								
							};
							
						}));
						
					}
					
				};
				
				$scope.completers.push($scope.configuration.autocomplete[key].fn);
				
			}
			
		});
		
		
		// Helper
		
		editor.keyBinding.origOnCommandKey = editor.keyBinding.onCommandKey;
		
		
		editor.keyBinding.onCommandKey = function(e, hashId, keyCode) {
			
			if(keyCode === 191) {
				
				xx("categories");
				
			} else {
				
				this.origOnCommandKey(e, hashId, keyCode);
				
			}
			
			
		};
		
		// http://stackoverflow.com/questions/29956494/how-to-trigger-autocomplete-on-character-ace-editor
		var xx = function(key){
			
			$scope.zz = {
				
				getCompletions: function(editor, session, pos, prefix, callback) {
					
					var wordList = $scope.configuration.autocomplete[key].wordList.sort();
					
					callback(null, wordList.map(function(word) {
						
						return {
							
							caption: word,
							value: word,
							meta: $scope.configuration.autocomplete[key].meta
							
						};
						
					}));
					
				}
				
			};
			
			//$scope.completers.push($scope.configuration.autocomplete[key].fn);		
		
			langTools.setCompleters([$scope.zz]);
			
			editor.commands.byName.startAutocomplete.exec(editor);
			
		};
		
		editor.commands.addCommand({
		    name: "myCommand",
		    bindKey: { win: "@", mac: "@" },
		    exec: function (editor) {
		         xx();
		    }
		});		
		
		
		langTools.setCompleters($scope.completers);
		
		editor.setOptions({
			
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: true	
			
		});		
		
		editor.setTheme("ace/theme/" + $scope.configuration.editor.theme);
		editor.getSession().setMode("ace/mode/" + $scope.configuration.editor.mode);
		editor.$blockScrolling = Infinity;		


        editor.on('change', (obj, editor) => {
            
            if(obj.action == "insert") {
            	
            	switch (obj.lines[0]) {
            		
            		case "? Domains":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("domains");
		            		
		            	}, 10);
            			
            		break;
            		
            		case "? Tables":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("tables");
		            		
		            	}, 10);
            			
            		break;

            		case "? Columns":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("columns");
		            		
		            	}, 10);
            			
            		break;

            		case "? Games":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("games");
		            		
		            	}, 10);
            			
            		break;
            		
            		case "? Functions":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("functions");
		            		
		            	}, 10);
            			
            		break;    
            		
            		case "? Currencies":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("currencies");
		            		
		            	}, 10);
            			
            		break;             		
            		
            		case "? Countries":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("countries");
		            		
		            	}, 10);
            			
            		break;               		
            		
            		case "? Keywords":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("keywords");
		            		
		            	}, 10);
            			
            		break;                		
            		
            		case "? Providers":

		            	setTimeout(function(){ 
							
							var Range = ace.require("ace/range").Range;
							var row = editor.selection.lead.row;
							var newText = "";
							editor.session.replace(new Range(row, obj.start.column, row, obj.end.column), newText);
		            	
		            		xx("providers");
		            		
		            	}, 10);
            			
            		break;               		
            		
            		default:
            		
            			langTools.setCompleters($scope.completers);
            		
            		break;
            		
            	}
            	
            }
            
            
            //console.log();
            //console.log(obj.action);
            //console.log(obj.lines);
           	//switch (obj.action) {
			
			//	case "insert":
			
					
					
			//	break;
			
            //}
            
        });



		
	});	

// EOF