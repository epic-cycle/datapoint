
ljs

	.addAliases({
		
		vendors		:
						[
							"/deps/main.js",
						],
					
		controllers	:
						[
							"/apps/ace/controllers/Ace.js",
						],
		app			:
						[
							"/apps/ace/app.js",							
							"/apps/init.js",
						],												
							
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'angular',
		'ace',
		
		/* ---: Local :--- */
		
		'controllers',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			function disableF5(e) {
				
				if ((e.which || e.keyCode) == 116) {
					
					e.preventDefault();
					var myEl = angular.element(document.querySelector('#editor'));
					var myScope = angular.element(myEl).scope(); 
					myScope.execute();
					
				}
				
			}
			
			$(document).ready(function(){
				
				$(document).on("keydown", disableF5);
				
			});
			
		}	
		
		
	)
;

// EOF
