// Configure application

function run(){

	renderComponents(view);
	
	$('.tooltipstered').tooltipster({
		
		animation: 'fade',
		delay: 200,
		theme: 'tooltipster-noir',
		trigger: 'click',
		interactive: true
		
	});
	
	toggleEditView();
	
}

// EOF