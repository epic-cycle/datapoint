/**
 *	Module for controller "Editsource"
 * 
 *  @doc	module
 * 	@name	controller.Editmodel
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Views', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with Data sources
	 * 
	 *	@doc	controller
	 * 	@name	Editsource
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Editmodel :------ */

	.controller('Views', function($rootScope, $scope, $element, $attrs, $parentScope, $location, $controller) {

	/* ------: Calls from child :------ */
		
		window.controller = { Views : $scope };
		
		
	/* ------: Component settings :------ */
		
		$scope.componentSettings = function(){
			
			$('.tooltipstered').tooltipster('close');
			
			var dim = {width:700, height:600};
			
			$scope.modal = $scope.createModal({
				
				title : "Settings",
				form : "componentsettings",
				dim : dim
				
			});
			
			$scope.modal.widgetid = window.widgetid;
			$scope.modal.widgets = widgets;
			$scope.modal.component = $scope.modal.widgets[$scope.modal.widgetid].attr('data-component'); // TODO : change dynamic !!! by window.widgetid
			
			
		};
		
		$scope.processForm = function(values){
			
			var settings = JSON.parse($("#gs-" + $scope.modal.widgetid).attr("data-settings"));
			
			$.each(values, function( index, value ) {
				
				settings[index] = value;
				
			});	
			
			$("#gs-" + $scope.modal.widgetid).attr("data-settings", JSON.stringify(settings));
			
			$rootScope.$broadcast($scope.modal.widgetid + ".apply", values);
			
			$rootScope.saveView();
			
			$scope.modal.window.window.close();
			
		};
		
	});	

// EOF