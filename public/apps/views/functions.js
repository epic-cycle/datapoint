function check(el){
	/*
	console.log($(el).attr("data-gs-width"));
	
	var id = el.id + "-layer";
	
	if($(el).width()<200){
		
        $("#" + id).remove();
        
        var width = $(el).width(), height = $(el).height();
        $("<div id='" + id + "' style='text-align:center'>SHOW</div>")
                .css("background", "white")
                //.css("opacity", 1)
                .css("position", "absolute")
                .css("zIndex", "1")
                .css("top", 0)
                .css("left", 0)
                .css("width", width)
                .css("height", height)
                .css("pointer-events", "none")
                .insertAfter($(el).find(".grid-stack-item-content"));		
			
		// show here centrally positioned label which can handle events, because layer can not do this
		
	} else {
		
        $("#" + id).remove();
        
	}
	*/
	
}


function renderComponents(serialization){
	

	$('.grid-stack').gridstack({
	    width: 12,
	    //cellHeight: 5,
	    verticalMargin: 2,
	    alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
	    resizable: {
	        handles: 'e, se, s, sw, w'
	    }
	});
	
	serialization = GridStackUI.Utils.sort(serialization);
	var grid = $('.grid-stack').data('gridstack');    
	grid.removeAll();
	grid.setAnimation(true);
	

	//remove from components button
	widgets = {};
	components = [];
/*
	    				<div style=\"display:table-cell;padding-left:5px;\"><input type=\"button\" class="parse-json" ng-click=\"updateComponent(\''+node.id+'\');\" value=\"Save\"/></div> \
	    				<div style=\"display:table-cell;padding-left:5px;\"><input type=\"button\" onclick=\"componentRemove(\''+node.id+'\');\" value=\"Detach\"/></div> \
	    				<div style=\"display:table-cell;padding-left:5px;\"><input type=\"button\" onclick=\"componentCollapse(\''+node.id+'\');\" value=\"Collapse\"/></div> \
	    				<div style=\"display:table-cell;padding-left:5px;\"><input type=\"button\" onclick=\"componentExpand(\''+node.id+'\');\" value=\"Expand\"/></div> \
*/

	_.each(serialization, function (node) {
		
		var show = "";
		
		if(node.view === undefined){
			
			node.view = {
				
				minwidth : 0,
				minheight : 0
				
			};
			
		}

		if($.parseJSON(node.header) !== true) show = 'display:none;'; else show = 'display:block;';
	    var x = grid.addWidget($(' \
	    	<div data-min-width="' + node.view.minwidth + '" data-min-height="' + node.view.minheight + '" label="' + node.label + '" onresize="check(this)" onmouseout="$(\'#toolbar-' + node.id + '\').hide()" onmouseover="$(\'#toolbar-' + node.id + '\').show()" id=\"gs-' + node.id + '\" class="gscomponent" style=\"margin-left:0px;margin-top:0px;opacity:1\" data-gs-locked=\"false\" data-gs-no-resize=\"false\" data-gs-no-move=\"false\"> \
	    		<div  class=\"grid-stack-item-content\" style=\"border-bottom-style:dashed;border-width:0px;border-color:gray;\"> \
					<span id=\"toolbar-' + node.id + '\" style="display:none;"> \
						<img id=\"maximize-' + node.id + '\" ng-click=\'toggleMaximize("' + node.id +'")\' style="cursor:pointer;display:block;position:absolute;z-index:999999999;right:4px;bottom:4px;" src="/resources/shared/imgs/uncheck-16px.png"/> \
	    				<img class="tooltipstered" onclick="window.widgetid=\''+node.id+'\'" data-tooltip-content="#tooltip_content" id=\"tools-' + node.id + '\" style="cursor:pointer;display:block;position:absolute;z-index:999999999;right:24px;bottom:4px;" src="/resources/shared/imgs/footer-more-16px.png"/> \
	    			</span> \
	    			<div style=\"'+show+';padding-bottom:1px;font-size:12px;font-family:verdana;padding-left:0px;cursor:move;border-color:#CCCCCC;background: linear-gradient(#F5F8FA, #D1E2ED);border-bottom-style:solid;border-width:1px;\"> \
	    				<span style=\"display:table-cell\">' + node.label + '</span> \
	    			</div> \
	    			<'+node.component+' id=\"' + node.id + '\" cmp=\"' + node.id + '\" ng-controller="Components" class=\"'+node.component+'-'+node.id+'\"> \
	    				'+node.component+' \
	    			</'+node.component+'> \
	    		</div> \
	    		<div id="gs-' + node.id + '-layer" style="width:100%;height:100%;background-image:url(/resources/shared/imgs/pattern-light-blue-256px.jpg);pointer-events:none;z-index:1;text-align:center;position:absolute;display:none;left:1px;"> \
	    		</div> \
	    		<div id="gs-' + node.id + '-label" ng-click=\'toggleMaximize("' + node.id +'")\' style="width:100%;height:30px;font-weight:bold;left:1px;color:#636363;z-index:1;cursor:pointer;text-align:center;position:absolute;display:none;top:0px;left:0px;"> \
	    			' + node.label + ' \
	    		</div> \
	    	</div> \
	    '), 
	    node.x, node.y, node.width, node.height);

		try {
			
			if(node.height < node.view.minheight || node.width < node.view.minwidth) {
				
				showLayer("gs-" + node.id);
				
			}
			
		} catch (error) {
			
			console.log(error);
			
		}

	    $(x).find(node.component).attr('settings', JSON.stringify(node.settings));
	    x.attr('data-component',node.component);
	    x.attr('data-model',node.model);
	    x.attr('data-header',node.header);
	    x.attr('data-settings', JSON.stringify(node.settings));
	    x.attr('data-view', JSON.stringify(node.view));
	    x.attr('data-label',node.label);
	    
	    widgets[node.id] = x;
	    
	    components.push( { id : node.id, label : node.component, isChecked  : true } );
	    
	});
	
	$('.grid-stack').on('resizestop', function(event, ui) {
		
		setTimeout(function(){ 
			
			if(parseInt($(event.target).attr("data-gs-width")) < parseInt($(event.target).attr("data-min-width")) || parseInt($(event.target).attr("data-gs-height")) < parseInt($(event.target).attr("data-min-height"))){
				
				hideLayer(event.target.id);
				showLayer(event.target.id);
				
			} else {
				
				hideLayer(event.target.id);
				
			}
			
		}, 200);
		
	});	

	$('.grid-stack').on('resizestart', function(event, ui) {
		
		hideLayer(event.target.id);
		
	});	

	// TODO : what is this below ?

	//$(".myDropdownCheckbox").dropdownCheckbox("reset", components);

}

function showLayer(id){
	
	$("#" + id + "-layer").show('scale',{ percent: 0 },100);
	$("#" + id + "-label").css("top", $("#" + id).height() / 2 - 10).show();

}

function restoreLayer(id){
	
	try {
		
		setTimeout(function(){ 
			
			var grid = $('.grid-stack').data('gridstack');
			
			if(parseInt($("#" + id).attr("data-gs-width")) < parseInt($("#" + id).attr("data-min-width")) || parseInt($("#" + id).attr("data-gs-height")) < parseInt($("#" + id).attr("data-min-height"))){
				
				showLayer(id);
				grid.setAnimation(true);
				
			} else {
				
				grid.setAnimation(true);				
				
			}
			
		}, 10);
		
	} catch (error) {
		
		console.log(error);
		
	}
	
}


function hideLayer(id){
	
	try {
		
		$("#" + id + "-layer").hide('scale',{ percent: 0 },100);
		$("#" + id + "-label").hide();
		
	} catch (error) {
		
		console.log(error);
		
	}
	
}



