
// Loading resources

ljs

	.addAliases({

	// Third party added in /js/deps.js

	// Application

	    functions	:
					    [
							"/apps/views/functions.js?" + Date.now(),
							"/js/functions/main.js?" + Date.now(),
					    ],
		    
	    controllers	:
					    [
					    		"/js/controllers/Objects.js",
							"/js/controllers/Mounts.js",
							"/js/controllers/Components.js",
							"/js/controllers/Items.js",
							"/js/controllers/Versions.js",
							"/apps/gridstack/controllers/Gridstack.js",
							"/apps/views/controllers/Views.js",
					    ],
					    	
		directives	:	[
							"/js/directives/d3menu.js?" + Date.now(),
							"/js/directives/dock.js?" + Date.now(),
							"/js/directives/editview.js?" + Date.now(),
							"/js/directives/mounts.js?" + Date.now(),
							"/js/directives/picture.js?" + Date.now(),
							"/js/directives/users.js?" + Date.now(),
							"/js/directives/roles.js?" + Date.now(),
							"/js/directives/daterange.js?" + Date.now(),
							"/js/directives/editdata.js?" + Date.now(),
							"/js/directives/filter.js?" + Date.now(),
							"/js/directives/datagrid.js?" + Date.now(),
							"/js/directives/paginator.js?" + Date.now(),
							"/js/directives/url.js?" + Date.now(),
							"/js/directives/singlestat.js?" + Date.now(),
							"/js/directives/modelbrowser.js?" + Date.now(),
							"/js/directives/graph.js?" + Date.now(),
							"/js/directives/tableastarget.js?" + Date.now(),
							"/js/directives/editor.js?" + Date.now(),
							"/js/directives/dbexplorer.js?" + Date.now()
							
						],
		
	    services	:
					    [
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",
							"/js/services/Mount.js",
							"/js/services/Data.js",
							"/js/services/Databuilder.js",
							"/js/services/Source.js",
							"/js/services/Model.js",
							"/js/services/Item.js",
							"/js/services/Report.js",
					    ],									
			    
	    app			:
					    [
							"/apps/views/app.js?" + Date.now(),
							"/apps/views/app.css?" + Date.now(),
							"/css/views.css?" + Date.now(),
							"/apps/init.js?" + Date.now()
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'jquery',
		'jqueryui',
		'dhtmlx',
		'chance',
		'jquerytouchp',
		'lodash',
		'momentjs',
		'gridstack',		
		'angular',
		'daterange',
		'datatables',
		'timer',
		'jqsqlbuilder',
		'angularbs',
		'md5',
		'tooltipster',
		'flowtype',
		'fittext',
		'boxfit',
		'datetimepicker',
		'select2',
		
		/* ---: Local :--- */
		
		'functions',
		'controllers',
		'directives',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(1);
			
			try {
				
				//view = JSON.parse('{!! $view !!}');
				//view = JSON.parse(view);
				
				function disableF5(e) {
					
					if ((e.which || e.keyCode) == 116) {
						
						e.preventDefault();
						
					}
					
				}
				
				$(document).ready(function(){
					
					$(document).on("keydown", disableF5);
					
				});	
				
			} catch(e){
				
				dhtmlx.message({
					type: "error",
					text: e,
					expire: 5000
				});						
				
			}			
			
		}	
	)
;

// EOF
