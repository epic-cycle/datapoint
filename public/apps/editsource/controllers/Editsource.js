/**
 *	Module for controller "Editsource"
 * 
 *  	@doc	module
 * 	@name	controller.Editsource
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Editsource', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with Data sources
	 * 
	 *	@doc	controller
	 * 	@name	Editsource
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Editsource :------ */

	.controller('Editsource', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, $location, Source, Model) {
		
		
	/* ------: Extend controller :------ */
		
		$controller('Components', {$scope: $scope, $attrs: $attrs, $element:$element});
		$controller('Objects', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		
	/* ------: Data model :------ */
		
		$scope.model =
		{
			id : $attrs.oid,
			type : $attrs.otype
		};			
		
	/* ------: Attributes :------ */

		$attrs.window = {
			
			toolbar : true,
			conf: true,
			viewport : $attrs.id,
			guid : chance.guid(),
			
		};
		
		
	/* ------: Configuration :------ */
		
		$scope.configuration = {
			
			editor : {
				
				mode : "json",
				theme : "ambiance"
				
			},
				
			autocomplete : {
				
				drivers : {
					
					wordList : [],
					meta : "driver"
				}
				
			}
		};
		
		
	/* ------: Save object :------ */
		
		$scope.save = function(){
			
			$scope.saveObject($scope.send());
			
		};
		
		
	/* ------: Prepare request to save object :------ */
		
		$scope.send = function(){
			
			var request = 
			{
				id : $attrs.oid,
				type : $attrs.otype,
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				},
				data : {
					datasource : $scope.presentation.editor.getValue()
				}
			};
			
			return request;
			
		};
		
		
	/* ------: Prepare request to get object :------ */
		
		$scope.request = function(){
			
			var request = 
			{
				id : $attrs.oid,
				type : $attrs.otype,
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				}
			};
			
			return request;
			
		};
		

	/* ------: Test datasource :------ */
		
		$scope.test = function(){
			
			var connection = {
				config : JSON.parse($scope.presentation.editor.getValue())
			};
			
			Source.test(connection)
			
			.success(function(response) {
				
				dhtmlx.message({
					text: "Successfull",
					expire: 10000
				});
				
			})		
			
			.error(function(response, status) {
				
				dhtmlx.message({
					type: "error",
					text: response.error,
					expire: 10000
				});
				
			});			
			
		};
		
	/* ------: Set datasource :------ */
		
		$scope.set = function(){	
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 400);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			
			var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
			
			$scope.windowModal.setSkin("material");
			
			$scope.sourcesLayout = $scope.windowModal.window.attachLayout("2U");	
			$scope.sourcesLayout.setSkin("material");								
			$scope.sourcesLayout.cells("a").setText("Categories");
			$scope.sourcesLayout.cells("b").setText("Sources");
			
			$scope.toolbarSources = new dhtmlXToolbarObject({
				parent : "toolbarObj",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
					},						
					{
						id: "apply", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
					}
				]					
			});
			
			$scope.toolbarSources.setAlign("right");			
			
			$scope.toolbarSources.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
					case "apply":
						
						var cid = $scope.gridCategories.getSelectedRowId();
						var sid = $scope.gridSources.getSelectedRowId();
						
						switch(cid) {
							
							case "external":
								
								$scope.presentation.editor.setValue($scope.data.configuration[sid].conf,1);							
								
							break;
							
							case "internal":
								
								//$scope.presentation.editor.setValue($scope.gridSources.cellByIndex($scope.gridSources.getRowIndex(sid),0).getValue(),1);
								$scope.presentation.editor.setValue(sid,1);
								
							break;					
							
						}
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});

			var categories = {
				rows : [
					{
						"id":"external",
						"data":["External"]
					},
					{
						"id":"internal",
						"data":["Internal"]
					}
				]
			};
			
			$scope.gridCategories = $scope.sourcesLayout.cells("a").attachGrid();
			$scope.gridCategories.setImagePath("/include/dhtmlx/codebase/imgs/");
			$scope.gridCategories.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridCategories.setHeader("Name");
			$scope.gridCategories.setInitWidths("*,*");
			$scope.gridCategories.setColAlign("left");
			$scope.gridCategories.setColTypes("ro");
			$scope.gridCategories.setColSorting("str");
			$scope.gridCategories.init();
			$scope.gridCategories.setSkin("material");
			$scope.gridCategories.parse(categories, "json");			
		
			$scope.gridCategories.attachEvent("onRowSelect", function(id,ind){
				
				switch(id){
					
					case "internal":
						
						Model.index({})
												
						/* ------: On success :------ */
						
						.success(function(response) {
							
							var sources = {
								rows : []
							};
							
							$.each(response.data, function(key, value) {
								
								sources.rows.push(
									
									{
										id : response.data[key].id,
										data:
										[
											response.data[key].name
										] 
									}
									
								);
								
							});
							
							$scope.gridSources.clearAll();
							$scope.gridSources.parse(sources, "json");
							
						});
						
					break;
					
					case "external":
						
						$.getJSON("/conf/datasources.json?" + new Date(), function(data) {
							
							$scope.data = data;
							
							var sources = {
								rows : []
							};
							
							$.each(data.datasources, function( index, value ) {
								sources.rows.push(
									{
										id: value.id,
										data : 
										[ 
											value.text
										]
									}
								);
							});							
							
							$scope.gridSources.clearAll();
							$scope.gridSources.parse(sources, "json");
							
						});
						
					break;
					
				}
				
			});
			
			$scope.gridSources = $scope.sourcesLayout.cells("b").attachGrid();
			$scope.gridSources.setImagePath("/include/dhtmlx/codebase/imgs/");
			$scope.gridSources.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridSources.setHeader("Name");
			$scope.gridSources.setInitWidths("*,*");
			$scope.gridSources.setColAlign("left");
			$scope.gridSources.setColTypes("ro");
			$scope.gridSources.setColSorting("str");
			$scope.gridSources.init();
			$scope.gridSources.setSkin("material");			
			
		};
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		$scope.layout = $scope.window.attachLayout("1C");
		$scope.layout.cells("a").setText("Configuration");
		$scope.layout.cells("a").attachURL("/app/ace/");
		$scope.layout.setSkin("material");
		
		
	/* ------: Link presentation :------ */
		
		$scope.presentation = $scope.layout.cells("a").getFrame().contentWindow;
		//$scope.presentation.editor = $scope.layout.cells("a").getFrame().contentWindow.angular.element("#editor").scope();
		
		
	/* ------: Send data to presentation :------ */
		
		$scope.layout.attachEvent("onContentLoaded", function(id){
			
			if (id === "a") {
				
				$scope.getObject($scope.request());
				
				$scope.$watch("object", function(object){
					
					if(object !== undefined) {
						
						$scope.presentation.editor.setValue(object.data.data.datasource,1);
						
					}
					
				});
				
				
			}
			
		});
		
		
	/* ------: Toolbar configuration :------ */
		
		// Define configuration
		
		$.getJSON("/conf/datasources.json?" + new Date(), function(data) {
			
			$.each(data.datasources, function( key ) {
				
				$scope.configuration.autocomplete.drivers.wordList.push(data.datasources[key].id);
				
			});
			
			$scope.conf.toolbar.private = 
			[
				{
					/* Old
					id: "set", type: "buttonSelect", text: "Set source to..", openAll: true, img: "datasource-32px.png",
					options: data.datasources
					*/
					id: "set", type: "button", img: "datasource-32px.png", text: "Set source to.."
				},
				{
					id: "test", type: "button", img: "play-18px.png", text: "Test datasource"
				},
				{
					id: "save", type: "button", img: "save-18px.png", text: "Save changes"
				},
				{ 
					id: "close", type: "button", img: "close-18px.png", text: "Close"
				}
			];
			
			
			// Load configuration
			
			$scope.toolbar.loadStruct($scope.conf.toolbar.private);
			
			
			// Attach events
			
			$scope.toolbar.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "test":
						
						$scope.test();
						
					break;
					
					
					case "close":
						
						$scope.close();
						
					break;
					
					
					case "save":
						
						$scope.save();
						
					break;
					
					case "set":
						
						$scope.set();
						
					break;					
					
					default:
						
						if
						(
							[
								"type-all",
								"type-role",
								"type-user",
								"subject"
							].includes(id) === false
						) {
							
							$scope.presentation.editor.setValue(data.configuration[id].conf,1);
							
						}
						
					break;
					
				}
				
			});
			
		})
		
		
		/* ------: On error :------ */
		
		.error(function(response, status) {
			
			dhtmlx.message({
				type: "error",
				text: "Get datasources : " + status,
				expire: 5000
			});
			
		});
		
	});	

// EOF