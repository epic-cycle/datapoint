
ljs

	.addAliases({

		vendors		:
					    [
					    	"/deps/main.js",
					    ],
		
		functions	:
						[
						
						],
						
		controllers	:
						[
					    	"/js/controllers/Objects.js",
							"/js/controllers/Components.js",						
							"/js/controllers/Users.js",
						],
							    	
		directives	:	
						[
						
						],
						
		services	:
						[
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",						
							"/js/services/Mount.js",
							"/js/services/App.js",
							"/js/services/View.js",
							"/js/services/Databuilder.js",
							"/js/services/Source.js",
							"/js/services/Model.js",
							"/js/services/Data.js",
							"/js/services/Report.js"
						],									
						
		app			:
						[
							"/apps/users/app.css?" + Date.now(),
							"/apps/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */

		'vendors',		
		'jquery',
		'chance',
		'dhtmlx',
		'angular',
		
		/* ---: Local :--- */
		
		'controllers',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(1);
			
		}	
	)
;

// EOF
