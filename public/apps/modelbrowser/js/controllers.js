'use strict';

/* Controllers */

angular.module('raw.controllers', ["factory.Object"])

	.controller('RawCtrl', function ($rootScope, $scope, dataService, Object) {
		
		$scope.presentationModel = {};
		
		$scope.setChartType = function(data){
			
			$.each($scope.charts, function(index) {
				
				if($scope.charts[index].title() == data.initiator.title){
					
					$scope.chartIndex = index;
					return false;
					
				};
				
			});	
			
			$scope.selectChart($scope.charts[$scope.chartIndex]);
			$scope.$apply();
			$scope.get(data.initiator.cid);
			
		};
		
		
		$scope.save = function(destination){
			
			// Create 
			
			$scope.presentationModel[destination] = {};
			
			// Model
			
			$scope.presentationModel[destination]["model"] = [];
			
			var dimensions = $scope.model.dimensions().values();
			
			$.each(dimensions, function(dimension) {
				
				$scope.presentationModel[destination]["model"].push(dimensions[dimension].value);
				
			});	
			
			// UI
			
			$scope.presentationModel[destination]["ui"] = [];
			
			var ui = $(".dimensions-model").find("ul");
			$.each(ui, function( index, value ) {
				$scope.presentationModel[destination]["ui"][index] = $(ui[index]).html();
			});	
			
			// get on save
			// get on apply
			// should store UI and for running
			
			console.log($scope.presentationModel);
			
		}
		
		$scope.get = function(destination){
			
			// Model
			try {
				$.each($scope.presentationModel[destination]["model"], function(dimension) {
					
					$scope.model.dimensions().values()[dimension].value = $scope.presentationModel[destination]["model"][dimension];
					
				});	
			} catch(error) {
				
			}
			
			// UI
			
			try {
			var ui = $(".dimensions-model").find("ul");
				
				$.each(ui, function( index, value ) {
					$(ui[index]).html($scope.presentationModel[destination]["ui"][index]);
				});	
				
			} catch (error) {
				
			}
			
		}		
		
		
		$scope.kaka = function(){
			
			window.metadata = $scope.metadata;
			window.chart = $scope.chart;
			window.data = $scope.data;
			window.text = $scope.text;
			window.models = $scope.model;
			
			//window.dimensions = $scope.model.dimensions().values()
			
			
			window.dimensions = [];
			//window.dimensions2 = {};
			
			var dimensions = $scope.model.dimensions().values();
			
			$.each(dimensions, function(dimension) {
				
				window.dimensions.push(dimensions[dimension].value);
				//window.dimensions2[$scope.model.dimensions().values()[dimension].title()] = dimensions[dimension].value;
				
			});		
			
			
		};




	$scope.start = function(){
		
      dataService.loadSample("data/dispersions.csv").then(
        function(data){
          $scope.text = data;
        }, 
        function(error){
          $scope.error = error;
        }
      );
		
	}

    $scope.samples = [
      { title : 'Cars (multivariate)', url : 'data/multivariate.csv' },
      { title : 'Movies (dispersions)', url : 'data/dispersions.csv' },
      { title : 'Music (flows)', url : 'data/flows.csv' },
      { title : 'Cocktails (correlations)', url : 'data/correlations.csv' }
    ]

    $scope.$watch('sample', function (sample){
      if (!sample) return;
      dataService.loadSample(sample.url).then(
        function(data){
          $scope.text = data;
        }, 
        function(error){
          $scope.error = error;
        }
      );
    });


    // init
    $scope.raw = raw;
    $scope.data = [];
    $scope.metadata = [];
    $scope.error = false;
    $scope.loading = true;

    $scope.categories = ['Correlations', 'Distributions', 'Time Series', 'Hierarchies', 'Others'];

    $scope.parse = function(text){

      if ($scope.model) $scope.model.clear();

      $scope.data = [];
      $scope.metadata = [];
      $scope.error = false;
      $scope.$apply();

		// TODO : those prorotype functions should be global
		
		String.prototype.encode = function(){
		    var hex, i;
		
		    var result = "";
		    for (i=0; i<this.length; i++) {
		        hex = this.charCodeAt(i).toString(16);
		        result += ("000"+hex).slice(-4);
		    }
		
		    return result
		}
		
		String.prototype.decode = function(){
		    var j;
		    var hexes = this.match(/.{1,4}/g) || [];
		    var back = "";
		    for(j = 0; j<hexes.length; j++) {
		        back += String.fromCharCode(parseInt(hexes[j], 16));
		    }
		
		    return back;
		}

      try {
      	
        var parser = raw.parser();
        $scope.data = parser(text);
        
        // Rolands : here should be data from back end
        
			Object.get(
				{
					id : window.model,
					type : "model",
					target : {
						auditory : "All", 
						id : null
					}
				}
			)
			
			.success(function(response) {	
				
				response = response.data;
				
				// Rolands :: datatype mapping (shoul be as config)
				
				$.getJSON("/conf/datatypes.json?" + new Date(), function(datatypes) {
					
					$scope.tables = [];
					var fields = JSON.parse(response.data.settings);
					var tables = {};
					var section = null;
					
					$.each(fields, function(field) {
						
						// TODO : encode table id for html
						
						section = fields[field]["Table title"].encode();
						
						if(tables[section] === undefined) {	
							
							tables[section] = {
								
								columns : [],
								name : fields[field]["Table name"],
								title : fields[field]["Table title"],
								section : section
								
							};
						}
						
						if(fields[field].Explorer == 1){
							
							try {
							
								tables[section].columns.push({
									
									table : fields[field]["Table name"],
									key : fields[field]["Field name"],
									title : fields[field]["Field title"],
									aggregate : fields[field].Aggregate,
									aggregations : fields[field].Aggregations,
									ordering : fields[field].Ordering,
									value : fields[field].Value,
									type : datatypes.model[fields[field].Type].modelexplorer
									
								});
								
							} catch (error) {
								
								console.log("Field " + field.decode() + " not added!");
								
							}
							
						}
						
					});
					
					$.each(tables, function(table) {
						
						$scope.tables.push(
							{
								name : tables[table].name,
								title : tables[table].title,
								columns : tables[table].fields
							}
						);							
						
					});
					
					$scope.tables = tables;
					
					$scope.metadata = parser.metadata(text); // set the same metadata as $scope.tables columns
					//console.log($scope.metadata);
					//$scope.metadata = columns;
					
					$scope.error = false;	
					
				});
				
			})
				
			.error(function(response) {	
				
				console.log(response);
				alert("error");
				
			})				
				
      } catch(e){
      	console.log(e);
        $scope.data = [];
        $scope.metadata = [];
        $scope.error = e.name == "ParseError" ? +e.message : false;
      }
      if (!$scope.data.length && $scope.model) $scope.model.clear();
      $scope.loading = false;
    }

    $scope.delayParse = dataService.debounce($scope.parse, 500, false);

    $scope.$watch("text", function (text){
      $scope.loading = true;
      $scope.delayParse(text);
    });

    $scope.charts = raw.charts.values().sort(function (a,b){ return a.title() < b.title() ? -1 : a.title() > b.title() ? 1 : 0; });
    $scope.chart = $scope.charts[0];
    $scope.model = $scope.chart ? $scope.chart.model() : null;

    $scope.$watch('error', function (error){
      if (!$('.CodeMirror')[0]) return;
      var cm = $('.CodeMirror')[0].CodeMirror;
      if (!error) {
        cm.removeLineClass($scope.lastError,'wrap','line-error');
        return;
      }
      cm.addLineClass(error, 'wrap', 'line-error');
      cm.scrollIntoView(error);
      $scope.lastError = error;

    })

    $('body').mousedown(function (e,ui){
      if ($(e.target).hasClass("dimension-info-toggle")) return;
      $('.dimensions-wrapper').each(function (e){
        angular.element(this).scope().open = false;
        angular.element(this).scope().$apply();
      })
    })

    $scope.codeMirrorOptions = {
      lineNumbers : true,
      lineWrapping : true,
      placeholder : 'Paste your text or drop a file here. No data on hand? Try one of our sample datasets!'
    }

    $scope.selectChart = function(chart){
      if (chart == $scope.chart) return;
      $scope.model.clear();
      $scope.chart = chart;
      $scope.model = $scope.chart.model();
    }

    function refreshScroll(){
      $('[data-spy="scroll"]').each(function () {
        $(this).scrollspy('refresh');
      });
    }

    $(window).scroll(function(){

      // check for mobile
      if ($(window).width() < 760 || $('#mapping').height() < 300) return;

      var scrollTop = $(window).scrollTop() + 0,
          mappingTop = $('#mapping').offset().top + 10,
          mappingHeight = $('#mapping').height(),
          isBetween = scrollTop > mappingTop + 50 && scrollTop <= mappingTop + mappingHeight - $(".sticky").height() - 20,
          isOver = scrollTop > mappingTop + mappingHeight - $(".sticky").height() - 20,
          mappingWidth = mappingWidth ? mappingWidth : $('.col-lg-9').width();
     
      if (mappingHeight-$('.dimensions-list').height() > 90) return;
      //console.log(mappingHeight-$('.dimensions-list').height())
      if (isBetween) {
        $(".sticky")
          .css("position","fixed")
          .css("width", mappingWidth+"px")
          .css("top","20px")
      } 

     if(isOver) {
        $(".sticky")
          .css("position","fixed")
          .css("width", mappingWidth+"px")
          .css("top", (mappingHeight - $(".sticky").height() + 0 - scrollTop+mappingTop) + "px");
          return;
      }

      if (isBetween) return;

      $(".sticky")
        .css("position","relative")
        .css("top","")
        .css("width", "");

    })


    $(document).ready(function() {
    	refreshScroll;
    	$scope.start();
	});


  })