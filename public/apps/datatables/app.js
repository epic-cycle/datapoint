/* ------: Configure application :------ */

$.getJSON( "/apps/datatables/modules.json", function(data) {

	var app = angular.module('appEditView', data.modules, 
		
		function($interpolateProvider) {
			$interpolateProvider.startSymbol('<%');
			$interpolateProvider.endSymbol('%>');
		}
		
	)
	.config(['$httpProvider', function($httpProvider) {
		
		$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
		
	}]);
	
	
/* ------: Run application :------ */

	app.run(function($rootScope) {
		
		console.log("appEditorView: application started");
		
	})
	

/* ------: Provider for parent :------ */	


	.provider('$rootScope', function () {
		this.$get = ['$window', function ($window) {
			return $window.parent.angular.element($window.frameElement).scope().$new();
		}];
	})


/* ------: Controller Datatables :------ */	
	
	.controller('Datatables', function($rootScope, $scope, $element, $attrs) {
		
		$scope.$watch('model.dataset', function(aaa) {
			
			if(aaa !== undefined) {
				
				var table = $("#example").DataTable( {
					
					aaData : $scope.model.dataset,
					aoColumns : $scope.model.columns,
					
				});
				
				$('#example tbody').on( 'click', 'tr', function () {
					
					$(this).toggleClass('selected');
					
				});	  
				
				$scope.getSelected = function(){
					
					var ids = $.map(table.rows('.selected').data(), function (item) {
						return item.id;
					});			
					
					return ids;
					
				};
				
				$scope.getSelectedData = function(){
					
					return table.rows('.selected').data();	
					
				};				
				
				$scope.deleteRows = function(data){
					
					table.rows('.selected').remove().draw( false );
					
				};
				
				$scope.insertRow = function(data){
					
					table.row.add(data).draw( false );
					
				};
			
			}
			
		});
		
	});
	
	
});
// EOF