
// Loading resources

ljs

	.addAliases({

	// Application

	    vendors	:
					    [
					    	"/deps/main.js",
					    ],

	    functions	:
					    [

					    ],
		    
	    controllers	:
					    [
					    	"/js/controllers/Objects.js",
							"/js/controllers/Components.js",
					    ],
					    	
		directives	:	[
			
						],
		
	    services	:
					    [
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",
							"/js/services/Report.js"
					    ],
			    
	    app			:
					    [
							"/apps/datatables/app.js?" + Date.now(),
							"/css/datatables.css?" + Date.now()
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'jqueryui',
		'dhtmlx',
		'chance',
		'angular',
		'datatables',
		
		/* ---: Local :--- */
		
		'controllers',
		'directives',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){

			$('body').fadeIn(500);
		
		}	
	)
;

// EOF
