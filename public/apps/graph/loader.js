
// Loading resources

ljs

	.addAliases({

	// Third party added in /js/deps.js

	// Application

	    vendors	:
					    [
					    	"/deps/main.js",
					    ],

	    functions	:
					    [

					    ],
		    
	    controllers	:
					    [
							"/apps/graph/controllers/Graph.js"
					    ],
					    	
		directives	:	[

							
						],
		
	    services	:
					    [

					    ],									
			    
	    app			:
					    [
							"/apps/init.js?" + Date.now()
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'jqueryui',
		'timer',
		'angular',
		'fittext',
		
		/* ---: Local :--- */
		
		'functions',
		'controllers',
		'directives',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){

			$('body').fadeIn(1);
		
		}	
	)
;

// EOF
