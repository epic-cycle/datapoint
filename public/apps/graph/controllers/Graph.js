/**
 *	Module for controller "Graph"
 * 
 *  @doc	module
 * 	@name	controller.Graph
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Graph', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with graphs
	 * 
	 *	@doc	controller
	 * 	@name	Graph
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Graph :------ */

	.controller('Graph', function($rootScope, $scope, $element, $attrs, $parentScope, $location, $controller) {
		
		$scope.model = {
			
			draw : false,
			libs : undefined,
			data : undefined,
			dimensions : undefined
		};
		
		$scope.setChartType = function(data){
			
			$.getJSON("/conf/charts.json?" + Date.now(), function(charts) {
				
				// Show description
				
				$("#graph").html("<div class='graph-description-container' style='display:none;'> \
						<span id='title' class='graph-description-text'><b>" + data.graph.title + "</b></span> <br/>\
						<img class='graph-description-image' src='" + charts[data.category].data[data.graph.id].img + "'/> \
						<br/><span id='description' class='graph-description-text'>" + charts[data.category].data[data.graph.id].description + "</span> \
				</div>");
					
				$(".graph-description-container").fadeIn("fast");
				$("#title").fitText(1.2, { minFontSize: '10px', maxFontSize: '15px' });
				$("#description").fitText(1.2, { minFontSize: '10px', maxFontSize: '15px' });
				
				// Set libraries
				
				$scope.model.libs = charts[data.category].data[data.graph.id].libs;
				$scope.model.libs.push("/graphs/" + data.graph.id + ".js?" + Date.now());
				
				// Loading resources
				
				ljs.load($scope.model.libs,
					
					function(){
						
					}	
					
				);
					
			})
			
			/* ------: On error :------ */
			
			.error(function(response, status) {
				
				alert("Unable to get charts");
				
			});					
			
		};
		
		$scope.remove = function(){
			
			try {
				
				d3.select("svg").remove();
				
			} catch(error){}
			
		};
		
		$scope.setData = function(data, dimensions){
			
			$scope.model.data = data;
			$scope.model.dimensions = dimensions;
			$scope.draw();
			
		};
		
        $scope.showLoading = function(){
			
            $("body").css("position", "absolute");
            $("body").css("overflow","hidden");
            var width = $("body").width() + 2, height = $("body").height();
            $("<span id='delaySpan'><span id='icon' style='display:inline-block'></span>Loading... <span style='font-size:14px;color:brown;' id='countup'></span> <span style='font-size:14px;'>sec</span></span>")
                    .css("left", width / 2 - 70)
                    .css("top", height / 2 - 30)
                    .css("position", "absolute")
                    .css("color", "#4f4f4f")
                    .css("background", "#ffffff")
                    .css("border", "1px solid #a8a8a8")
                    .css("border-radius", "3px")
                    .css("-webkit-border-radius", "3px")
                    .css("box-shadow", "0 0 10px rgba(0, 0, 0, 0.25")
                    .css("font-family", "Arial, sans-serif")
                    .css("font-size", "16px")
                    .css("padding", "0.4em")
                    .insertAfter("body");
            $("<div id='delayDiv'></div>")
                    .css("background", "#2D5972")
                    .css("opacity", 0.3)
                    .css("position", "absolute")
                    .css("top", 0)
                    .css("left", 0)
                    .css("width", width)
                    .css("height", height + 30)
                    .insertAfter("body");
				
			// Start countup timer
			$('#countup').runner({
				autostart: true,
				countdown: false,
				startAt: 0,
				milliseconds: true,
				format: function(value) {
					return (value / 1000).toFixed(2);
				}				
			});
			
        };
        
        $scope.hideLoading = function() {
            $("#delayDiv").remove();
            $("#delaySpan").remove();
            $("body").css("overflow","visible");
        };		
		
		$scope.draw = function() {
			
			// get model
			// build query
			// request data
			
			$scope.model.draw = true;
			$scope.remove();
			
			$(".graph-description-container").hide();
			
			try {
				
				draw($scope.model.data, $scope.model.dimensions);			
				
			} catch(error){
				
				alert(error);
				$scope.model.draw = false;
				$(".graph-description-container").show();
			}
			
		};
		
		$(window).resize(function() {
			
			if($scope.model.draw === true) {
				
				$scope.remove();
				$scope.draw();
				
			}
			
		});
		
	});	

// EOF