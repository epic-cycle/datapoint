
// Loading resources

ljs

	.addAliases({

	// Application
		
		vendors		:
						[
							"/deps/main.js",
						],
						
		controllers	:
					    [
							"/js/controllers/Components.js",
							"/js/controllers/Objects.js",
							"/apps/editmodel/controllers/Editmodel.js",							
						],
						
		directives	:	[
						
						],
						
		services	:
						[
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",
							"/js/services/Source.js",
							"/js/services/Mount.js",
							"/js/services/Databuilder.js",
							"/js/services/Model.js",
							"/js/services/Interpolators.js",
							"/js/services/Report.js"
						],
						
		app			:
						[
							"/apps/init.js?" + Date.now(),						
							"/apps/editmodel/app.css?" + Date.now(),
						],
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'dhtmlx',
		'chance',
		'timer',
		'angular',
		'momentjs',
		
		/* ---: Local :--- */
		
		'controllers',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(500);
			
		}	
	)
;

// EOF
