/**
 *	Module for controller "Editmodel"
 * 
 *  	@doc	module
 * 	@name	controller.Editmodel
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

/**
 *	TODO
 * 
 *	If there is no created/saved object role/user then keep all settings otherwise reload (On edit button), respectively make a copy
 *	Create a button remove role/user for this object
*/

angular.module('controller.Editmodel', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with Models
	 * 
	 *	@doc	controller
	 * 	@name	Editmodel
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Editmodel :------ */

	.controller('Editmodel', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, $location, Databuilder, Object, Source, Mount, Model, Interpolators) {
		
		
	/* ------: Extend controller :------ */
		
		$controller('Components', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		$controller('Objects', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		
		
	/* ------: Attributes :------ */
		
		$attrs.window = {
			toolbar : true,
			conf: true,
			viewport : $attrs.id,
			guid : chance.guid(),
		};
		
		
	/* ------: Data model :------ */
		
		$scope.model =
		{
			id : $attrs.oid,
			type : $attrs.otype,
			target : {
				auditory : "All", 
				id : undefined
			},
			data : {
				query : undefined,
				datasource : undefined,
				settings : undefined,
				caching : {
					enabled : undefined,
					storage : undefined,
					expiration : undefined,
				}
			},
			layout : {
				cells : {}
			},
			datatypes : undefined,
			dragFromResults : false,
			interpolators : {},
			mapping : {},
			parameters : {},
			
		};		
		
		
	/* ------: Prototype :------ */
		
		// TODO : those prorotype functions should be global
		
		String.prototype.encode = function(){
		    var hex, i;
		
		    var result = "";
		    for (i=0; i<this.length; i++) {
		        hex = this.charCodeAt(i).toString(16);
		        result += ("000"+hex).slice(-4);
		    }
		
		    return result;
		};
		
		String.prototype.decode = function(){
		    var j;
		    var hexes = this.match(/.{1,4}/g) || [];
		    var back = "";
		    for(j = 0; j<hexes.length; j++) {
		        back += String.fromCharCode(parseInt(hexes[j], 16));
		    }
		
		    return back;
		};
		
		
	/* ------: SQL beauty :------ */
		
		$scope.beautify = function(){
			
			Databuilder.prettysql({
				id:[$attrs.oid],
				sql:$scope.presentation.editor.getValue()
			})
			
			.success(function(response) {
				
				$scope.presentation.editor.setValue($(response).text(),1);
				
			})
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: response.error,
					expire: 30000
				});
				
			});
			
		};
		
		
	/* ------: Load data :------ */
		
		$scope.loadData = function(object){
			
			switch(object.data.editor){
				
				case "rscript":
					
					$scope.presentation.editor.setValue(object.data.query,1);
					
				break;				
				
				case "sqlquery":
					
					$scope.presentation.editor.setValue(object.data.query,1);
					
				break;
				
				
				case "modeldesigner":
					
					$scope.presentation.DesignerApp.NodeEntities.ClearNodeCanvas($scope.presentation.DesignerApp.NodeEntities.CurrentNodeCanvas);
					$scope.presentation.DesignerApp.NodeEntities.AddNodeCanvas(JSON.parse(object.data.model));
					
				break;
				
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Not defined how to load the data for " + object.data.editor,
						expire: 5000
					});
					
				break;
				
			}
			
		};
		
		
	/* ------: Load settings :------ */
		
		$scope.loadSettings = function(object){
			
			$scope.model.data.caching = object.data.caching;
			$scope.gridAllowedFields.clearAll();
			$scope.gridAllowedFields.parse(
				dhxMapElementToHeaderJsonToGrid(
					JSON.parse(object.data.settings),
					$scope.configuration.fields
				)									
				,"json"
			);
			
			// TODO : old sorting, when tested, remove this line
			//$scope.gridAllowedFields.sortRows(0,"str","asc");			
			
		};
		
		
	/* ------: Save object :------ */
		
		$scope.save = function(){
			
			$scope.saveObject($scope.send());
			
		};
		
		
	/* ------: Process form :------ */
		
		$scope.processForm = function (data){
			
			$scope.modal.callback = null;
			
			switch ($scope.modal.form) {
				
				case "caching":
					
					$scope.model.data.caching = data;
					$scope.modal.window.window.close();					
					delete $scope.modal;
					
				break;
				
				
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Business logic for this form does not exist",
						expire: 5000
					});						
					
				break;
				
			}
			
		};

		
	/* ------: Prepare request to save object :------ */
		
		function dhxMapElementToHeaderJsonToGrid(settings, headers){
			
			var data = {
				rows : []
			};
			
			var n = 1;
			
			$.each( settings, function( key, value ) {
				
				var x = [];
				
				$.each( headers, function( key2, value2 ) {
					
					if(settings[key][headers[key2].header] === undefined)
						x.push("");
					else
						x.push(settings[key][headers[key2].header]);
					
				});		
				
				
				data.rows.push({id:key,data:x});
				
				n++;
				
			});
			
			return JSON.stringify(data);
		}
		
		function dhxSerializeGridToJson(grid){
			
			// NOTICE
			
			// Do not use forEachRow
			// because the order of iteration will be the order in which rows were added to the grid
			
			// Use standard loop
			// The order of iteration is the order of rows in the grid
			
			var json = {};
			
			for (var i = 0; i < grid.getRowsNum(); i++){
				
				id = grid.getRowId(i);

				var sid = "_" + (grid.cells(id,0).getValue() + ":" + grid.cells(id,1).getValue()).encode();
				
				json[sid] = {};
				
				grid.forEachCell(id,function(cellObj,ind){
					
					json[sid][grid.getColLabel(ind)] = grid.cells(id,ind).getValue();
					
				});
				
			}
			
			// TODO : remove code below, old implementation
			
			/*
			grid.forEachRow(function(id){
				
				json[id] = {};
				
				grid.forEachCell(id,function(cellObj,ind){
					
					json[id][grid.getColLabel(ind)] = grid.cells(id,ind).getValue();
					
				});
				
			});
			
			*/
			
			return JSON.stringify(json);
			
		}
		
		
		$scope.remove = function(){
			
			$scope.gridAllowedFields.deleteSelectedRows();
			
		};
		
		$scope.send = function(){
			
			switch($scope.editor) {
				
				case "rscript":
					
					return	{
						id : $attrs.oid,
						type : $attrs.otype,
						target : {
							auditory : $scope.target.auditory, 
							id : $scope.target.id
						},
						data : {
							query : $scope.presentation.editor.getValue(),
							datasource : $scope.datasource,
							settings : "{}",
							caching : $scope.model.data.caching,
							editor : $scope.editor,
							skipsettings : $scope.toolbar.getItemState("skipsettings"),
							interpolators : $scope.model.interpolators,
							mapping : $scope.model.mapping,
							parameters : $scope.model.parameters
						}
					};				
					
				case "sqlquery":
					
					return	{
						id : $attrs.oid,
						type : $attrs.otype,
						target : {
							auditory : $scope.target.auditory, 
							id : $scope.target.id
						},
						data : {
							query : $scope.presentation.editor.getValue(),
							datasource : $scope.datasource,
							settings : dhxSerializeGridToJson($scope.gridAllowedFields),
							caching : $scope.model.data.caching,
							editor : $scope.editor,
							skipsettings : $scope.toolbar.getItemState("skipsettings"),
							interpolators : $scope.model.interpolators
						}
					};
					
					
				case "modeldesigner":
					
					return	{
						id : $attrs.oid,
						type : $attrs.otype,
						target : {
							auditory : $scope.target.auditory, 
							id : $scope.target.id
						},
						data : {
							model : JSON.stringify($scope.presentation.DesignerApp.NodeEntities.ExportToJSON()),
							datasource : $scope.datasource,
							settings : dhxSerializeGridToJson($scope.gridAllowedFields),
							caching : $scope.model.data.caching,
							editor : $scope.editor
						}
					};					
					
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Save for " + $scope.editor + " is not defined",
						expire: 5000
					});
					
				break;
				
			}
			
		};
		
		
	/* ------: Prepare request to get object :------ */
		
		$scope.request = function(){
			
			var request = 
			{
				id : $attrs.oid,
				type : $attrs.otype,
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				}
			};
			
			return request;
			
		};
		
		
	/* ------: Show loading :------ */
		
	        $scope.showLoading = function(){
				
	            $("#editor").css("position", "relative");
	            $("body").css("overflow","hidden");
	            var width = $("#editor").width() + 2, height = $("#editor").height();
	            $("<span id='delaySpan'><span id='icon' style='display:inline-block'></span>Loading... <span style='font-size:14px;color:brown;' id='countup'></span> <span style='font-size:14px;'>sec</span></span>")
	                    .css("left", width / 2 - 70)
	                    .css("top", height / 2 - 30)
	                    .css("position", "absolute")
	                    .css("zIndex", 9999999999)
	                    .css("color", "#4f4f4f")
	                    .css("background", "#ffffff")
	                    .css("border", "1px solid #a8a8a8")
	                    .css("border-radius", "3px")
	                    .css("-webkit-border-radius", "3px")
	                    .css("box-shadow", "0 0 10px rgba(0, 0, 0, 0.25")
	                    .css("font-family", "Arial, sans-serif")
	                    .css("font-size", "16px")
	                    .css("padding", "0.4em")
	                    .insertAfter("#editor");
	            $("<div id='delayDiv'></div>")
	                    .css("background", "#2D5972")
	                    .css("opacity", 0.3)
	                    .css("position", "absolute")
	                    .css("zIndex", 9999999999)
	                    .css("top", 0)
	                    .css("left", 0)
	                    .css("width", width)
	                    .css("height", height + 30)
	                    .insertAfter("#editor");
					
				// Start countup timer
				$('#countup').runner({
					autostart: true,
					countdown: false,
					startAt: 0,
					milliseconds: true,
					format: function(value) {
						return (value / 1000).toFixed(2);
					}				
				});
				
	        };
        
        
	/* ------: Hide loading :------ */
        
	        $scope.hideLoading = function() {
	            $("#delayDiv").remove();
	            $("#delaySpan").remove();
	        };
		
		
	/* ------: Execute data model :------ */
		
		$scope.execute = function(){
			
			switch($scope.driver){
				
				case "mysql":
				case "pgsql":
				case "clickhouse":
					
					switch($scope.editor) {
						
						case "sqlquery" :
							
							$scope.showLoading();
							
							var request = {
								
								id : [$scope.datasource.id],
								data : {
									connection : {
										id : $scope.datasource.id
									},
									request : {
										query : $scope.presentation.editor.getValue(),
										interpolators : $scope.model.interpolators
									}
								}								
								
							};
							
							Databuilder.testSQL(request)
							
							.success(function(response) {
								
								$scope.hideLoading();
								
								if(response.data.length === 0) {
									
									dhtmlx.message({
										text: "Data set is empty.",
										expire: 30000
									});
									
								} 
								
								$scope.result(
									dhxMapElementToHeaderJsonToGrid(
										response.data,
										$scope.configuration.fields
									)							
								);
								
							})
							
							.error(function(response) {
								
								$scope.hideLoading();
								$scope.gridResults.clearAll();
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 30000
								});
								
							});
							
						break;
						
						case "modeldesigner":
							
							var tables = $scope.presentation.DesignerApp.NodeEntities.ExportToJSON();
							var columns = {};
							
							$.each(tables, function (tablekey, value) {
								
								$.each(tables[tablekey].column, function (columnkey, value) {	
									
									var id = (tables[tablekey].name + ":" + tables[tablekey].column[columnkey].name).encode();
									
									columns[id] = {
										"Field name" : tables[tablekey].column[columnkey].name,
										"Field title" : tables[tablekey].column[columnkey].title,
										"Type" : tables[tablekey].column[columnkey].type,
										"Table name" : tables[tablekey].name,
										"Table title" : tables[tablekey].classname
									};
									
								});
								
							});
							
							//x = JSON.parse(JSON.stringify(x).split('"classname":').join('"Table":'));
							
							$scope.result(
								dhxMapElementToHeaderJsonToGrid(
									columns,
									$scope.configuration.fields
								)							
							);
							
							// TODO : can not group if table name containts non-alphanumeric chars
							
							//$scope.gridResults.groupBy(0);
							//$scope.gridResults.collapseAllGroups();
							
						break;
						
						
						default:
							
							dhtmlx.message({
								type: "error",
								text: "Execute is not supported for " + $scope.editor,
								expire: 30000
							});						
							
						break;
						
					}
					
				break;
				
				
				default:
					
					switch($scope.editor) {
						
						case "rscript":
							
							$scope.showLoading();
							console.log($scope.model.interpolators);
							var request = {
								
								id : [$scope.datasource.id],
								data : {
									connection : {
										id : $scope.datasource.id
									},
									request : {
										query : $scope.presentation.editor.getValue(),
										interpolators : $scope.model.interpolators,
										mapping : $scope.model.mapping,
										parameters : $scope.model.parameters
									}
								}								
								
							};
							
							try {
								
								Databuilder.testScript(request)
								
								.success(function(response) {
									
									$scope.hideLoading();
									
									if(response.data.length === 0) {
										
										dhtmlx.message({
											text: "Response is empty",
											expire: 10000
										});
										
									} 
									
									$scope.editorResults.setContent(response.data);
									$scope.layout.cells($scope.model.layout.cells.result).expand();									
										
								})
								
								.error(function(response) {
									
									$scope.hideLoading();
									$scope.editorResults.setContent("");
									
									dhtmlx.message({
										type: "error",
										text: response.error,
										expire: 30000
									});
									
								});
								
							} catch(error) {
								
								$scope.hideLoading();
								
								dhtmlx.message({
									type: "error",
									text: error,
									expire: 10000
								});								
									
							}
							
						break;
						
						default:
							
							dhtmlx.message({
								type: "error",
								text: "Execute for driver \"" + $scope.driver + "\" is not implemented!",
								expire: 30000
							});						
							
						break;
						
					}
					
				break;
				
			}
			
		};
		
		
	/* ------: Select auditory :------ */
		
		$scope.selectSource = function(object) {
			
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 400);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			
			var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
			
			$scope.windowModal.setSkin("material");
			
			
		/* ------: Switch between target auditory :------ */
			
			Source.index({})
				
			/* ------: On success :------ */
				
			.success(function(response) {
				
				$scope.roles.available = response.data;
				
				$scope.sourcesLayout = $scope.windowModal.window.attachLayout("2U");	
				$scope.sourcesLayout.setSkin("material");								
				$scope.sourcesLayout.cells("a").setText("Available data sources");
				$scope.sourcesLayout.cells("b").setText("Editor");
				
				$scope.toolbarSources = new dhtmlXToolbarObject({
					parent : "toolbarObj",
					icons_path: "/resources/shared/imgs/",
					json: [
						{
							id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
						},						
						{
							id: "select", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
						}
					]					
				});
				
				$scope.toolbarSources.setAlign("right");
				
				var editors = {
					rows : [
						{
							"id":"sqlquery",
							"data":["SQL Query"]
						},
						{
							"id":"modeldesigner",
							"data":["Model designer"]
						},
						{
							"id":"rscript",
							"data":["R Script"]
						}						
					]
				};				
				
				var available = {
					rows : []
				};
				
				$.each($scope.roles.available, function( index, value ) {
					available.rows.push(
						{
							id: $scope.roles.available[index].id,
							data : 
							[ 
								$scope.roles.available[index].name
							]
						}
					);
				});	
				
				$scope.gridAvailableSubjects = $scope.sourcesLayout.cells("a").attachGrid();
				$scope.gridAvailableSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.gridAvailableSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.gridAvailableSubjects.setHeader("Name");
				$scope.gridAvailableSubjects.setInitWidths("*,*");
				$scope.gridAvailableSubjects.setColAlign("left");
				$scope.gridAvailableSubjects.setColTypes("ro");
				$scope.gridAvailableSubjects.setColSorting("str");
				$scope.gridAvailableSubjects.enableMultiselect(true);
				$scope.gridAvailableSubjects.init();
				$scope.gridAvailableSubjects.setSkin("material");
				$scope.gridAvailableSubjects.parse(available, "json");			
				
				$scope.gridAvailableEditors = $scope.sourcesLayout.cells("b").attachGrid();
				$scope.gridAvailableEditors.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.gridAvailableEditors.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.gridAvailableEditors.setHeader("Name");
				$scope.gridAvailableEditors.setInitWidths("*,*");
				$scope.gridAvailableEditors.setColAlign("left");
				$scope.gridAvailableEditors.setColTypes("ro");
				$scope.gridAvailableEditors.setColSorting("str");
				$scope.gridAvailableEditors.init();
				$scope.gridAvailableEditors.setSkin("material");
				$scope.gridAvailableEditors.parse(editors, "json");	
				
				$scope.toolbarSources.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "select":
							
							$scope.datasource = { id : $scope.gridAvailableSubjects.getSelectedId().split(',') };
							
							if($scope.datasource.id.length === 1) {
								
								$scope.datasource.id = $scope.datasource.id[0];
								$scope.toolbar.setItemText("source", $scope.gridAvailableSubjects.cellById($scope.datasource.id,0).getValue());
								
							} else {
								
								$scope.toolbar.setItemText("source", "Multiple");
								
							}
							
							$scope.editor = $scope.gridAvailableEditors.getSelectedId();
							
							$scope.windowModal.unload();
							
							var object = {
								data : {
									datasource : {
										id : $scope.datasource.id
									},
									editor : $scope.editor
								}
							};
							
							$scope.loadUI(object);
							
						break;
						
						case "close":
							
							$scope.windowModal.unload();
							
						break;
						
					}
					
				});
				
				
				
				if(1==2) {
				
				Source.index({})
				.success(function(response) {
					
					$scope.roles.assigned = response.data;
					
					// Show data in window
					
					$scope.auditoryLayout = $scope.windowModal.window.attachLayout("2U");	
					$scope.auditoryLayout.setSkin("material");
					
					$scope.toolbarAvailableSubjects = $scope.auditoryLayout.cells("a").attachToolbar({
						icons_path: "/resources/apps/modelbuilder/imgs/",
						xml: "/resources/apps/modelbuilder/toolbar-available-subjects.xml"
					});		
					
					$scope.toolbarAssignedSubjects = $scope.auditoryLayout.cells("b").attachToolbar({
						icons_path: "/resources/apps/modelbuilder/imgs/",
						xml: "/resources/apps/modelbuilder/toolbar-assigned-subjects.xml"
					});										
					
					$scope.toolbarAvailableSubjects.attachEvent("onClick", function(id) {
						
						switch(id) {
							
							case "add-subject":
								
								$scope.target.id = $scope.gridAvailableSubjects.getSelectedId();
								$scope.toolbar.setItemText("subject", $scope.gridAvailableSubjects.cells($scope.target.id, 0).getValue());
								$scope.windowModal.unload();
								
							break;
							
						}
						
					});
					
					$scope.toolbarAssignedSubjects.attachEvent("onClick", function(id) {
						
						switch(id) {
							
							case "edit-subject":
								
								$scope.target.id = $scope.gridAssignedSubjects.getSelectedId();
								$scope.toolbar.setItemText("subject", $scope.gridAssignedSubjects.cells($scope.target.id, 0).getValue());
								$scope.windowModal.unload();
								$scope.getObject($scope.request());
								
							break;										
							
						}
						
					});								
					
					var available = {
						rows : []
					};
					
					var assigned = {
						rows : []
					};
					
					switch($scope.toolbar.getItemText("subject-type")) {
						
						case "Role":
							
							$scope.windowModal.window.setText("Roles");
							$scope.auditoryLayout.cells("a").setText("Available roles");
							$scope.auditoryLayout.cells("b").setText("Assigned roles");
							
							$.each($scope.roles.available, function( index, value ) {
								available.rows.push(
									{
										id: $scope.roles.available[index].id,
										data : 
										[ 
											$scope.roles.available[index].name
										]
									}
								);
							});	
							$.each($scope.roles.assigned, function( index, value ) {
								assigned.rows.push(
									{
										id: $scope.roles.assigned[index].id,
										data : 
										[ 
											$scope.roles.assigned[index].name
										]
									}
								);
							});											
						break;
							
						case "User":
							$scope.windowModal.window.setText("Users");
							$scope.auditoryLayout.cells("a").setText("Available users");
							$scope.auditoryLayout.cells("b").setText("Assigned users");
							//win.showHeader();
							$.each(response.data.available, function( index, value ) {
								available.rows.push(
									{
										id: response.data.available[index].id,
										data : 
										[ 
											response.data.available[index].name + " (" + response.data.available[index].email + ")"
										]
									}
								);
							});	
							$.each(response.data.assigned, function( index, value ) {
								assigned.rows.push(
									{
										id: response.data.assigned[index].id,
										data : 
										[ 
											response.data.assigned[index].name + " (" + response.data.assigned[index].email + ")"
										]
									}
								);
							});											
						break;
							
					}
					
					$scope.gridAvailableSubjects = $scope.auditoryLayout.cells("a").attachGrid();
					$scope.gridAvailableSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
					$scope.gridAvailableSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
					$scope.gridAvailableSubjects.setHeader("Name");
					$scope.gridAvailableSubjects.setInitWidths("*,*");
					$scope.gridAvailableSubjects.setColAlign("left");
					$scope.gridAvailableSubjects.setColTypes("ed");
					$scope.gridAvailableSubjects.setColSorting("str");
					$scope.gridAvailableSubjects.init();
					$scope.gridAvailableSubjects.setSkin("material");
					$scope.gridAvailableSubjects.parse(available, "json");
					
					$scope.gridAssignedSubjects = $scope.auditoryLayout.cells("b").attachGrid();
					$scope.gridAssignedSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
					$scope.gridAssignedSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
					$scope.gridAssignedSubjects.setHeader("Name");
					$scope.gridAssignedSubjects.setInitWidths("*,*");
					$scope.gridAssignedSubjects.setColAlign("left");
					$scope.gridAssignedSubjects.setColTypes("ed");
					$scope.gridAssignedSubjects.setColSorting("str");
					$scope.gridAssignedSubjects.init();
					$scope.gridAssignedSubjects.setSkin("material");
					$scope.gridAssignedSubjects.parse(assigned, "json");
				})	
				.error(function(response) {
					
					dhtmlx.message({
						type: "error",
						text: "Error to get roles for object",
						expire: 5000
					});					
					
				});						
		
				}
		
			})
				
			/* ------: On error :------ */
				
			.error(function(response, status) {
				
				dhtmlx.message({
					type: "error",
					text: response,
					expire: 5000
				});					
				
			});
			
		};
		
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		
		
	/* ------: Toolbar configuration :------ */
		
		// Define configuration
		
		$scope.conf.toolbar.private =  [
			
			{
				id: "source", type: "button", img: "datasource-32px.png", text: "Select source.."
			},	
			{
				id:   "sep_id",
				type: "separator"
			},		
			{
				id: "beautify", type: "button", img: "beautify-32px.png", text: "Beautify"
			},
			{
				id: "remove", type: "button", img: "remove-32px.png", text: "Remove selected"
			},			
			{
				id: "interpolators", type: "button", img: "interpolators-32px.ico", text: "Interpolators"
			},
			{
				id: "mapping", type: "button", img: "mapping-32px.png", text: "Mapping"
			},
			{
				id: "parameters", type: "button", img: "params-32px.png", text: "Parameters"
			},			
			{
				id: "execute", type: "button", img: "play-18px.png", text: "Execute"
			},
			{
				id: "caching", type: "button", img: "cache-32px.png", text: "Caching"
			},
			{
				id: "skipsettings", type: "buttonTwoState", img: "direct-query-32px.png", text: "Skip settings"
			},			
			{
				id: "save", type: "button", img: "save-18px.png", text: "Save changes"
			},
			{ 
				id: "close", type: "button", img: "close-18px.png", text: "Close"
			}
			
		];
		
		// Load configuration
		
		$scope.toolbar.loadStruct($scope.conf.toolbar.private);
		
		// Attach events
		
		$scope.toolbar.attachEvent("onClick", function(id) {
			
			switch(id) {
				
				case "interpolators":
						
					$scope.interpolators();
					
				break;					
				
				case "mapping":
						
					$scope.mapping();
					
				break;	
				
				case "parameters":
						
					$scope.parameters();
					
				break;					
				
				case "beautify":
						
					$scope.beautify();
					
				break;	
				
				
				case "source":
					
					$scope.selectSource();
					
				break;				
				
				
				case "remove":
					
					$scope.remove();
					
				break;
				
				
				case "caching":
					
					$scope.caching();
					
				break;
				
				
				case "execute":
					
					$scope.execute();
					
				break;
				
				
				case "close":
					
					$scope.close();
					
				break;
				
				
				case "save":
					
					$scope.save();
					
				break;
				
			}
			
		});
	
	/* ------: Mapping :------ */	
		
		$scope.mapping = function(){
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 400);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			
			var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
			
			$scope.windowModal.setSkin("material");
			
			$scope.mappingLayout = $scope.windowModal.window.attachLayout("1C");	
			$scope.mappingLayout.setSkin("material");								
			$scope.mappingLayout.cells("a").setText("Mapping");
				
			$scope.toolbarMapping = new dhtmlXToolbarObject({
				parent : "toolbarObj",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
					},						
					{
						id: "apply", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
					}
				]					
			});
			
			$scope.toolbarMapping.setAlign("right");			
			
			// grid
			
			$scope.gridMapping = $scope.mappingLayout.cells("a").attachGrid();
			$scope.gridMapping.setImagePath("/include/dhtmlx/codebase/imgs/");
			$scope.gridMapping.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridMapping.setHeader("Name,Value");
			$scope.gridMapping.setInitWidths("*,*");
			$scope.gridMapping.setColAlign("left,left");
			$scope.gridMapping.setColTypes("ro,ed");
			$scope.gridMapping.setColSorting("str,str");
			$scope.gridMapping.init();
			$scope.gridMapping.setSkin("material");
			
			var available = {
				rows : []
			};
			
			switch(typeof $scope.datasource.id) {
				
				case "string":
					
					try {
						
						value = $scope.model.mapping[$scope.datasource.list.id];
						
					} catch(error) {
						
						value = "";
						
					}				
						
					available.rows.push(
						{
							id : $scope.datasource.list.id,
							data : 
							[ 
								$scope.datasource.list.name, value
							]
						}
					);					
					
				break;
				
				case "object":
				
					$.each($scope.datasource.list, function( index, source ) {
						
						try {
								
							value = $scope.model.mapping[source.id];
								
						} catch(error) {
							
							value = "";
							
						}				
							
						available.rows.push(
							{
								id : source.id,
								data : 
								[ 
									source.name, value
								]
							}
						);
							
					});				
					
				break;
				
			}
			
			$scope.gridMapping.parse(available, "json");				
			
			$scope.toolbarMapping.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
					case "apply":
						
						$scope.model.mapping = {};
						
						$scope.gridMapping.forEachRow(function(id){
							
							$scope.gridMapping.forEachCell(id,function(cellObj,ind){
								
								$scope.model.mapping[id] = $scope.gridMapping.cells(id,ind).getValue();
								
							});
							
						});
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});			
			
		};
		

	/* ------: Parameters :------ */	
		
		$scope.parameters = function(){
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 400);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			
			var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
			
			$scope.windowModal.setSkin("material");
			
			$scope.mappingLayout = $scope.windowModal.window.attachLayout("1C");	
			$scope.mappingLayout.setSkin("material");								
			$scope.mappingLayout.cells("a").setText("Mapping");
				
			$scope.toolbarMapping = new dhtmlXToolbarObject({
				parent : "toolbarObj",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
					},						
					{
						id: "apply", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
					}
				]					
			});
			
			$scope.toolbarMapping.setAlign("right");			
			
		/* ------: Create gird :------ */
			
			$scope.gridMapping = $scope.mappingLayout.cells("a").attachGrid();
			$scope.gridMapping.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
			$scope.gridMapping.setIconsPath("/vendor/proprietary/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridMapping.setHeader("Source,Parameter,Value");
			$scope.gridMapping.setInitWidths("*,*,*");
			$scope.gridMapping.setColAlign("left,left,left");
			$scope.gridMapping.setColTypes("ro,ro,ed");
			$scope.gridMapping.setColSorting("sr,str,str");
			$scope.gridMapping.init();
			$scope.gridMapping.setSkin("material");

		/* ------: Get interpolators :------ */
			
			var available = {
				rows : []
			};
			
			Interpolators.get({
				
				id : $scope.datasource.id
				
			})
			
			.success(function(response) {
				
				var datasources = [];
				
				switch(typeof $scope.datasource.id) {
					
					case "string":
						
						datasources.push($scope.datasource.list);
						
					break;
					
					case "object":
						
						datasources = $scope.datasource.list;
						
					break;
					
				}
				
				$.each(datasources, function( index, datasource ) {
					
					$.each(response[datasource.id], function( index2, interpolator ) {	
						
						var id = datasource.id + "." + interpolator
						
						var value = typeof $scope.model.parameters[id] === undefined ? '' : $scope.model.parameters[id];
						
						available.rows.push({
							
							id : id,
							data : 
							[ 
								datasource.name, interpolator, value
							]
							
						});
						
					});
					
				});
				
				$scope.gridMapping.parse(available, "json");				
				
				$scope.gridMapping.groupBy(0);
				
			})
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: response,
					expire: 30000
				});
				
			});				
			
			
			$scope.toolbarMapping.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
					case "apply":
						
						$scope.model.parameters = {};
						
						$scope.gridMapping.forEachRow(function(id){
							
							$scope.gridMapping.forEachCell(id,function(cellObj,ind){
								
								$scope.model.parameters[id] = $scope.gridMapping.cells(id,ind).getValue();
								
							});
							
						});
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});
			
			/*
			switch(typeof $scope.datasource.id) {
				
				// Object.get({})
				
				// .success
				
				// Params.get({id:[array of objects]})
				// params/get/12,13,15
				// params/pipe/15
				// interpolators/get/12,13,15
				// interpolators/pipe/15
				
				case "string":
					
					try {
						
						value = $scope.model.mapping[$scope.datasource.list.id];
						
					} catch(error) {
						
						value = "";
						
					}				
						
					available.rows.push(
						{
							id : $scope.datasource.list.id,
							data : 
							[ 
								$scope.datasource.list.name, value
							]
						}
					);					
					
				break;
				
				case "object":
				
					$.each($scope.datasource.list, function( index, source ) {
						
						try {
								
							value = $scope.model.mapping[source.id];
								
						} catch(error) {
							
							value = "";
							
						}				
							
						available.rows.push(
							{
								id : source.id,
								data : 
								[ 
									source.name, value
								]
							}
						);
							
					});				
					
				break;
				
			}
			
			*/
			
		};
		
		
		
	/* ------: Interpolators :------ */	
		
		$scope.interpolators = function(){
			
			// TODO : create modal with grid and two columns
			// parse query
			// first column : interpolator name
			// second query : get from model interpolator valure
			// when apply then change model
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 400);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			
			var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
			
			$scope.windowModal.setSkin("material");
			
			$scope.sourcesLayout = $scope.windowModal.window.attachLayout("1C");	
			$scope.sourcesLayout.setSkin("material");								
			$scope.sourcesLayout.cells("a").setText("Interpolators");
				
			$scope.toolbarSources = new dhtmlXToolbarObject({
				parent : "toolbarObj",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
					},						
					{
						id: "apply", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
					}
				]					
			});
			
			$scope.toolbarSources.setAlign("right");			
			
			// grid
			
			$scope.gridInterpolators = $scope.sourcesLayout.cells("a").attachGrid();
			$scope.gridInterpolators.setImagePath("/include/dhtmlx/codebase/imgs/");
			$scope.gridInterpolators.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridInterpolators.setHeader("Name,Value");
			$scope.gridInterpolators.setInitWidths("*,*");
			$scope.gridInterpolators.setColAlign("left,left");
			$scope.gridInterpolators.setColTypes("ro,ed");
			$scope.gridInterpolators.setColSorting("str,str");
			$scope.gridInterpolators.init();
			$scope.gridInterpolators.setSkin("material");
			
			
			// get interpolators
			

				
			var available = {
				rows : []
			};
			
			var pattern = /\{\{(.*?)\}\}/g;
			var match;
			var value;
			
			while ((match = pattern.exec($scope.presentation.editor.getValue())) !== null)
			{
				
				try {
					
					value = $scope.model.interpolators[match[1]];
					
				} catch(error) {
					
					value = "";
					
				}
				
				available.rows.push(
					{
						id : match[1],
						data : 
						[ 
							match[1], value
						]
					}
				);	
				
			}		
			
			$scope.gridInterpolators.parse(available, "json");				
			
			// events
			
			$scope.toolbarSources.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
					case "apply":
						
						$scope.model.interpolators = {};
						
						$scope.gridInterpolators.forEachRow(function(id){
							
							$scope.gridInterpolators.forEachCell(id,function(cellObj,ind){
								
								$scope.model.interpolators[id] = $scope.gridInterpolators.cells(id,ind).getValue();
								
							});
							
						});
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});			
			
		};
		
		
	/* ------: Caching :------ */
		
		$scope.caching = function(){
			
			$scope.modal = $scope.createModal({
				
				title : "Caching",
				form : "caching",
				dim : {width:500, height:400}
				
			});
			
		};
		
		
	/* ------: Aspect mapping :------ */
		
		$scope.aspectMapping = function(aspect){
			
			Object.get({
				
				id : $scope.gridData.getSelectedRowId(),
				type : "model"
				
			})
			
			.success(function(response) {
				
				var xx = JSON.parse(response.data.data.settings);
				var mapValues = {options:[]};
				$scope.mapValuesTypes = {};
				
				$.each(xx, function(index) {
					
					if(aspect.mapkey !== undefined && aspect.mapkey === index) { 
						aspect.mapkeyIndex = xx[index]["Field name"]; //index; 
						aspect.mapkeyText = xx[index]["Field title"] === "" ? xx[index]["Field name"] : xx[index]["Field title"];
					}
					
					if(aspect.mapvalue !== undefined && aspect.mapvalue === index) { 
						aspect.mapvalueIndex = xx[index]["Field name"]; //index; 
						aspect.mapvalueText = xx[index]["Field title"] === "" ? xx[index]["Field name"] : xx[index]["Field title"];
					}
					
					mapValues.options.push({
						value: xx[index]["Field name"], //index, 
						text: xx[index]["Field title"] === "" ? xx[index]["Field name"] : xx[index]["Field title"]
					});
					
					//$scope.mapValuesTypes[index] = xx[index].Type;
					$scope.mapValuesTypes[xx[index]["Field name"]] = xx[index].Type;
					
				});													
				
				var cmbMapKeys = $scope.gridAspectOptions.cells("mapkey",1).getCellCombo();
				cmbMapKeys.enableFilteringMode(true);
				cmbMapKeys.load(mapValues);
				
				var cmbMapValue = $scope.gridAspectOptions.cells("mapvalue",1).getCellCombo();
				cmbMapValue.enableFilteringMode(true);
				cmbMapValue.load(mapValues);														
				
			/* ------: Set configuration:------ */
			
				//loop through loading values
				
				$scope.gridAspectOptions.cells("mapkey",1).setValue(aspect.mapkeyText);
				$scope.gridAspectOptions.cells("mapkey",1).getCellCombo().setComboValue(aspect.mapkeyIndex);
				
				$scope.gridAspectOptions.cells("mapvalue",1).setValue(aspect.mapvalueText);
				$scope.gridAspectOptions.cells("mapvalue",1).getCellCombo().setComboValue(aspect.mapvalueIndex);
				
			});			
			
		};
		
		
	/* ------: Load user interface :------ */
		
		$scope.loadUI = function(object, reload){
			
		/* ------: Get mount name :------ */
			
			Mount.get({id:object.data.datasource.id})
			
			.success(function(response) {
				
				switch(typeof response.name){
					
					case "string":
						
						$scope.toolbar.setItemText("source", response.name);
						
					break;
					
					default:
						
						$scope.toolbar.setItemText("source", "Multiple");
						
					break;
					
				}
				
				$scope.datasource = { 
					id : object.data.datasource.id,
					list : response
				};
				
				$scope.editor = object.data.editor;
				
				try {
					
					$scope.model.interpolators = object.data.interpolators;
					
				} catch(error) {}
				
				try {
					
					$scope.model.mapping = object.data.mapping;
					
				} catch(error) {}
				
				try {
					
					$scope.model.parameters = object.data.parameters;
					
				} catch(error) {}				
				
			/* ------: Get datasource object :------ */	
				
				var request = 
				{
					id : object.data.datasource.id,
					type : "source",
					target : {
						auditory : "All", 
						id : null
					}
				};					
				
				Object.get(request)
				
				.success(function(response) {	
					
					response = response.data;
					
					var source = null;
					
					switch($scope.editor) {
						
						case "modeldesigner":
						case "sqlquery":
							
							source = JSON.parse(response.data.datasource);	
							
						/* ------: Get schema :------ */
							
							Databuilder.schema({id : [object.data.datasource.id]})
							
							.success(function(objects) {	
								
							/* ------: Initialize schema variables :------ */
								
								var tables = [];
								var schemas = [];
									
								var editormode = source.driver;
								
								$.getJSON("/conf/drivers/" + source.driver + ".json?" + new Date()).done(function(driver) {
									
								/* ------: Set schema variables :------ */
									
									switch(source.driver) {
										
										case "mysql":
										case "pgsql":
											
											$.each(objects, function(index) {
												
												tables.push(objects[index].tablename);
												schemas.push(objects[index].schemaname);
												
											});								
											
										break;
										
										
										case "clickhouse":
											
											editormode = "sql";
											
											$.each(objects, function(index) {
												
												tables.push(objects[index].name);
												
											});																
											
										break;
										
									}
									
								/* ------: User interface :------ */
									
									switch(source.driver) {
										
									/* ------: External :------ */
										
										case "mysql":
										case "pgsql":
										case "clickhouse":
											
											if($scope.driver !== source.driver) {
												
											/* ------: Set configuration :------ */
												
												$scope.configuration = {
													
													editor : {
														
														mode : editormode,
														theme : "ambiance"
														
													},
													
													autocomplete : {
														
														// get dynamically all schemas from selected connection
														
														"schemas" : {
															
															wordList : $.unique(schemas),
															meta : "SCHEMA"
															
														},
														
														// get dynamically all tables from selected connection
														
														"tables" : {
															
															wordList : tables,
															meta : "TABLE"
															
														},
														
														// get dynamically all columns from selected connection
														
														"columns" : {
															
															wordList : [],
															meta : "COLUMN"
															
														},	
														
														// get dynamically all views from selected connection
														
														"views" : {
															
															wordList : [],
															meta : "VIEW"
															
														},											
														
														// get dynamically from json file
														
														"keywords" : {
															
															wordList : driver.keywords,
															meta : "KEYWORD"
															
														},	
														
														"functions" : {
															
															wordList : driver.functions,
															meta : "FUNCTION"
															
														},	
														
														"categories" : {
															
															wordList : [
																"? Tables",
																"? Columns",
																"? Functions",
																"? Keywords",
															],
															meta : "CATEGORY"
															
														},													
														
													},
													
													fields : 
													[
														{
															header: "Table name",
															align: "left",
															type: "ro",
															sorting: "str"
														},									
														{
															header: "Field name",
															align: "left",
															type: "ed", // TODO : tree
															sorting: "str"
														},
														{
															header: "Sample value",
															align: "left",
															type: "ro",
															sorting: "str"
														},
														{
															header: "Table title",
															align: "left",
															type: "ed",
															sorting: "str"
														},									
														{
															header: "Field title",
															align: "left",
															type: "ed",
															sorting: "str"
														},
														{
															header: "Type",
															align: "left",
															type: "combo",
															sorting: "str"
														},
														{
															header: "Aspect",
															align: "left",
															type: "ch",
															sorting: "str"
														},	
														{
															header: "aspectconfig",
															align: "left",
															type: "ed",
															sorting: "str"
														},	
														{
															header: "Aggregate",
															align: "left",
															type: "ch",
															sorting: "str"
														},													
														{
															header: "Value",
															align: "left",
															type: "ch",
															sorting: "str"
														},	
														{
															header: "Explorer",
															align: "left",
															type: "ch",
															sorting: "str"
														},										
														{
															header: "Filtering",
															align: "left",
															type: "ch",
															sorting: "str"
														},
														{
															header: "Ordering",
															align: "left",
															type: "ch",
															sorting: "str"
														},
														{
															header: "Aggregations",
															align: "left",
															type: "ch",
															sorting: "str"
														}
													]									
													
												};
												
												Databuilder.fields({
													
													id : [object.data.datasource.id],
													table : ':all'
													
												})
												
												.success(function(response) {
													
													fields = $.map( response, function( val, i ) {
														return response[i].column_name;
													});	
													
													$scope.configuration.autocomplete.columns.wordList = fields;
													
												})
												
												.error(function(response) {
													
													dhtmlx.message({
														type: "error",
														text: response.error,
														expire: 30000
													});
													
												});										
												
											/* ------: Set grid configuration :------ */
												
												var config = { headers: "", headers2: "", aligns: "", types: "", sortings: "" };
												
												$.each($scope.configuration.fields, function(index) {
													
													if(index === 0 || index === 1 || index === 2 || index === 3 || index === 4) 
														config.headers2 = config.headers2 + $scope.configuration.fields[index].header + ",";
													else 
														config.headers2 = config.headers2 + "" + ",";
													
													config.headers = config.headers + $scope.configuration.fields[index].header + ",";
													config.aligns = config.aligns + $scope.configuration.fields[index].align + ",";
													config.types = config.types + $scope.configuration.fields[index].type + ",";
													config.sortings = config.sortings + $scope.configuration.fields[index].sorting + ",";
													
												});
												
												$.each(config, function(index) {
													config[index] = config[index].substring(0, config[index].length - 1);
												});	
												
												
											/* ------: Layout :------ */
												
												switch(object.data.editor) {
												
												/* ------: sqlquery :------ */
													
													case "sqlquery":
														
													/* ------: Create layout :------ */	
														
														$scope.model.layout.cells.editor = "a";									
														$scope.model.layout.cells.availablefields = "b";
														$scope.model.layout.cells.allowedfields = "c";
														
														$scope.layout = $scope.window.attachLayout("3U");
														$scope.layout.setSkin("material");
														$scope.layout.cells($scope.model.layout.cells.editor).setText("Query");
														$scope.layout.cells($scope.model.layout.cells.editor).attachURL("/app/ace/");
														
														$scope.layout.cells($scope.model.layout.cells.availablefields).collapse();								
														$scope.layout.cells($scope.model.layout.cells.allowedfields).collapse();
														
													/* ------: Set skip settings state layout :------ */
														
														try {
														
															$scope.toolbar.setItemState("skipsettings", JSON.parse(object.data.skipsettings));
														
														} catch (error) {
															
															
														}
														
													/* ------: Load data in tab :------ */
														
														$scope.layout.attachEvent("onContentLoaded", function(id){
															
															switch(id) {
																
																case "a":
																	
																	/* ------: Link presentation :------ */
																	
																	$scope.presentation = $scope.layout.cells($scope.model.layout.cells.editor).getFrame().contentWindow;
																	
																	/* ------: Set driver and load data :------ */
																	
																	$scope.driver = source.driver;
																	$scope.loadData(object);
																	$scope.loadSettings(object);														
																	
																break;
																
															}
															
														});	
														
													break;
													
												/* ------: modeldesigner :------ */
													
													case "modeldesigner":
														
													/* ------: Create layout :------ */
														
														$scope.model.layout.cells.schema = "a";
														$scope.model.layout.cells.editor = "b";
														$scope.model.layout.cells.availablefields = "c";
														$scope.model.layout.cells.allowedfields = "d";
														
														$scope.layout = $scope.window.attachLayout("4U");
														$scope.layout.setSkin("material");									
														$scope.layout.cells($scope.model.layout.cells.schema).setText("Schema");
														$scope.layout.cells($scope.model.layout.cells.editor).setText("Data model");
														
														
													/* ------: Tabbar for editor :------ */
														
														$scope.tabbar = $scope.layout.cells($scope.model.layout.cells.editor).attachTabbar({
															
															tabs: [
																
																{
																	id : "tab-gui", 
																	text : "Graphical",
																	width : null,
																	index : null,
																	active : true,
																	enabled : true,
																	close : false
																},
																
																{
																	id : "tab-code",
																	text : "Code",
																	width : null,
																	index : null,
																	active : false,
																	enabled : true,
																	close : false
																}
																
															]	
															
														});		
														
														
													/* ------: Load data in tab :------ */
														
														$scope.tabbar.attachEvent("onContentLoaded", function(id){
															
															switch(id) {
																
																case "tab-gui":
																	
																	/* ------: Link presentation :------ */
																	
																	$scope.presentation = $scope.tabbar.cells("tab-gui").getFrame().contentWindow;
																	
																	/* ------: Set driver and load data :------ */
																	
																	$scope.driver = source.driver;
																	$scope.loadData(object);
																	$scope.loadSettings(object);														
																	
																break;
																
																case "tab-code":
																	
																	/* ------: Link presentation :------ */
																	
																	$scope.presentation2 = $scope.tabbar.cells("tab-code").getFrame().contentWindow;
																	
																break;														
																
															}
															
														});	
														
														
														$scope.tabbar.tabs("tab-gui").attachURL("/apps/modeldesigner/index.html#");
														$scope.tabbar.tabs("tab-code").attachURL("/apps/jsoneditor/examples/08_custom_ace.html");
														
														
													/* ------: Tab events :------ */
														
														$scope.tabbar.attachEvent("onSelect", function(id, lastId){
															
															switch(id) {
																
																case "tab-gui":
																	
																	$scope.presentation.DesignerApp.NodeEntities.ClearNodeCanvas($scope.presentation.DesignerApp.NodeEntities.CurrentNodeCanvas);
																	$scope.presentation.DesignerApp.NodeEntities.AddNodeCanvas($scope.presentation2.x2());
																	
																	
																break;														
																
																case "tab-code":
																	
																	$scope.presentation2.x($scope.presentation.DesignerApp.NodeEntities.ExportToJSON());
																	
																break;
																
															}
															
															return true;
															
														});	
														
														
														
														
														
														
														
														
														
														
														
														
														
														
														
														
														//$scope.layout.cells($scope.model.layout.cells.editor).attachURL("/apps/modeldesigner/index.html#"); // change to laravel view
														
														
														$scope.layout.cells($scope.model.layout.cells.availablefields).collapse();								
														$scope.layout.cells($scope.model.layout.cells.allowedfields).collapse();
														
														$scope.gridSchema = $scope.layout.cells($scope.model.layout.cells.schema).attachGrid();
														
														$scope.gridSchema.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
														$scope.gridSchema.setHeader(
														    "<div style='text-align:left;font-size:13px;color:#494949;'>Total tables : <span id='tablecount'></span></div>,#cspan,#cspan",
														    null,
														    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
														);
														$scope.gridSchema.attachHeader(
														    "#text_filter,#cspan,#cspan",
														    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
														);										
														$scope.gridSchema.setInitWidths("*,50,50");
														$scope.gridSchema.setColAlign("left,left,left");
														$scope.gridSchema.setColTypes("tree,ro,img");
														$scope.gridSchema.setColSorting("str,str,str");
														//$scope.gridSchema.setNoHeader(true);
														$scope.gridSchema.enableRowsHover(true,'grid_hover');
														$scope.gridSchema.enableTreeCellEdit(false);
														$scope.gridSchema.enableAutoWidth(true);
														$scope.gridSchema.init();
														
														
														/*
														$scope.aaa =$scope.layout.cells($scope.model.layout.cells.editor).attachToolbar({
															icons_path: "/resources/shared/imgs/",
															json: [
																{
																	id: "clear", type: "button", mode: "select", text: "Clear all", img: "clear-32px.png"
																},												
																{
																	id: "code", type: "button", mode: "select", text: "Switch to code", img: "code-32px.png"
																}
															]
														});	
														*/
														
														
														Databuilder.schema({
															id : [$scope.datasource.id]
														})
														
														.success(function(response) {
															
															$("#tablecount").html(response.length);
															
															var schema = {};
															
															switch(source.driver){
																
																case "pgsql":
																case "mysql":
																	
																	schema.rows = $.map( response, function( val, i ) {
																		return {
																			id : response[i].tablename,
																			data:
																			[
																				{"value" : " " + response[i].tablename, "image":"folder.gif"},
																				"",
																				"https://www.hscripts.com/freeimages/icons/mini-icons/right/right17.gif"
																			],										
																		};
																	});
																	
																break;
																
																
																case "clickhouse":
																	
																	schema.rows = $.map( response, function( val, i ) {
																		return {
																			id : response[i].name,
																			data:
																			[
																				{"value" : " " + response[i].name, "image":"folder.gif"},
																				"",
																				"https://www.hscripts.com/freeimages/icons/mini-icons/right/right17.gif"
																			],										
																		};
																	});
																	
																break;														
																
															}
															
															$scope.gridSchema.parse(schema,"json");
															
															$scope.gridSchema.forEachRow(function(id){
																$scope.gridSchema.setCellTextStyle(id,2,"cursor:pointer;"); 
															});	
															
														})	
														
														.error(function(response) {
															
															console.log(response);
															
														});
														
														
														$scope.gridSchema.setColWidth(0, "100%");
														
														$scope.gridSchema.attachEvent("onRowSelect",function(tablename,ind){
															
															if(ind === 2){
																
																// get columns and pass to addTable
																// Databuilder.fields
																// id: [36]
																// table:transactions
																
																Databuilder.fields({
																	
																	id : [$scope.datasource.id],
																	table : tablename
																	
																})
																
																.success(function(response) {
																	
																	var modal = new dhtmlXWindows();
																	modal.window = modal.createWindow($scope.winid, 0, 0, 800, 500);
																	modal.window.centerOnScreen();
																	modal.window.hideHeader();	
																	modal.window.setModal(true);	
																	modal.setSkin("material");
																	var layout = modal.window.attachLayout("2U");					
																	layout.cells("a").setText("Select fields");
																	layout.cells("b").setText("Table properties");
																	
																	$scope.gridTableFields = layout.cells("a").attachGrid();
																	$scope.gridTableFields.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
																	
																	$scope.gridTableFields.setHeader(
																	    "Field name, Data type",
																	    null,
																	    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
																	);	
																	
																	$scope.gridTableFields.attachHeader(
																	    "#text_filter,#text_filter",
																	    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
																	);		
																	
																	$scope.gridTableFields.setColAlign("left,left");
																	$scope.gridTableFields.setColTypes("ro,ro");
																	$scope.gridTableFields.setColSorting("str,str");
																	$scope.gridTableFields.setStyle("color:#545454; font-weight:bold;");	
																	$scope.gridTableFields.enableDragAndDrop(true);
																	$scope.gridTableFields.enableAutoHeight(false);
																	$scope.gridTableFields.enableMultiselect(true);
																	$scope.gridTableFields.init();
																	$scope.gridTableFields.setSkin("material");
																	
																	$scope.gridTableFields.setStyle(
																		"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
																	);		
																	
																	var fields = {};
																	
																	switch(source.driver){
																		
																		case "pgsql":
																		case "mysql":
																		case "clickhouse":
																			
																			fields.rows = $.map( response, function( val, i ) {
																				return {
																					id : response[i].column_name,
																					data:
																					[
																						response[i].column_name,
																						response[i].data_type
																					],										
																				};
																			});																	
																			
																		break;
																		
																	}
																	
																	$scope.gridTableFields.parse(fields, "json");
																	
																	$scope.gridTableProps = layout.cells("b").attachGrid();
																	$scope.gridTableProps.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
																	
																	$scope.gridTableProps.setHeader(
																	    "Property, Value",
																	    null,
																	    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
																	);	
																	
																	$scope.gridTableProps.setColAlign("left,left");
																	$scope.gridTableProps.setColTypes("ro,ed");
																	$scope.gridTableProps.setColSorting("str,str");
																	$scope.gridTableProps.setStyle("color:#545454; font-weight:bold;");	
																	$scope.gridTableProps.enableAutoHeight(false);
																	$scope.gridTableProps.enableEditEvents(true,false,false);
																	$scope.gridTableProps.init();
																	$scope.gridTableProps.setSkin("material");
																	
																	$scope.gridTableProps.setStyle(
																		"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
																	);	
																	
																	
																	data = {
																		
																		rows:
																		[
																			
																			{ 
																				id:"tablename",
																				data:
																				[
																					"Table name",
																					tablename
																				] 
																			},
																			
																			{
																				id:"tabletitle",
																				data:
																				[
																					"Table title",
																					tablename
																				] 
																			},
																			
																			{
																				id:"tablecolor",
																				data:
																				[
																					"Color",
																					"Red"
																				] 
																			}			
																		]
																		
																	};
																	
																	$scope.gridTableProps.parse(data, "json");
																	
																	var dhxSBar2 = modal.window.attachStatusBar({text: "", height: 34});
																	dhxSBar2.setText("<div id='toolbarObj2' style='width:100%;text-align:center;'></div>");
																	
																	$scope.toolbarAddTable = new dhtmlXToolbarObject({
																		parent : "toolbarObj2",
																		icons_path: "/resources/shared/imgs/",
																		json: [
																			{
																				id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
																			},						
																			{
																				id: "addall", type: "button", mode: "apply", text: "Add all", img: "add-all-32px.png"
																			},					
																			{
																				id: "apply", type: "button", mode: "apply", text: "Add selected", img: "select-32px.png"
																			}
																		]					
																	});
																	
																	$scope.toolbarAddTable.setAlign("right");	
																	
																	$scope.toolbarAddTable.attachEvent("onClick", function(id) {
																		
																		var selectedFields;
																		
																		switch(id) {
																			
																			case "apply":
																				
																				selectedFields = [];
																				
																				$.each($scope.gridTableFields.getSelectedRowId().split(","), function(index, id) {
																					
																					selectedFields.push(id);
																					
																				});
																				
																				$scope.addNewObjectToModel(selectedFields, source, tablename);
																				
																				modal.unload();
																				
																			break;
																			
																			case "addall":
																				
																				selectedFields = [];
																				
																				for (var i = 0; i < $scope.gridTableFields.getRowsNum(); i++){
																					
																					selectedFields.push($scope.gridTableFields.getRowId(i));
																					
																				}
																				
																				$scope.addNewObjectToModel(selectedFields, source, tablename);
																				
																				modal.unload();
																				
																			break;
																			
																			case "close":
																				
																				modal.unload();
																				
																			break;
																			
																		}
																		
																	});
																			
																})
																
																.error(function(response) {
																	
																	dhtmlx.message({
																		type: "error",
																		text: response.error,
																		expire: 30000
																	});
																	
																})
																
															}
															
														});							
														
														//$scope.gridSchema.setColWidth(1, "33%");
														//$scope.gridSchema.setColWidth(2, "33%");
														
														/*
														
														$scope.gridSchema.groupBy(1);
														$scope.gridSchema.collapseAllGroups();
														
														$scope.gridSchema.attachEvent("onRowDblClicked", function(name){
															
															alert(1);
															
														});	
														*/
														
													break;
													
													default:
														
														$scope.layout = $scope.window.attachLayout("3U");
														$scope.layout.setSkin("material");									
														$scope.layout.cells("a").setText("Query");
														$scope.layout.cells("a").attachURL("/app/ace/");																
														object.data.editor = "sqlquery";
														$scope.editor = object.data.editor;
														
													break;
													
												}
												
												// For all
												
												$scope.layout.cells($scope.model.layout.cells.availablefields).setText("Available fields");
												$scope.layout.cells($scope.model.layout.cells.allowedfields).setText("Allowed fields and settings");
												
												$scope.layout.attachEvent("onDblClick", function(name){
													
													if(name !== $scope.model.layout.cells.editor) {
														
														$scope.layout.cells(name).undock();
														$scope.layout.dhxWins.window(name).maximize();
														$scope.layout.dhxWins.window(name).button("minmax").hide();
														$scope.layout.dhxWins.window(name).button("park").hide();
														
													} 
													
												});		
												
												
											/* ------: Available fields :------ */
												
												// Grid
												
												$scope.gridResults = $scope.layout.cells($scope.model.layout.cells.availablefields).attachGrid();
												$scope.gridResults.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
												$scope.gridResults.setHeader(config.headers2);
												$scope.gridResults.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter");
												$scope.gridResults.setColAlign(config.aligns);
												$scope.gridResults.setColTypes(config.types);
												$scope.gridResults.setColSorting(config.sortings);
												$scope.gridResults.setStyle("color:#545454; font-weight:bold;");	
												$scope.gridResults.enableDragAndDrop(true);
												$scope.gridResults.setDragBehavior("sibling", true);
												$scope.gridResults.enableAutoHeight(false);
												$scope.gridResults.enableMultiselect(true);
												$scope.gridResults.enableTreeCellEdit(false);
												$scope.gridResults.init();
												$scope.gridResults.setSkin("material");
												$scope.gridResults.setStyle(
													"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
												);
												
												// Hide all fields
												
												$.each($scope.configuration.fields, function(index) {
													$scope.gridResults.setColWidth(index, "0");
												});
												
												// Show only fields depending on editor
												
												switch(object.data.editor) {
													
													case "sqlquery":
														
														$scope.gridResults.setColWidth(1, "50%");
														$scope.gridResults.setColWidth(2, "50%");
														
													break;
													
													
													case "modeldesigner":
														
														$scope.gridResults.setColWidth(0, "20%");
														$scope.gridResults.setColWidth(1, "20%");
														$scope.gridResults.setColWidth(2, "20%");
														$scope.gridResults.setColWidth(3, "20%");
														$scope.gridResults.setColWidth(4, "20%");
														
													break;
													
												}
												
												
											/* ------: Allowed fields and settings :------ */
												
												$scope.gridAllowedFields = $scope.layout.cells($scope.model.layout.cells.allowedfields).attachGrid();
												$scope.gridAllowedFields.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
												$scope.gridAllowedFields.setHeader(config.headers);
												$scope.gridAllowedFields.attachHeader("#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#text_filter,#master_checkbox,#master_checkbox,#master_checkbox,#master_checkbox,#master_checkbox,#master_checkbox,#master_checkbox,#master_checkbox");
												$scope.gridAllowedFields.setColAlign(config.aligns);
												$scope.gridAllowedFields.setColTypes(config.types);
												$scope.gridAllowedFields.setColSorting(config.sortings);
												$scope.gridAllowedFields.setStyle("color:#545454; font-weight:bold;");	
												$scope.gridAllowedFields.enableDragAndDrop(true, false);
												$scope.gridAllowedFields.setDragBehavior("sibling", true);
												$scope.gridAllowedFields.enableMultiselect(true);
												$scope.gridAllowedFields.enableTreeCellEdit(false);
												$scope.gridAllowedFields.enableColumnAutoSize(true,600,200);
												$scope.gridAllowedFields.init();
												$scope.gridAllowedFields.setSkin("material");
												$scope.gridAllowedFields.setStyle(
													"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
												);
												
												
												// Hide fields
												
												$.each($scope.configuration.fields, function(index) {
													
													if($scope.gridAllowedFields.getColLabel(index) === 'aspectconfig') {
														
														$scope.gridAllowedFields.setColWidth(index, "0");
														
													} else {
														
														$scope.gridAllowedFields.setColWidth(index, "*");
														
													}
													
												});
												
												
											/* ------: Checkbox events :------ */	
												
												$scope.gridAllowedFields.attachEvent("onCheck", function(rId,cInd,state){
													
													if(cInd === 6 && state === true){
														
													/* ------: Modal window to configure aspect :------ */
														
														$scope.windowModal = new dhtmlXWindows();
														$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 1400, 500);	
														$scope.windowModal.window.centerOnScreen();
														$scope.windowModal.window.hideHeader();	
														$scope.windowModal.window.setModal(true);	
														
														var dhxSBar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
														dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
														
														$scope.windowModal.setSkin("material");
														
														$scope.layoutAspects = $scope.windowModal.window.attachLayout("4W");	
														$scope.layoutAspects.setSkin("material");								
														$scope.layoutAspects.cells("a").setText("Aspect");
														$scope.layoutAspects.cells("b").setText("Data");
														$scope.layoutAspects.cells("c").setText("Options");
														$scope.layoutAspects.cells("d").setText("Preview");
														
														$scope.toolbarSources = new dhtmlXToolbarObject({
															parent : "toolbarObj",
															icons_path: "/resources/shared/imgs/",
															json: [
																{
																	id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
																},						
																{
																	id: "select", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
																}
															]					
														});
														
														$scope.toolbarSources.setAlign("right");
														
														$scope.toolbarSources.attachEvent("onClick", function(id) {
															
															switch(id) {
																
																case "select":
																	
																	var aspect = {
																		type : $scope.gridAspects.getSelectedRowId(),
																		model : $scope.gridData.getSelectedRowId(),
																		loading : $scope.gridAspectOptions.cells("loading",1).getCellCombo().getSelectedValue(),
																		mapkey : $scope.gridAspectOptions.cells("mapkey",1).getCellCombo().getSelectedValue(),
																		mapkeyType : $scope.mapValuesTypes[$scope.gridAspectOptions.cells("mapkey",1).getCellCombo().getSelectedValue()],
																		mapvalue : $scope.gridAspectOptions.cells("mapvalue",1).getCellCombo().getSelectedValue(),
																		mapvalueType : $scope.mapValuesTypes[$scope.gridAspectOptions.cells("mapvalue",1).getCellCombo().getSelectedValue()],
																	}
																	
																	$scope.gridAllowedFields.cells(rId,7).setValue(JSON.stringify(aspect));
																	
																	$scope.windowModal.unload();
																	
																break;
																
																case "close":
																	
																	$scope.windowModal.unload();
																	
																break;
																
															}
															
														});
														
														
													/* ------: Aspects list :------ */
														
														$scope.gridAspects = $scope.layoutAspects.cells("a").attachGrid();
														$scope.gridAspects.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
														
														$scope.gridAspects.setHeader(
														    "Title",
														    null,
														    ["background-color:#f7f8f9;"]
														);	
														
														$scope.gridAspects.setColAlign("left");
														$scope.gridAspects.setColTypes("ro");
														$scope.gridAspects.setColSorting("str");
														$scope.gridAspects.setStyle("color:#545454");	
														$scope.gridAspects.enableAutoHeight(false);
														$scope.gridAspects.enableEditEvents(true,false,false);
														$scope.gridAspects.init();
														$scope.gridAspects.setSkin("material");
														
														$scope.gridAspects.setStyle(
															"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
														);	
														
														
														data = {
															
															rows:
															[
																
																{ 
																	id : "select",
																	data:
																	[
																		"Select",
																	] 
																},
																{ 
																	id : "multiselect",
																	data:
																	[
																		"Multi-select",
																	] 
																},																	
																{ 
																	id : "multivalue",
																	data:
																	[
																		"Multi-value",
																	] 
																},														
																{ 
																	id : "checkbox",
																	data:
																	[
																		"Checkbox",
																	] 
																},
																{ 
																	id : "radiobox",
																	data:
																	[
																		"Radiobox",
																	] 
																}														
																
															]
															
														};
														
														$scope.gridAspects.parse(data, "json");												
														
														
													/* ------: Data list :------ */
														
														Model.index({})
														
														/* ------: On success :------ */
														
														.success(function(response) {
															
															var data = {};
															data.rows = [];
															
															$.each(response.data, function(key, value) {
																
																data.rows.push(
																	
																	{
																		id : response.data[key].id,
																		data:
																		[
																			response.data[key].name
																		] 
																	}
																	
																);
																
															});
															
															$scope.gridData = $scope.layoutAspects.cells("b").attachGrid();
															$scope.gridData.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
															
															$scope.gridData.setHeader(
															    "Title",
															    null,
															    ["background-color:#f7f8f9;"]
															);	
															
															$scope.gridData.setColAlign("left");
															$scope.gridData.setColTypes("ro");
															$scope.gridData.setColSorting("str");
															$scope.gridData.setStyle("color:#545454");	
															$scope.gridData.enableAutoHeight(false);
															$scope.gridData.enableEditEvents(true,false,false);
															$scope.gridData.init();
															$scope.gridData.setSkin("material");
															
															$scope.gridData.setStyle(
																"font-size:12px;", "font-size:13px;","font-size:13px;", "font-size:13px;"
															);	
															
															$scope.gridData.parse(data, "json");
															
															$scope.gridData.attachEvent("onRowSelect", function(id,ind){
																
																$scope.aspectMapping($scope.aspect);
																
															});
															
														/* ------: Set configuration :------ */
															
															try {
																
																var aspect = JSON.parse($scope.gridAllowedFields.cells(rId,7).getValue());
																
																$scope.gridAspects.selectRowById(aspect.type);
																$scope.gridData.selectRowById(aspect.model);
																
															} catch(error) {
																
																var aspect = {};
																
															}
															
															$scope.aspect = aspect;
															
														/* ------: Options :------ */
															
															$scope.gridAspectOptions = $scope.layoutAspects.cells("c").attachGrid();
															$scope.gridAspectOptions.setImagePath("/include/dhtmlx/codebase/imgs/");
															$scope.gridAspectOptions.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
															$scope.gridAspectOptions.setHeader(
															    "Option,Value",
															    null,
															    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
															);
															$scope.gridAspectOptions.setStyle("color:#545454");
															//$scope.gridAspectOptions.setHeader("Option,Value");
															$scope.gridAspectOptions.setInitWidths("*,*");
															$scope.gridAspectOptions.setColAlign("left,left");
															$scope.gridAspectOptions.setColTypes("ro,combo");
															$scope.gridAspectOptions.setColSorting("str,str");
															$scope.gridAspectOptions.init();
															$scope.gridAspectOptions.setSkin("material");
															
															data = {
																
																rows:
																[
																	
																	{ 
																		id:"loading",
																		data:
																		[
																			"Loading",
																			"clicklazy"
																			//aspect.loading
																		] 
																	},
																	
																	{
																		id:"mapkey",
																		data:
																		[
																			"Map key",
																			""
																			//aspect.mapkey
																		] 
																	},
																	
																	{
																		id:"mapvalue",
																		data:
																		[
																			"Map value",
																			""
																			//aspect.mapvalue
																		] 
																	}			
																]
																
															};
															
															
															$scope.gridAspectOptions.parse(data, "json");
															
															// Loading
															
															var loadingTypes = {
																options: [
																	{value: "init", text: "Init"},
																	{value: "clickfull", text: "Click full"},
																	{value: "clicklazy", text: "Click lazy"},
																	{value: "searchfull", text: "Search full"},
																	{value: "searchlazy", text: "Search lazy"}
																]
															};
															
															var cmbLoadingTypes = $scope.gridAspectOptions.cells("loading",1).getCellCombo();
															cmbLoadingTypes.enableFilteringMode(true);
															cmbLoadingTypes.load(loadingTypes);
															
															// Mapping
															
															$scope.aspectMapping(aspect);
															
															
														});
														
														
														// TODO : get index of header which name is "Type" ???
														
													/* ------: Preview :------ */
														
														// Data.select (selected model)
														
														
													}
													
												});
												
												
											/* ------: Types combo box :------ */
												
												var types = {
													
													options: [
														
														{value: "integer", text: "Integer"},
														{value: "double", text: "Decimal"},
														{value: "string", text: "String"},
														{value: "date", text: "Date"},
														{value: "datetime", text: "DateTime"},
														
													]
													
												};
												
												
												// TODO : get index of header which name is "Type"
												
												cmbTypes = $scope.gridAllowedFields.getColumnCombo(5);
												cmbTypes.enableFilteringMode(true);
												cmbTypes.load(types);
												
												
											/* ------: Attach events :------ */
												
												$scope.gridResults.attachEvent("onBeforeDrag",function(id){
													
													$scope.model.dragFromResults = true;
													$scope.layout.cells($scope.model.layout.cells.allowedfields).expand();
													return true; 
													
												});	
												
												$scope.gridResults.attachEvent("onDrag",function(sid,tid){
													
													return true;
													
												});	
												
												$scope.gridAllowedFields.attachEvent("onBeforeDrag",function(id){
													
													return true; 
													
												});	
												
												$scope.gridAllowedFields.attachEvent("onDrag",function(sid,tid){
													
													var confirm = true;	
													
													$.each(sid.split(','), function( index, value ) {
														
														if($scope.gridAllowedFields.doesRowExist(value) && $scope.model.dragFromResults === true) {
															
															dhtmlx.message({
																type: "error",
																text: "Field " + value.decode() + " already allowed",
																expire: 10000
															});		
															
															confirm = false;
															
														} else {
															
															confirm = true;
															
														}		
														
													});
													
													$scope.model.dragFromResults = false;
													
													return confirm;
													
												});		
												
												$scope.gridAllowedFields.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
													
													try {
													
														if($scope.model.dragFromResults === true) {
														
															$.each(sId.split(','), function( index, value ) {
																
																$scope.gridAllowedFields.cells(value,5).setValue(
																	$scope.model.datatypes[$scope.driver][$scope.gridAllowedFields.cells(value,5).getText()].model
																)	
																
															});
															
														}
														
													} catch (error) {
														
														// TODO : create unified error handler and put there new Error().stack
														
														dhtmlx.message({
															type: "error",
															text: error,
															expire: 10000
														});													
														
													}
													
												});										
												
											} else {
												
												if(reload) {
													
													/* ------: Load data :------ */								
													
													$scope.loadData(object);
													$scope.loadSettings(object);
													
												}
												
											}
											
										break;
										
									/* ------: Not supported :------ */
										
										default:
											
											dhtmlx.message({
												type: "error",
												text: "Unsupported driver " + source.driver,
												expire: 10000
											});
											
										break;
										
									}
									
								})
								
								.fail(function(jqxhr, textStatus, error ) {
									
									alert("Error to get " + source.driver);
									
								});
								
							})
							
							.error(function(response, status) {
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 10000
								});
								
							});	
							
						break;
						
						case "rscript":
							
							switch(object.data.editor) {
								
								case "rscript":
								
								/* ------: Set configuration :------ */
									
									$scope.configuration = {
										
										editor : {
											
											mode : "r",
											theme : "ambiance"
											
										},
										
										autocomplete : {}											
										
									}
									
								/* ------: Create layout :------ */	
									
									$scope.model.layout.cells.editor = "a";									
									$scope.model.layout.cells.result = "b";
									
									$scope.layout = $scope.window.attachLayout("2E");
									$scope.layout.setSkin("material");
									$scope.layout.cells($scope.model.layout.cells.editor).setText("R Script");
									$scope.layout.cells($scope.model.layout.cells.result).setText("Results");
									$scope.layout.cells($scope.model.layout.cells.editor).attachURL("/app/ace/");
									$scope.editorResults = $scope.layout.cells($scope.model.layout.cells.result).attachEditor({content:""});
									$scope.layout.cells($scope.model.layout.cells.result).collapse();
									
								/* ------: Load data in tab :------ */
									
									$scope.layout.attachEvent("onContentLoaded", function(id){
										
										switch(id) {
											
											case "a":
												
												/* ------: Link presentation :------ */
												
												$scope.presentation = $scope.layout.cells($scope.model.layout.cells.editor).getFrame().contentWindow;
												
												/* ------: Load data :------ */
												
												//$scope.driver = source.driver;
												
												$scope.loadData(object);
												
											break;
											
										}
										
									});										
									
								break;
								
							}							
							
						break;	
						
						default:
							
							dhtmlx.message({
								type: "error",
								text: $scope.editor + " not implemented",
								expire: 5000
							});						
							
						break;
						
					}
					
				});
				
				
			})
			
			.error(function(response, status) {
				
				dhtmlx.message({
					type: "error",
					text: response,
					expire: 5000
				});
				
			});	
				
		};
		
		
	/* ------: Add new object to model :------ */
		
		$scope.addNewObjectToModel = function(fields, source, tablename){
			
			var columns = [];
			var type = null;
			
			$.each(fields, function(index, id) {
				
				try {
					
					type = $scope.model.datatypes[source.driver][$scope.gridTableFields.cellById(id,1).getValue()].model;
					
				} catch (error) {
					
					dhtmlx.message({
						type: "error",
						text: "["+$scope.gridTableFields.cellById(id,1).getValue()+"] " + error,
						expire: 30000
					});													
					
				}
				
				try {
					
					columns.push({
						"name": $scope.gridTableFields.cellById(id,0).getValue(),
						"title": $scope.gridTableFields.cellById(id,0).getValue(),
						"type": type,
						"length": "",
						"defaultvalue": "",
						"enumvalue": "",
						"ai": false,
						"pk": false,
						"nu": false,
						"ui": false,
						"in": false,
						"un": false,
						"fillable": false,
						"guarded": false,
						"visible": false,
						"hidden": false,
						"colid": "c115",
						"order": 0
					});
					
				} catch (error) {
					
					dhtmlx.message({
						type: "error",
						text: error,
						expire: 30000
					});												
					
				}
				
			});
			
			var table = {
				"name": tablename,
				"color": $scope.gridTableProps.cellById("tablecolor",1).getValue(),
				"position":{"x":50,"y":50},
				"classname": $scope.gridTableProps.cellById("tabletitle",1).getValue(), 
				"namespace":"",
				"increment":false,
				"timestamp":false,
				"softdelete":false,
				"column": columns,
				"relation":[],
				"seeding":[]
			};
			
			var currentModel = $scope.presentation.DesignerApp.NodeEntities.ExportToJSON();
			currentModel.push(table);
			
			$scope.presentation.DesignerApp.NodeEntities.ClearNodeCanvas($scope.presentation.DesignerApp.NodeEntities.CurrentNodeCanvas);
			$scope.presentation.DesignerApp.NodeEntities.AddNodeCanvas(currentModel);
			
		};
		
		
	/* ------: Create grid for results :------ */
		
		$scope.result = function(data){
			
			$scope.gridResults.clearAll();
			$scope.gridResults.parse(data,"json");
			$scope.gridResults.sortRows(0,"str","asc");
			
			$scope.layout.cells($scope.model.layout.cells.availablefields).expand();
			
		};
		
		
	/* ------: Watch for object changes :------ */
		
		$scope.$watch("object", function(object){
			
			if(object !== undefined) {
				
				try {
					
					typeof object.data.data.datasource;
					typeof object.data.data.id;
					
					$scope.loadUI(object.data, true);
					
				} catch (error) {
					
					$scope.selectSource();
					
				}
				
			}
			
		});		
		
		
	/* ------: Get model :------ */
		
		// TODO : get datasources and the getObject
		
		$.getJSON("/conf/datatypes.json?" + new Date(), function(data) {
			
			$scope.model.datatypes = data;
			$scope.getObject($scope.request());	
			
		})
		
		/* ------: On error :------ */
		
		.error(function(response, status) {
			
			dhtmlx.message({
				type: "error",
				text: "Get datasources : " + status,
				expire: 5000
			});
			
		});		
		
	});	

// EOF