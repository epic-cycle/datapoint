
// Loading resources

ljs

	.addAliases({
		
	    functions	:
					    [
							"/js/functions/main.js",
					    ],
		    
	    controllers	:
					    [
							"/js/controllers/Mounts.js",
							"/js/controllers/Components.js",
					    ],
		
		directives	:	[

						],
							
		services	:	[
							"/js/services/Mount.js",
							"/js/services/Report.js"
						],
		
		app			:
						[
							"/css/app.css",
							"/apps/main/app.css",
							"/apps/main/app.js?" + new Date(),
						],
		    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'jquery',
		'jqueryui',
		'jqdock',
		'chance',
		'webix',
		'd3',
		'dhtmlx',
		'bootstrap',
		'jquerytouchp',
		'lodash',
		'momentjs',
		'gridstack',		
		'angular',
		'note',
		'loading',
		'slidereveal',
		
		/* ---: Local :--- */
		
		'functions',
		'controllers',
		'directives',
		'services',
		'app',
		
		/* ---: Callback :--- */
		
		function(){
			
			$("body").show("fast");
			console.log("Loader: resources loaded");
			
		}	
		
	)
;

// EOF
