
	$.getJSON( "/apps/main/modules.json", function( data ) {
		
	/* ------: Configure application :------ */
		
		var app = angular.module('appDataPoint', data.modules, 
			function($interpolateProvider) {
				$interpolateProvider.startSymbol('<%');
				$interpolateProvider.endSymbol('%>');
			}
		)
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
		}]);
		
		
	/* ------: Run application :------ */
		
		app.run(function($rootScope, Report) {
			
		/* ------: Initialize :------ */
			
			console.log("appDataPoint: application started");
			
			$rootScope.windows = new dhtmlXWindows();
			$rootScope.windows.attachViewportTo("viewport"); 
			
		/* ------: Toolbar :------ */
			
			webix.ui({
				container:"toolbar",
				view:"toolbar",
				id:"toolbar",
				name:"toolbar",
				paddingY:0,
				elements: [
					{ 
						view:"button",
						align:"right",
						type:"imageButton",
						image:"/resources/shared/imgs/edit-grid-32px.png",
						label:"<span style='color:black;'>Edit</span>",
						width:100,
						on:{
							'onItemClick':function(newv, oldv){
								
								$rootScope.editView();
								
							}
						} 					
					},
					{ 
						view:"button",
						align:"right",
						type:"imageButton",
						image:"/resources/shared/imgs/save-16px.png",
						label:"<span style='color:black;'>Save</span>",
						width:100,
						on:{
							'onItemClick':function(newv, oldv){
								
								$rootScope.saveView();
								
							}
						} 					
					},
					{ 
						view:"button",
						align:"right",
						type:"imageButton",
						image:"/resources/shared/imgs/create-report-16px.png",
						label:"<span style='color:black;'>Report</span>",
						width:100,
						on:{
							'onItemClick':function(newv, oldv){
								
								$rootScope.createReport();
								
							}
						} 					
					},
					{ 
						view:"button",
						align:"right",
						type:"imageButton",
						image:"/resources/shared/imgs/sync-16px.png",
						label:"<span style='color:black;'>Sync</span>",
						width:100,
						on:{
							'onItemClick':function(newv, oldv){
								
								$rootScope.sync();
								
							}
						} 					
					},					
					{ 
						view:"button",
						align:"right",
						type:"imageButton",
						image:"/resources/shared/imgs/info-18px.png",
						label:"<span style='color:black;'>Help</span>",
						width:100,
						on:{
							'onItemClick':function(newv, oldv){
								
								$(".note-editor").slideToggle();
								
								$('#notes').summernote({
									height: 300
								});
								
								$('#notes').summernote('fontSize', 24);
								
							}
						} 					
					},					
						
					{ 
						view:"richselect", width:300,
						id:"activeWindows",
						name:"activeWindows",
						options:[]
					},
				],
			});		
			
			
		/* ------: Synchronization between Datapoint instances :------ */
			
			$rootScope.sync = function () {
				
				$rootScope.windowModal = new dhtmlXWindows();
				$rootScope.windowModal.window = $rootScope.windowModal.createWindow("sync", 0, 0, 1000, 600);	
				$rootScope.windowModal.window.centerOnScreen();
				$rootScope.windowModal.window.hideHeader();	
				$rootScope.windowModal.window.setModal(true);				

				$rootScope.sourcesLayout = $rootScope.windowModal.window.attachLayout("3T");	
				$rootScope.sourcesLayout.setSkin("material");								
				$rootScope.sourcesLayout.cells("a").setText("Synchronization");	
				$rootScope.sourcesLayout.cells("a").setHeight(75);
				$rootScope.sourcesLayout.cells("b").setText("Local instance");	
				$rootScope.sourcesLayout.cells("c").setText("External instance");	
				
/*	formData = [
				//{type: "settings", position: "label-top", labelWidth: "auto", inputWidth: 135, blockOffset: 12},
				{type: "block", inputWidth: "auto", list:[
					{type: "input", width: 185, label: "Local instance:"},
					{type: "newcolumn"},
					{type: "button", width: 150, value: "Reload", offsetLeft: 100},
					{type: "button", width: 150, value: "To external >>", offsetLeft: 100},
					{type: "button", width: 150, value: "<< To local", offsetLeft: 100},
					{type: "button", width: 150, value: "Sync", offsetLeft: 100},
					{type: "newcolumn"},
					{type: "input", offsetLeft: 20, label: "External instance:", value: "https://datapoint.io"},
				]}
			];				
				
				
				myForm = $rootScope.sourcesLayout.cells("a").attachForm(formData);
			*/
				$rootScope.toolbarAvailableSubjects = $rootScope.sourcesLayout.cells("a").attachToolbar({
					icons_path: "/resources/shared/imgs/",
					json: [
{
    id:     "info",
    type:   "text",
    text:   "External instance",
                                                       
    title:  "Tooltip here",
    userdata: {
        p1: "value1" ,
        p2: "value2"
    }
},						
						{
							id: "create", type: "buttonInput", mode: "select", img: "select-32px.png", width: 300
						},
						{
							id: "create", type: "button", mode: "select", text: "Connect", img: "select-32px.png"
						},						
					]
				});	
				
			//	$rootScope.toolbarAvailableSubjects.addInput("input_new",3,"",100);
				
			//	var dhxBar = $rootScope.windowModal.window.attachToolbar();
				
				
				//Sync.diff
				
				//Sync.send
				
			};
			
		/* ------: Create report :------ */
			
			$rootScope.createReport = function () {
				
				$rootScope.windowModal = new dhtmlXWindows();
				$rootScope.windowModal.window = $rootScope.windowModal.createWindow("asdasd", 0, 0, 500, 500);	
				$rootScope.windowModal.window.centerOnScreen();
				$rootScope.windowModal.window.hideHeader();	
				$rootScope.windowModal.window.setModal(true);				

				$rootScope.sourcesLayout = $rootScope.windowModal.window.attachLayout("1C");	
				$rootScope.sourcesLayout.setSkin("material");								
				$rootScope.sourcesLayout.cells("a").setText("Create report");
				
				
				
				var dhxSBar = $rootScope.windowModal.window.attachStatusBar({text: "", height: 34});
				dhxSBar.setText("<div id='toolbarObj' style='width:100%;text-align:right;'></div>");
				
				$rootScope.windowModal.setSkin("material");				
				
				$rootScope.toolbarCreateReport = new dhtmlXToolbarObject({
					parent : "toolbarObj",
					icons_path: "/resources/shared/imgs/",
					json: [
						{
							id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
						},						
						{
							id: "create", type: "button", mode: "select", text: "Apply", img: "select-32px.png"
						}
					]					
				});
				
				$rootScope.toolbarCreateReport.setAlign("right");				

	formData = [
				{type: "settings", position: "label-left", labelWidth: 140, inputWidth: 260},
				{type: "label", label: "", position: "label-right", list:[
					{type: "label", label: "Settings", position: "label-right", list:[
						{name: "reportName", type: "input", label: "Report name:", value: ""},
						{name: "reportAlias", type: "input", label: "Report alias:", value: ""},
					]},
					{type: "label", label: "API", position: "label-right", list:[
						{type: "combo", label: "Format:", name: "apiformat", options:[
							{text: "-not selected-", value: "", selected: true},
							{text: "HTML", value: "html"},
							{text: "JSON", value: "json"},
							{text: "Excel", value: "excel"}
						]},
					]},
					{type: "label", label: "Schedule", position: "label-right", list:[
						{type: "combo", label: "Format:", name: "apiformat", options:[
							{text: "-not selected-", value: "", selected: true},
							{text: "HTML", value: "html"},
							{text: "JSON", value: "json"},
							{text: "Excel", value: "excel"}
						]},
						{type: "combo", label: "Target:", name: "apiformat", options:[
							{text: "-not selected-", value: "", selected: true},
							{text: "E-mail", value: "html"},
							{text: "GDrive", value: "json"},
							{text: "Filesystem", value: "excel"}
						]},	
						{type: "input", label: "Crontab format:", value: ""},
					]},					
				]}
			];
				
				myForm = $rootScope.sourcesLayout.cells("a").attachForm(formData);
				
				$rootScope.toolbarCreateReport.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "create":
							
							try {
								
								var values = myForm.getFormData();
								
								var reportName = values.reportAlias;
								
								var topMostWindow = $rootScope.windows.getTopmostWindow();
								
								var topMostWindowIframe = $rootScope.windows.window(topMostWindow._idd).getFrame();
								
								Report.save({
									
									id : reportName,
									view : topMostWindowIframe.contentWindow.view,
									cmp : 'view'
									
								})
								
								/* ------: On success :------ */
									
								.success(function(response) {
									
									dhtmlx.message({
										text: "Report created (" + reportName + ")",
										expire: 5000
									});
									
									$rootScope.windowModal.unload();
									
								})			
								
								/* ------: On error :------ */
									
								.error(function(response) {
									
									dhtmlx.message({
										type: "error",
										text: "Error to save report : " + response.error,
										expire: 5000
									});						
									
								});					
								
								if(topMostWindow !== null) {
									
									$rootScope.$broadcast(topMostWindow._idd + ".saveForReport", reportName);
									
								} else {
									
									dhtmlx.message({
										type: "error",
										text: "Nothing to save for report",
										expire: 5000
									});	
									
								}
								
							} catch (e) {
								
								dhtmlx.message({
									type: "error",
									text: e,
									expire: 5000
								});						
								
							}
							
						break;
						
						case "close":
							
							$rootScope.windowModal.unload();
							
						break;
						
					}
					
				});				
				
			};
			
			
		/* ------: Save viev :------ */
			
			$rootScope.saveView = function () {
				
				try {
					
					var topMostWindow = $rootScope.windows.getTopmostWindow();
					
					if(topMostWindow !== null) {
						
						var topMostWindowIframe = $rootScope.windows.window(topMostWindow._idd).getFrame();
						var gridstack = topMostWindowIframe.contentWindow.angular.element(".grid-stack").scope();
						gridstack.saveGridstack();						
					
					} else {
						
						dhtmlx.message({
							type: "error",
							text: "Nothing to save!",
							expire: 5000
						});	
						
					}
					
				} catch (e) {
					
					dhtmlx.message({
						type: "error",
						text: e,
						expire: 5000
					});						
					
				}				
				
			};
			
		/* ------: Edit viev :------ */
			
			$rootScope.editView = function () {
				
				try {
				
					var topMostWindow = $rootScope.windows.getTopmostWindow();
					
					if(topMostWindow !== null) {
						
						var topMostWindowIframe = $rootScope.windows.window(topMostWindow._idd).getFrame();
						topMostWindowIframe.contentWindow.toggleEditView();	
						
					} else {
						
						dhtmlx.message({
							type: "error",
							text: "Nothing to edit!",
							expire: 5000
						});	
						
					}
					
				} catch (e) {
					
					dhtmlx.message({
						type: "error",
						text: e,
						expire: 5000
					});						
					
				}
				
			};
			
		/* ------: Toogle toolbar :------ */
			
			$rootScope.toggleToolbar = function () {
				
				$rootScope.windows.forEachWindow(function(win){
					
					if(win.isVisibleToolbar === false) {
						
						win.showToolbar();
						win.isVisibleToolbar = true;
						
					} else {
						
						win.hideToolbar();
						win.isVisibleToolbar = false;
						
					}
					
				});	   
				
			};
			
		/* ------: Toogle header :------ */
			
			$rootScope.toggleHeader = function () {
				
				$rootScope.windows.forEachWindow(function(win){
					
					if(win.isVisibleHeader === false) {
						
						win.showHeader();
						win.isVisibleHeader = true;
						
					} else {
						
						win.hideHeader();
						win.isVisibleHeader = false;
						
					}
					
				});	   
				
			};			
			
		/* ------: Resize :------ */	
			
			$rootScope.resize = function () {
				
				$rootScope.windows.forEachWindow(function(win){
					
					if(win.isMaximized()){
						win.minimize();
						win.maximize();
					}
					
				});	    
				
				if($rootScope.windowModal !== undefined) {
					$rootScope.windowModal.forEachWindow(function(win){
						
						win.centerOnScreen();
						
					});	 			
				}
				
			};
			
		/* ------: Play view stop :------ */
				
			$rootScope.playviewsstop = function(){
				
				$(".navbar").css("display", "block");
				$("#viewport").removeAttr('style');
				$("#viewport").css("width", "100%");
				$("#playmenu").hide();
				clearInterval($rootScope.play);
				
			};
			
		/* ------: Play view start :------ */
			
			$rootScope.playviews = function(){
				
				//show form
				
				$rootScope.rotate = [];
				
				$(".navbar").css("display", "none");
				$("#viewport").css("position", "absolute");
				$("#viewport").css("top", "0px");
				$("#playmenu").show();
				$rootScope.resize();
				
				
				
				$rootScope.play = setInterval(function(){ 
					
					if($rootScope.rotate.length === 0) {
						
						$rootScope.windows.forEachWindow(function(win){
							
							$rootScope.rotate.push(win._idd);
							
						});	 			
						
					}
						
					$($rootScope.windows.window($rootScope.rotate[0]).cell).hide();
					$rootScope.windows.window($rootScope.rotate[0]).bringToTop();
					
					$($rootScope.windows.window($rootScope.rotate[0]).cell).fadeIn( "slow", function() {
						$rootScope.rotate.shift();									
					});						
					
				}, 30000);					
				
			};
			
		/* ------: Close window :------ */
			
			$rootScope.close = function(id){
				
				//$rootScope.windows.window(id).close();
				$rootScope.windows.window(id).hide();
				
			};
			
		/* ------: Load window :------ */
			
			$rootScope.load = function(id, title){
				
				//$("#viewport").LoadingOverlay("show", {
    				//fade  : [100, 100]
				//});
				
			/* -------------------------------- */
				
				// Create window				
				
				var winid = chance.guid();
				$rootScope.window = $rootScope.windows.createWindow(winid, 10, 10, $(document).width()-40, $(document).height()-100);
				$rootScope.windows.window(winid).maximize();
				$rootScope.windows.window(winid).setText(title);	
				
				// get text from API or from clicked menu item
				
				//$rootScope.windows.window(winid).addUserButton("help", 0, "help", "help");
				//$rootScope.windows.window(winid).button("help").attachEvent("onClick", $scope.aaa());
				
			/* -------------------------------- */
				
				// Toolbar configuration
				
				var toolbarconf = [
					{
						id:     "editgrid",
						type:   "buttonTwoState",
						text:   "Edit grid"
					},
					{
						id:     "toolbar",
						type:   "buttonTwoState",
						text:   "Toolbar",
					},	
					{
						id:     "saveview",
						type:   "button",
						text:   "Save view",
					}
				];
				
			/* -------------------------------- */
				
				// Attach Toolbar to Window
				
				var toolbar = $rootScope.windows.window(winid).attachToolbar({
					icons_path: "/resources/shared/imgs/",
					json: toolbarconf,					
				});
				
				
				$rootScope.windows.window(winid).hideToolbar();
				$rootScope.windows.window(winid).hideHeader();
				$rootScope.windows.window(winid).isVisibleToolbar = false;	
				$rootScope.windows.window(winid).isVisibleHeader = false;
				
			/* -------------------------------- */
				
				// Attach toolbar onStateChange event
				
				toolbar.attachEvent("onStateChange", function(id) {
					
					var ifr = $rootScope.windows.window(winid).getFrame();
					
					switch(id) {
						
						case "editgrid":
							
							ifr.contentWindow.toggleEditView();
							
						break;
						
						case "toolbar":
							
							
							
						break;
						
					}
					
				});
				
			/* -------------------------------- */
				
				// Attach toolbar onClick event
				
				toolbar.attachEvent("onClick", function(id) {
					
					var ifr = $rootScope.windows.window(winid).getFrame();				
					
					switch(id) {
						
						case "showheaders":
							alert(2);
						break;
						
						case "hideheaders":
							alert(3);
						break;
						
						case "saveview":
							alert(4);
						break;					
						
					}
					
				});	
				
			/* -------------------------------- */
				
				// Load URL				
				
				$rootScope.windows.window(winid).attachURL(id + "#?winid=" + winid);
				
				$rootScope.windows.window(winid).attachEvent("onContentLoaded", function(win){
					
					$("#viewport").LoadingOverlay("hide", true);
					
				});
				
				
				
			/* -------------------------------- */
				
				// Refresh open windows list
				
				$rootScope.refreshWindowList();
				
			/* -------------------------------- */
				
				// Hanldle window resizing
					
				$(window).resize(function() {
					
					$rootScope.resize();
					
				});		
				
				
			};
			
		/* ------: Refresh window list :------ */	
			
			$rootScope.refreshWindowList = function () {
				
				$$("toolbar").removeView("activeWindows");
				
				var topMostWindow = $rootScope.windows.getTopmostWindow();
				
				if(topMostWindow !== null) {
					
					var winid = topMostWindow._idd;
					
					$rootScope.activeWindows = [];
					
					$rootScope.windows.forEachWindow(function(win){
						
						if(!win.isHidden()){
							
							$rootScope.activeWindows.push({
								id: win._idd, 
								value: win.getText()
							});
							
						}
						
					});	 
					
					$$("toolbar").addView(
						{
							view: "richselect", width:300,
							id: "activeWindows",
							name: "activeWindows",
							value: winid,
							options: $rootScope.activeWindows,
							on:{
								
								'onChange': function(winid){
									
									$rootScope.windows.window(winid).bringToTop();
									
								}
								
							}  						
						}, 100
					);	
					
				}
				
			};

		/* ------: Check and run playlist :------ */
			
			$rootScope.keyup = function(e){
				
				//$(document).unbind("keyup", $rootScope.keyup);
				
				if (e.keyCode == 27) {
					
					var slider = $("#playmenu").slideReveal({
						
						push : false,
						overlay : true,
						autoEscape: false
						
					});
					
					slider.slideReveal("show");
					
					$("#playmenu-content").show();
					
				}	
				
			}
			
			$rootScope.playlist = function () {
				
				if(window.location.pathname.indexOf("/app/playlist/")!==-1){
					
					$(document).keyup($rootScope.keyup);
					
					/*
					$(document).keyup(function(e) {
						
						if (e.keyCode == 27) { // escape key maps to keycode `27`
								
							var slider = $("#playmenu").slideReveal({
								
								push: false,
								overlay: true						
								hidden = function(){
									alert(1);
								}
								
							});
							
							slider.slideReveal("show");						
						}
						
					});	
					*/
					
					var path = window.location.pathname.split("/");	
					
					var views = Array ();
					
					switch(path[3]) {
						
						case "12":
							
							//views = Array(101,114,113,172);
							views = Array(101,114,113);
							
						break;
						
						case "13":
							
							views = Array(172,101,114,113);
							
						break
						
					}
					
					$.each(views, function(index, value) {
						
						setTimeout(function() {
							
							$rootScope.load('/app/views/' + value + "/?space=10",'as');
							
							if(index === views.length - 1) {
								
								$rootScope.playviews();
								
							}
							
						}, index * 30000);						
						
					});					
					
				}
				
			}
		
		/* ------: Initialize playlist :------ */
			
			$rootScope.playlist();
			
		});
		
		
		angular.element(document).ready(function() {
			
			angular.bootstrap(document, ["appDataPoint"]);
			
		});	
		
	});
	
// EOF
