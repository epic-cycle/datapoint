/**
 *	Module for controller "Spreadjs"
 * 
 *  	@doc	module
 * 	@name	controller.Spreadjs
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Spreadjs', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Spreadjs
	 * 
	 *	@doc	controller
	 * 	@name	Users
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Spreadjs', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, $location) {
		
		$("#content").wijspread({ sheetCount: 1 });
		var spread = $("#content").wijspread("spread");
		spread.tabStripVisible(!spread.tabStripVisible());
		spread.useWijmoTheme = true;
		//var hScrollbar = spread.showHorizontalScrollbar();
		//var vScrollbar = spread.showVerticalScrollbar();
		spread.showHorizontalScrollbar(false);
		spread.showVerticalScrollbar(false);
		var sheet = spread.getActiveSheet();
		sheet.setRowHeaderVisible(false);
		sheet.setColumnHeaderVisible(false);
		sheet.setColumnCount(100,1);
		
	        $scope.dataEmpty = function(){
				
				
				$scope.hideLoading();
				
	            $("#content").css("position", "relative");
	            $("body").css("overflow","hidden");
	            var width = $("#content").width() + 2, height = $("#content").height();
	            $("<span id='delaySpan'>Data not found, try to change criteria! </span>")
	                    .css("left", width / 2 - 130)
	                    .css("top", height / 2 - 30)
	                    .css("position", "absolute")
	                    .css("color", "#4f4f4f")
	                    .css("background", "#ffffff")
	                    .css("border", "1px solid #a8a8a8")
	                    .css("border-radius", "3px")
	                    .css("-webkit-border-radius", "3px")
	                    .css("box-shadow", "0 0 10px rgba(0, 0, 0, 0.25")
	                    .css("font-family", "Arial, sans-serif")
	                    .css("font-size", "16px")
	                    .css("padding", "0.4em")
	                    .insertAfter("#content");
	            $("<div id='delayDiv'></div>")
	                    .css("background", "#2D5972")
	                    .css("opacity", 0.3)
	                    .css("position", "absolute")
	                    .css("top", 0)
	                    .css("left", 0)
	                    .css("width", width)
	                    .css("height", height + 30)
	                    .insertAfter("#content");
	        };
		
	        $scope.showLoading = function(){
				
				$scope.hideLoading();
				
	            $("#content").css("position", "relative");
	            $("body").css("overflow","hidden");
	            var width = $("#content").width() + 2, height = $("#content").height();
	            $("<span id='delaySpan'><span id='icon' style='display:inline-block'></span>Loading... <span style='font-size:14px;color:brown;' id='countup'></span> <span style='font-size:14px;'>sec</span></span>")
	                    .css("left", width / 2 - 70)
	                    .css("top", height / 2 - 30)
	                    .css("position", "absolute")
	                    .css("color", "#4f4f4f")
	                    .css("background", "#ffffff")
	                    .css("border", "1px solid #a8a8a8")
	                    .css("border-radius", "3px")
	                    .css("-webkit-border-radius", "3px")
	                    .css("box-shadow", "0 0 10px rgba(0, 0, 0, 0.25")
	                    .css("font-family", "Arial, sans-serif")
	                    .css("font-size", "16px")
	                    .css("padding", "0.4em")
	                    .insertAfter("#content");
	            $("<div id='delayDiv'></div>")
	                    .css("background", "#2D5972")
	                    .css("opacity", 0.3)
	                    .css("position", "absolute")
	                    .css("top", 0)
	                    .css("left", 0)
	                    .css("width", width)
	                    .css("height", height + 30)
	                    .insertAfter("#content");
					
				// Start countup timer
				$('#countup').runner({
					autostart: true,
					countdown: false,
					startAt: 0,
					milliseconds: true,
					format: function(value) {
						return (value / 1000).toFixed(2);
					}				
				});
				
	        };
	        
	        $scope.hideLoading = function() {
	            $("#delayDiv").remove();
	            $("#delaySpan").remove();
	        };
		
		
	        function screenAdption() {
				
	            // 2. spread and its container
	            var windowHeight = $(window).height(),
	                spreadHeight = windowHeight - 5;
				
	            // adjust height if too small
	            spreadHeight = Math.max(spreadHeight, 120);
	            
	            $("#content").height(spreadHeight);
	            // sometimes the window height changed for some unknown reason, so check and do adjust
	            var windowHeight2 = $(window).height();
				
	            if (windowHeight2 > windowHeight) {
	                spreadHeight += $(window).height() - windowHeight;
	                $("#content").height(spreadHeight);
	            }
	            
	            $("#content").wijspread("refresh");
	        }
			
			
	        screenAdption();
	        var resizeTimeout = null;
	        $(window).bind("resize", function () {
	            if (resizeTimeout === null) {
	                resizeTimeout = setTimeout(function () {
	                    screenAdption();
	                    clearTimeout(resizeTimeout);
	                    resizeTimeout = null;
	                }, 100);
	            }
	        });
		
		
		$scope.setSpreadStyle = function() {
			
			spread.autoFitType($.wijmo.wijspread.AutoFitType.CellWithHeader);
			spread.tabStripVisible(false);
			spread.showHorizontalScrollbar(false);
			spread.showVerticalScrollbar(false);
			
			var defaultStyle = new $.wijmo.wijspread.Style();
			defaultStyle.hAlign = $.wijmo.wijspread.HorizontalAlign.left;
			defaultStyle.vAlign = $.wijmo.wijspread.VerticalAlign.center;
			defaultStyle.font = "9pt Arial";
			
		};
		
		$scope.isPage = function(page) {
			
			if($scope.model.pages["page" + page] === undefined) {
				
				return false;
				
			}
			else {
				
				return true;
				
			}
			
		};
		
		$scope.resetSpread = function(){
			
			$scope.presentation.spread.clearSheets();
			
			$scope.presentation.pages = [];
			
		};
		
		$scope.createGrid = function (data){
			
			
			
			var spread = $("#content").wijspread("spread");
			
			$scope.setSpreadStyle(spread);
			$scope.resetSpread(spread);
			
			var pc = 0;
			var page = 1;
			
			spread.addSheet(pc,new $.wijmo.wijspread.Sheet("page" + page));
			spread.setActiveSheet("page" + page);
			
			var sheet = spread.getActiveSheet();
			sheet.isPaintSuspended(true);
			
			
			sheet.setDataSource(data);
			sheet.selectionUnit($.wijmo.wijspread.SelectionUnit.Row);
			sheet.selectionBorderColor("Accent 1");
			sheet.defaults.rowHeight = 30;
			sheet.setDefaultStyle(defaultStyle, $.wijmo.wijspread.SheetArea.viewport);
			sheet.setRowHeight(0, 40.0,$.wijmo.wijspread.SheetArea.colHeader);
			//sheet.setRowHeaderVisible(false);
			
			 //sheet.setColumnWidth(0, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(1, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(2, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(3, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			
			  var cols = 0;
			$.each(data[0], function() {
				var colHeaderText = sheet.getValue(0, cols, $.wijmo.wijspread.SheetArea.colHeader); 
				sheet.setValue(0, cols, "          " + colHeaderText + "      ", $.wijmo.wijspread.SheetArea.colHeader);
				sheet.autoFitColumn(cols);
				cols += 1;
			});
			
			
			 //var row = sheet.getRow(0, $.wijmo.wijspread.SheetArea.colHeader);
			//row.backColor("Red");
			//row.foreColor("White");
			
			var rows = sheet.getRowCount();
			
			var cellrange =new $.wijmo.wijspread.Range(0, 0, rows, cols);
			var hideRowFilter =new $.wijmo.wijspread.HideRowFilter(cellrange);
			sheet.rowFilter(hideRowFilter);
			
			
			spread.useWijmoTheme = true;
			sheet.isPaintSuspended(false);
			spread.repaint();	
			// hideLoading();			
		};
		
		$scope.createPage = function(data) {
			
			var spread = $scope.presentation.spread;
			
			spread.autoFitType($.wijmo.wijspread.AutoFitType.CellWithHeader);
			spread.tabStripVisible(false);
			
		/* ------: Scroll bars :------ */
			
			if($scope.model.hidescrollx === 1){
				
				spread.showHorizontalScrollbar(false);
				
			} else {
			
				spread.showHorizontalScrollbar(true);
				
			}
			
			if($scope.model.hidescrolly === 1){
				
				spread.showVerticalScrollbar(false);
				
			} else {
			
				spread.showVerticalScrollbar(true);
				
			}
			
			
		/* ------: Settings :------ */
			
			var defaultStyle = new $.wijmo.wijspread.Style();
			defaultStyle.hAlign = $.wijmo.wijspread.HorizontalAlign.left;
			defaultStyle.vAlign = $.wijmo.wijspread.VerticalAlign.center;
			//defaultStyle.padding = $.wijmo.wijspread.Padding(5, 5, 5, 5);
			defaultStyle.font = "9pt Arial";
			
			spread.addSheet($scope.model.pageCurrent,new $.wijmo.wijspread.Sheet("page" + $scope.model.pageCurrent));
			
			spread.setActiveSheet("page" + $scope.model.pageCurrent);
			
			var sheet = spread.getActiveSheet();
			
			sheet.isPaintSuspended(true);
			
			sheet.setDataSource(data);
			sheet.selectionUnit($.wijmo.wijspread.SelectionUnit.Row);
			sheet.selectionBorderColor("Accent 1");
			sheet.defaults.rowHeight = 30;
			sheet.setDefaultStyle(defaultStyle, $.wijmo.wijspread.SheetArea.viewport);
			

		/* ------: Column headers :------ */
			
			if($scope.model.hideheader === 1){
				
				sheet.setRowHeight(0, 0,$.wijmo.wijspread.SheetArea.colHeader);
				
			} else {
			
				sheet.setRowHeight(0, 40.0,$.wijmo.wijspread.SheetArea.colHeader);
				
			}
			
			
		/* ------: Row numbers :------ */
			
			if($scope.model.hiderownumbers === 1){
				
				sheet.setRowHeaderVisible(false);
				
			}
			
			
		/* ------: Custom column width :------ */
			
			//sheet.setColumnWidth(0, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(1, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(2, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			//sheet.setColumnWidth(3, 120.0,$.wijmo.wijspread.SheetArea.viewport);
			
			
		/* ------: Column title mapping :------ */
			
			// Create non-visible column (this is necessary to fix a bug when first column and first row formatting is different from other)
			
			sheet.addColumns(0, 1);
			sheet.setColumnWidth(0, 0,$.wijmo.wijspread.SheetArea.viewport);
			
			// Let's map
			
			var cols = 1;
			var total = 0;
			
			$.each(data[0], function(colHeaderText, value) {
				
				//var colHeaderText = sheet.getValue(0, cols, $.wijmo.wijspread.SheetArea.colHeader); 
			
				var column = angular.copy($scope.model.columns[colHeaderText]);
				
				try {
					
					if (column["Field title"].length > 0) {
						
						colHeaderText = column["Field title"];
						
					} 
					
				} catch(error) {
					
					//colHeaderText = "Unknown";
					
				}
				
				try {
					
					switch (column.Type) {
						
						case "double":
							
							// Align field to right
							sheet.getColumn(cols).hAlign($.wijmo.wijspread.HorizontalAlign.right);	
							
							// Two decimalplaces 
							// http://sphelp.grapecity.com/webhelp/SpreadHClientUG/Spread~$.wijmo.wijspread.FormatMode.html
							sheet.getColumn(cols).formatter(new $.wijmo.wijspread.GeneralFormatter("N", $.wijmo.wijspread.FormatMode.StandardNumericMode));
							
						break;
						
						case "integer":
							
							sheet.getColumn(cols).hAlign($.wijmo.wijspread.HorizontalAlign.right);	
							//sheet.getColumn(cols).formatter(new $.wijmo.wijspread.GeneralFormatter("N", $.wijmo.wijspread.FormatMode.StandardNumericMode));
							
						break;						
						
					}
					
				} catch(error){
					
					/*
					dhtmlx.message({
						type: "error",
						text: error,
						expire: 10000
					});					
					*/
					
				}
				
				
				
				// Cell left side padding
				sheet.getColumn(cols).textIndent(1);
				
				sheet.setValue(0, cols, "          " + colHeaderText + "      ", $.wijmo.wijspread.SheetArea.colHeader);
				sheet.autoFitColumn(cols);
			
				// Cell right side padding
				sheet.setColumnWidth(cols, sheet.getColumnWidth(cols, $.wijmo.wijspread.SheetArea.viewport) + 6,$.wijmo.wijspread.SheetArea.viewport);
			
				total = total + sheet.getColumnWidth(cols, $.wijmo.wijspread.SheetArea.viewport) + 45;
			
				sheet.setFormatter(0, 0, "# ??/??");
			
				cols += 1;
				
			});
			
			
			// TODO : set last column to max width if there is an empty space
			

			/*
			
			if(total < $( document ).innerWidth()){
				
				var diff = $(document ).innerWidth() - total;
				sheet.setColumnWidth(cols - 1, sheet.getColumnWidth(cols - 1, $.wijmo.wijspread.SheetArea.viewport) + diff,$.wijmo.wijspread.SheetArea.viewport);				
				
			}
			*/
			
			
			/*
			var row = sheet.getRow(0, $.wijmo.wijspread.SheetArea.colHeader);
			row.backColor("Red");
			row.foreColor("White");
			*/
			
			var rows = sheet.getRowCount();
			
			var cellrange =new $.wijmo.wijspread.Range(0, 0, rows, cols);
			var hideRowFilter =new $.wijmo.wijspread.HideRowFilter(cellrange);
			sheet.rowFilter(hideRowFilter);
			
			
			
			
			spread.useWijmoTheme = true;
			
			
			$scope.presentation.pages["page" + $scope.model.pageCurrent] = {
				
				created : new Date()
				
			};
			

		/* ------: Zoom :------ */

			// TODO : user can zoom in/out holding CTRL and scrolling mouse wheel
			// Implement horizontal slidebar to do the same

			spread.allowUserZoom = true;
			
			if($scope.model.zoom > 0) {
				
				sheet.zoom($scope.model.zoom);
				
			} else {
				
				sheet.zoom(1.2);
				
			}
			
		/* ------: Paint :------ */
			
			sheet.isPaintSuspended(false);
			spread.repaint();
			

			
		};
		
		$scope.render = function(data){
			
			if($scope.model.rerender === true) {
				
				$scope.model.rerender = false;	
				$scope.resetSpread();
				$scope.createPage(data);
				
			} else {
					
				if($scope.presentation.pages["page" + $scope.model.pageCurrent] !== undefined) {
					
					$scope.presentation.spread.setActiveSheet("page" + $scope.model.pageCurrent);
					console.log("Load from cache = " + $scope.model.pageCurrent);
					
				} else {
					
					$scope.createPage(data);
					console.log("Render new = " + $scope.model.pageCurrent);
					
				}
				
			}
			
		};
		
	/* ------: Model :------ */
		
		$scope.$watch('model.dataset', function(data){
			
			if(data !== undefined) {
				
				if(data.length === 0) {
					
					$scope.dataEmpty();
					
				} else {
					
					$scope.presentation.spread = $("#content").wijspread("spread");
					$scope.render(data);
					
				}
				
			}
			
		});
		
	});
	

// EOF