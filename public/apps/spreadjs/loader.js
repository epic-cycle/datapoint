
// Loading resources

ljs

	.addAliases({

	// Application

	    vendors	:
					    [
					    	"/deps/main.js",
					    ],

	    functions	:
					    [

					    ],
		    
	    controllers	:
					    [
							"/apps/spreadjs/controllers/Spreadjs.js",	
					    ],
					    	
		directives	:	[
			
						],
		
	    services	:
					    [

					    ],									
			    
	    app			:
					    [
					    	"/apps/spreadjs/app.css",
							"/apps/init.js?" + Date.now()
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'jqueryui',
		'spreadjs',
		'timer',
		'angular',
		'dhtmlx',
		
		/* ---: Local :--- */
		
		'controllers',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(500);
			
			function disableF5(e) {
				
				if ((e.which || e.keyCode) == 116) {
					
					e.preventDefault();
					
				}
				
			}
			
			$(document).ready(function(){
				
				$(document).on("keydown", disableF5);
				
			});			
			
		}	
	)
;

// EOF
