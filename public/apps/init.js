/**
 *	Application initialization
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

	var app;

	$.getJSON($("html").attr('app')+"?" + Date.now(), function(application) {
		
		(function(orig) {
		    angular.modules = [];
		    angular.module = function() {
		        if (arguments.length > 1) {
		            angular.modules.push(arguments[0]);
		        }
		        return orig.apply(null, arguments);
		    }
		})(angular.module);		
		
		app = angular.module(application.name, application.modules, 
			
			function($interpolateProvider) {
				
				$interpolateProvider.startSymbol(application.interpolators.start);
				$interpolateProvider.endSymbol(application.interpolators.end);
				
			}
			
		)
		
		.config(['$httpProvider', function($httpProvider) {
			
			var headers = $httpProvider.defaults.headers;
			headers.common["X-Requested-With"] = 'XMLHttpRequest';
			
		}])
		
		.run(function($rootScope) {
			
			console.log("Application " + application.name + " started.");
			if(typeof run === "function") run();
			
		});
		
		/* ------: Parent scope :------ */
			
		if(window.frameElement) {
			
			app.factory('$parentScope', function($window) {
				return $window.parent.angular.element($window.frameElement).scope();
			});
			
			app.provider('$rootScope', function () {
				this.$get = ['$window', function ($window) {
					return $window.parent.angular.element($window.frameElement).scope().$new();
				}];
			});				
			
		} else {
			
			app.factory('$parentScope', function($window) {
				return false;
			});		
			
		}
		
		angular.element(document).ready(function() {
			
			angular.bootstrap(document, [application.name]);
			
		});
		
	});
	
// EOF