
// Loading resources

ljs

	.addAliases({

	// Third party added in /js/deps.js

	// Application

	    functions	:
					    [
							"/apps/gridstack/functions.js?" + Date.now()
					    ],
		    
	    controllers	:
					    [
							"/js/controllers/Components.js",
							"/js/controllers/Objects.js",
							"/apps/gridstack/controllers/Gridstack.js",
					    ],
					    	
		directives	:	[
							"/js/directives/d3menu.js",
							"/js/directives/dock.js",
							"/js/directives/editview.js",
							"/js/directives/mounts.js",
							"/js/directives/picture.js",			
							"/js/directives/users.js",							
							"/js/directives/roles.js",	
							"/js/directives/daterange.js",
							"/js/directives/editdata.js",
							"/js/directives/filter.js",
							"/js/directives/datagrid.js",
							"/js/directives/paginator.js",
							"/js/directives/url.js",
							"/js/directives/singlestat.js",
							"/js/directives/modelbrowser.js"
							
						],

	    services	:
					    [
							"/js/services/Object.js"
					    ],									
			    
	    app			:
					    [
							"/apps/init.js?" + Date.now()							
					    ],									
	    
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'jquery',
		'jqueryui',
		'jquerytouchp',
		'lodash',
		'momentjs',
		'gridstack',
		'chance',
		'angular',
		'angularbs',
		'dhtmlx',
		
		/* ---: Local :--- */
		
		'functions',
		'controllers',
		'directives',
		'services',
		'app',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(500);
			
		}	
	)
;

// EOF