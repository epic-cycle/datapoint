
var widgets = {};

function addComponent(component){

    var serialization = [{
		x: 0, 
		y: 0,
		width: 2,
		height: 2,
		header: 1,
		id: "cmp-" + chance.guid(),
		component: component,
		label: component,
		settings : "{}"
    }];

	serialization = GridStackUI.Utils.sort(serialization);
	
	var grid = $('.grid-stack').data('gridstack');

	_.each(serialization, function (node) {
		
		var html = getComponentHTML(node);
		widgets[node.id] = grid.addWidget($(html), node.x, node.y, node.width, node.height);
		setAttributes(widgets[node.id], node);
		
	});
	
}

function getComponentHTML(node){

	var html = ' \
		<div data-gs-min-height="0"> \
			<div class="grid-stack-item-content" ondblclick="componentSettings(\'' + node.id + '\');"> \
				<div style="display:table;width:100%;"> \
					<div style="text-align:left;padding-left:3px;display:table-cell;font-size:15px;vertical-align:middle;"> \
						<label for="zz"><span onclick="setLabel(\'' + node.id + '\')" style="cursor:pointer;background-color:#156100;" id="label-' + node.id + '" class="label label-default">' + node.label + '</span>&nbsp;<span id="zz" onclick="componentSettings(\'' + node.id + '\');" style="cursor:pointer" class="label label-default">Settings</span>&nbsp;<span id="zz2" onclick="componentView(\'' + node.id + '\');" style="cursor:pointer" class="label label-default">View</span></label> \
					</div> \
					<div style="padding-right:5px;width:100%;text-align:right;display:table-cell;font-size:15px;vertical-align:middle;"> \
						<a id="yy" href="javascript:remove(\'' + node.id + '\');"><img src="/resources/shared/imgs/delete-18px.png"></a> \
					</div> \
				</div> \
			</div> \
		</div> \
	';			

	return html;
}

function setLabel(id){
	
 	window.parent.controller.editview.setLabel(id);

}

function componentSettings(id){
	
	window.parent.controller.editview.componentSettings(id);
	
}

function componentView(id){
	
	window.parent.controller.editview.componentView(id);
	
}

function setAttributes(component, node) {
	
	component.attr('id', node.id);
	component.attr('data-component', node.component);
	component.attr('data-model', node.model);
	component.attr('data-header', node.header);
	component.attr('data-label', node.label);
	component.attr('data-settings', JSON.stringify(node.settings));
	component.attr('data-view', JSON.stringify(node.view));	
	//$("#model-" + node.id).val(node.model);
	//$("#header-" + node.id).prop("checked", $.parseJSON(node.header));
}
	
function remove(id) {
	var grid = $('.grid-stack').data('gridstack'); 
	grid.removeWidget(document.getElementById(id));
}

$('.grid-stack').on('change', function (e, items) {

	

});