/**
 *	Module for controller "Gridstack"
 * 
 *  @doc	module
 * 	@name	controller.Gridstack
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Gridstack', [])

/* -------------------------------- */

	/**
	 *	Controller to operate with Data sources
	 * 
	 *	@doc	controller
	 * 	@name	Gridstack
	 *	@duty	Rolands Strickis
	*/

/* ------: Controller Gridstack :------ */

	.controller('Gridstack', function($scope, $controller, $attrs, Object) {
		
	/* ------: Extend controller :------ */		
		
		$controller('Objects', {$scope: $scope, $attrs: $attrs});
		
		
	/* ------: Initialize Gridstack :------ */
		
		$scope.initGridstack = function(response){
			
			if(response !== undefined) {
				
				$('.grid-stack').gridstack({
					width: 12,
					verticalMargin: 2,
					alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
					resizable: {
						handles: 'e, se, s, sw, w'
					}
				});
				
				var serialization = JSON.parse(response.data);
				
				serialization = GridStackUI.Utils.sort(serialization);
				
				var grid = $('.grid-stack').data('gridstack');    
				
				grid.removeAll();
				
				
				var components = [];		
				
				_.each(serialization, function (node) {
					
					components.push({
						
						"id": node.id,
						"component": node.component
						
					});
					
				});	 
				
				_.each(serialization, function (node) {
					
					var html = getComponentHTML(node);
					widgets[node.id] = grid.addWidget($(html), node.x, node.y, node.width, node.height);
					setAttributes(widgets[node.id], node);
					
				});	 
			}
			
		};
		
	/* ------: Save Gridstack :------ */
		
		$scope.saveGridstack = function (target){
			
			var view = $scope.getView(target);
			
			$scope.saveObject(view);
			
		};
		
		
		$scope.getView = function (target){
			
			var view = 
			{
				id : $(".grid-stack").attr("viewid"),
				type : "view",
				target : target,
				data: $scope.getJSON()
			};
			
			return view;			
			
		};
		
		
		$scope.getJSON = function (){
			
			var res = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
				el = $(el);
				var node = el.data('_gridstack_node');
				var settings = {};
				
				try {
					
					settings = JSON.parse(el.attr('data-settings'));
					
				} catch(e) {
					
					settings = {};
					
				}
				
				try {
					
					view = JSON.parse(el.attr('data-view'));
					
				} catch(e) {
					
					view = {};
					
				}				
				
				return {
					id: el.attr('id').replace("gs-", ""), // remove prefix "gs-"
					x: node.x,
					y: node.y,
					width: node.width,
					height: node.height,
					component: el.attr('data-component'),
					model: el.attr('data-model'),
					header: el.attr('data-header'),
					label: el.attr('data-label'),
					settings: settings,
					view: view
				};
			});
			
			return JSON.stringify(res);			
			
		};
		
	});	

// EOF