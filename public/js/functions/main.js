
// Permutations

function permutations(arr) {
	
	if (arr.length === 0) {
		
		return [];
		
	} else if (arr.length === 1) {
		
		return arr[0];
		
	} else {
		
		var result = [];
		var allCasesOfRest = permutations(arr.slice(1));
		for (var c in allCasesOfRest) {
			for (var i = 0; i < arr[0].length; i++) {
				result.push(arr[0][i] + "." + allCasesOfRest[c]);
			}
		}
		return result;
		
	}
	
}

// Convert rows from associative to numeric

function convertRowsAssocToNum(obj) {
	
    var numericArray = [];
    
    for (var item in obj.rows){
		
        numericArray.push(obj.rows[item]);
        convertRowsAssocToNum(obj.rows[item]);
        
    }
    obj.rows = numericArray;
    
	return obj;
}

function convertDataAssocToNum(obj) {
	
    var numericArray = [];
    
    for (var item in obj){
        numericArray.push(obj[item]);
    }
    
    obj = numericArray;
    
	return obj;
	
}

// Remove empty arrays

function removeEmptyArrays(obj) {

	var isEmpty = true;

	for (var key in obj) {
		var val = obj[key];
		if (val === null || typeof val !== 'object' || (obj[key] = removeEmptyArrays(val))) {
			isEmpty = false;
		} else {
			delete obj[key];
		}
    }

    return isEmpty ? undefined : obj;
}

function toggleEditView(active){

	var pe = "auto";
	var cu = "move";
	
	if(active === false) {
		pe = "auto";
		cu = "auto";
		window.tgs = false;	
	} else {
		if(window.tgs === false) {
			pe = "none";
			cu = "move";
			window.tgs = true;
		} else {
			pe = "auto";
			cu = "auto";
			window.tgs = false;
		}
	}
	
	try {
		
		//el.innerHTML = "Edit grid = " + window.tgs;
		var grid = $('.grid-stack').data('gridstack');  
		grid.resizable($(".gscomponent"), window.tgs);
		grid.movable($(".gscomponent"), window.tgs);	
		$(".gscomponent").css( "opacity", 1 );
	
		$('.dhxwin_active').css("pointer-events", pe); 
		$('.viewport').css("cursor", cu); 
		
	} catch (error) {
		
		console.log(error);
		
	}
}
