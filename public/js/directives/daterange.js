
/* -------------------------------- */

	id = "daterange";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;font-size:14px;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:#f4fcf2;"> \
						<div class="layout"> \
							<div class="daterange pull-right" style="text-align:right;height:100%;background: transparent; cursor: pointer; padding: 18px; border: 0px solid #ccc;width:100%;"> \
							    <span class="pull-left" style="display:none;"><span onclick="alert(1);preventDefault();">1st quarter</span> | 2nd quarter | 3rd quarter | 4th quarter</span>  &nbsp;&nbsp;\
							    <i class="glyphicon glyphicon-calendar"></i>&nbsp; \
							    <span class="daterange-label" style="font-size:14px;font-weight:bold;"></span> <b class="caret"></b> \
							</div> \
						</div> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* -------------------------------- */		    	
				
				$attrs.id = chance.guid();
				$element.attr("id", $attrs.id);
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				$element.find('.layout').attr("id", "layout-" + $attrs.id);
				$element.find('.daterange').attr("id", "daterange-" + $attrs.id);
				$element.find('.daterange-label').attr("id", "daterange-label-" + $attrs.id);
				
			/* -------------------------------- */		    	
				
				$scope.model = {
					
					startDate : null,
					endDate : null,
					chosenLabel : "Last 10 min",
					field : JSON.parse($attrs.settings).field,
					datagrid : JSON.parse($attrs.settings).datagrid,
					autoapply : JSON.parse($attrs.settings).autoapply
					
				};
				
			/* -------------------------------- */
				
				$scope.init = function(){
					
					var picker = $("#daterange-" + $attrs.id).data('daterangepicker');
					
					switch($scope.model.chosenLabel){
						
						case "Last 10 min":
							
							$scope.model.startDate = moment().subtract(10, 'minutes').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().format('YYYY-MM-DD HH:mm:ss');
							
						break;
						
						
						case "Last 1 hour":
							
							$scope.model.startDate = moment().subtract(1, 'hour').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						
						case "Today":
							
							$scope.model.startDate = moment().startOf('day').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().endOf('day').format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						
						case "Yesterday":
							
							$scope.model.startDate = moment().subtract(1, 'days').startOf('day').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().subtract(1, 'days').endOf('day').format('YYYY-MM-DD HH:mm:ss');
							
						break;		
						
						
						case "Last 7 Days":
							
							$scope.model.startDate = moment().subtract(6, 'days').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						
						case "Last 30 Days":
							
							$scope.model.startDate = moment().subtract(29, 'days').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						
						case "This Month":
							
							$scope.model.startDate = moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().endOf('month').format('YYYY-MM-DD HH:mm:ss');
							
						break;		
						
						
						case "Last Month":
							
							$scope.model.startDate = moment().subtract(1, 'month').startOf('month').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().subtract(1, 'month').endOf('month').format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						case "This Year":
							
							$scope.model.startDate = moment().startOf('year').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().endOf('year').format('YYYY-MM-DD HH:mm:ss');
							
						break;	
						
						case "Last Year":
							
							$scope.model.startDate = moment().subtract(1, 'year').startOf('year').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().subtract(1, 'year').endOf('year').format('YYYY-MM-DD HH:mm:ss');
							
						break;							

						case "From the beginning":
							
							$scope.model.startDate = moment().subtract(20, 'year').format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = moment().format('YYYY-MM-DD HH:mm:ss');
							
						break;							
						
						case "Custom Range":
							
							$scope.model.startDate = picker.startDate.format('YYYY-MM-DD HH:mm:ss');
							$scope.model.endDate = picker.endDate.format('YYYY-MM-DD HH:mm:ss');							
							
						break;
						
					}
					
					//$scope.model.chosenLabel = $("#daterange-label-" + $attrs.id).html();
					
				};
				
			/* -------------------------------- */			
				
				$("#daterange-" + $attrs.id).daterangepicker({
					
					locale: {
						format: "YYYY-MM-DD",
					},
					linkedCalendars: false,
					startDate: moment().subtract(10, 'minutes'),
					endDate: moment(),
					ranges: {
						'Last 10 min': [moment().subtract(10, 'minutes'), moment()],
						'Last 1 hour': [moment().subtract(1, 'hour'), moment()],
						'Today': [moment().startOf('day'), moment().endOf('day')],
						'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
						'Last 7 Days': [moment().subtract(6, 'days'), moment()],
						'Last 30 Days': [moment().subtract(29, 'days'), moment()],
						'This Month': [moment().startOf('month'), moment().endOf('month')],
						'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
						'This Year': [moment().startOf('year'), moment().endOf('year')],
						'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],						
						'From the beginning': [moment().subtract(20, 'year').startOf('month'), moment()]
					}
					
				});
				
			/* -------------------------------- */
				
				// Probably bug, need to reinitialize to show time picker and do not show custom range opened
				
				$("#daterange-" + $attrs.id).daterangepicker({
					
					locale: {
						format: "YYYY-MM-DD",
					},
					timePicker: true,
					linkedCalendars: false,
					startDate: moment().subtract(10, 'minutes'),
					endDate: moment(),
					ranges: {
						'Last 10 min': [moment().subtract(10, 'minutes'), moment()],
						'Last 1 hour': [moment().subtract(1, 'hour'), moment()],
						'Today': [moment().startOf('day'), moment().endOf('day')],
						'Yesterday': [moment().subtract(1, 'days').startOf('day'), moment().subtract(1, 'days').endOf('day')],
						'Last 7 Days': [moment().subtract(6, 'days'), moment()],
						'Last 30 Days': [moment().subtract(29, 'days'), moment()],
						'This Month': [moment().startOf('month'), moment().endOf('month')],
						'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
						'This Year': [moment().startOf('year'), moment().endOf('year')],
						'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],						
						'From the beginning': [moment().subtract(100, 'year').startOf('month'), moment()]
					}
					
				});
				
			/* -------------------------------- */			
				
				$("#daterange-" + $attrs.id).on('apply.daterangepicker', function(ev, picker) {
					
					if(picker.chosenLabel !== "Custom Range"){
						
						$("#daterange-label-" + $attrs.id).html(picker.chosenLabel);
						$("#daterange-label-" + $attrs.id).prop('title', picker.startDate.format('YYYY-MM-DD HH:mm:ss') + " - " + picker.endDate.format('YYYY-MM-DD HH:mm:ss'));
						
					} else {
						
						$("#daterange-label-" + $attrs.id).html(picker.startDate.format('MMMM D, YYYY') + ' - ' + picker.endDate.format('MMMM D, YYYY'));
						
					}
					
					$scope.model.chosenLabel = picker.chosenLabel;
					
					if($scope.model.autoapply === 1){
						
						$rootScope.$broadcast($scope.model.datagrid);
						
					} else {
						
						 $("#viewport-" + $attrs.id).css('background-color',"#ffe8e0");
						
					}
					
				});
				
			/* -------------------------------- */
				
				$scope.init();
				$("#daterange-label-" + $attrs.id).html("Last 10 min");
				
				
			/* ------: Response :------ */
				
				$rootScope.$on("daterange.request", function(event, source){
					
					$scope.init();
					
					$rootScope.$broadcast(source + ".response",{
						component : "daterange", // $element[0].localName ???
						data : $scope.model
					});
					
					if($scope.model.autoapply === 0){
						
						$("#viewport-" + $attrs.id).css('background-color',"#f4fcf2");
						
					}
					
				});
				
				
			/* ------: Apply configuration :------ */
				
				$rootScope.$on($attrs.cmp + ".apply", function (event, data) {
					
					$.each(data, function( index, value ) {
						
						$scope.model[index] = value;
						
					});
					
				});				
				
			}
		};
	});
	
// EOF