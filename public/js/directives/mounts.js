/**
 *	Component to edit mounts
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Settings :------ */

	id = "mounts";
	
	
/* ------: Module :------ */
	
	angular.module("directive." + id, [])
		
		
	/* ------: Directive :------ */
		
		.directive(id, function() {
			
			return {
				
			/* ------: Properties :------ */
				
				restrict: 'AE',
				replace: false,
				controller: 'Mounts',
				scope: true,
				
				
			/* ------: Template :------ */	       
				
				template: ' \
					<div style="height:100%;width:100%;"> \
						<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
						</div> \
					</div> \
				',
				
				
			/* ------: Link function :------ */	  	    
				
				link: function($scope, $element, $attrs) {
					
				/* ------: Attributes :------ */		    	
					
					var attrs = {
						viewport : "viewport-" + $attrs.id,
						guid : $attrs.id,
					};					
					
					$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
					
					
				/* ------: Create window :------ */
					
					$scope.construct($element, attrs);
					
					
				/* ------: Attach Layout to Window :------ */
					
					$scope.layout = $scope.window.attachLayout("1C");
					$scope.layout.cells("a").setText("List of items");
					
					
				/* ------: Attach Toolbar to Layout :------ */
					
					$scope.toolbar = $scope.layout.attachToolbar({
						
						icons_path: "/resources/components/mounts/imgs/",
						xml: "/resources/components/mounts/toolbar.xml"
						
					});
					
					
				/* ------: Attach Treegrid to Layout :------ */
					
					$scope.treeGridMounts = $scope.layout.cells("a").attachGrid();
					
					
				/* ------: Attach Toolbar button events :------ */
					
					$scope.toolbar.attachEvent("onClick", function(id) {
						
						switch(id) {
							
							case "create":
								$scope.storeMount();
							break;
							
							case "delete":
								$scope.deleteMount();
							break;
							
							case "editasview":
								$scope.editView();
							break;
							
							case "editasmodel":
								$scope.editModel();
							break;
							
							case "editassource":
								$scope.editSource();
							break;						        
							    
						}
						
					});		
					
				}
			};
		});
		
	/* ------: end of directive :------ */	
	
// EOF