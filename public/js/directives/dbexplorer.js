
/* -------------------------------- */

	id = "dbexplorer";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope, $controller, Databuilder, Data) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
 			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
						asas \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
			
			/* ------: Attributes :------ */		    	
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};					
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				
			/* ------: Create window :------ */
				
				$scope.construct($element, attrs);
				
				
			/* ------: Attach Layout to Window :------ */
				
				$scope.layout = $scope.window.attachLayout("1C");
				$scope.layout.cells("a").setText("Tables");
				$scope.layout.cells("a").hideHeader();
				
				
			/* ------: Attach toolbar :------ */
				
				var tbconf =  [
					{
						id:     "expand",
						type:   "button",
						img:	"expand-18px.png",
						text:   "Expand",
					},
					{
						id:     "collapse",
						type:   "button",
						img:	"collapse-18px.png",
						text:   "Collapse",
					},
				];
				
				$scope.toolbar = $scope.windows.window(attrs.guid).attachToolbar({
					icons_path: "/resources/shared/imgs/",
					align: "left",
					json : tbconf
				});
				
				
			/* ------: Attach event to toolbar :------ */
				
				$scope.toolbar.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "expand":	
							
							$scope.treeGridTables.expandAll();
							
						break;
						
						
						case "collapse":
							
							$scope.treeGridTables.collapseAll();
							
						break;
						
					}
					
				});
				
				
			/* ------: Attach Treegrid to Layout :------ */
				
				$scope.treeGridTables = $scope.layout.cells("a").attachGrid();	
				
				
			/* ------: Attach events to Treegrid :------ */
				
				$scope.treeGridTables.attachEvent("onRowDblClicked", function(rId,cInd){
				    
				    if(rId.indexOf("::") !== -1) {
				    	
				    	var res = rId.split("::");
				    	
				    	rId = res[1];
				    	
				    }
				    
				    $rootScope.$broadcast('scanner-started', { any: rId });
				    
				});
				
				
			/* ------: Get schema :------ */
				
				var schema = {};
				
				Databuilder.fields({
										
					id : [21],
					table : ':all'
					
				})
				
				.success(function(response) {
					
					schema.rows = $.map(response.data, function(fields, table) {
						
						var columns = $.map(fields, function(field) {
							
							return {
								
								id : table + '::' + field, 
								data : [field]
								
							};
							
						});
						
						return {
							id : table,
							data :
							[
								{"value" : " " + table, "image":"folder.gif"}
								
							],	
							rows : columns
						};
						
					});	
					
					$scope.treeGridTables.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
					$scope.treeGridTables.setIconsPath("/vendor/proprietary/dhtmlx/codebase/imgs/dhxgrid_material/tree/");
					$scope.treeGridTables.setHeader("Name");
					$scope.treeGridTables.attachHeader(
					    "#select_filter,#cspan,#cspan",
					    ["background-color:#f7f8f9;","background-color:#f7f8f9;"]
					);
					$scope.treeGridTables.setInitWidths("*");
					$scope.treeGridTables.setColAlign("left");
					$scope.treeGridTables.setColTypes("tree");
					$scope.treeGridTables.setColSorting("str");
					$scope.treeGridTables.enableDragAndDrop(false);
					$scope.treeGridTables.enableMultiselect(false);
					$scope.treeGridTables.enableTreeCellEdit(false);
					
					$scope.treeGridTables.init();
					$scope.treeGridTables.setSkin("material");
					$scope.treeGridTables.parse(schema, "json");
					$scope.treeGridTables.sortRows(0,"str","asc");
					
				})
				
				.error(function(response) {
					
					alert("Error to get fields2");
					
				});	
				
			}
			
		};
		
	});
	
// EOF