
/* -------------------------------- */
	id = "users";
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function(User) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;background-color:white;font-size:12px;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Attributes :------ */
				
				$attrs.id = chance.guid();
				$element.attr("id", $attrs.id);
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				
				
			/* ------: Attach application :------ */				
				
				$scope.windows.window($attrs.id).attachURL("/app/users/#?winid=" + $attrs.id);
				
			}
		};
	});
	
// EOF