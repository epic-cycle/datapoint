
/* -------------------------------- */

	id = "singlestat";
	
/* -------------------------------- */
	
	angular.module("directive." + id, [])
		
		.directive(id, function($rootScope, Data) {
			
			return {
				
			/* -------------------------------- */
				
				restrict: 'AE',
				replace: false,
				scope: true,
				
			/* -------------------------------- */		    
				
				template: ' \
					<div style="height:100%;width:100%;background-color:#282c36;overflow:hidden;color:#e8e8e8;"> \
						<div class="viewport" style="display:table;height:100%;width:100%;background-color:white;vertical-align:middle"> \
							<div class="container" style="display:table-row;width:100%;font-size:13px;height:100%;vertical-align:middle"> \
								<div class="singlestat"style="font-family:arial;text-align:left;padding:20px;width:100%;display:table-cell;height:100%;vertical-align:middle"> \
									<span class="singlestatlabel"></span> \
									<span class="singlestatvalue"></span> \
								</div> \
							</div> \
						</div> \
					</div> \
				',
				
			/* -------------------------------- */		    
				
				link: function($scope, $element, $attrs) {
					
				/* ------: Attributes :------ */
					
					$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
					$element.find('.singlestat').attr("id", "singlestat-" + $attrs.id);
					$element.find('.container').attr("id", "container-" + $attrs.id);
					$element.find('.singlestatlabel').attr("id", "singlestatlabel-" + $attrs.id);
					$element.find('.singlestatvalue').attr("id", "singlestatvalue-" + $attrs.id);
					
					
				/* ------: Transaformations :------ */
					
					var interpolators = {
						
						original : JSON.parse($attrs.settings).interpolators || [],
						transformed : {}
						
					};
					
					$.each(interpolators.original, function( index, value ) {
						interpolators.transformed[interpolators.original[index].key] = interpolators.original[index].value;
					});
					
				/* ------: Model :------ */
					
					$scope.model = {
						
						previous : null,
						reloadinterval : JSON.parse($attrs.settings).reloadinterval,
						label : JSON.parse($attrs.settings).label,
						showdiff : JSON.parse($attrs.settings).showdiff,
						backgroundcolor : JSON.parse($attrs.settings).backgroundcolor,
						broadcast : JSON.parse($attrs.settings).broadcast,
						subscribe : JSON.parse($attrs.settings).subscribe,
						transform : JSON.parse($attrs.settings).transform,
						interpolators : interpolators.transformed,
						id : JSON.parse($attrs.settings).model,
						data : undefined
						
					};
					
					$("#container-" + $attrs.id).css("background-color", $scope.model.backgroundcolor);
					
					
				/* ------: Load data :------ */
					
					$scope.$on('load', function(event, data) {
						
							// Transform
							
							switch ($scope.model.transform) {
								
								case "humantime" :
									
									if(moment().unix() > moment(data, 'YYYY-MM-DD HH:MM:ss').unix()){
										
										$scope.model.label = "Event started ";
										
									} else {
										
										$scope.model.label = "Event will start ";
										
									}
									
									data = moment(data).fromNow();	
									
								break;
								
								case "changecolor" :
									
									data = parseFloat(data);
									if (data === 0) {
										
										data = "<span style='color:white;font-family:\"Product Sans\"'>" + data + "%</span>";
										
									}
									else if(data > 0) {
										
										data = "<span style='color:#64d450;font-family:\"Product Sans\"'>+" + data + "%</span>";
										
									} else {
										
										data = "<span style='color:#ff6661;font-family:\"Product Sans\"'>" + data + "%</span>";
										
									}
									
									$("#singlestat-" + $attrs.id).boxfit({maximum_font_size:40});
									
								break;
								
								case "optibetlabel" :
									
									$scope.model.label = "<span style='font-family:\"Product Sans\"'>" + $scope.model.label + "</span>";
									
									
									$("#singlestat-" + $attrs.id).boxfit({
										maximum_font_size:30,
										align_center:false
									});
									
								break;	
								
								case "optibetheader" :
									
									$scope.model.label = "<span style='font-family:\"Product Sans\"'>" + $scope.model.label + "</span>";
									
									
									$("#singlestat-" + $attrs.id).boxfit({
										align_center:true,
										align_middle:true,
										maximum_font_size:40
									});
									
									//$("#singlestat-" + $attrs.id).css("vertical-align","bottom");
									
									
								break;	
								
								case "optibetheader2" :
									
									$scope.model.label = "<span style='font-family:\"Product Sans\"'>" + $scope.model.label + "</span>";
									
									
									$("#singlestat-" + $attrs.id).boxfit({
										align_center:false,
										align_middle:true,
										maximum_font_size:40
									});
									
									
								break;									
								
								case "optibettitle" :
									
									$scope.model.label = "<span style='font-family:\"Product Sans\"'>" + $scope.model.label + "</span>";
									
									
									$("#singlestat-" + $attrs.id).boxfit({
										align_center:false,
										align_middle:true,
										maximum_font_size:40
									});
									
									//$("#singlestat-" + $attrs.id).css("vertical-align","bottom");
									
									
								break;									
								
								case "optibetlogo" :
									
									$scope.model.label = "<span style='font-family:\"Product Sans\"'>" + $scope.model.label + "</span>";
									
									$("#singlestat-" + $attrs.id).boxfit({
										align_center:false,
										maximum_font_size:50
									});
									
								break;									
								
							}
							
							// Show
							
							if($scope.model.label.indexOf("{value}") !== -1) {
								
								$("#singlestatlabel-" + $attrs.id).html($scope.model.label.replace("{value}", data));
								
							} else {
								
								$("#singlestatlabel-" + $attrs.id).html($scope.model.label);
								$("#singlestatvalue-" + $attrs.id).html(data);
								
							}
							
							// check {{key}} and replace with data[key]
						
					});
					
					
				/* ------: Get data :------ */
					
					$scope.load = function(data) {
						
						if(parseInt($scope.model.id, 10) > 0) {
							
							Data.select({
								
								id : $scope.model.id,
								
								// TODO : get from broadcasting, else trigger this only when broadcasted (if setting)
								// else autoload
								
								interpolators : $scope.model.interpolators
								// response : array, object, string
								
							})
							
							.success(function(response) {
								
								// TODO : setting to specify key else get first (break in loop)
								// console.log(response.data[0].Event);
								
								if(response.data.length >0) {
									
									$.each(response.data[0], function(index, value) {
										
										var diff = {
											
											text : "",
											value : 0
											
										};
										
										if($scope.model.showdiff === 1) {
											
											if($scope.model.previous !== null) {
												
												diff.value = value - $scope.model.previous;
												
												
											} else {
												
												diff.value = 0;
												
											}
											
											$scope.model.previous = value;
											
											if(diff.value < 0) {
												
												$("#container-" + $attrs.id).css("background-color","#f4c0b5");
												
											} else if(diff.value >= 0) {
												
												$("#container-" + $attrs.id).css("background-color", $scope.model.backgroundcolor);
												
											}
											
											//$("#singlestatvalue-" + $attrs.id).html(value + " <span style='font-size:16px;'>(" + diff.value + ")<span>");
											
											$scope.$broadcast('load', value);
											
										} else {
											
											$scope.$broadcast('load', value);
											
										}
										
										if($scope.model.broadcast !== "") {
											
											$rootScope.$broadcast($scope.model.broadcast, {
												
												data: response.data
												
											});								
											
										}								
										
										return false;
										
									});
									
								}
								
								// TODO : if previous smaller/bigger
								
							})
								
							.error(function(response, status) {
								
								$("#singlestatvalue-" + $attrs.id).html("-");
									
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 10000
								});
								
							});	
							
						} else {
							
							$scope.$broadcast('load', data);
							
						}
						
					};
					
					
				/* ------: Set interval :------ */
					
					if($scope.model.reloadinterval > 0){
						
						$scope.refresh = setInterval(function(){ 
							
							// TODO : show loading if it takes more than x seconds
							
							//$("#singlestatlabel-" + $attrs.id).html("<img src='/resources/shared/imgs/loader-16px.gif'/>");
							
							$scope.load();
							
						}, $scope.model.reloadinterval * 1000);
						
					}
					
					
				/* ------: Do first load :------ */
					
					// TODO : only if allowed
					
					$scope.load();
					
					
				/* ------: Subscribe :------ */
					
					if($scope.model.subscribe !== "") {
						
						$rootScope.$on($scope.model.subscribe, function (event, data) {
							
							switch (event.name) {
								
								case "topevent" :
									
									var res = data.data[0].Event.split(" : ");
									
									$scope.model.interpolators = {
										
										sport : res[0],
										event_name : res[1]
										
									};
									
									$scope.load();
									
								break;
								
								case "topeventcoupons" :
									
									$scope.load(Math.round(100 * data.data[0].Coupons / data.data[0].Total));
									
								break;	
								
								case "topeventcouponspercent" :
									
									//$scope.load(); // load as input data
									
								break;								
								
							}
							
						});
						
					}
					
				}
				
			};
			
		});
		
// EOF