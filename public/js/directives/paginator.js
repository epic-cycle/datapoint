
/* -------------------------------- */
	id = "paginator";
/* -------------------------------- */

angular.module("directive." + id, ["ui.bootstrap"])

/* -------------------------------- */
	
	.directive(id, function($rootScope) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			controller: "Pagination", 
			scope: true,		    
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="width:100%;height:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:#F9FFEB;"> \
					</div> \
					<div class="pagination" style="background-color:#F9FFEB;width:100%;height:100%;text-align:center;font-size:14px;color:black;font-weight:bold;margin-top:-7px;"> \
						<ul items-per-page="model.itemsPerPage" ng-change="pageChanged()" uib-pagination total-items="model.totalItems" ng-model="model.currentPage" max-size="model.maxSize" class="pagination-sm" boundary-links="true" num-pages="model.numPages"></ul> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
			
			/* ------: Attributes :------ */
				
				$element.attr("id", $attrs.id);
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				$element.find('.pagination').attr("id", "pagination-" + $attrs.id);
				
				$attrs.viewport = "viewport-" + $attrs.id;
				$attrs.guid = $attrs.id;	
				
				
			/* ------: Data model :------ */
				
				$scope.model = {
					
					filterhash : undefined,
					totalItems : undefined,
					recordsperpage : undefined,
					currentPage : 1,
					maxSize : undefined,
					itemsPerPage : undefined,
					clicked : undefined,
					consensus : 0,
					counter : 0,
					brokers : {
						filter : null,
						daterange : null
					}					
					
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, $attrs);
				
				
			/* ------: Attach paginator to Window:------ */
				
				$scope.window.attachObject("pagination-" + $attrs.id);
				
			}
		};
	})
	
/* -------------------------------- */	

	.controller('Pagination', function ($rootScope, $scope, $attrs, $element, $window, $log, Data) {
		
	/* ------: Start timer :------ */
		
		$scope.startTimer = function(){
			
			$("#pagination-" + $attrs.id).hide();
			var width = $("#pagination-" + $attrs.id).width() + 2, height = $("#pagination-" + $attrs.id).height();
            $("<span id='delaySpan'><span id='icon' style='display:inline-block'></span>Calculating pages... <span style='font-size:14px;color:brown;' id='countup-" + $attrs.id + "'></span> <span style='font-size:14px;'>sec</span></span>")
                    .css("left", width / 2 - 70)
                    .css("top", height / 2 - 30)
                    .css("position", "absolute")
                    .css("color", "#4f4f4f")
                    .css("background", "#ffffff")
                    .css("border", "1px solid #a8a8a8")
                    .css("border-radius", "3px")
                    .css("-webkit-border-radius", "3px")
                    .css("box-shadow", "0 0 10px rgba(0, 0, 0, 0.25")
                    .css("font-family", "Arial, sans-serif")
                    .css("font-size", "16px")
                    .css("padding", "0.4em")
                    .css("z-index", "99999")		                    
                    .insertAfter("#pagination-" + $attrs.id);
            $("<div id='delayDiv'></div>")
                    .css("background", "#2D5972")
                    .css("opacity", 0.3)
                    .css("position", "absolute")
                    .css("top", 0)
                    .css("left", 0)
                    .css("width", width)
                    .css("height", height + 30)
                    .insertAfter("#pagination-" + $attrs.id);
				
			$("#countup-" + $attrs.id).runner({
				autostart: true,
				countdown: false,
				startAt: 0,
				milliseconds: true,
				format: function(value) {
					return (value / 1000).toFixed(2);
				}				
			});					
			
		};
		
		
	/* ------: Stop timer :------ */
		
		$scope.stopTimer = function(){
			
			$("#delayDiv").remove();
			$("#delaySpan").remove();					
			$("#pagination-" + $attrs.id).show();
			
		};
		
		
	/* ------: On page change :------ */
		
		$scope.pageChanged = function() {
			
			$scope.model.clicked = true;
			
			$rootScope.$broadcast(JSON.parse($attrs.settings).datagrid);
			
		};
		
		
	/* ------: Responsive :------ */
		
		$scope.adjustMaxsize = function(){
			
			// Get window width
			
			$scope.windowWidth = "innerWidth" in window ? window.innerWidth : document.documentElement.offsetWidth;
			
			// Change maxSize based on window width
			
			if($scope.windowWidth > 1000) {
				$scope.model.maxSize = 11; 		 
			} else if($scope.windowWidth > 800) {
				$scope.model.maxSize = 9;
			} else if($scope.windowWidth > 600) {
				$scope.model.maxSize = 7;
			} else if($scope.windowWidth > 400) {
				$scope.model.maxSize = 5;
			} else {
				$scope.model.maxSize = 2;
			}				
			
		};	
		
		
	/* ------: Request response :------ */
		
		$rootScope.$on($attrs.id + ".response", function(event, response){
			
			$scope.model.counter += 1; 
			
			$scope.model.brokers[response.component] = response.data;
			
			if($scope.model.counter === $scope.model.consensus) {
				
				$scope.stopTimer();
				
				$scope.startTimer();
				
				Data.count({
					
					// do not count if there is no range (could be checked also live tuples)
					
					// model
					id : JSON.parse($("#" + JSON.parse($attrs.settings).datagrid).attr("settings")).model,
					
					// filter
					filter: $scope.model.brokers.filter.filter,
					
					// daterange
					daterange : $scope.model.brokers.daterange
					
				})
				
				.success(function(response) {
					
					try {
					
						$scope.stopTimer();
						$scope.model.filterhash = $.md5(JSON.stringify($scope.model.brokers.filter.filter));
						$scope.model.totalItems = response[0].count;
						$scope.model.itemsPerPage = $scope.model.brokers.filter.limit;
						$scope.model.currentPage = 1;
						$scope.adjustMaxsize();
					
					} catch (error) {

						dhtmlx.message({
							type: "error",
							text: error,
							expire: 10000
						});
						
					}
					
				})		
				
				.error(function(response, status) {
					
					$scope.stopTimer();
					
					dhtmlx.message({
						type: "error",
						text: response.error,
						expire: 10000
					});
					
				});	
				
			}
			
		});
		
		
	/* ------: Trigger :------ */
		
		$rootScope.$on(JSON.parse($attrs.settings).datagrid, function(event, data){
			
			if($scope.model.clicked === true){
				
				$scope.model.clicked = false;
				
			} else {
			
				$scope.model.counter = 0;
				$scope.model.consensus = 2;	// this number should be get from component count
				
				$.each($scope.model.brokers, function(index) {
					
					$rootScope.$broadcast(index + ".request", $attrs.id);
					
				});
				
			}
			
		});		
		
		
	/* ------: Response :------ */
		
		$rootScope.$on("paginator.request", function(event, source){
			
			$rootScope.$broadcast(source + ".response",{
				component : "paginator",
				data : {
					page : $scope.model.currentPage
				}
			});
			
		});
		
		
	/* ------: On window resize :------ */
		
		angular.element($window).bind('resize', function () {
			
			$scope.adjustMaxsize();
			$scope.$apply();
			
		});
		
	});

/* -------------------------------- */

//EOF