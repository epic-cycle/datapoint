
/* -------------------------------- */

	id = "graph";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Model :------ */
				
				$scope.model = {
					
				};
				
				
			/* ------: Presentation :------ */
				
				$scope.presentation = {
					
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				$scope.windows.window(attrs.guid).attachURL("/app/graph/?" + new Date());
				
				
			/* ------: Presentation :------ */
				
				$scope.windows.window(attrs.guid).attachEvent("onContentLoaded", function(win){
				
				/* ------: Link :------ */
					
					$scope.presentation = win.getFrame().contentWindow;
					$scope.pres = win.getFrame();
					
					setTimeout(function() { 
						
						/* ------: Set chart type :------ */
							
							$scope.presentation.angular.element("#graph").scope().setChartType(JSON.parse($attrs.settings));
							
						/* ------: Attach events :------ */
							
							$($($scope.pres).contents().find("body")).on("click", function() {
								
								var settings = JSON.parse($attrs.settings);
								
								settings.initiator = {
									
									id : settings.graph.id,
									cid : $attrs.id,
									oid : "graph",
									title : settings.graph.title
									
								};								
								
								$rootScope.$broadcast(JSON.parse($attrs.settings).modelbrowser, settings);
								
							});	
							
						}, 	100
					);
					
				});
				
			}
		};
	});
	
// EOF