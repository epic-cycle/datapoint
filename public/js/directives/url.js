
/* -------------------------------- */

	id = "url";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function() {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */
			
			template: ' \
				<div style="height:100%;width:100%;background-color:#3d4352"> \
					<div class="viewport" style="height:100%;width:100%%;background-color:#3d4352;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Model :------ */
				
				$scope.model = {
					
					reloadinterval : JSON.parse($attrs.settings).reloadinterval
					
				};
				
				
			/* ------: Presentation :------ */
				
				$scope.presentation = {
					
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				
				
			/* ------: Load URL :------ */
				
				$scope.load = function(data) {				
					
					$scope.windows.window(attrs.guid).attachURL(JSON.parse($attrs.settings).url);
					
				};
				
			/* ------: Set interval :------ */
				
				if($scope.model.reloadinterval > 0){
					
					$scope.refresh = setInterval(function(){ 
						
						$scope.load();
						
					}, $scope.model.reloadinterval * 1000);
					
				}
				
			/* ------: Call Load URL :------ */	
				
				$scope.load();
				
			}
			
		};
		
	});
	
// EOF