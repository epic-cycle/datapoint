/*

I figured out at the architectural level how to implement generation of reports and to setup criteria directly from the view. In any view, whatever components there are, the user will be able to create an entry that stores the current configuration of the view components. Also user will be able to specify schedule to execute this configuration and send or store somewhere as well as user will be able to assign a friendly URL. The configuration will be used during the logged-in time to generate a report or can be called after the API.

*/


/* -------------------------------- */

	id = "modelbrowser";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope, Data, Object) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Model :------ */
				
				$scope.model = {
					
					brokers : {
						
						filter : null,
						daterange : null
						
					},
					
					dataset : undefined,
					columns : undefined,
					dimensions : undefined,
					limit : undefined
					
				};
				
				
			/* ------: Presentation :------ */
				
				$scope.presentation = {
					
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				
				// check if in a view is a more than 1 graph (reference to this)
				// if no than attach RAW
				// else show the view for selection
				// the same selection could be achieved by clicking on the graph itself
				
				$scope.windows.window(attrs.guid).attachURL("/apps/modelbrowser/index.html?" + new Date());
				
				
			/* ------: Send model id to presentation :------ */
				
				$scope.windows.window(attrs.guid).attachEvent("onContentLoaded", function(win){
					
					$scope.presentation = win.getFrame().contentWindow;
					$scope.presentation.model = JSON.parse($attrs.settings).model;
					
				});
				
				
			/* ------: Apply :------ */
				
				$scope.apply = function(oid){
					
					var model = $("#" + $attrs.id).find('iframe').contents().find("#presentation");
					$(model).trigger( "click" );
					
					var dimensions = $("#" + $attrs.id).find('iframe')[0].contentWindow.dimensions;
					var limit = $("#" + $attrs.id).find('iframe')[0].contentWindow.limit;
					
					$scope.model.limit = limit;
					
					var request = {
						
						id : JSON.parse($attrs.settings).model,
						dimensions : JSON.stringify(dimensions),
						limit : $scope.model.limit
						
					};
					
					$.each($scope.model.brokers, function(index) {
						
						switch(index) {
							
							case "filter":
								
								if ($scope.model.brokers[index] !== null) {
									
									request.filter = JSON.stringify($scope.model.brokers.filter.filter);
									
								}
								
							break;
							
							case "daterange":
								
								if ($scope.model.brokers[index] !== null) {
									
									request.daterange = JSON.stringify($scope.model.brokers.daterange);
									
								}								
								
							break;							
							
						}
						
					});	
					
					// TODO : "#graph" should be renamed to "#target" or "#content" ???
					// or get target id as parameter "oid" ???
					
					// TODO : do not call iframe, send some trigger to component
					
					$("#" + $scope.initiator.cid).find('iframe')[0].contentWindow.angular.element("#" + $scope.initiator.oid).scope().showLoading();
					
					Data.select(request)
					
					.success(function(response) {
						
					/* ------: Get columns :------ */
						
						Object.get({
							
							id : request.id,
							type : "model",
							
						})
						
						.success(function(model) {
							
							model = model.data;
							
							$scope.model.columns = JSON.parse(model.data.settings);
							$scope.model.dataset = response.data;
							$scope.model.dimensions = dimensions;
							$scope.model.duration = response.duration;
							$scope.model.cached = response.cached !== undefined ? response.cached : undefined;
							
						/* ------: Send data :------ */	
							
							// TODO : Needs refactoring, remove switch and create broadcasting
							
							switch($scope.initiator.oid) {
								
								case "graph":
									
									$("#" + $scope.initiator.cid).find('iframe')[0].contentWindow.angular.element("#" + $scope.initiator.oid).scope().hideLoading();
									$("#" + $scope.initiator.cid).find('iframe')[0].contentWindow.angular.element("#" + $scope.initiator.oid).scope().setData(response.data, dimensions);									
									
								break;
								
								case "spreadjs":
									
									$rootScope.$broadcast($scope.initiator.cid, $scope.model);
									
								break;
								
							}
							
						});						
						
					})
					
					.error(function(response, status) {
						
						$("#" + $scope.initiator.cid).find('iframe')[0].contentWindow.angular.element("#" + $scope.initiator.oid).scope().hideLoading();
						
						dhtmlx.message({
							type: "error",
							text: response.error,
							expire: 10000
						});
						
					});
					
				};
				
				
			/* ------: Attach toolbar :------ */
				
				var tbconf =  [
					{
						id: "save", type: "button", img: "save-18px.png", text: "Save"
					},
					//{
					//	id: "get", type: "button", img: "save-18px.png", text: "Get"
					//},					
					{
						id: "apply", type: "button", img: "apply-32px.png", text: "Apply"
					},
					
				];
				
				myToolbar = $scope.windows.window(attrs.guid).attachToolbar({
					icons_path: "/resources/shared/imgs/",
					align: "left",
					json : tbconf
				});
				
				myToolbar.addSpacer("save");
				
				myToolbar.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "apply":	
							
							$rootScope.$broadcast($attrs.id);
							
							/*
							var text = $("#" + $attrs.id).find('iframe')[0].contentWindow.text;
							
							
							var chartindex = $scope.presentation.angular.element("#presentation").scope().chartIndex;
							$("#" + $scope.initiator).find('iframe')[0].contentWindow.angular.element("#ziza").scope().jaja(text, dimensions, chartindex);
							*/
							
						break;
						
						case "get":
							
							$scope.presentation.angular.element("#presentation").scope().get($scope.initiator.cid);
							
						break;
						
						case "save":
							
							$scope.presentation.angular.element("#presentation").scope().save($scope.initiator.cid);
							
						break;
						
					}
					
				});
				
				
			/* ------: Receive response :------ */
				
				$rootScope.$on($attrs.id + ".response", function(event, response){
					
					$scope.model.counter += 1; 
					
					$scope.model.brokers[response.component] = response.data;
					
					if($scope.model.counter === $scope.model.consensus) {
						
						$scope.apply();
						
					}
					
				});
				
				
			/* ------: Broadcast request on trigger :------ */
				
				$rootScope.$on($attrs.id, function(event, data){
					
					// TODO : this section should be refactored
					// * graph should be as target
					
					//if(data !== undefined && data.graph !== undefined) {
					if(data !== undefined && data.initiator !== undefined) {
						
						$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-help").hide();
						//$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-description").html(data.graph.title);
						$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-description").html(data.initiator.title);
						$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-description").show();
						$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-gui").show();
						$("#" + $attrs.id).find('iframe').contents().find("#modelexplorer-limit").show();
						
						$scope.initiator = data.initiator;
						//$scope.oid = data.oid;
						$scope.presentation.angular.element("#presentation").scope().setChartType(data);
						
					} else {
						
						$scope.model.counter = 0;
						$scope.model.consensus = 0;
						
						$.each(view, function(index) {
							
							if (typeof $scope.model.brokers[view[index].component] == 'object') {
								
								if(
									typeof view[index].settings == "object" && 
									view[index].settings.datagrid == $attrs.id
								) {
									
									$scope.model.consensus = $scope.model.consensus + 1;
									
								}
								
							}
							
						});							
						
						if($scope.model.consensus>0) {
							
							$.each($scope.model.brokers, function(index) {
								
								$rootScope.$broadcast(index + ".request", $attrs.id);
								
							});
							
						} else {
							
							$scope.apply();
							
						}
						
					}
					
				});
				
			}
		};
	});
	
// EOF