/**
 *	Component to build and represent table-like data with sortable columns and filters
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/

/* ------: Settings :------ */

	var id = "datagrid";

/* ------: Module :------ */

	angular.module("directive." + id, [])
		
	/* ------: Directive :------ */
		
		.directive(id, function($rootScope, User, Data, Object) {
			
			return {
				
			/* ------: Properties :------ */
				
				restrict: 'AE',
				replace: false,
				scope: true,
				
			/* ------: Template :------ */	    
				
				template: ' \
					<div style="height:100%;width:100%;"> \
						<div class="viewport" style="text-align:left;height:100%;width:100%;background-color:white;background-color:white;font-size:12px;"> \
						</div> \
					</div> \
				',
				
			/* ------: Link function :------ */
				
				link: function($scope, $element, $attrs) {
					
				/* ------: Attributes :------ */
					
					$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
					
					var attrs = {
						viewport : "viewport-" + $attrs.id,
						guid : $attrs.id,
					};
					
					
				/* ------: Model :------ */
					
					$scope.model = {
						
						pageCurrent : undefined,
						pageToLoad : 1,
						dataset : undefined,
						filterhash : undefined,
						rerender : undefined,
						columns : undefined, // get titles of fields (from filter or what??? ),
						lastcaller : undefined,
						daterange : undefined,
						consensus : 0,
						counter : 0,
						brokers : {
							filter : null,
							daterange : null,
							paginator : null
						},
						hidestatusbar : JSON.parse($attrs.settings).hidestatusbar,
						hiderownumbers : JSON.parse($attrs.settings).hiderownumbers,
						hideheader : JSON.parse($attrs.settings).hideheader,
						hidescrollx : JSON.parse($attrs.settings).hidescrollx,
						hidescrolly : JSON.parse($attrs.settings).hidescrolly,
						zoom : JSON.parse($attrs.settings).zoom,
						reloadinterval : JSON.parse($attrs.settings).reloadinterval,
						autoloadenabled : JSON.parse($attrs.settings).autoloadenabled,
						broadcast : JSON.parse($attrs.settings).broadcast,
						subscribe : JSON.parse($attrs.settings).subscribe,
						interpolators : {}
						
					};
					
					
				/* ------: Presentation :------ */
					
					$scope.presentation = {
						
						controller : undefined,
						spread : undefined, // rename to grid
						pages : []
						
					};
					
					
				/* ------: Create Window :------ */
					
					$scope.construct($element, attrs);
					$scope.windows.window(attrs.guid).attachURL("/app/spreadjs");
					$scope.windows.window(attrs.guid).attachEvent("onContentLoaded", function(win){
						
						// TODO : need to investigate why scope is not available onContentLoaded
						
						setTimeout(function(){ 
							
							$scope.presentation.controller = $scope.windows.window(attrs.guid).getFrame().contentWindow.angular.element("#content").scope();
							
						/* ------: Get columns :------ */
							
							Object.get({
								
								id : JSON.parse($attrs.settings).model,
								type : "model",
								
							})
							
							.success(function(response) {
								
								response = response.data;
								
								$scope.model.columns = JSON.parse(response.data.settings);
								
							/* ------: Decode columns:------ */
								
								var cols = {};
								var column = null;
								
								$.each($scope.model.columns, function(index, value) {
										
									column = {
										
										"Field title" : value["Field name"],
										"Type" : value.Type,
										
									};
									
									cols[value['Field name']] = column;
									
								});	
								
								$scope.model.columns = cols;
								
							/* ------: Autoload:------ */
								
								if($scope.model.autoloadenabled ===1){
									
									$scope.getData();
									
								}
								
							/* ------: Autoreload:------ */
								
								if($scope.model.reloadinterval > 0){
									
									$scope.refresh = setInterval(function(){ 
										
										$scope.getData();
										
									}, $scope.model.reloadinterval * 1000);
									
								}
								
								
							/* ------: Subscribe :------ */
								
								if($scope.model.subscribe !== "") {
									
									$rootScope.$on($scope.model.subscribe, function (event, data) {
										
										switch (event.name) {
											
											case "eventx" :
												
												var res = data.data[0].Event.split(" : ");
												
												$scope.model.interpolators = {
													
													sport : res[0],
													event_name : res[1]
													
												};
												
												$scope.getData();
												
											break;
											
										}
										
									});
									
								}
								
							});
							
						}, 1000);
						
					});
					
					
				/* ------: Status bar:------ */
					
					if($scope.model.hidestatusbar !== 1){
						
						$scope.statusbar = $scope.windows.window(attrs.guid).attachStatusBar({
						    text: "<span id='count-" + $attrs.id + "'>0</span> &nbsp; | &nbsp; <span id='page-" + $attrs.id + "'>0</span> &nbsp; | &nbsp; Loading time: <span style='color:#00A6BF' id='duration-" + $attrs.id + "'>0</span> ms &nbsp; | &nbsp; <span style='font-family:arial;' id='state-" + $attrs.id + "'>0</span>",
						    height: 35,
						    align: "left"
						});
						
					}
					
					
				/* ------: Prototype :------ */
					
					// TODO : those prorotype functions should be global
					
					String.prototype.encode = function(){
					    var hex, i;
					
					    var result = "";
					    for (i=0; i<this.length; i++) {
					        hex = this.charCodeAt(i).toString(16);
					        result += ("000"+hex).slice(-4);
					    }
					
					    return result;
					    
					};
					
					String.prototype.decode = function(){
					    var j;
					    var hexes = this.match(/.{1,4}/g) || [];
					    var back = "";
					    for(j = 0; j<hexes.length; j++) {
					        back += String.fromCharCode(parseInt(hexes[j], 16));
					    }
					
					    return back;
					};
					
					
				/* ------: Get data :------ */
					
					$scope.getData = function(data) {
						
						$scope.presentation.controller.showLoading();
						
						var request = {
							
							id : JSON.parse($attrs.settings).model,
							interpolators : $scope.model.interpolators
							
						};
						
						$.each($scope.model.brokers, function(index) {
							
							switch(index) {
								
								case "paginator":
									
									if ($scope.model.brokers[index] !== null) {
										
										request.page = $scope.model.brokers.paginator.page;
										
									}								
									
								break;
								
								case "filter":
									
									if ($scope.model.brokers[index] !== null) {
										
										request.filter = $scope.model.brokers.filter.filter;
										request.limit = $scope.model.brokers.filter.limit;
										request.orderby = $scope.model.brokers.filter.orderby;
										request.ordervector = $scope.model.brokers.filter.ordervector;
										
									}
									
								break;
								
								case "daterange":
									
									if ($scope.model.brokers[index] !== null) {
										
										request.daterange = $scope.model.brokers.daterange;
										
									}								
									
								break;							
								
							}
							
						});	
						
						
						Data.select(request)
						
						.success(function(response) {
							
							try {						
								
								if ($scope.model.brokers.paginator !== null) {
									
									$scope.model.pageCurrent = $scope.model.brokers.paginator.page;
									
								}
								
								$scope.model.rerender = true;
								
								switch(typeof response.data){
									
									case "object":
										
										$scope.model.dataset = response.data;
										
									break;
									
									default:
									
										$scope.model.dataset = response;
									
									break;
									
								}
								
								$scope.presentation.controller.hideLoading();
								
								$("#duration-" + $attrs.id).html(response.duration);
								
								if(response.cached !== undefined) {
									
									$("#state-" + $attrs.id).html("Server cached " +  moment(response.cached).fromNow());
									$("#state-" + $attrs.id).css("color", "#2457FF");
									
								}
								
								if($scope.model.broadcast !== "") {
									
									$rootScope.$broadcast($scope.model.broadcast, {
										
										data: response.data
										
									});								
									
								}								
								
							} catch (e) {
								
								$scope.presentation.controller.hideLoading();
								
								dhtmlx.message({
									type: "error",
									text: e,
									expire: 10000
								});							
								
							}
							
						})
						
						.error(function(response, status) {
							
							dhtmlx.message({
								type: "error",
								text: response.error,
								expire: 10000
							});
							
							$scope.presentation.controller.hideLoading();
							
						});
						
					};
					
					
				/* ------: Old subscribe :------ */
					
					/*
					
					$rootScope.$on("paginator.count." + $attrs.id, function(event, data) {
						
						$("#count-" + $attrs.id).html("Total: " + data.count);
						
						
					});
					
					$rootScope.$on($attrs.id, function(event, data) {
						
						switch(data.component) {
							
							case "paginator":
								
								$scope.model.lastcaller = data.component;
								$scope.model.pageToLoad = data.page;
								$rootScope.$broadcast("filter." + $attrs.id, data);
								
							break;
							
							
							case "daterange":
								
								$scope.model.lastcaller = data.component;
								$scope.model.daterange = data.data;
								$rootScope.$broadcast("filter." + $attrs.id, data);
								
							break;						
							
							
							case "paginator.filter":
								
							break;
							
							
							case "filter":
								
								data.daterange = $scope.model.daterange;
								
								if($scope.presentation.spread === undefined) {
									
									$scope.getData(data);
									$("#page-" + $attrs.id).html("Page: 1");
									$("#state-" + $attrs.id).html("No cache");
									$("#state-" + $attrs.id).css("color", "green");	
									$rootScope.$broadcast("paginator." + $attrs.id, data);
									
								} else {
									
									//if($scope.model.filterhash !== $.md5(JSON.stringify(data))) {
									// check if local cache enabled, if yes ok, if not then just get data
									if($scope.model.lastcaller !== "paginator" && $scope.model.lastcaller !== "daterange") {
										
										$scope.model.pageToLoad = 1;
										$scope.presentation.spread.clearSheets();
										$scope.presentation.pages = [];
										$scope.getData(data);
										$("#page-" + $attrs.id).html("Page: 1");
										$("#state-" + $attrs.id).html("No cache");
										$("#state-" + $attrs.id).css("color", "green");									
										$rootScope.$broadcast("paginator." + $attrs.id, data);
										
									} else {
										
										if(JSON.parse($attrs.settings).clientcacheenabled == "0") {
											
											$scope.presentation.controller.resetSpread();
											
										}
										
										if($scope.presentation.pages["page" + $scope.model.pageToLoad] !== undefined) {
											
											$scope.presentation.spread.setActiveSheet("page" + $scope.model.pageToLoad);
											$("#duration-" + $attrs.id).html("0");
											$("#page-" + $attrs.id).html("Page: " + $scope.model.pageToLoad);
											$("#state-" + $attrs.id).html("Client cached " +  moment($scope.presentation.pages["page" + $scope.model.pageToLoad].created).fromNow());
											$("#state-" + $attrs.id).css("color", "red");
											//$scope.statusbar.setText("Page " + $scope.model.pageToLoad + ", <span style='color:red'>Cached </span>" + moment($scope.presentation.pages["page" + $scope.model.pageToLoad].created).fromNow());
											
										} else {
											
											$scope.getData(data);		
											$("#page-" + $attrs.id).html("Page: " + $scope.model.pageToLoad);
											$("#state-" + $attrs.id).html("No cache");
											$("#state-" + $attrs.id).css("color", "green");	
											
											// only if hash different
											//$rootScope.$broadcast("paginator." + $attrs.id, data);
											
											//$scope.statusbar.setText("Page " + $scope.model.pageToLoad + ", <span style='color:green'>Fresh</span>");
											
										}
										
									}
									
								}
								
								$scope.model.lastcaller = data.component;
								
							break;
							
						}
					});
					
					*/
					
					
				/* ------: Receive response :------ */
					
					$rootScope.$on($attrs.id + ".response", function(event, response){
						
						$scope.model.counter += 1; 
						
						$scope.model.brokers[response.component] = response.data;
						
						if($scope.model.counter === $scope.model.consensus) {
							
							$scope.getData();
							
						}
						
					});
					
					
				/* ------: Broadcast request on trigger :------ */
					
					$rootScope.$on($attrs.id, function(event, data){
						
						$scope.model.counter = 0;
						$scope.model.consensus = 0;
						
						$.each(view, function(index) {
							
							if (typeof $scope.model.brokers[view[index].component] == 'object') {
								
								if(
									typeof view[index].settings == "object" && 
									view[index].settings.datagrid == $attrs.id
								) {
									
									$scope.model.consensus = $scope.model.consensus + 1;
									
								}
								
							}
							
						});							
						
						$.each($scope.model.brokers, function(index) {
							
							$rootScope.$broadcast(index + ".request", $attrs.id);
							
						});				
						
					});
					
				}
				
			};
			
		});
		
// EOF