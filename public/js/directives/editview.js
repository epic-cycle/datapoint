/**
 *	Component to edit views
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Settings :------ */

	id = "editview";
	
	
/* ------: Module :------ */

	angular.module("directive." + id, [])
		
		
	/* ------: Directive :------ */
		
		.directive(id, function() {
			
			return {
				
			/* ------: Properties :------ */
				
				restrict: 'AE',
				replace: false,
				scope: true,
				
				
			/* ------: Template :------ */	    
				
				template: ' \
					<div style="height:100%;width:100%;" class="container fill"> \
						<div class="viewport" style="height:100%;width:100%;"> \
						</div> \
					</div> \
				',
				
				
			/* ------: Link function :------ */	    
				
				link: function($scope, $element, $attrs) {
					
				/* ------: Attributes :------ */	    	
					
					var guid = chance.guid();
					
					$attrs = {
						guid : guid,
						viewport : "viewport-" + guid,
					};
					
					$element.attr("id", $attrs.guid);
					$element.find('.viewport').attr("id", "viewport-" + $attrs.guid);
					
					
				/* ------: Create window :------ */
					
					$scope.construct($element, $attrs);
					
					
				/* ------: Attach Layout to Window :------ */
					
					$scope.windows.window($attrs.guid).attachURL("/app/editview/15");
					
				}
			};
		});
		
// EOF