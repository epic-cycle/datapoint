/**
 *	Component to show graphical menu
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Settings :------ */

	id = "dock";

/* ------: Directive :------ */

	angular.module("directive." + id, [])
		
		.directive(id, function($rootScope) {
			
			return {
			
			/* ------: Properties :------ */
				
				restrict: 'AE',
				replace: 'true',
				scope: true,
				
			/* ------: Template :------ */
				
				template: ' \
					<div style="width:100%;position:absolute;z-index:9999999;"> \
						<div class="jqDockAuto" data-jqdock-align="bottom" data-jqdock-size="50" data-jqdock-labels="true" data-jqdock-fade-in="800"> \
							<!-- <a href="/auth/logout"><img style="display:none;" src="/resources/shared/imgs/logout.png" title="" /></a> --> \
						</div> \
						<div style="z-index:99999;display:table;border-top-style:solid;border-color:green;border-width:1px;background-image:url(/resources/shared/textures/texture-bricks-127x252px.jpg);z-index:1;position:fixed;bottom:0px;width:100%;background-color:black;height:30px;"> \
							<div style="padding-left:3px;padding-top:3px;display:table-cell;text-align:left;width:33%;"> \
								<img style="display:none;" style="height:31px;" src="/resources/shared/imgs/menu-configure.png"/> \
							</div> \
							<div style="padding-top:5px;padding-right:3px;display:table-cell;text-align:center;width:33%;"> \
								<span style="color:white">Click here to add favourite views.</span> \
							</div> \
							<div style="padding-right:3px;display:table-cell;text-align:center;width:33%;"> \
								<img style="display:none;" src="/resources/shared/imgs/menu-save.png"/> \
							</div> \
						</div> \
					</div> \
				',
				
			/* ------: Link function :------ */
				
				link: function($scope, $element, $attrs) {
				
				/* ------: Attributes :------ */
					
					$attrs.id = chance.guid();
					$element.attr("id", $attrs.id);
					$element.find(".jqDockAuto").attr("id", "jqDockAuto-" + $attrs.id);
					
				/* ------: Init :------ */
					
					$("#jqDockAuto-" + $attrs.id).jqdock();
					
				}
			};
		});
	
// EOF