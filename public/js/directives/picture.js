
/* -------------------------------- */
	id = "picture";
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:#282c36;"> \
						<div class="layout"> \
							<div style="display:table;height:100%;color:#e8e8e8;padding-left:20px;"> \
								<table> \
									<tr> \
										<td> \
											<img style="height:60px;margin-top:25px;" src="/assets/optibet/logo.png"> \
										</td> \
										<td class="value" style="vertical-align:bottom;font-size:40px;"> \
										</td> \
									</tr> \
								</table> \
							</div> \
						</div> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* -------------------------------- */		    	
				
				$attrs.id = chance.guid();
				$element.attr("id", $attrs.id);
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				$element.find('.layout').attr("id", "layout-" + $attrs.id);
				$element.find('.value').attr("id", "value-" + $attrs.id);
				
				if($attrs.cmp == "cmp-61b4e4cb-35da-5f81-a1c8-1ee8cb047b91") {
					
					$("#value-" + $attrs.id).html("&nbsp;| <b>LV</b>");
					
				} else if($attrs.cmp == "cmp-3d906798-1ad1-5c1f-9a1b-0abb7c66fa59") {
					
					$("#value-" + $attrs.id).html("&nbsp;| <b>EE</b>");
					
				} else if($attrs.cmp == "cmp-98049511-971b-5f0c-a165-68c1b3edb021") {
					
					$("#value-" + $attrs.id).html("&nbsp;| <b>COM</b>");
					
				}
				
				
			/* -------------------------------- */
				
				// Create Window
				
				//$scope.construct($element, $attrs);
				
				//$scope.windows.window($attrs.id).attachURL("/app/editview/15")
				
			}
		};
	});
	
// EOF