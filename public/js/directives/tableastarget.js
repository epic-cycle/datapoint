
/* -------------------------------- */

	id = "tableastarget";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="text-align:left;height:100%;width:100%;background-color:white;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Model :------ */
				
				$scope.model = {
					rerender : undefined,
					pageCurrent : undefined,
					columns : undefined,
					dataset : undefined					
				};
				
				
			/* ------: Presentation :------ */
				
				$scope.presentation = {
				
				};
				
				
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				$scope.windows.window(attrs.guid).attachURL("/app/spreadjs");
				
				
			/* ------: Attach status bar :------ */	
				
				$scope.statusbar = $scope.windows.window(attrs.guid).attachStatusBar({
				    text: "<span id='count-" + $attrs.id + "'>0</span> &nbsp; | &nbsp; <span id='page-" + $attrs.id + "'>0</span> &nbsp; | &nbsp; Loading time: <span style='color:#00A6BF' id='duration-" + $attrs.id + "'>0</span> ms &nbsp; | &nbsp; <span style='font-family:arial;' id='state-" + $attrs.id + "'>0</span>",
				    height: 35,
				    align: "left"
				});				

			/* ------: Prototype :------ */
				
				// TODO : those prorotype functions should be global
				
				String.prototype.encode = function(){
				    var hex, i;
				
				    var result = "";
				    for (i=0; i<this.length; i++) {
				        hex = this.charCodeAt(i).toString(16);
				        result += ("000"+hex).slice(-4);
				    }
				
				    return result;
				    
				};
				
				String.prototype.decode = function(){
				    var j;
				    var hexes = this.match(/.{1,4}/g) || [];
				    var back = "";
				    for(j = 0; j<hexes.length; j++) {
				        back += String.fromCharCode(parseInt(hexes[j], 16));
				    }
				
				    return back;
				};
				
			/* ------: Presentation :------ */
				
				$scope.windows.window(attrs.guid).attachEvent("onContentLoaded", function(win){
				
				/* ------: Link :------ */
					
					$scope.pres = win.getFrame();
					
					setTimeout(function() { 
						
						$scope.presentation.controller = $scope.windows.window(attrs.guid).getFrame().contentWindow.angular.element("#content").scope();
						
						/* ------: Attach events :------ */
							
							$($($scope.pres).contents().find("body")).on("click", function() {
								
								var settings = JSON.parse($attrs.settings);
								
								settings.initiator = {
									
									id : "tableastarget",
									cid : $attrs.id,
									oid : "spreadjs",
									title : "Table"
									
								};
								
								$rootScope.$broadcast(JSON.parse($attrs.settings).datasource, settings);
								
							});	
							
						}, 	1000
					);
					
				});
				
				
			/* ------: Receive response :------ */
				
				$rootScope.$on($attrs.id, function(event, data){
					console.log(data);
					$scope.presentation.controller.hideLoading();
					
					$("#duration-" + $attrs.id).html(data.duration);
					
					if(data.cached !== undefined) {
						
						$("#state-" + $attrs.id).html("Server cached " +  moment(data.cached).fromNow());
						$("#state-" + $attrs.id).css("color", "#2457FF");
						
					} else {
						
						$("#state-" + $attrs.id).html("No cache");
						$("#state-" + $attrs.id).css("color", "green");							
						
					}
					
					$("#count-" + $attrs.id).html("Total: " + data.dataset.length);
					
					// TODO : refactoring here
					
					// Columns
					
					var cols = {};
					var column = null;
					
					try {
					
						$.each(data.columns, function(index, value) {
							
							cols[value["Field name"]] = value;
							
						});
					
					} catch(error) {
						
						
					}
					
					
					try {
						
						if(data.dimensions !== undefined) {
						
							$.each(data.dimensions[0], function(index, value) {
								
								if(value.aggregation !== undefined) {
									
									column = {
										
										"Field title" : data.columns["_" + (value.table +  ":" + value.key).encode()]["Field title"] + " (" + value.aggregation + ")",
										"Type" : data.columns["_" + (value.table +  ":" + value.key).encode()].Type
										
									};
									
									cols[value.key + "__" + value.aggregation] = column;
									
								} else {
									
									column = {
										
										"Field title" : data.columns["_" + (value.table +  ":" + value.key).encode()]["Field title"],
										"Type" : data.columns["_" + (value.table +  ":" + value.key).encode()].Type
										
									};
									
									cols[value.key] = column;							
									
								}
								
							});	
						
						}
					
					} catch (error) {
						
						console.log(error);
						
						dhtmlx.message({
							type: "error",
							text: error,
							expire: 30000
						});
						
					}
					
					try {
						
						if(data.dimensions !== undefined) {
						
							$.each(data.dimensions[1], function(index, value) {
								
								if(value.aggregation !== undefined) {
									
									column = {
										
										"Field title" : data.columns["_" + (value.table +  ":" + value.key).encode()]["Field title"] + " (" + value.aggregation + ")",
										"Type" : data.columns["_" + (value.table +  ":" + value.key).encode()].Type
										
									};
									
									cols[value.key + "__" + value.aggregation] = column;
									
								} else {
									
									column = {
										
										"Field title" : data.columns["_" + (value.table +  ":" + value.key).encode()]["Field title"],
										"Type" : data.columns["_" + (value.table +  ":" + value.key).encode()].Type
										
									};
									
									cols[value.key] = column;							
									
								}
								
							});	
						
						}
						
					} catch (error) {
						
						console.log(error);
						
						dhtmlx.message({
							type: "error",
							text: error,
							expire: 30000
						});
						
					}
					
					$scope.model.columns = cols;
					
					// Dataset
					
					$scope.model.rerender = true;
					$scope.model.pageCurrent = 1;
					$scope.model.dataset = data.dataset;
					
				});				
				
			}
			
		};
	});
	
// EOF