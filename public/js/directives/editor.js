
/* -------------------------------- */

	id = "editor";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	.directive(id, function($rootScope, $controller, Databuilder, Data) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
 
		/* -------------------------------- */		    
			
			template: ' \
				<div style="height:100%;width:100%;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
				
			/* ------: Include controllers :------ */
				
				$controller('Items', {$scope: $scope, $attrs: $attrs, $element:$element});
				$controller('Versions', {$scope: $scope, $attrs: $attrs, $element:$element});
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
			/* ------: Model :------ */
				
				$scope.model = {

					id : $attrs.id,
					
					type : 'component',
					
					item : undefined,
					
					datasource : {
						
						id : JSON.parse($attrs.settings).source
						
					},
					
					dataset : undefined,
					
					object : null,
					
					target : {
						
						auditory : "User"
						
					}
					
				};
				
			/* ------: Presentation :------ */
				
				$scope.presentation = {
					
				};
				
			/* ------: Configuration :------ */

				var tables = [];
				var fields = [];
				var schemas = [];
				
				Databuilder.schema({id : [$scope.model.datasource.id]})
				
				// return columns in schema or get seperately ????
				
				.success(function(objects) {
					
					$.each(objects, function(index) {
						
						tables.push(objects[index].name);
						
					});	
					
					Databuilder.fields({
						
						id : [$scope.model.datasource.id],
						table : tables
						
					})
					
					.success(function(response) {
					
						fields = $.map( response, function( val, i ) {
							return response[i].column_name;
						});	
						
						// get values
						
						$.getJSON("/conf/drivers/clickhouse.json?" + new Date())
						
						.done(function(driver) {
							
							$scope.configuration = {
								
								editor : {
									
									mode : 'sql',
									theme : "ambiance"
									
								},
								
								autocomplete : {
									
									// get dynamically all schemas from selected connection
									
									"schemas" : {
										
										wordList : schemas,
										meta : "SCHEMA"
										
									},
									
									"databases" : {
										
										wordList : ["OPTIBET"],
										meta : "DATABASE"
										
									},
									
									// get dynamically all tables from selected connection
									
									"tables" : {
										
										wordList : tables,
										meta : "TABLE"
										
									},
									
									// get dynamically all columns from selected connection
									
									"columns" : {
										
										wordList : $.unique(fields),
										meta : "COLUMN"
										
									},	
									
									// get dynamically all views from selected connection
									
									"views" : {
										
										wordList : [],
										meta : "VIEW"
										
									},											
									
									// get dynamically from json file
									
									"keywords" : {
										
										wordList : driver.keywords,
										meta : "KEYWORD"
										
									},	
									
									"functions" : {
										
										wordList : driver.functions,
										meta : "FUNCTION"
										
									},													
									
									"categories" : {
										
										wordList : [
											"? Domains", 
											"? Games", 
											"? Currencies", 
											"? Providers",
											"? Countries",
											"? Tables",
											"? Columns",
											"? Functions",
											"? Keywords",
										].sort(),
										meta : "CATEGORY"
										
									},		
									
									"domains" : {
										
										wordList : [
											"optibet.lv", 
											"optibet.ee", 
											"optibet.com"
										].sort(),
										meta : "DOMAIN"
										
									},
									
									"games" : {
										
										wordList : [],
										meta : "GAME"
										
									},		
									
									"currencies" : {
										
										wordList : [],
										meta : "CURRENCY"
										
									},									
									
									"countries" : {
										
										wordList : [],
										meta : "COUNTRY"
										
									},										
									
									"providers" : {
										
										wordList : [
											"netent", 
											"mgs", 
											"medialive", 
											"playngo", 
											"ctxm",
											"nyx",
											"egt"
										].sort(),
										meta : "PROVIDER"
										
									}									
									
								}
								
							};
							
							//  Get "Casino games"
							
							Data.select({
								
								id : 24
								
							})
							
							.success(function(response) {
								
								$.each(response.data, function(index) {
									
									$scope.configuration.autocomplete.games.wordList.push(response.data[index].name_en);
									
								});	
								
							})
							
							.error(function(response) {
								
								if(response == "Unauthorized.") {
									
									response = {
										error : "Unauthorized to select data 'Casino games'"
									};
									
								}
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 30000
								});
								
							});							
							
							//  Get "Currencies"
							
							Data.select({
								
								id : 25
								
							})
							
							.success(function(response) {
								
								$.each(response.data, function(index) {
									
									$scope.configuration.autocomplete.currencies.wordList.push(response.data[index].name);
									
								});	
								
							})
							
							.error(function(response) {
								
								if(response == "Unauthorized.") {
									
									response = {
										error : "Unauthorized to select data 'Currencies'"
									};
									
								}
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 30000
								});
								
							});								
							
							//  Get "Countries"
							
							Data.select({
								
								id : 26
								
							})
							
							.success(function(response) {
								
								$.each(response.data, function(index) {
									
									$scope.configuration.autocomplete.countries.wordList.push(response.data[index].name_en);
									
								});	
								
							})
							
							.error(function(response) {
								
								if(response == "Unauthorized.") {
									
									response = {
										error : "Unauthorized to select data 'Countries'"
									};
									
								}
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 30000
								});
								
							});								
							
						});							
						
					})
					
					.error(function(response) {
						
						if(response == "Unauthorized.") {
							
							response = {
								error : "Unauthorized to get fields " + id
							};
							
						}
						
						dhtmlx.message({
							type: "error",
							text: response.error,
							expire: 30000
						});
						
					});
					
				})
				
				.error(function(response) {
					
					if(response == "Unauthorized.") {
						
						response = {
							error : "Unauthorized to get schema " + id
						};
						
					}
					
					dhtmlx.message({
						type: "error",
						text: response.error,
						expire: 30000
					});
					
				});
			
			/* ------: Create Window :------ */
				
				$scope.construct($element, attrs);
				$scope.windows.window(attrs.guid).attachURL("/app/ace");
				
			/* ------: Load data in tab :------ */
				
				$scope.windows.window(attrs.guid).attachEvent("onContentLoaded", function(win){
					
					setTimeout(function(){ 
						
						$scope.presentation = win.getFrame().contentWindow;
						
						$scope.loadItem($attrs.id);
						
					}, 1000);
					
				});
				
				
				$scope.execute = function(){
					
					$("#" + JSON.parse($attrs.settings).target).find('iframe')[0].contentWindow.angular.element("#spreadjs").scope().showLoading();
					
					Databuilder.runSQL({
						
						id : [$scope.model.datasource.id],
						
						data : {
							
							connection : {
								id : $scope.model.datasource.id
							},
							request : {
								query : $scope.presentation.editor.getValue()
							}
							
						}	
						
					})
					
					.success(function(response) {
						
						$scope.model.dataset = response.data;
						$scope.model.duration = response.duration;
						$rootScope.$broadcast(JSON.parse($attrs.settings).target, $scope.model);
						
					})
					
					.error(function(response, status) {
						
						$("#" + JSON.parse($attrs.settings).target).find('iframe')[0].contentWindow.angular.element("#spreadjs").scope().hideLoading();
						
						dhtmlx.message({
							type: "error",
							text: response.error,
							expire: 30000
						});
						
					});
					
				};
					
			/* ------: Attach toolbar :------ */
				
				var tbconf =  [
					{
						id:     "versions",
						type:   "button",
						img:	"versions-32px.png",
						text:   "Versions",
					},
					{
						id:   "sep_id",
						type: "separator"
					},
					{
						id:     "info",
						type:   "text",
						text:   "Item: ",
					},
					{
						id: "items", type: "button", img: "arrow-right-18px.png", text: "Default"
					},	
					{
						id:   "sep_id",
						type: "separator"
					},
					{
						id: "save", type: "button", img: "save-18px.png", text: "Save"
					},					
					{
						id: "beautify", type: "button", img: "beautify-32px.png", text: "Beautify"
					},					
					{
						id: "execute", type: "button", img: "apply-32px.png", text: "Execute"
					},
					
				];
				
				$scope.toolbar = $scope.windows.window(attrs.guid).attachToolbar({
					icons_path: "/resources/shared/imgs/",
					align: "left",
					json : tbconf
				});
				
				$scope.toolbar.addSpacer("sep_id");
				
				$scope.toolbar.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "execute":	
							
							$scope.execute();
							
						break;
						
						
						case "beautify":
							
							$scope.beautify();
							
						break;

						case "save":
							
							$scope.saveItem($attrs.id, $scope.presentation.editor.getValue());
							
						break;	
						
						case "items":
							
							$scope.getItems($attrs.id);
							
						break;
						
						case "versions":
							
							$scope.modalRevertVersion();
							
						break;						
						
					}
					
				});

			/* ------: Watch object :------ */
				
				$scope.$watch('object', function(object){
					
					if(object !== undefined) {
						
						$scope.itemLoad(object.data);
						
					}
					
				});

			/* ------: Load item :------ */
				
				$scope.itemLoad = function(data){
					
					$scope.presentation.editor.setValue(data, 1);
					
				};
				
			/* ------: SQL beautify :------ */
				
				$scope.beautify = function(){
					
					Databuilder.prettysql({
						
						id: [],
						sql:$scope.presentation.editor.getValue()
						
					})
					
					.success(function(response) {
						
						$scope.presentation.editor.setValue($(response).text(),1);
						
					})
					
					.error(function(response) {
						
						if(response == "Unauthorized.") {
							
							response = {
								error : "Unauthorized to beautify"
							};
							
						}
						
						dhtmlx.message({
							type: "error",
							text: response.error,
							expire: 30000
						});
						
					});
					
				};
				
				$scope.$on('scanner-started', function(event, args) {
				
				   $scope.presentation.editor.session.insert($scope.presentation.editor.getCursorPosition(), args.any + ", ");
				   $scope.beautify();
				   // if its field then add comma 
				   
				});
				
			/* ------: Listener : loaditem :------ */
			
				$scope.$on('loaditem', function(events, args){

					$scope.itemLoad(args.data);
					
				});
				
			}
			
		};
		
	});
	
// EOF