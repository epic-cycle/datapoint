
/* -------------------------------- */

	id = "filter";
	
/* -------------------------------- */

angular.module("directive." + id, [])

	/* ------: Directive :------ */	
		
	.directive(id, function($rootScope, $controller, Object, Data) {
		
		return {
			
		/* -------------------------------- */
			
			restrict: 'AE',
			replace: false,
			scope: true,
			
		/* -------------------------------- */
			
			template: ' \
				<div style="width:100%;height:100%;font-size:14px;text-align:left;"> \
					<div class="viewport" style="height:100%;width:100%;background-color:white;"> \
					</div> \
					<div class="filter" style="padding:5px;width:100%;overflow:auto;height:100%;background-color:#FFFDE8"> \
						<div class="filterrecords-content"> \
							<label class="checkbox-inline"><input class="filterrecords" type="checkbox"> Filter records</label> \
							<div class="where" style="padding:5px;width:100%;overflow:auto;background-color:#FFFDE8"></div> \
						</div> \
						<div class="filteraggregates-content"> \
							<label class="checkbox-inline"><input class="filteraggregates" type="checkbox"> Filter aggregates</label> \
							<div class="having" style="padding:5px;width:100%;overflow:auto;background-color:#FFFDE8"></div> \
						</div> \
					</div> \
					<div class="order" style="display:none;padding-left:10px;overflow:auto;padding-top:6px;height:100%;background-color:#FFFDE8"> \
						<div style="display: table-cell;padding-left:5px;width:100px;"> \
							Order by: \
						</div> \
						<div style="display: table-cell;padding-left:5px;"> \
							<select class="form-control orderby"></select> \
						</div> \
						<div style="display: table-cell;padding-left:5px;"> \
							<select class="ordervector form-control"><option value="DESC">Descending</option><option value="ASC">Ascending</option></select> \
						</div> \
						<div></div> \
						<div style="display: table-cell;padding-left:5px;width:100px;padding-top:10px;"> \
							Per page: \
						</div> \
						<div style="display: table-cell;padding-left:5px;"> \
							<select class="limitxx form-control"> \
								<option value="10">Records</option> \
								<option style="display:none;" value="100">Period</option> \
							</select> \
						</div> \
						<div></div> \
						<div style="display: table-cell;padding-left:5px;width:100px;padding-top:10px;"> \
							Count: \
						</div> \
						<div style="display: table-cell;padding-left:5px;"> \
							<select class="limit form-control"> \
								<option value="10">10</option> \
								<option value="100">100</option> \
								<option value="1000">1000</option> \
								<option value="5000">5000</option> \
								<option value="10000">10000</option> \
							</select> \
						</div> \
					</div> \
				</div> \
			',
			
		/* -------------------------------- */		    
			
			link: function($scope, $element, $attrs) {
			
			/* ------: Controllers :------ */
				
				$controller('Items', {$scope: $scope, $attrs: $attrs});
				
				
			/* ------: Attributes :------ */
				
				$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
				$element.find('.test').attr("id", "test-" + $attrs.id);
				$element.find('.filter').attr("id", "filter-" + $attrs.id);
				$element.find('.filterrecords').attr("id", "filterrecords-" + $attrs.id);
				$element.find('.filteraggregates').attr("id", "filteraggregates-" + $attrs.id);
				$element.find('.filterrecords-content').attr("id", "filterrecords-content" + $attrs.id);
				$element.find('.filteraggregates-content').attr("id", "filteraggregates-content" + $attrs.id);				
				$element.find('.where').attr("id", "where-" + $attrs.id);
				$element.find('.having').attr("id", "having-" + $attrs.id);
				$element.find('.order').attr("id", "order-" + $attrs.id);
				$element.find('.button').attr("id", "button-" + $attrs.id);
				$element.find('.internal-submit').attr("id", "internal-submit-" + $attrs.id);
				$element.find('.external-submit').attr("id", "external-submit-" + $attrs.id);
				$element.find('.save-filter').attr("id", "save-filter-" + $attrs.id);
				$element.find('.limit').attr("id", "limit-" + $attrs.id);
				$element.find('.limitsql').attr("id", "limitsql-" + $attrs.id);
				$element.find('.orderby').attr("id", "orderby-" + $attrs.id);
				$element.find('.ordervector').attr("id", "ordervector-" + $attrs.id);
				
				var attrs = {
					viewport : "viewport-" + $attrs.id,
					guid : $attrs.id,
				};
				
				
			/* ------: Model :------ */
				
				$scope.model = {
					
					aspects : {},
					filterhash : undefined,
					
					plugins : [
						
						"sortable",
						"bt-checkbox"
						
					],
					
					filterItems : {
						
						where : [],
						having : []
						
					},
					
					settings : JSON.parse($attrs.settings),

					item : undefined,
					
					itemdata : undefined
					
				};
				
				
			/* ------: Window :------ */
				
				$scope.construct($element, attrs);
				
				
			/* ------: Layout:------ */
				
				if($scope.model.settings.hideorderandpaging == "1") {
					
					$scope.layout = $scope.window.attachLayout("1C");
					
					$scope.layout.cells("a").hideHeader();
					$scope.layout.cells("a").showInnerScroll();
					$scope.layout.cells("a").setText("Filter");
					
					$scope.layout.cells("a").attachObject("filter-" + $attrs.id);
					
				} else {
					
					$scope.layout = $scope.window.attachLayout("2E");
					
					$scope.layout.cells("a").hideHeader();
					$scope.layout.cells("b").hideHeader();
					
					$scope.layout.cells("a").showInnerScroll();
					$scope.layout.cells("b").showInnerScroll();
					
					$scope.layout.cells("a").setText("Options");
					$scope.layout.cells("b").setText("Filter");
					
					$scope.layout.cells("a").setMinHeight(125);
					$scope.layout.cells("a").setHeight(85);
					
					$scope.layout.cells("a").attachObject("order-" + $attrs.id);
					$scope.layout.cells("b").attachObject("filter-" + $attrs.id);					
					
					$("#order-" + $attrs.id).show();
					
				}
				
				
			/* ------: Toolbar :------ */
				
				/* ------: Configuration :------ */
				
				var tbconf =  [
					
					{
						id: "save", type: "button", img: "save-18px.png", text: "Save filter"
					},
					
					{
						id: "usepresets", type: "buttonTwoState", img: "list-32px.png", text: "Use presets"
					},
					 
					{
						id: "items", type: "button", img: "arrow-right-18px.png", text: "Default"
					},					 
					 
					{
						id: "execute", type: "button", img: "apply-32px.png", text: "Apply filter"
					}
					
				];
				
				/* ------: Attach toolbar :------ */
				
				$scope.toolbar = $scope.layout.attachToolbar({
					icons_path: "/resources/shared/imgs/",
					align: "left",
					json : tbconf
				});
				
				$scope.toolbar.hideItem("items");
				
				$scope.toolbar.addSpacer("items");
				
				/* ------: Attach event onStateChange :------ */
				
				$scope.toolbar.attachEvent("onStateChange", function(id, state){
					
					switch(id) {
						
						case "usepresets":
							
							switch(state){
								
								case true:
									
									$scope.toolbar.showItem("items");
									
								break;
								
								case false:
									
									$scope.toolbar.hideItem("items");
									
								break;								
								
							}
							
						break;
						
					}
					
				});				
				
				/* ------: Attach event onClick :------ */
				
				$scope.toolbar.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "save":
							
							$scope.save();
							
						break;
						
						
						case "execute":
							
							$scope.execute({});
							
						break;
						
						
						case "items":
							
							$scope.getItems($attrs.id);
							
						break;						
						
					}
					
				});	
				
				
			/* ------: Item watch:------ */
				
				$scope.$watch('model.item', function(item) {
				
					if(item !== undefined) {
				
						// TODO : replace multivalue with select2 and clear origin input
						// Remove this watcher if setRules.queryBuilder.filter works
						
						console.log(item);
					
					}
				
				});				
				
				
			/* ------: Create filter :------ */
				
				$scope.$watch('settings', function(settings) {
				
				/* ------: Setup filter :------ */
					
					if(settings !== undefined) {
						
					/* ------: Override default behaviour :------ */
						
						
						
						$("#where-" + $attrs.id).on('setRules.queryBuilder.filter', function(e) {
							
							//console.log(e.value);
							
						});
						
						$("#where-" + $attrs.id).on('afterUpdateRuleFilter.queryBuilder', function(e, rule) {
							
							
							/*
							$(e.target).find(".rule-filter-container").find(".form-control").css('min-width', '200px').removeClass('form-control').select2({
								allowClear: true
							});
							
							$(e.target).find('.select2-results').on('click', 'li', function(){
								
								$(this).find('li').toggle();
								
							});
							*/
							
						});
						
						$("#having-" + $attrs.id).on('afterUpdateRuleFilter.queryBuilder', function(e, rule) {
							
							/*
							$(e.target).find(".rule-filter-container").find(".form-control").css('min-width', '200px').removeClass('form-control').select2({
								allowClear: true
							});
							
							$(e.target).find('.select2-results').on('click', 'li', function(){
								
								$(this).find('li').toggle();
								
							});
							*/
							
						});						
						
						$("#where-" + $attrs.id).on('afterAddRule.queryBuilder', function(e, rule, error, value) {
							
							/*
							$(e.target).find(".rule-filter-container").find(".form-control").css('min-width', '200px').removeClass('form-control').select2({
								allowClear: true
							});
							
							$(e.target).find('.select2-results').on('click', 'li', function(){
								
								$(this).find('li').toggle();
								
							});
							*/
							
						});
						
						$("#where-" + $attrs.id).on('afterCreateRuleFilters.queryBuilder', function(e, rule, error, value) {
							
							//$(e.target).find(".rule-filter-container").find(".form-control").val("aaa");
							
							/*
							$(e.target).find(".rule-filter-container").find(".form-control").css('min-width', '200px').removeClass('form-control').select2({
								allowClear: true
							});
							
							$(e.target).find('.select2-results').on('click', 'li', function(){
								
								$(this).find('li').toggle();
								
							});
							*/
							
						});
						
						$("#having-" + $attrs.id).on('afterCreateRuleFilters.queryBuilder', function(e, rule, error, value) {
							
							// $(e.target).find(".rule-filter-container").find(".form-control").val("aaa");
							
							/*
							$(e.target).find(".rule-filter-container").find(".form-control").css('min-width', '200px').removeClass('form-control').select2({
								allowClear: true
							});
							
							$(e.target).find('.select2-results').on('click', 'li', function(){
								
								$(this).find('li').toggle();
								
							});
							*/
							
						});						
						
						$("#where-" + $attrs.id).on('afterCreateRuleOperators.queryBuilder', function(e) {
							
							//$(e.target).find(".rule-operator-container").find(".form-control").css('min-width', '150px').removeClass('form-control').select2();
							
						});	
						
						$("#having-" + $attrs.id).on('afterCreateRuleFilters.queryBuilder', function(e,u) {
							
							
						});
						
						$("#having-" + $attrs.id).on('afterCreateRuleOperators.queryBuilder', function(e) {
							
							//$(e.target).find(".rule-operator-container").find(".form-control").css('min-width', '150px').removeClass('form-control').select2();
							
						});							
						
						$("#where-" + $attrs.id).on('getRules.queryBuilder.filter', function(e) {
							
							// TODO : refactor recursively (because of subrules)
							
							$.each(e.value.rules, function(index, rule) {
								
								switch(rule.data.aspect.mode) {
									
									case "multivalue":
										
										if (typeof rule.value === 'string') {
											
											e.preventDefault();
											var values = rule.value;
											e.value.rules[index].value = [];
											e.value.rules[index].value = values.split(",");
											
											if(rule.data.aspect.mapkeyType === "integer") {
												
												$.each(rule.value, function(index2) {	
													
													if(!isNaN(parseInt(e.value.rules[index].value[index2]))) {
														
														e.value.rules[index].value[index2] = parseInt(e.value.rules[index].value[index2]);
														
													}
													
												});
												
											}
											
										} 
										
									break;
									
								}
								
							});
							
						});
						
						
					/* ------: Create plugins :------ */
						
						/* ------: Plugin : Dateange picker :------ */
						
						$.fn.filter_daterangepicker = function(options) {
							
							//https://github.com/mistic100/jQuery-QueryBuilder/issues/241
							
							function createInput(el){
								
								$(el).attr('id', $(el).attr('name'));
								
								$("#" + $(el).attr('id')).daterangepicker({
									
									"singleDatePicker": true,
									"showDropdowns": true,
									"timePicker": options.timePicker,
									"timePicker24Hour": true,
									"timePickerSeconds": true,
									"autoApply": true,
									"locale": {
										"format": options.format
									}							    
									
								});
								
							}
							
							this.on("click", createInput(this));
							
							return this;
							
						};
						
						/* ------: Plugin : Select2 :------ */
						
						$.fn.filter_multivalue = function(options) {
							
							// http://select2.github.io/select2/#programmatic
							
							function createInput(el){
								
								$(el).attr('id', $(el).attr('name'));
								
								$("#" + $(el).attr('id')).css('min-width', '200px').removeClass('form-control');
								
								switch(options.aspect.loading) {
									
									case "init":
										
										$("#" + $(el).attr('id')).select2({
											
											data : options.values,
											multiple: true
											
										});
										
									break;
									
									
									case "searchfull":
										
										$("#" + $(el).attr('id')).select2({
											
											multiple: true,
											
											initSelection: function (element, callback) {
												
												$.each($scope.model.itemdata.where.rules, function(index, value) {
													
													var values = el[0].value.split(",");
													
													var data = [];
													
													$.each(values, function(index, value) {
														
														data.push({"text": value, "id": value});
														
													});
													
													callback(data);
													
												});
												
											},
											
											query: function (query) {
												
												// TODO : implement here delay
												
												if(query.term.length > 2) {
													
													switch(options.aspect.mapkeyType) {
														
														case "integer":
															
															if (query.term.match(/^[0-9]+$/) === null) {
																
																query.callback({results:[]});
																return false;
																
															} else {
																
																query.term = " = " + query.term;
																
															}
															
														break;
														
														default:
															
															query.term = " LIKE '%" + query.term + "%'";
															
														break;
														
													}
													
													Data.select({
														
														id : options.aspect.model,
														filter : {
															query : {
																sql :  options.aspect.mapkey + query.term
															}
														}
														
													})
													
													.success(function(response) {
														
														list = [];
														
														$.each(response.data, function(ind, value) {
															
															list.push({
																id : value[options.aspect.mapkey].toString(),
																text : value[options.aspect.mapvalue].toString()
															});
															
														});	
														
														
														var data = {
															results : list
														};
														
														query.callback(data);
														
													})
													
													.error(function(response) {
														
														query.callback({results:[]});	
														
														dhtmlx.message({
															type: "error",
															text: response.error,
															expire: 10000
														});	
														
													});
													
												} else {
													
													query.callback({results:[]});
													
												}
												
											}
											
										});
										
									break;								
									
									
									case "clickfull":
										
										$("#" + $(el).attr('id')).select2({
											
											multiple: true,
											
											// TODO : No need for query, just Data.select
											
											query: function (query) {
												
												Data.select({id:options.aspect.model})
												
												.success(function(response) {
													
													list = [];
													
													$.each(response.data, function(ind, value) {
													
														list.push({
															id : value[options.aspect.mapkey],
															text : value[options.aspect.mapvalue]
														});
														
													});	
													
													var data = {
														results : list
													};
													
													query.callback(data);
													
												})
												
												.error(function(response) {
													
													alert("Aspect data loading error");
													
												});
												
											}
											
										});
										
									break;									
									
									
									default:
										
										dhtmlx.message({
											type: "error",
											text: "Aspect config loading error",
											expire: 30000
										});
										
									break;
									
								}
								
								
								
							}
							
							this.on("click", createInput(this));
							
							return this;
							
						};					
						
						
					/* ------: Setup filter :------ */
						
						$.each($scope.settings, function(index, value) {
							
							var item = {
								
								id : $scope.settings[index]["Field name"], //index (table prefix),
								label : $scope.settings[index]["Field title"],
								optgroup : $scope.settings[index]["Table title"],
								data : {
									aspect : { type : null }
								}
								
							};
							
							switch($scope.settings[index].Aspect) {
								
								case "0":
									
									switch($scope.settings[index].Type) {
										
										case "integer":
											
											item.type = "integer";
											
										break;
										
										
										case "double":
											
											item.type = "double";
											
										break;
										
										
										case "string":
											
											item.type = "string";
											item.default_value = "";
											item.validation = {
												format: "string",
												min: 0,
												max: 1000,
												callback: function (value){
													return true; // Validation disabled
												}
											};
											
										break;
										
										
										case "select":
											
											item.type = "string";
											item.input = "select";
											item.operators = Array('equal', 'not_equal');						
											item.description = "Please select one item";
											//ob.values = values[response.row[index].cell[0]];
											
										break;
											
											
										case "radio":
											
											item.type = "integer";
											item.input = "radio";
											item.values = {
												1: 'Yes',
												0: 'No'
											};
											item.colors = {
												1: 'success',
												0: 'danger'
											};		
											item.operators = ['equal'];
											item.description = "This is a test";
											
										break;												
											
											
										case "date":
											
											item.type = 'date';
											item.input = 'text';
											item.plugin = 'filter_daterangepicker';
											item.plugin_config = {
											//	plugin : "my_plugin",
											//	defaultDate: new Date(),
												format: 'YYYY-MM-DD',
												timePicker : false
											};
											//item.validation = {
											//	callback: function (value){
											//		return true; // Validation disabled
											//	}
											//};
											
										break;
										
										
										case "datetime":
											
											item.type = 'date';
											item.input = 'text';
											item.plugin = 'filter_daterangepicker';
											item.plugin_config = {
											//	plugin : "my_plugin",
											//	defaultDate: new Date(),
												format: 'YYYY-MM-DD HH:mm:ss',
												timePicker : true
											};
											//item.validation = {
											//	callback: function (value){
											//		return true; // Validation disabled
											//	}
											//};
											
										break;										
											
											
										default:
											
											item.type = "string";
											item.default_value = "";
											item.validation = {
												format: "string",
												min: 0,
												max: 1000,
												callback: function (value){
													return true; // Validation disabled
												}
											},
											item.operators = Array('contains', 'equal', 'not_equal', 'is_null', 'is_not_null');
											
										break;
										
									}
									
								break;
								
								
								case "1":
									
									var aspect = JSON.parse($scope.settings[index].aspectconfig);
									
									// NOTICE : passed aspect.type to getRules becomes as an empty string
									// Do not know why, so aspect.mode should be used in getRules instead of aspect.type
									// TODO : refactor code, remove aspect.type completely, and use aspect.mode
									
									aspect.mode = aspect.type; 
									
									switch(aspect.mode) {
										
										case "checkbox":
											
											item.type = aspect.mapkeyType;
											item.input = 'checkbox';
											item.values = $scope.model.aspects[index];
											item.operators = ['in', 'not_in'];
											item.color = 'primary';
											item.default_value = "";
											item.validation = {
												format: "string",
												min: 0,
												max: 1000,
												callback: function (value){
													return true; // Validation disabled
												}
											};
											
										break;
										
										case "multivalue":
											
											item.type = 'string';
											item.input = 'text';
											item.data = {
												aspect : aspect
											};
											item.plugin = 'filter_multivalue';
											item.operators = ['in', 'not_in'];
											item.plugin_config = {
												values : $scope.model.aspects[index],
												aspect : aspect
											};
											
										break;		
										
										case "multiselect":
											
											item.type = 'string';
											item.input = 'select';
											item.multiple = true;
											item.operators = ['in', 'not_in'];
											item.values = $scope.model.aspects[index];
											
										break;											
										
									}
									
								break;
								
							}
							
							// Add to filter list
							
							if($scope.settings[index].Filtering == 1) {
								
								switch($scope.settings[index].Aggregate){
									
									case "0":
										
										$scope.model.filterItems.where.push(item);
										
									break;
									
									
									case "1":
										
										$scope.model.filterItems.having.push(item);
										
									break;									
									
								}
								
							}
							
							
							// Add to orderby list
							
							if($scope.settings[index].Ordering == 1) {
								
								if(item.label.length === 0) item.label = item.id;
								$("#orderby-" + $attrs.id)
								.append($('<option>', { value : item.id })
								.text(item.label)); 
								
							}
							
						});
						
						
						// Build filter
						
						try {
							
							if($scope.model.filterItems.where.length > 0){
								
								$("#where-" + $attrs.id).queryBuilder({
									
									plugins : $scope.model.plugins,
									filters : $scope.model.filterItems.where,
									select_placeholder : "",
									//optgroups : {
									//	core: '-- Core',
									//},
									default_condition : "AND" 
									
								});
								
							} else {
								
								$("#filterrecords-" + $attrs.id).prop("checked", false);
								$("#filterrecords-content" + $attrs.id).hide();
								
							}
							
							if($scope.model.filterItems.having.length > 0){
								
								$("#having-" + $attrs.id).queryBuilder({
									
									plugins : $scope.model.plugins,
									filters : $scope.model.filterItems.having,
									select_placeholder : "",
									default_condition : "AND" 
									
								});
								
							} else {
								
								$("#filteraggregates-" + $attrs.id).prop("checked", false);
								$("#filteraggregates-content" + $attrs.id).hide();
								
							}
							
							if($scope.model.filterItems.where.length === 0 && $scope.model.filterItems.having.length === 0) {
								
								$("#filter-" + $attrs.id).append("<p style='color:brown;padding:20px;'>All items are disabled in the filter.</p>");
								
							}
							
						} catch (error) {
							
							dhtmlx.message({
								type: "error",
								text: error,
								expire: 30000
							});		
							
						}
						
						// Load item
						
						$scope.loadItem($attrs.id);
						
					}
					
				});
				
				
			/* ------: Load item :------ */	
				
				$scope.itemLoad = function(data){
					
					if(data.data !== "") {
						
						try {
							
							$scope.model.itemdata = JSON.parse(data.data);
							
							$("#filteraggregates-" + $attrs.id).prop("checked", JSON.parse($scope.model.itemdata.filteraggregates));
							$("#filterrecords-" + $attrs.id).prop("checked", JSON.parse($scope.model.itemdata.filterrecords));
							$("#limit-" + $attrs.id).val($scope.model.itemdata.limit);
							$("#ordervector-" + $attrs.id).val($scope.model.itemdata.ordervector);
							$("#orderby-" + $attrs.id).val($scope.model.itemdata.orderby);
							
							if($scope.model.itemdata.where != "") {
								
								$('#where-' + $attrs.id).queryBuilder('setRules', $scope.model.itemdata.where);
								
							}
							
							if($scope.model.itemdata.having != "") {
							
								$('#having-' + $attrs.id).queryBuilder('setRules', $scope.model.itemdata.having);
							
							}
							
						} catch(e){
							
							dhtmlx.message({
								type: "error",
								text: "Can not load an item : " + e,
								expire: 30000
							});
							
						}
						
					}
					
				};
				
				
			/* ------: Get conditions :------ */
				
				$scope.getConditions = function(){
					
					// check do we need get record and aggregation filter
					
					var conditions = 
					{
						component : $element[0].localName,
						filter : {
							filterrecords : $("#filterrecords-" + $attrs.id)[0].checked,
							filteraggregates : $("#filteraggregates-" + $attrs.id)[0].checked,
							query : $("#filterrecords-" + $attrs.id)[0].checked ? $('#where-' + $attrs.id).queryBuilder('getSQL') : undefined,
							querydata : $("#filterrecords-" + $attrs.id)[0].checked ? $('#where-' + $attrs.id).queryBuilder('getRules') : undefined,
							having : $("#filteraggregates-" + $attrs.id)[0].checked ? $('#having-' + $attrs.id).queryBuilder('getSQL') : undefined,
							havingdata : $("#filteraggregates-" + $attrs.id)[0].checked ? $('#having-' + $attrs.id).queryBuilder('getRules') : undefined
						},
						orderby : $("#orderby-" + $attrs.id).val(),
						ordervector : $("#ordervector-" + $attrs.id).val(),
						limit : $("#limit-" + $attrs.id).val(),
					};
					
					return conditions;
					
				};
				
				
			/* ------: Apply filter :------ */
				
				$scope.execute = function(data){
					
					var conditions = $scope.getConditions();
					
					if 
					(
						
						(
							$("#filteraggregates-" + $attrs.id)[0].checked &&
							$.isEmptyObject(conditions.filter.havingdata)
						) 
						
						||
						
						(
							$("#filterrecords-" + $attrs.id)[0].checked &&
							$.isEmptyObject(conditions.filter.querydata)
						)						
						
					) {
						
						dhtmlx.message({
							type: "error",
							text: "Error in filter, verify conditions!",
							expire: 10000
						});
						
					} else {
						
						if
						(
							$scope.model.filterhash == $.md5(JSON.stringify(conditions)) && 
							JSON.parse($attrs.settings).ignoresamecond == 1 &&
							data.component !== "paginator"
						)
						{
							
							dhtmlx.message({
								type: "error",
								text: "Same conditions",
								expire: 5000
							});
							
						} else 
						{
							
							$scope.model.filterhash = $.md5(JSON.stringify(conditions));
							
							$rootScope.$broadcast(JSON.parse($attrs.settings).datagrid);
							
						}
						
					}
					
				};
				
				
			/* ------: Response :------ */
				
				$rootScope.$on("filter.request", function(event, source){
					
					$rootScope.$broadcast(source + ".response",{
						component : "filter",
						data : $scope.getConditions()
					});
					
				});	
				
				
			/* ------: Save filter :------ */
				
				$scope.save = function(){
					
					try {
						
						var data = {
							
							where : $scope.model.filterItems.where.length > 0 ? $('#where-' + $attrs.id).queryBuilder('getRules') : null,
							having : $scope.model.filterItems.having.length > 0 ? $('#having-' + $attrs.id).queryBuilder('getRules') : null
							
						};
						
						if (!$.isEmptyObject(data.where) || !$.isEmptyObject(data.having)) {
							
							data.orderby = $("#orderby-" + $attrs.id).val(),
							data.ordervector = $("#ordervector-" + $attrs.id).val(),
							data.limit = $("#limit-" + $attrs.id).val(),
							data.filterrecords = $("#filterrecords-" + $attrs.id)[0].checked,
							data.filteraggregates = $("#filteraggregates-" + $attrs.id)[0].checked,
							
							// TODO : change to Item save
							
							$scope.saveItem($attrs.id, data);
							
							/*
							
							Object.save({
								
								id : $attrs.cmp,
								type : "component",
								data : data
								
							})
							
							.success(function(response) {
								
								// TODO : change notify message (Item "xx" saved.)
								
								dhtmlx.message({
									text: "Done",
									expire: 30000
								});
								
							})
							
							.error(function(response) {
								
								// TODO : change notify message (Error to save item "xx" : error.message)
								
								dhtmlx.message({
									type: "error",
									text: response.error,
									expire: 30000
								});
								
							});
							
							*/
							
						}
						
					} catch (e) {
						
						dhtmlx.message({
							
							type: "error",
							text: "Save filter (" + e + ")",
							expire: 10000
							
						});
						
					}
				};
				
				
			/* ------: Get model :------ */
				
				try {
					
					Object.get({
						
						id : JSON.parse($("#" + JSON.parse($attrs.settings).datagrid).attr("settings")).model,
						type : "model",
						
					})
					
					.success(function(response) {
						
						response = response.data;
						
						// Get aspects
						
						var settings = JSON.parse(response.data.settings); 
						
						var count = {
							
							total : 0,
							totalprocessed : 0,
							aspects : 0,
							aspectsprocessed : 0
							
						};
						
						// Count toal
						
						for (var k in settings) { if (settings.hasOwnProperty(k)) { ++count.total; } }
						
						// Count apsects
						
						$.each(settings, function(index, value) {
							
							if(settings[index].Aspect == "1"){
								
								++count.aspects;
								
							}
							
						});
						
						$.each(settings, function(index, value) {
							
							if(settings[index].Aspect == "1") {
								
								var aspectconfig = JSON.parse(settings[index].aspectconfig);
								
								if(aspectconfig.loading === "init") {
									
									Data.select({
										
										id : aspectconfig.model
										
									})
									
									.success(function(data) {
										
										++count.totalprocessed;
										++count.aspectsprocessed;
										
										var list = null;
										
										switch (aspectconfig.type){
											
											case "checkbox":
											case "multiselect":
												
												list = {};
												
												$.each(data.data, function(ind, value) {
													
													list[value[aspectconfig.mapkey]] = value[aspectconfig.mapvalue];
													
												});
												
												$scope.model.aspects[index] = list;
												
											break;
											
											
											case "multivalue":
												
												list = [];
												
												$.each(data.data, function(ind, value) {
													
													list.push({
														id : value[aspectconfig.mapkey],
														text : value[aspectconfig.mapvalue]
													});
													
												});											
												
												$scope.model.aspects[index] = list;
												
											break;
											
										}
										
										
										if(count.total === count.totalprocessed && count.aspects === count.aspectsprocessed) {
											
											$scope.settings = JSON.parse(response.data.settings);
											
										}
										
									})
									
									.error(function(response, status) {
										
										dhtmlx.message({
											
											type: "error",
											text: "Filter configuration error (" + response.error + ")",
											expire: 30000
											
										});	
										
									});
									
								} else {
									
									$scope.model.aspects[index] = {};
									++count.totalprocessed;
									++count.aspectsprocessed;	
									
									if(count.total === count.totalprocessed && count.aspects === count.aspectsprocessed) {
										
										$scope.settings = JSON.parse(response.data.settings);
										
									}									
									
								}
								
							} else {
								
								++count.totalprocessed;
								
								if(count.total === count.totalprocessed && count.aspects === count.aspectsprocessed) {
									
									$scope.settings = JSON.parse(response.data.settings);
									
								}
								
							}
							
						});
						
					});
					
				} catch(e) {
					
					dhtmlx.message({
						
						type: "error",
						text: "Filter configuration error (" + e + ")",
						expire: 30000
						
					});					
					
				}
				
				
			/* ------: Response :------ */
				
				$rootScope.$on("filter." + JSON.parse($attrs.settings).datagrid, function(event, data) {
					
					$scope.execute(data);
					
				});
				
				
			/* ------: Listener : loaditem :------ */
				
				$scope.$on('loaditem', function(events, data){
					
					$scope.itemLoad(data);
					
				});
				
			}
			
		};
		
	});
		
// EOF