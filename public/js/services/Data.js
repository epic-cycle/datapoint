/**
 *	Factory to communicate with REST service for Views
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Data", [])

	.factory("Data", function($http, Model) {

	/* -------------------------------- */			
		
		var controller = "/data";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"select" : {
					
					method:"POST",
					url: controller + "/select",
					title:"Select",
					objects : {
						
						get : function (){
							return Model.index({});	
						},
						storage : "models"
						
					}	
					
				},
				
				"count" : {
					
					method:"POST",
					url: controller + "/count",
					title:"Count records"
					
				},				
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data !== undefined) {
					
					if(data.id !== undefined) { 
						
						if($.isArray(data.objects)) {
							
							id = "/" + data.id.join(',');
							
						} else {
							
							id = "/" + data.id;
							
						}
						
					} 
				} else {
					
					data = {};
					
				}
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	   
		
	});

// EOF