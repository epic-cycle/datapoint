/**
 *	Factory to communicate with REST service for Interpolators
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Interpolators", [])

	.factory("Interpolators", function($http) {

	/* -------------------------------- */			
		
		var controller = "/interpolators";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"get" : {
					method:"GET",
					url: controller,
					title:"Get mount"
				},
				
				"pipe" : {
					method:"GET",
					url: controller + "/pipe",
					title:"Create mount"
				}
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data !== undefined) {
					
					if(data.id !== undefined) { 
						
						if($.isArray(data.objects)) {
							
							id = "/" + data.id.join(',');
							
						} else {
							
							id = "/" + data.id;
							
						}
						
					} 
				} else {
					
					data = {};
					
				}
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	   
		
	});

// EOF