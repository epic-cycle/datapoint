/**
 *	Factory to communicate with REST service for Users
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.User", [])

	.factory("User", function($http) {

	/* -------------------------------- */
		
		var controller = "/users";

	/* -------------------------------- */
		
		var service = { 
			
			methods :  {
				
				"index" : {
					method:"GET",
					url: controller,
					title:"Get all users"
				},
				
				"delete" : {
					method:"DELETE",
					url: controller,
					title:"Delete user",
					objects : {
						
						get : function (){
							return service.index({});	
						},
						storage : "users"
						
					}
				},
				
				"create" : { 
					method:"POST",
					url: controller,
					title:"Create user"
				},
				
				"setPermissions" : { 
					method:"PUT", 
					url: controller + "/permissions",				
					title:"Set permissions",
					objects : {
						
						get : function (){
							return service.index({});	
						},
						storage : "users"
						
					}
				},				
				
				"getPermissions" : {
					method:"GET",
					url: controller + "/permissions",
					title:"Get permissions"
				},	
				
				"removePermissions" : {
					method:"DELETE",
					url: controller + "/permissions",
					title:"Remove permissions"
				},					
				
				"getRoles" : {
					method:"GET",
					url: controller + "/roles",
					title:"Get roles"
				},	
				
				"setRoles" : {
					method:"PUT",
					url: controller + "/roles/set",
					title:"Set roles"
				},
				
				"addRoles" : {
					method:"PUT",
					url: controller + "/roles/add",
					title:"Add roles"
				},		
				
				"removeRoles" : {
					method:"PUT",
					url: controller + "/roles/remove",
					title:"Remove roles"
				},					
				
			}
			
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var objects = "";
				
				if(data.objects !== undefined) { 
					
					objects = "/" + data.objects.join(',');
					
				} 
					
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + objects,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */
	    
});