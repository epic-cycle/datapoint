/**
 *	Factory to communicate with REST service for Items
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Report", [])

	.factory("Report", function($http) {
		
	/* -------------------------------- */			
		
		var controller = "/report";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"save" : {
					method : "PUT",
					url : controller,
					title :"Save report"
				}
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data.id !== undefined) { 
					
					if($.isArray(data.objects)) {
						id = "/" + data.id.join(',');
					} else {
						id = "/" + data.id;
					}
					
				} 
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	 
		
	});

// EOF