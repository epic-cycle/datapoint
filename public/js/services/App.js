/**
 *	Factory to communicate with REST service for Mounts
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.App", [])

	.factory("App", function($http, View, User) {

	/* -------------------------------- */			
		
		var controller = "/app";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"views" : {
					method:"GET",
					url: controller + "/views",
					title:"Views",
					objects : {
						
						get : function (){
							return View.index({});	
						},
						storage : "views"
						
					}
				},
				
				"users" : {
					method:"GET",
					url: controller + "/users",
					title:"Users"
				},
				
				"roles" : {
					method:"GET",
					url: controller + "/roles",
					title:"Roles"
				},
				
				"editmodel" : {
					method:"GET",
					url: controller + "/editmodel",
					title:"Edit model",
					objects : {
						
						get : function (){
							return View.index({});	// Change to Model.index()
						},
						storage : "models"
						
					}						
				},
				
				"graph" : {
					method:"GET",
					url: controller + "/graph",
					title:"Graphs"
				},

				"ace" : {
					method:"GET",
					url: controller + "/ace",
					title:"Ace"
				},
				
				"spreadjs" : {
					method:"GET",
					url: controller + "/spreadjs",
					title:"SpreadJS"
				},				
				
				"editview" : {
					method:"GET",
					url: controller + "/editview",
					title:"Edit view",
					objects : {
						
						get : function (){
							return View.index({});	
						},
						storage : "views"
						
					}					
				},
				
				"gridstack" : {
					method:"GET",
					url: controller + "/gridstack",
					title:"Gridstack",
					objects : {
						
						get : function (){
							return View.index({});	
						},
						storage : "views"
						
					}						
				},				
				
			}
			
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data !== undefined) {
					
					if(data.id !== undefined) { 
						
						if($.isArray(data.objects)) {
							
							id = "/" + data.id.join(',');
							
						} else {
							
							id = "/" + data.id;
							
						}
						
					} 
				} else {
					
					data = {};
					
				}
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	   
		
	});

// EOF