/**
 *	Factory to communicate with REST service for Items
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Item", [])

	.factory("Item", function($http) {
		
	/* -------------------------------- */			
		
		var controller = "/items";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"index" : {
					method : "POST",
					url : controller + "/index",
					title :"Get items"
				},
				
				"save" : {
					method : "PUT",
					url : controller,
					title :"Save item"
				},
				
				"get" : {
					method : "POST",
					url : controller + "/get",
					title :"Get item"
				},
				
				"add" : {
					method : "POST",
					url : controller + "/add",
					title :"Add item"
				},
				
				"delete" : {
					method : "DELETE",
					url : controller,
					title :"Delete item"
				},
				
				"update" : {
					method : "POST",
					url : controller + "/update",
					title :"Update item"
				}					
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data.id !== undefined) { 
					
					if($.isArray(data.objects)) {
						id = "/" + data.id.join(',');
					} else {
						id = "/" + data.id;
					}
					
				} 
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	 
		
	});

// EOF