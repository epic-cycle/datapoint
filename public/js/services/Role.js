/**
 *	Factory to communicate with REST service for Roles
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Role", [])

	.factory("Role", function($http) {

	/* -------------------------------- */			
		
		var controller = "/roles";
		
	/* -------------------------------- */			

		var service = { 
			
			methods :  {
				
				"index" : {
					method:"GET",
					url: controller,
					title:"Get all roles"
				},
				
				"create" : {
					method:"POST",
					url: controller,
					title:"Create role"
				},
				
				"delete" : {
					method:"DELETE",
					url: controller,
					title:"Delete role"
				},					
				
				"setPermissions" : { 
					method:"PUT", 
					url: controller + "/permissions",				
					title:"Set permissions"
					
				},
				
				"getPermissions" : {
					method:"GET",
					url: controller + "/permissions",
					title:"Get permissions"
				},	
				
				"removePermissions" : {
					method:"DELETE",
					url: controller + "/permissions",
					title:"Remove permissions"
				},					
				
			}
			
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var objects = "";
				
				if(data.objects !== undefined) { 
					
					objects = "/" + data.objects.join(',');
					
				} 
					
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + objects,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	    
	    
	});