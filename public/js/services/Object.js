/**
 *	Factory to communicate with REST service for Objects
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Object", [])

	.factory("Object", function($http) {
		
	/* -------------------------------- */			
		
		var controller = "/objects";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"save" : {
					method:"POST",
					url: controller + "/save",
					title:"Save object"
				},
				
				"get" : {
					method:"POST",
					url: controller,
					title:"Get object",
					objects : {
						
						get : function (){
							return {data:[{id:"1",name:"asd"}]};	
						},
						storage : "objects"
						
					}					
				},
				
				"roles" : {
					method:"POST",
					url: controller + "/roles",
					title:"Get roles"
				},	
				
				"users" : {
					method:"POST",
					url: controller + "/users",
					title:"Get users"
				},
				
				"versions" : {
					method:"POST",
					url: controller + "/versions",
					title:"Get versions"
				},
				
				"revert" : {
					method:"POST",
					url: controller + "/revert",
					title:"Revert version"
				}	
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data.id !== undefined) { 
					
					if($.isArray(data.objects)) {
						id = "/" + data.id.join(',');
					} else {
						id = "/" + data.id;
					}
					
				} 
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	 
		
	});

// EOF