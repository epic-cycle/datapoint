/**
 *	Factory to communicate with REST service for Source
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Source", [])

	.factory("Source", function($http) {

	/* -------------------------------- */
		
		var controller = "/sources";

	/* -------------------------------- */
		
		var service = { 
			
			methods :  {
				
				"index" : {
					method:"GET",
					url: controller,
					title:"Get all datasources"
				},	
				"get" : {
					method:"GET",
					url: controller,
					title:"Get datasource"
				},				
				"test" : {
					method:"POST",
					url: controller + "/test",
					title:"Test datasource"
				},
				
			}
			
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var objects = "";
				
				if(data.objects !== undefined) { 
					
					objects = "/" + data.objects.join(',');
					
				} 
					
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + objects,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */
	    
});