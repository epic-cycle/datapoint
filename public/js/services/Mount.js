/**
 *	Factory to communicate with REST service for Mounts
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */



/* ------: Factory :------ */

angular.module("factory.Mount", [])

	.factory("Mount", function($http) {

	/* -------------------------------- */			
		
		var controller = "/mounts";
		
	/* -------------------------------- */			
		
		var service = { 
			
			methods :  {
				
				"index" : {
					method:"GET",
					url: controller,
					title:"Get all mounts"
				},
				
				"get" : {
					method:"GET",
					url: controller,
					title:"Get mount"
				},
				
				"store" : {
					method:"POST",
					url: controller,
					title:"Create mount"
				},	
				
				"update" : {
					method:"PUT",
					url: controller,
					title:"Update mount",
					objects : {
						
						get : function (){
							return $http.get(controller);
						},
						storage : "mounts"
						
					}
				},	
				
				"setparent" : {
					method:"PUT",
					url: controller + "/setparent",
					title:"Set parent",
					objects : {
						
						get : function (){
							return $http.get(controller);
						},
						storage : "mounts"
						
					}
				},					
				
				"delete" : {
					method:"DELETE",
					url: controller,
					title:"Delete mount",
					objects : {
						
						get : function (){
							return $http.get(controller);
						},
						storage : "mounts"
						
					}					
				}
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data !== undefined) {
					
					if(data.id !== undefined) { 
						
						if($.isArray(data.objects)) {
							
							id = "/" + data.id.join(',');
							
						} else {
							
							id = "/" + data.id;
							
						}
						
					} 
				} else {
					
					data = {};
					
				}
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	   
		
	});

// EOF