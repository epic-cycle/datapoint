/**
 *	Factory to communicate with REST service for Databuilder
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Databuilder", [])

	.factory("Databuilder", function($http, Source) {

	/* -------------------------------- */
		
		var controller = "/databuilder";

	/* -------------------------------- */
		
		var service = { 
			
			methods :  {
				
				"testSQL" : {
					method:"POST",
					url: controller + "/test/sql",
					title:"SQL Test"
				},
				
				"testScript" : {
					method:"POST",
					url: controller + "/test/script",
					title:"Script Test"
				},				
				
				"runSQL" : {
					method:"POST",
					url: controller + "/run/sql",
					title:"SQL Run",
					objects : {
						
						get : function (){
							return Source.index({});	
						},
						storage : "sources"
						
					}					
				},
				
				"prettysql" : {
					method:"POST",
					url: controller + "/prettysql",
					title:"SQL Beautify"
				},
				
				"schema" : {
					method:"GET",
					url: controller + "/schema",
					title:"Get schema",
					objects : {
						
						get : function (){
							return Source.index({});	
						},
						storage : "sources"
						
					}
				},
				
				"fields" : {
					method:"POST",
					url: controller + "/fields",
					title:"Get fields",
					objects : {
						
						get : function (){
							return Source.index({});	
						},
						storage : "sources"
						
					}					
				}
				
			}
			
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var objects = "";
				
				if(data !== undefined && data.id !== undefined) { 
					
					id = "/" + data.id.join(',');
					
				}
					
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */
	    
});