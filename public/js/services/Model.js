/**
 *	Factory to communicate with REST service for Views
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

/* ------: Factory :------ */

angular.module("factory.Model", [])

	.factory("Model", function($http) {

	/* -------------------------------- */			
		
		var controller = "/models";
		
	/* -------------------------------- */			

		var service = { 
			
			methods :  {
				
				"index" : {
					method: "GET",
					url: controller,
					title: "Get all models"
				}
				
			}
		};
		
	/* -------------------------------- */
		
		angular.forEach(service.methods, function(value, key) {
			
			service[key] = function(data){
				
				var id = "";
				
				if(data !== undefined) {
					
					if(data.id !== undefined) { 
						
						if($.isArray(data.objects)) {
							
							id = "/" + data.id.join(',');
							
						} else {
							
							id = "/" + data.id;
							
						}
						
					} 
				} else {
					
					data = {};
					
				}
				
				data.reqid = chance.hammertime();
				
				var params = {
					method: service.methods[key].method,
					url: service.methods[key].url + id,
					headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
					data: $.param(data)
				};
				
				return $http(params);			  	
				
			};
			
		});
		
	/* -------------------------------- */
		
		return service;
		
	/* -------------------------------- */	   
		
	});

// EOF