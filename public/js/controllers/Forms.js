/**
 *	Module for controller "Forms"
 * 
 *  	@doc	module
 * 	@name	controller.Forms
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Forms', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Forms
	 * 
	 *	@doc	controller
	 * 	@name	Forms
	 *	@duty	Rolands Strickis
	 *	@link	
	*/
	
	.controller('Forms', function($rootScope, $scope, $compile, $http, $attrs, $element) {
		
		$scope.submit = function(){

			if($scope.processForm !== undefined) {
				
				$scope.processForm($scope.getData());
				
			} else {
				
				$scope.parent.processForm($scope.getData());
				
			}
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};	
		
		$scope.cancelForm = function (){
			
			if($scope.modal !== undefined) {
			
				$scope.modal.window.window.close();
				delete $scope.modal;				
				
			} else {
				
				$scope.parent.modal.window.window.close();
				delete $scope.parent.modal;
				
			}
		};		
		
	});

// EOF