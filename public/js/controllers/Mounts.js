/**
 *	Module for controller "Mounts"
 * 
 *  	@doc	module
 * 	@name	controller.Mounts
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Mounts', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with mounts
	 * 
	 *	@doc	controller
	 * 	@name	Mounts
	 *	@duty	Rolands Strickis
	 *	@link	https://docs.optibet.lv/admin/mounts.html
	*/

	.factory('$parentScope', function($window) {
		return $window.parent.angular.element($window.frameElement).scope();
	})

	.controller('Mounts', function($rootScope, $scope, $compile, $http, $attrs, $parentScope, Mount) {
		
	/* -------------------------------- */
		
		// Get mounts on controller initialization
		
		Mount.index()
			
		/* -------------------------------- */	
			
			// If response from server is OK then 
			// display mounts and create functions
			
		.success(function(response) {
				
			/* -------------------------------- */	
				
				// Convert data to DHTMLX tree grid compatible format
				
				response = removeEmptyArrays(response);
				response = convertRowsAssocToNum(response);
				
			/* -------------------------------- */	
				
				// Create tree grid object and load data into it
				
				$scope.treeGridMounts.setImagePath("/vendor/proprietary/dhtmlx/codebase/imgs/");
				$scope.treeGridMounts.setIconsPath("/vendor/proprietary/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.treeGridMounts.setHeader("Name,Friendly URL");
				$scope.treeGridMounts.setInitWidths("*,200");
				$scope.treeGridMounts.setColAlign("left,left");
				$scope.treeGridMounts.setColTypes("tree,ed");
				$scope.treeGridMounts.setColSorting("str,str");
				$scope.treeGridMounts.enableDragAndDrop(true);
				$scope.treeGridMounts.enableMultiselect(true);
				$scope.treeGridMounts.init();
				$scope.treeGridMounts.setSkin("material");
				$scope.treeGridMounts.parse(response, "json");
				
			/* -------------------------------- */
				
				// Attach events to tree grid object
				
				/*
				 *	@doc	event
				 *	@name	onEditCell
				 *	@link	
				*/
				
				$scope.treeGridMounts.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol){
					
					$scope.setParent(sId,tId);
					
				});	
				
				$scope.treeGridMounts.attachEvent("onEditCell", function(stage, rId, cInd, nValue, oValue){
					
					// Check action
					
					switch(stage) {
						
						case 0:
							
							var value = $scope.treeGridMounts.cellById(rId,cInd).getValue();
							
						break;
						
						case 1:
							
							// 
							
						break;
						
						case 2:
							
							switch(cInd) {
								
								case 0:
									
									$scope.updateMount(rId, nValue, oValue, "name", cInd);
									
								break;
								
								case 1:
									
									$scope.updateMount(rId, nValue, oValue, "furl", cInd);
									
								break;								
								
								default:
									
									alert("No actions for this column");
									
								break;
								
							}
							
						break;
						
						default:
							
							alert("Unknown method");
							
						break;
					}
					
					return true;
					
				});
				
				
			/* -------------------------------- */					
				
				// Start application to edit view
				
				/**
				 *	@doc	function
				 *	@name	editView
				*/
				
				
				$scope.editView = function() {
					
					var id = $scope.treeGridMounts.getSelectedId();
					var title = "Edit view : " + $scope.treeGridMounts.getItemText(id);
					
					$parentScope.load("app/editview/" + id, title);
					
				};
				
			/* -------------------------------- */					
				
				// Start application to edit source
				
				/**
				 *	@doc	function
				 *	@name	editSource
				*/
				
				$scope.editSource = function() {
					
					var id = $scope.treeGridMounts.getSelectedId();
					var title = "Edit source : " + $scope.treeGridMounts.getItemText(id);					
					
					$parentScope.load("app/editsource/" + id, title);
					
				};
				
				
			/* -------------------------------- */					
				
				// Start application to edit model
				
				/**
				 *	@doc	function
				 *	@name	editModel
				*/
				
				$scope.editModel = function() {
					
					var id = $scope.treeGridMounts.getSelectedId();
					var title = "Edit model : " + $scope.treeGridMounts.getItemText(id);
					
					$parentScope.load("app/editmodel/" + id, title);
					
				};
				
				
			/* -------------------------------- */	
				
				// Delete mount and remove it into tree grid
				
				/**
				 *	@doc	function
				 *	@name	deleteMount
				*/
				
				$scope.deleteMount = function() {
					
					// Define request
					
					var request = {
						
						id : $scope.treeGridMounts.getSelectedId()
						
					};
					
					// Do request
					
					Mount.delete(request)
					
					.success(function(response) {
						
						$scope.treeGridMounts.deleteRow(response);
						
					})
					
					.error(function(response) {
						
						// TODO
						
					});
				};
				
				
			/* -------------------------------- */	
				
				// Store new mount and show it into tree grid
				
				/**
				 *	@doc	function
				 *	@name	storeMount
				*/
				
				$scope.storeMount = function() {
					
					// Define request
					
					var request = {
						
						parent : $scope.treeGridMounts.getSelectedId(),
						name : "New mount"
						
					};
					
					// Do request
					
					Mount.store(request)
					
					.success(function(response) {
						
						$scope.treeGridMounts.addRow(response.id,[request.name, 'text', 'text', 1, 1], 0, request.parent);
						
					})
					
					.error(function(response) {
						
						// TODO
						
					});
				};
				
			/* -------------------------------- */	
				
				// Update parent
				
				/**
				 *	@doc	function
				 *	@name	updateMount
				*/
				
				$scope.setParent = function(id, pid) {
					
					Mount.setparent({
						
						id : id,
						data: [
							{
								id : id,
								parent : pid
							}
						]
							
					})
						
					.error(function(response) {
						
						// TODO : move item back to initial position
						
						dhtmlx.message({
							type: "error",
							text: "Error to set parent : " + response.error,
							expire: 10000
						});	
						
					});
					
				};				
				
			/* -------------------------------- */	
				
				// Update mount
				
				/**
				 *	@doc	function
				 *	@name	updateMount
				*/
				
				$scope.updateMount = function(id, newValue, oldValue, field, cInd) {
					
					// Define request
					
					var request = {
						
						id : id,
						data: [
							{
								field : field,
								value : newValue
							}
						]
						
					};
					
					// Do request
					
					Mount.update(request)
					
					.success(function(response) {
						
						
					})
					
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: response.error,
							expire: 10000
						});						
						
						$scope.treeGridMounts.cellById(id, cInd).setValue(oldValue);
						
					});
				};
				
			})
			
		.error(function(response) {
			
			dhtmlx.message({
				type: "error",
				text: "Get all mounts : " + response,
				expire: 5000
			});			
			
		});
			
		
	});
		
// EOF