/**
 *	Module for controller "Versions"
 * 
 *  @doc	module
 * 	@name	controller.Versions
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Versions', [])

	/**
	 *	Controller to operate with Versions
	 * 
	 *	@doc	controller
	 * 	@name	Versions
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Versions', function($rootScope, $scope, $compile, $http, $attrs, Object) {
		
	/* ------: Modal window for revert to version :------ */
		
		$scope.modalRevertVersion = function() {
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 500);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			$scope.windowModal.setSkin("material");
			
		/* ------: Create bottom toolbar :------ */
			
			var bottomToolbar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			bottomToolbar.setText("<div id='toolbarObject' style='width:100%;text-align:center;'></div>");
			
			$scope.toolbarRevertVersion = new dhtmlXToolbarObject({
				parent : "toolbarObject",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "revert", type: "button", text: "Revert to selected", img: "revert-32px.png"
					},							
					{
						id: "close", type: "button", text: "Close", img: "close-18px.png"
					}
				]					
			});
			
			$scope.toolbarRevertVersion.setAlign("right");	
			
			$scope.toolbarRevertVersion.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "revert":
						
						$scope.revertVersion($scope.gridVersions.getSelectedId());
						$scope.windowModal.unload();
						
					break;
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});	
			
		/* ------: Create grid :------ */
			
			try {
				
				$scope.versionsLayout = $scope.windowModal.window.attachLayout("1C");	
				$scope.versionsLayout.setSkin("material");	
				$scope.versionsLayout.cells("a").setText("Versions");
				
				$scope.gridVersions = $scope.versionsLayout.cells("a").attachGrid();
				$scope.gridVersions.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.gridVersions.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.gridVersions.setHeader("Save date");
				$scope.gridVersions.setInitWidths("*");
				$scope.gridVersions.setColAlign("left");
				$scope.gridVersions.setColTypes("ro");
				$scope.gridVersions.setColSorting("str");
				$scope.gridVersions.init();
				$scope.gridVersions.setSkin("material");
				
			} catch(error) {}
			
		/* ------: Get versios :------ */
			
			Object.versions({
				
				id : [$scope.model.id],
				type : $scope.model.type,
				item : $scope.model.item,
				target : $scope.model.target
				
			})
			
			/* ------: On success :------ */
				
			.success(function(response) {
				
				var data = {};
				
				data.rows = $.map(response.data, function(val, i) {
					
					var commit = JSON.parse(val);
					
					return {
						
						id : commit.hash,
						data:
						[
							moment.unix(commit.date).format("YYYY-MM-DD HH:mm:ss")
						]
						
					};
					
				});						
				
				$scope.gridVersions.parse(data, "json");
				
			})			
			
			/* ------: On error :------ */
				
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to get object versions : " + response.error,
					expire: 5000
				});						
				
			});
			
		};
		
	/* ------: Revert a version :------ */
		
		$scope.revertVersion = function(version) {
			
			Object.revert({
				
				id : [$scope.model.id],
				type : $scope.model.type,
				item : $scope.model.item,
				version : version,
				target : $scope.model.target
				
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.object = response;
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to revert version : " + response.error,
					expire: 10000
				});	
				
			});
			
		};
		
	});
		
// EOF