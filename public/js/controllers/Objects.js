/**
 *	Module for controller "Objects"
 * 
 *  @doc	module
 * 	@name	controller.Objects
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Objects', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Objects
	 * 
	 *	@doc	controller
	 * 	@name	Objects
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Objects', function($rootScope, $scope, $compile, $http, $attrs, Object) {
		
		window.controller = { objects: $scope };
		
		$scope.saveObject = function(object) {
			
			Object.save(object)
				
			.success(function(response) {
				
				dhtmlx.message({
					text: "Done",
					expire: 5000
				});
				
			})
				
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: response,
					expire: 5000
				});
				
			});				
			
		};
		
		$scope.getObject = function(object) {
			
			Object.get(object)
				
			.success(function(response) {
				
				$scope.object = response;
				
			})
				
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error",
					expire: 5000
				});
				
			});				
			
		};

	/* ------: Modal window for revert to version :------ */
		
		$scope.modalRevertVersion = function() {
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 500);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();	
			$scope.windowModal.window.setModal(true);	
			$scope.windowModal.setSkin("material");
			
		/* ------: Create bottom toolbar :------ */
			
			var bottomToolbar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			bottomToolbar.setText("<div id='toolbarObject' style='width:100%;text-align:center;'></div>");
			
			$scope.toolbarRevertVersion = new dhtmlXToolbarObject({
				parent : "toolbarObject",
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "revert", type: "button", text: "Revert to selected", img: "revert-32px.png"
					},							
					{
						id: "close", type: "button", text: "Close", img: "close-18px.png"
					}
				]					
			});
			
			$scope.toolbarRevertVersion.setAlign("right");	
			
			$scope.toolbarRevertVersion.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "revert":
						
						$scope.revertVersion($scope.gridVersions.getSelectedId());
						$scope.windowModal.unload();
						
					break;
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});	
			
			try {
				
				$scope.versionsLayout = $scope.windowModal.window.attachLayout("1C");	
				$scope.versionsLayout.setSkin("material");	
				$scope.versionsLayout.cells("a").setText("Versions");
				
				$scope.gridVersions = $scope.versionsLayout.cells("a").attachGrid();
				$scope.gridVersions.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.gridVersions.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.gridVersions.setHeader("Save date");
				$scope.gridVersions.setInitWidths("*");
				$scope.gridVersions.setColAlign("left");
				$scope.gridVersions.setColTypes("ro");
				$scope.gridVersions.setColSorting("str");
				$scope.gridVersions.init();
				$scope.gridVersions.setSkin("material");
				
			} catch(error) {}
			
			Object.versions({
				
				id : [$scope.model.id],
				type : $scope.model.type,
				target : $scope.target
				
			})
			
			/* ------: On success :------ */
				
			.success(function(response) {
				
				var data = {};
				
				data.rows = $.map(response.data, function(val, i) {
					
					var commit = JSON.parse(val);
					
					return {
						
						id : commit.hash,
						data:
						[
							moment.unix(commit.date).format("YYYY-MM-DD HH:mm:ss")
						]
						
					};
					
				});						
				
				$scope.gridVersions.parse(data, "json");
				
			})			
			
			/* ------: On error :------ */
				
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to get object versions : " + response.error,
					expire: 5000
				});						
				
			});
			
		};
	
	});
		
// EOF