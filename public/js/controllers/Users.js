/**
 *	Module for controller "Users"
 * 
 *  @doc	module
 * 	@name	controller.Users
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Users', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Usersmounts
	 * 
	 *	@doc	controller
	 * 	@name	Users
	 *	@duty	Rolands Strickis
	 *	@link	
	*/
	

	.controller('Users', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, User, Role, Mount, Object, App, View, Databuilder, Model, Data) {
		
	/* ------: Extend controller :------ */		
		
		$controller('Components', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		
		
	/* ------: Get users :------ */				
		
		User.index({})
			
			
		/* ------: On success :------ */
			
		.success(function(response) {
			
			$scope.model = {
				dataset : response.data,
				columns : [
					{ mData: "id", sTitle: "ID"},
					{ mData: "name", sTitle: "Name"},
					{ mData: "email", sTitle: "Email"},						
					{ mData: "created_at", sTitle: "Created"},
					{ mData: "updated_at", sTitle: "Updated"}
				]
			};				
			
		})
		
		.error(function(response, status) {
			
			dhtmlx.message({
				type: "error",
				text: "Get all users : " + status,
				expire: 5000
			});			
			
		});
		
		
	/* ------: Attributes :------ */
		
		$attrs.window = {
			toolbar : true,
			viewport : "viewport",
			guid : chance.guid(),
		};
		
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		$scope.layout = $scope.window.attachLayout("1C");
		$scope.layout.cells("a").setText("Users");
		$scope.layout.cells("a").attachURL("/app/datatables/");
		$scope.presentation = $scope.layout.cells("a").getFrame().contentWindow;
		
		
	/* ------: Attach Statusbar to Layout :------ */
		
		$scope.statusbar = $scope.layout.cells("a").attachStatusBar({
		    text:   "Statusbar",
		    height: 35
		});
		
		
		// Define configuration
		
		$scope.conf.toolbar.private = [
			{ 
				id: "createuser", type: "button", img: "add-user-20px.png", text: "Create user"
			},
			{ 
				id: "deleteuser", type: "button", img: "delete-user-20px.png", text: "Delete user"
			},
			{ 
				id: "setpermissions", type: "button", img: "permissions-20px.png", text: "Permissions"
			},	
			{ 
				id: "setroles", type: "button", img: "role.png", text: "Roles"
			},					
			
			{ 
				id: "close", type: "button", img: "close-18px.png", text: "Close"
			}
		];
		
		// Load configuration
		
		$scope.toolbar.loadStruct($scope.conf.toolbar.private);
		
		// Attach events
		
		$scope.toolbar.attachEvent("onClick", function(id) {
			
			var data = null;
			
			switch(id) {
				
				case "close":
					
					$scope.close();
					
				break;
				
				
				case "deleteuser":
					
					var user = {
						
						ids : $scope.presentation.angular.element("#viewport").scope().getSelected()
						
					};
					
					User.delete(user)
						
					/* ------: On success :------ */
						
					.success(function(response) {
						
						if(response > 0) {
							
							$scope.presentation.angular.element("#viewport").scope().deleteRows(user);
							
							dhtmlx.message({
								text: "Users deleted",
								expire: 5000
							});									
							
						} else {
							
							dhtmlx.message({
								type: "error",
								text: "Users has not been deleted",
								expire: 5000
							});
							
						}
						
					})
					
					/* ------: On error :------ */
					
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to delete user",
							expire: 5000
						});
						
					});								
					
				break;
				
				
				case "createuser":
					
					$scope.createUser();
					
				break;
				
				
				case "setpermissions":
					
					$scope.setPermissions();
					
				break;		
				
				case "setroles":
					
					$scope.setRoles();
					
				break;
				
			}
		});
			

	/* ------: Toolbar configuration :------ */
		
		$scope.processForm = function (data){
			
			$scope.modal.callback = null;
			
			var user = {};
			
			switch ($scope.modal.form) {
				
				case "createuser":
					
					/* ------: User data :------ */
						
					user = {
						name : data.name,
						email : data.email,
						password : data.password
					};
					
					
					/* ------: Create user :------ */
						
					User.create(user)
					
					/* ------: On Success :------ */
						
					.success(function(response) {
						
						user.id = response.id;
						user.created_at = response.created_at;
						user.updated_at = response.updated_at;
						
						$scope.presentation.angular.element("#viewport").scope().insertRow(user);
						$scope.modal.window.window.close();
						
					})
					
					/* ------: On error :------ */
						
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to create user",
							expire: 5000
						});
						
					});	
					
				break;
				
				
				case "setpermissions":
					
					user = {
						objects : $scope.presentation.angular.element("#viewport").scope().getSelected(),
						permissions: data.data
					};
					
					switch (data.method) {
						
						case "add":
							
							/* ------: Update permissions :------ */
							
							User.setPermissions(user)
							
							/* ------: On Success :------ */
								
							.success(function(response) {
								
								dhtmlx.message({
									text: "Done",
									expire: 5000
								});
								
								
								// Refresh permissions
								
								var user = {
									objects : $scope.presentation.angular.element("#viewport").scope().getSelected()
								};
								
								User.getPermissions(user)
								
								/* ------: On success :------ */
								
								.success(function(response) {
									
									$scope.modal.data.permissions = response;
									$scope.modal.callback = data.method;											
									
								})
								
								/* ------: On error :------ */
								
								.error(function(response) {
									
									dhtmlx.message({
										type: "error",
										text: "Can not refresh permissions",
										expire: 5000
									});
									
								});											
								
							})
							
							/* ------: On error :------ */
								
							.error(function(response) {
								
								dhtmlx.message({
									type: "error",
									text: "Error to update permissions",
									expire: 5000
								});
								
							});	
							
						break;
						
						
						case "remove":
							
							/* ------: Update permissions :------ */
							
							User.removePermissions(user)
							
							/* ------: On Success :------ */
								
							.success(function(response) {
								
								$scope.modal.callback = data.method;
								
								dhtmlx.message({
									text: "Done",
									expire: 5000
								});
								
							})
							
							/* ------: On error :------ */
								
							.error(function(response) {
								
								dhtmlx.message({
									type: "error",
									text: "Error to remove permissions",
									expire: 5000
								});
								
							});	
							
						break;
						
					}
					
				break;
				
				
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Business logic for this form does not exist",
						expire: 5000
					});						
					
				break;
				
			}
			
		};
		

	/* ------: Create user :------ */
		
		$scope.createUser = function() {
			
			$scope.modal = $scope.createModal({
				
				title : "Create user",
				form : "createuser"
				
			});
			
		};

	/* ------: Set roles :------ */
		
		$scope.setRoles = function() {
			
			var id = $scope.presentation.angular.element("#viewport").scope().getSelected();
			
			var details = $.map($scope.presentation.angular.element("#viewport").scope().getSelectedData(), function(val) {
				
				return val;
				
			});
			
			$scope.modal = $scope.createModal({
				
				title : "Roles",
				form : "setroles",
				data : { id : id, details : details },
				dim : { width : 800, height : 700 }
				
			});
				
		};
		
		
	/* ------: set permissions :------ */
		
		$scope.setPermissions = function() {
			
			var controllers = 
			{
				users: { 
					view: "multicombo",
					label: "Methods",
					name : "Users",
					methods: User.methods
				},
				roles: { 
					view: "multicombo",
					label: "Methods",
					name : "Roles",
					methods: Role.methods
				},
				mounts: { 
					view: "combo",
					label: "Methods",
					name : "Mounts",
					methods: Mount.methods
				},
				objects: { 
					view: "multicombo",
					label: "Methods",
					name : "Objects",
					methods: Object.methods
				},
				app: { 
					view: "combo",
					label: "App",
					name : "Applications",
					methods: App.methods
				},
				databuilder: { 
					view: "combo",
					label: "Methods",
					name : "Databuilder",
					methods: Databuilder.methods
				},
				data: { 
					view: "combo",
					label: "Methods",
					name : "Data",
					methods: Data.methods
				}				
			};												
			
			var data = {
				
				objects : $scope.presentation.angular.element("#viewport").scope().getSelected()
				
			};
			
			User.getPermissions(data)
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.modal = $scope.createModal({
					
					title : "Permissions",
					form : "setpermissions",
					data : { permissions : response, controllers: controllers },
					dim : {width:650, height:550}
					
				});
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Can not get permissions",
					expire: 5000
				});
				
			});				
			
		};
		
	/* ------: Compose request :------ */	
		
		$scope.request = function(){
			
			var request = 
			{
				id : $attrs.viewid,
				type : "view",
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				}
			};
			
			return request;
			
		};
		
	});		


// EOF