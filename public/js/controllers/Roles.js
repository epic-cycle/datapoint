/**
 *	Module for controller "Roles"
 * 
 *  @doc	module
 * 	@name	controller.Users
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Roles', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Roles
	 * 
	 *	@doc	controller
	 * 	@name	Users
	 *	@duty	Rolands Strickis
	 *	@link	
	*/
	

	.controller('Roles', function($rootScope, $scope, $element, $attrs, $controller, $parentScope, User, Role, Mount, Object, App, View, Data, Databuilder) {
		
	/* ------: Extend controller :------ */		
		
		$controller('Components', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		
		
	/* ------: Get users :------ */				
		
		Role.index({})
			
			
		/* ------: On success :------ */
			
		.success(function(response) {
			
			$scope.model = {
				dataset : response.data,
				columns : [
					{ mData: "id", sTitle: "ID"},
					{ mData: "name", sTitle: "Name"},
					{ mData: "created_at", sTitle: "Created"},
					{ mData: "updated_at", sTitle: "Updated"}
				]
			};				
			
		})
		
		.error(function(response, status) {
			
			dhtmlx.message({
				type: "error",
				text: "Get all roles : " + status,
				expire: 5000
			});			
			
		});
		
		
	/* ------: Attributes :------ */
		
		$attrs.window = {
			toolbar : true,
			viewport : "viewport",
			guid : chance.guid(),
		};
		
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		$scope.layout = $scope.window.attachLayout("1C");
		$scope.layout.cells("a").setText("Roles");
		$scope.layout.cells("a").attachURL("/app/datatables/");
		$scope.presentation = $scope.layout.cells("a").getFrame().contentWindow;
		
		
	/* ------: Attach Statusbar to Layout :------ */
		
		$scope.statusbar = $scope.layout.cells("a").attachStatusBar({
		    text:   "Statusbar",
		    height: 35
		});
		

		// Define configuration
		
		$scope.conf.toolbar.private = [
			{ 
				id: "createrole", type: "button", img: "add-user-20px.png", text: "Create role"
			},
			{ 
				id: "deleterole", type: "button", img: "delete-user-20px.png", text: "Delete role"
			},
			{ 
				id: "setpermissions", type: "button", img: "permissions-20px.png", text: "Permissions"
			},					
			{ 
				id: "close", type: "button", img: "close-18px.png", text: "Close"
			}
		];
		
		// Load configuration
		
		$scope.toolbar.loadStruct($scope.conf.toolbar.private);
		
		// Attach events
		
		$scope.toolbar.attachEvent("onClick", function(id) {
			
			var data = null;
			
			switch(id) {
				
				case "close":
					
					$scope.close();
					
				break;
				
				
				case "deleterole":
					
					var object = {
						ids : $scope.presentation.angular.element("#viewport").scope().getSelected()
					};
					
					Role.delete(object)
						
					/* ------: On success :------ */
						
					.success(function(response) {
						
						if(response > 0) {
							
							$scope.presentation.angular.element("#viewport").scope().deleteRows(object);
							
							dhtmlx.message({
								text: "Roles deleted",
								expire: 5000
							});									
							
						} else {
							
							dhtmlx.message({
								type: "error",
								text: "Roles has not been deleted",
								expire: 5000
							});
							
						}
						
					})
					
					/* ------: On error :------ */
					
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to delete user",
							expire: 5000
						});
						
					});								
					
				break;
				
				
				case "createrole":
					
					$scope.createRole();
					
				break;
				
				
				case "setpermissions":
					
					$scope.setPermissions();
					
				break;						
				
			}
		});
			

		

		
	/* ------: Toolbar configuration :------ */
		
		$scope.processForm = function (data){
			
			$scope.modal.callback = null;
			
			var object = {};
			
			switch ($scope.modal.form) {
				
				case "createrole":
					
					/* ------: User data :------ */
						
					object = {
						name : data.name,
					};
					
					
					/* ------: Create user :------ */
						
					Role.create(object)
					
					/* ------: On Success :------ */
						
					.success(function(response) {
						
						object.id = response.id;
						object.created_at = response.created_at;
						object.updated_at = response.updated_at;
						
						$scope.presentation.angular.element("#viewport").scope().insertRow(object);
						$scope.modal.window.window.close();
						
					})
					
					/* ------: On error :------ */
						
					.error(function(response, status) {
						
						dhtmlx.message({
							type: "error",
							text: "Create role : " + status,
							expire: 5000
						});
						
					});	
					
				break;
				
				
				case "setpermissions":
					
					object = {
						objects : $scope.presentation.angular.element("#viewport").scope().getSelected(),
						permissions: data.data
					};
					
					switch (data.method) {
						
						case "add":
							
							/* ------: Update permissions :------ */
							
							Role.setPermissions(object)
							
							/* ------: On Success :------ */
								
							.success(function(response) {
								
								dhtmlx.message({
									text: "Done",
									expire: 5000
								});
								
								
								// Refresh permissions
								
								var object = {
									objects : $scope.presentation.angular.element("#viewport").scope().getSelected()
								};
								
								Role.getPermissions(object)
								
								/* ------: On success :------ */
								
								.success(function(response) {
									
									$scope.modal.data.permissions = response;
									$scope.modal.callback = data.method;											
									
								})
								
								/* ------: On error :------ */
								
								.error(function(response) {
									
									dhtmlx.message({
										type: "error",
										text: "Can not refresh permissions",
										expire: 5000
									});
									
								});											
								
							})
							
							/* ------: On error :------ */
								
							.error(function(response, status) {
								
								dhtmlx.message({
									type: "error",
									text: "Set permissions : " + status,
									expire: 5000
								});
								
							});	
							
						break;
						
						
						case "remove":
							
							/* ------: Update permissions :------ */
							
							Role.removePermissions(object)
							
							/* ------: On Success :------ */
								
							.success(function(response) {
								
								$scope.modal.callback = data.method;
								
								dhtmlx.message({
									text: "Done",
									expire: 5000
								});
								
							})
							
							/* ------: On error :------ */
								
							.error(function(response) {
								
								dhtmlx.message({
									type: "error",
									text: "Error to remove permissions",
									expire: 5000
								});
								
							});	
							
						break;
						
					}
					
				break;
				
				
				default:
					
					dhtmlx.message({
						type: "error",
						text: "Business logic for this form does not exist",
						expire: 5000
					});						
					
				break;
				
			}
			
		};
		

	/* ------: Create user :------ */
		
		$scope.createRole = function() {
			
			$scope.modal = $scope.createModal({
				
				title : "Create role",
				form : "createrole"
				
			});
			
		};
		
		
	/* ------: set permissions :------ */
		
		$scope.setPermissions = function() {

			var controllers = 
			{
				users: { 
					view: "multicombo",
					label: "Methods",
					name : "Users",
					methods: User.methods
				},
				roles: { 
					view: "multicombo",
					label: "Methods",
					name : "Roles",
					methods: Role.methods
				},
				mounts: { 
					view: "multicombo",
					label: "Methods",
					name : "Mounts",
					methods: Mount.methods
				},
				objects: { 
					view: "multicombo",
					label: "Methods",
					name : "Objects",
					methods: Object.methods
				},
				app: { 
					view: "combo",
					label: "App",
					name : "Applications",
					methods: App.methods
				},
				databuilder: { 
					view: "combo",
					label: "Methods",
					name : "Databuilder",
					methods: Databuilder.methods
				},
				data: { 
					view: "combo",
					label: "Methods",
					name : "Data",
					methods: Data.methods
				}					
			};												
			
			var data = {
				
				objects : $scope.presentation.angular.element("#viewport").scope().getSelected()
				
			};
			
			Role.getPermissions(data)
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.modal = $scope.createModal({
					
					title : "Permissions",
					form : "setpermissions",
					data : { permissions : response, controllers: controllers },
					dim : {width:650, height:550}
					
				});
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Can not get permissions",
					expire: 5000
				});
				
			});				
			
		};
		
	/* ------: Compose request :------ */	
		
		$scope.request = function(){
			
			var request = 
			{
				id : $attrs.viewid,
				type : "view",
				target : {
					auditory : $scope.target.auditory, 
					id : $scope.target.id
				}
			};
			
			return request;
			
		};
		
	});		


// EOF