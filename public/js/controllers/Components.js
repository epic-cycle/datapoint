/**
 *	Module for controller "Components"
 * 
 *  @doc	module
 * 	@name	controller.Components
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Components', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Components
	 * 
	 *	@doc	controller
	 * 	@name	Components
	 *	@duty	Rolands Strickis
	 *	@link	
	*/
	
	.controller('Components', function($rootScope, $scope, $compile, $http, $attrs, $element, $location, $parentScope, User, Role, Object, Report) {
		
		$scope.conf = {
			toolbar : {
				public : null,
				private : null
			}
		};
		
		$scope.target = {
			auditory : "All",
			id : null
		};
		
		$scope.roles = {
			available : {},
			assigned : {}
		};
		
		$scope.users = {
			available : {},
			assigned : {}
		};			
		
		
	/* ------: Maximize window/component :------ */
		
		$scope.toggleMaximize = function(id) {
			
			try {
				
				if($("#gs-" + id).attr("maximized") == "true"){
					
					restoreLayer("gs-" + id);
					$("#gs-" + id).attr("maximized","false");
					$("#gs-" + id).removeAttr('style');
					// TODO : bug if change overflow
					//$('html, body').css({overflow: 'auto'});
					
				} else {
					
					// Non-animated
					
					/*
					$("#gs-" + id).css("position","absolute");
					$("#gs-" + id).css("top",$(document).scrollTop() + "px");
					$("#gs-" + id).css("left","0px");
					$("#gs-" + id).css("width","100%");
					$("#gs-" + id).css("height", $(window).height() - 3 + "px");
					$("#gs-" + id).css("z-index","2");
					$("#gs-" + id).attr("maximized","true");
					// TODO : bug if change overflow
					//$('html, body').css({overflow: 'hidden'});					
					*/
					
					
					//$("#gs-" + id).css("position","absolute");
					//$("#gs-" + id).css("top",$(document).scrollTop() + "px");
					//$("#gs-" + id).css("left","0px");
					//$("#gs-" + id).css("width","100%");
					
					// Animated scroll down
					
					//$("#gs-" + id).css("height", "600px");
					//$("#gs-" + id).attr("maximized","true");					
					//hideLayer("gs-" + id);
					
					// Animated full
					
					var grid = $('.grid-stack').data('gridstack');
					grid.setAnimation(false);
					
					$("#gs-" + id).css("z-index","2");
					$("#gs-" + id + "-label").hide();
					
					$("#gs-" + id).animate({
						
						position: "absolute",
						top: $(document).scrollTop() + "px",
						left: "0px",
						width: "100%",
						height: $(window).height() - 3 + "px"
						
					}, 200, function() {
						
						$scope.resize();
						$("#gs-" + id).attr("maximized","true");
						
						setTimeout(function(){ 
							hideLayer("gs-" + id);
						}, 100);
						
					});					
					
					
					
				}
				
				angular.element("#" + id).scope().resize();
				
			} catch (error) {
				
				dhtmlx.message({
					type: "error",
					text: error,
					expire: 5000
				});					
				
			}
			
		};
		
	/* ------: Resize window :------ */
		
		$scope.resize = function () {
			
			$scope.windows.forEachWindow(function(win){
				
				if($("#gs-" + win._idd).attr("maximized") == "true"){
					
					$("#gs-" + win._idd).css("height", $(window).height() - 3 + "px");		
					
				}
				
				if(win.isMaximized()){
					
					setTimeout(function(){ 	
						
						win.minimize();
						win.maximize();
						
					}, 200);	
					
				}
				
			});	    
			
			
			if($scope.windowModal !== undefined) {
				
				if($scope.windowModal.w !== null) {	
					
					$scope.windowModal.forEachWindow(function(win){
						
						win.centerOnScreen();
						
					});	 			
					
				}
				
			}
			
		};
		
	/* ------: Create window :------ */
		
		$scope.construct = function (element, attrs) {
			
				$scope.winid = attrs.guid;
				$scope.viewport = attrs.viewport;
				
			/* -------------------------------- */
				
				$scope.windows = new dhtmlXWindows();
				
				$scope.windows.attachViewportTo($scope.viewport); 
				$scope.window = $scope.windows.createWindow($scope.winid, 0, 0, 600,600);
				
				setTimeout(function() {
					
					$scope.windows.window($scope.winid).maximize();
					
				}, 1);
				
				
				$scope.windows.window($scope.winid).setText(attrs.id);				    	
				
				$scope.windows.window($scope.winid).hideHeader();					
				
				
				$scope.conf.toolbar.public = [
					{
						id:     "versions",
						type:   "button",
						img:	"versions-32px.png",
						text:   "Versions",
					},		
					{
						id:   "sep_id",
						type: "separator"
					},					
					{
						id:     "info",
						type:   "text",
						text:   "Auditory: ",
					},							
					{
						id: "subject-type", type: "buttonSelect", mode: "select",
						selected: "type-all",
						options: [
							{ 
								id: "type-all", type: "button", text: "All", img: "default-18px.png"
							},								
							{ 
								id: "type-role", type: "button", text: "Role", img: "role.png"
							},
							{ 
								id: "type-user", type: "button", text: "User", img: "user.png"
							},								
						]
					},						
					{
						id: "subject", type: "button", mode: "select", text: "Select subject..", img: "arrow-right-18px.png", hidden:  true
					},
					{
						id:   "sep_id",
						type: "separator"
					}
				];
				
			if(attrs.toolbar === true) {
				
			/* -------------------------------- */
				
				var conf = {};
				
				if(attrs.conf === true) {
					
					conf = $scope.conf.toolbar.public;
					
				}
				
				// Attach Toolbar to Window
				
				$scope.toolbar = $scope.windows.window($scope.winid).attachToolbar({
					icons_path: "/resources/shared/imgs/",
					json: conf
				});
				
				//$scope.windows.window($scope.winid).hideToolbar();	
				
			/* ------: Attach Toolbar button events :------ */
				
				$scope.toolbar.attachEvent("onClick", function(id) {
					
					switch(id) {
						
						case "versions":
							
							$scope.modalRevertVersion();
							
						break;	
						
						case "type-all":
							
							$scope.toolbar.hideItem("subject");
							$scope.target.auditory = "All";
							$scope.target.id = null;
							$scope.toolbar.setItemText("subject", "Select subject..");
							$scope.getObject($scope.request());
							
						break;						
						
						
						case "type-role":
							
							$scope.target.auditory = "Role";
							$scope.toolbar.showItem("subject");
							$scope.selectAuditory($scope.target.auditory);
							
						break;	
						
						
						case "type-user":
							
							$scope.target.auditory = "User";
							$scope.toolbar.showItem("subject");
							$scope.selectAuditory($scope.target.auditory);
							
						break;	
							
							
						case "subject":
							
							switch($scope.toolbar.getItemText("subject-type")) {
								
								case "Role":
									
									$scope.target.auditory = "Role";
									$scope.selectAuditory($scope.target.auditory);
									
								break;
								
								
								case "User":
									
									$scope.target.auditory = "User";
									$scope.selectAuditory($scope.target.auditory);	
									
								break;
								
							}
							
						break;
						
					}
					
				});					
			
			
			/* ------: Select auditory :------ */
				
				$scope.selectAuditory = function(auditory) {
					
				/* ------: Create modal window :------ */
					
					$scope.toolbar.showItem("subject");
					$scope.windowModal = new dhtmlXWindows();
					$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 500);	
					$scope.windowModal.window.centerOnScreen();
					$scope.windowModal.window.hideHeader();	
					$scope.windowModal.window.setModal(true);	
					$scope.windowModal.setSkin("material");
					
				/* ------: Create bottom toolbar :------ */
					
					var dhxSBar2 = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
					dhxSBar2.setText("<div id='toolbarObj2' style='width:100%;text-align:center;'>asdasdasdads</div>");
					
					$scope.toolbarAddTable = new dhtmlXToolbarObject({
						parent : "toolbarObj2",
						icons_path: "/resources/shared/imgs/",
						json: [
							{
								id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
							}
						]					
					});
					
					$scope.toolbarAddTable.setAlign("right");	
					
					$scope.toolbarAddTable.attachEvent("onClick", function(id) {
						
						switch(id) {
							
							case "close":
								
								$scope.windowModal.unload();
								
							break;
							
						}
						
					});					
					
				/* ------: Switch between target auditory :------ */
					
					switch(auditory) {
						
						
					/* ------: Auditory is the Role :------ */
						
						case "Role":
							
						/* ------: Get all roles :------ */
							
							Role.index({})
								
							/* ------: On success :------ */
								
							.success(function(response) {
								
								$scope.roles.available = response.data;
								
								var object = {
									id : $attrs.oid,
									type : $attrs.otype
								};
								
								Object.roles(object)
								.success(function(response) {
									
									$scope.roles.assigned = response.data;
									
									// Show data in window
									
									$scope.auditoryLayout = $scope.windowModal.window.attachLayout("2U");	
									$scope.auditoryLayout.setSkin("material");
									
									$scope.toolbarAvailableSubjects = $scope.auditoryLayout.cells("a").attachToolbar({
										icons_path: "/resources/apps/modelbuilder/imgs/",
										xml: "/resources/apps/modelbuilder/toolbar-available-subjects.xml"
									});		
									
									$scope.toolbarAssignedSubjects = $scope.auditoryLayout.cells("b").attachToolbar({
										icons_path: "/resources/apps/modelbuilder/imgs/",
										xml: "/resources/apps/modelbuilder/toolbar-assigned-subjects.xml"
									});										
									
									$scope.toolbarAvailableSubjects.attachEvent("onClick", function(id) {
										
										switch(id) {
											
											case "add-subject":
												
												$scope.target.id = $scope.gridAvailableSubjects.getSelectedId();
												$scope.toolbar.setItemText("subject", $scope.gridAvailableSubjects.cells($scope.target.id, 0).getValue());
												$scope.windowModal.unload();
												
											break;
											
										}
										
									});
									
									$scope.toolbarAssignedSubjects.attachEvent("onClick", function(id) {
										
										switch(id) {
											
											case "edit-subject":
												
												$scope.target.id = $scope.gridAssignedSubjects.getSelectedId();
												$scope.toolbar.setItemText("subject", $scope.gridAssignedSubjects.cells($scope.target.id, 0).getValue());
												$scope.windowModal.unload();
												$scope.getObject($scope.request());
												
											break;										
											
										}
										
									});								
									
									var available = {
										rows : []
									};
									
									var assigned = {
										rows : []
									};
									
									switch($scope.toolbar.getItemText("subject-type")) {
											
										case "Role":
											$scope.windowModal.window.setText("Roles");
											$scope.auditoryLayout.cells("a").setText("Available roles");
											$scope.auditoryLayout.cells("b").setText("Assigned roles");
											
											$.each($scope.roles.available, function( index, value ) {
												available.rows.push(
													{
														id: $scope.roles.available[index].id,
														data : 
														[ 
															$scope.roles.available[index].name
														]
													}
												);
											});	
											$.each($scope.roles.assigned, function( index, value ) {
												assigned.rows.push(
													{
														id: $scope.roles.assigned[index].id,
														data : 
														[ 
															$scope.roles.assigned[index].name
														]
													}
												);
											});											
										break;
											
										case "User":
											$scope.windowModal.window.setText("Users");
											$scope.auditoryLayout.cells("a").setText("Available users");
											$scope.auditoryLayout.cells("b").setText("Assigned users");
											//win.showHeader();
											$.each(response.data.available, function( index, value ) {
												available.rows.push(
													{
														id: response.data.available[index].id,
														data : 
														[ 
															response.data.available[index].name + " (" + response.data.available[index].email + ")"
														]
													}
												);
											});	
											$.each(response.data.assigned, function( index, value ) {
												assigned.rows.push(
													{
														id: response.data.assigned[index].id,
														data : 
														[ 
															response.data.assigned[index].name + " (" + response.data.assigned[index].email + ")"
														]
													}
												);
											});											
										break;
											
									}
									
									$scope.gridAvailableSubjects = $scope.auditoryLayout.cells("a").attachGrid();
									$scope.gridAvailableSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
									$scope.gridAvailableSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
									$scope.gridAvailableSubjects.setHeader("Name");
									$scope.gridAvailableSubjects.setInitWidths("*,*");
									$scope.gridAvailableSubjects.setColAlign("left");
									$scope.gridAvailableSubjects.setColTypes("ed");
									$scope.gridAvailableSubjects.setColSorting("str");
									$scope.gridAvailableSubjects.init();
									$scope.gridAvailableSubjects.setSkin("material");
									$scope.gridAvailableSubjects.parse(available, "json");
									
									$scope.gridAssignedSubjects = $scope.auditoryLayout.cells("b").attachGrid();
									$scope.gridAssignedSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
									$scope.gridAssignedSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
									$scope.gridAssignedSubjects.setHeader("Name");
									$scope.gridAssignedSubjects.setInitWidths("*,*");
									$scope.gridAssignedSubjects.setColAlign("left");
									$scope.gridAssignedSubjects.setColTypes("ed");
									$scope.gridAssignedSubjects.setColSorting("str");
									$scope.gridAssignedSubjects.init();
									$scope.gridAssignedSubjects.setSkin("material");
									$scope.gridAssignedSubjects.parse(assigned, "json");
								})	
								.error(function(response) {
									
									dhtmlx.message({
										type: "error",
										text: "Error to get roles for object",
										expire: 5000
									});						
									
								});						
							})
								
							/* ------: On error :------ */
								
							.error(function(response) {
								
								dhtmlx.message({
									type: "error",
									text: "Error to get roles",
									expire: 5000
								});						
								
							});
							
						break;
						
					/* ------: Auditory is the User :------ */
						
						case "User":
							
						/* ------: Get all users :------ */
							
							User.index({})
								
							/* ------: On success :------ */
								
							.success(function(response) {
								
								$scope.users.available = response.data;
								
								var object = {
									id : $attrs.oid,
									type : $attrs.otype,
								};
								
								Object.users(object)
								
								.success(function(response) {
									
									$scope.users.assigned = response.data;
									
									// Show data in window
									
									$scope.auditoryLayout = $scope.windowModal.window.attachLayout("2U");	
									$scope.auditoryLayout.setSkin("material");
									
									$scope.toolbarAvailableSubjects = $scope.auditoryLayout.cells("a").attachToolbar({
										icons_path: "/resources/apps/modelbuilder/imgs/",
										xml: "/resources/apps/modelbuilder/toolbar-available-subjects.xml"
									});		
									
									$scope.toolbarAssignedSubjects = $scope.auditoryLayout.cells("b").attachToolbar({
										icons_path: "/resources/apps/modelbuilder/imgs/",
										xml: "/resources/apps/modelbuilder/toolbar-assigned-subjects.xml"
									});										
									
									$scope.toolbarAvailableSubjects.attachEvent("onClick", function(id) {
										
										switch(id) {
											
											case "add-subject":
												
												$scope.target.id = $scope.gridAvailableSubjects.getSelectedId();
												$scope.toolbar.setItemText("subject", $scope.gridAvailableSubjects.cells($scope.target.id, 0).getValue());
												$scope.windowModal.unload();
												
											break;
											
										}
										
									});
									
									$scope.toolbarAssignedSubjects.attachEvent("onClick", function(id) {
										
										switch(id) {
											
											case "edit-subject":
												
												$scope.target.id = $scope.gridAssignedSubjects.getSelectedId();
												$scope.toolbar.setItemText("subject", $scope.gridAssignedSubjects.cells($scope.target.id, 0).getValue());
												$scope.windowModal.unload();
												$scope.getObject($scope.request());
												
											break;										
											
										}
										
									});								
									
									var available = {
										rows : []
									};
									
									var assigned = {
										rows : []
									};
									
									switch($scope.toolbar.getItemText("subject-type")) {
											
										case "Role":
											$scope.windowModal.window.setText("Roles");
											$scope.auditoryLayout.cells("a").setText("Available roles");
											$scope.auditoryLayout.cells("b").setText("Assigned roles");
											
											$.each($scope.users.available, function( index, value ) {
												available.rows.push(
													{
														id: $scope.users.available[index].id,
														data : 
														[ 
															$scope.users.available[index].name
														]
													}
												);
											});	
											$.each($scope.users.assigned, function( index, value ) {
												assigned.rows.push(
													{
														id: $scope.users.assigned[index].id,
														data : 
														[ 
															$scope.users.assigned[index].name
														]
													}
												);
											});											
										break;
											
										case "User":
											$scope.windowModal.window.setText("Users");
											$scope.auditoryLayout.cells("a").setText("Available users");
											$scope.auditoryLayout.cells("b").setText("Assigned users");
											$.each($scope.users.available, function( index, value ) {
												available.rows.push(
													{
														id: $scope.users.available[index].id,
														data : 
														[ 
															$scope.users.available[index].name + " (" + $scope.users.available[index].email + ")"
														]
													}
												);
											});	
											$.each($scope.users.assigned, function( index, value ) {
												assigned.rows.push(
													{
														id: $scope.users.assigned[index].id,
														data : 
														[ 
															$scope.users.assigned[index].name + " (" + $scope.users.assigned[index].email + ")"
														]
													}
												);
											});											
										break;
											
									}
									
									$scope.gridAvailableSubjects = $scope.auditoryLayout.cells("a").attachGrid();
									$scope.gridAvailableSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
									$scope.gridAvailableSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
									$scope.gridAvailableSubjects.setHeader("Name");
									$scope.gridAvailableSubjects.setInitWidths("*,*");
									$scope.gridAvailableSubjects.setColAlign("left");
									$scope.gridAvailableSubjects.setColTypes("ed");
									$scope.gridAvailableSubjects.setColSorting("str");
									$scope.gridAvailableSubjects.init();
									$scope.gridAvailableSubjects.setSkin("material");
									$scope.gridAvailableSubjects.parse(available, "json");
									
									$scope.gridAssignedSubjects = $scope.auditoryLayout.cells("b").attachGrid();
									$scope.gridAssignedSubjects.setImagePath("/include/dhtmlx/codebase/imgs/");
									$scope.gridAssignedSubjects.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
									$scope.gridAssignedSubjects.setHeader("Name");
									$scope.gridAssignedSubjects.setInitWidths("*,*");
									$scope.gridAssignedSubjects.setColAlign("left");
									$scope.gridAssignedSubjects.setColTypes("ed");
									$scope.gridAssignedSubjects.setColSorting("str");
									$scope.gridAssignedSubjects.init();
									$scope.gridAssignedSubjects.setSkin("material");
									$scope.gridAssignedSubjects.parse(assigned, "json");
								})	
								.error(function(response) {
									
									dhtmlx.message({
										type: "error",
										text: "Error to get roles for object",
										expire: 5000
									});
									
								});						
							})
								
							/* ------: On error :------ */
								
							.error(function(response) {
								
								dhtmlx.message({
									type: "error",
									text: "Error to get roles",
									expire: 5000
								});						
								
							});
							
						break;
						
					}
					
				};
				
			/* ------: Revert a version :------ */
				
				$scope.revertVersion = function(version) {
					
					Object.revert({
						
						id : [$scope.model.id],
						type : $scope.model.type,
						target : $scope.target,
						version : version
						
					})
					
					/* ------: On success :------ */
					
					.success(function(response) {
						
						$scope.object = response;
						
					})
					
					/* ------: On error :------ */
					
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to revert version : " + response.error,
							expire: 5000
						});	
						
					});
					
				};
				
			/* ------: Modal window for revert to version :------ */
				
				$scope.modalRevertVersion = function() {
					
				/* ------: Create modal window :------ */
					
					$scope.windowModal = new dhtmlXWindows();
					$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 500);	
					$scope.windowModal.window.centerOnScreen();
					$scope.windowModal.window.hideHeader();	
					$scope.windowModal.window.setModal(true);	
					$scope.windowModal.setSkin("material");
					
				/* ------: Create bottom toolbar :------ */
					
					var bottomToolbar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
					bottomToolbar.setText("<div id='toolbarObject' style='width:100%;text-align:center;'></div>");
					
					$scope.toolbarRevertVersion = new dhtmlXToolbarObject({
						parent : "toolbarObject",
						icons_path: "/resources/shared/imgs/",
						json: [
							{
								id: "revert", type: "button", text: "Revert to selected", img: "revert-32px.png"
							},							
							{
								id: "close", type: "button", text: "Close", img: "close-18px.png"
							}
						]					
					});
					
					$scope.toolbarRevertVersion.setAlign("right");	
					
					$scope.toolbarRevertVersion.attachEvent("onClick", function(id) {
						
						switch(id) {
							
							case "revert":
								
								$scope.revertVersion($scope.gridVersions.getSelectedId());
								$scope.windowModal.unload();
								
							break;
							
							case "close":
								
								$scope.windowModal.unload();
								
							break;
							
						}
						
					});	
					
					try {
						
						$scope.versionsLayout = $scope.windowModal.window.attachLayout("1C");	
						$scope.versionsLayout.setSkin("material");	
						$scope.versionsLayout.cells("a").setText("Versions");
						
						$scope.gridVersions = $scope.versionsLayout.cells("a").attachGrid();
						$scope.gridVersions.setImagePath("/include/dhtmlx/codebase/imgs/");
						$scope.gridVersions.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
						$scope.gridVersions.setHeader("Save date");
						$scope.gridVersions.setInitWidths("*");
						$scope.gridVersions.setColAlign("left");
						$scope.gridVersions.setColTypes("ro");
						$scope.gridVersions.setColSorting("str");
						$scope.gridVersions.init();
						$scope.gridVersions.setSkin("material");
						
					} catch(error) {}
					
					Object.versions({
						
						id : [$scope.model.id],
						type : $scope.model.type,
						target : $scope.target
						
					})
					
					/* ------: On success :------ */
						
					.success(function(response) {
						
						var data = {};
						
						data.rows = $.map(response.data, function(val, i) {
							
							var commit = JSON.parse(val);
							
							return {
								
								id : commit.hash,
								data:
								[
									moment.unix(commit.date).format("YYYY-MM-DD HH:mm:ss")
								]
								
							};
							
						});						
						
						$scope.gridVersions.parse(data, "json");
						
					})			
					
					/* ------: On error :------ */
						
					.error(function(response) {
						
						dhtmlx.message({
							type: "error",
							text: "Error to get object versions : " + response.error,
							expire: 5000
						});						
						
					});
					
				};
				
				
			/* -------------------------------- */
				
			}			
				
				$(window).resize(function() {
					
					setTimeout(function(){ 
						
						if(window.resizing === undefined || window.resizing === false){
							
							window.resizing = true;
							
							$(window).mouseup(function() {
								
								$(window).unbind('mouseup');
								window.resizing = false;
								
								setTimeout(function(){ 
									
									$scope.resize();
									
								}, 100);
								
							});
							
						}
						
						$scope.resize();
						
					}, 100);					
					
				});
				
			/* -------------------------------- */
				
				$('.grid-stack').on('change', function (e, items) {
					
					try {
						
						//$element.context.localName
						var wid = items[0].el[0].children[0].children[1].id;
						
					} catch(error){}
					
					setTimeout(function(){
						if($scope.windows.window(wid) !== null){						
							if($scope.windows.window(wid).isMaximized()){
								$scope.windows.window(wid).minimize();
								$scope.windows.window(wid).maximize();
							}
						}
					}, 100);
					
				});
				
				
			/* -------------------------------- */
			    	
		};
		
	/* ------: Create modal window :------ */
		
		$scope.createModal = function(params){
			
			var guid = chance.guid();
			
			if(params.dim === undefined) {
				
				params.dim = {
					
					width: 500,
					height:500
					
				};
				
			}
			
			var modal = new dhtmlXWindows();
			modal.window = modal.createWindow($scope.winid, 0, 0, params.dim.width, params.dim.height);
			modal.window.centerOnScreen();
			modal.window.hideHeader();	
			modal.window.setModal(true);	
			modal.setSkin("material");
			
			var layout = modal.window.attachLayout("1C");					
			layout.cells("a").setText(params.title);
			layout.cells("a").attachURL("/form/" + params.form);
			
			var bottomBar = modal.window.attachStatusBar({text: "", height: 34});
			bottomBar.setText("<div id='toolbar-" + guid + "' style='width:100%;text-align:center;'>BUTTONS</div>");
			
			var toolbarAddTable = new dhtmlXToolbarObject({
				parent : "toolbar-" + guid,
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", mode: "select", text: "Close", img: "close-18px.png"
					},
					{
						id: "apply", type: "button", mode: "select", text: "Apply", img: "apply-32px.png"
					}
				]					
			});
			
			toolbarAddTable.setAlign("right");
			
			toolbarAddTable.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "close":
						
						modal.unload();
						
					break;
					
					case "apply":
						
						layout.cells("a").getFrame().contentWindow.angular.element("#form").scope().submit();
						
					break;
					
				}
				
			});	
			
			$(window).resize(function() {
				
				if($scope.modal !== undefined) {
					
					if($scope.modal.window.window.cell !== null) {	
						
						$scope.modal.window.window.centerOnScreen();
						
					}
					
				}
				
			});
			
			var object = {
				window : modal,
				layout : layout,
				form : params.form,
				data : params.data
			};
			
			return object;
			 
		};		 

	/* ------: Close window :------ */		    
		    
		$scope.close = function(){
			
			var id = $location.search().winid;
			
			if(id === undefined) {
				
				//$scope.windows.window($attrs.window.guid).close();
				$scope.windows.window($attrs.window.guid).hide();
				
			}
			else {
				
				$parentScope.close(id);
				
			}
			
			$rootScope.refreshWindowList();
			
		};		    
		
		
	/* ------: Save component model for report :------ */
		
		$rootScope.$on($rootScope.windows.getTopmostWindow()._idd + ".saveForReport", function (event, id) {
			
			Report.save({
				
				id : id,
				model : $scope.model,
				cmp : $attrs.cmp
				
			})
			
			/* ------: On success :------ */
				
			.success(function(response) {
				
				if($attrs.cmp !== undefined) {
					
					/*
					
					dhtmlx.message({
						text: "Component saved to report " + $attrs.cmp,
						expire: 5000
					});
					
					*/
					
				}
				
			})			
			
			/* ------: On error :------ */
				
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to save report : " + response.error,
					expire: 5000
				});						
				
			});
			
		});
		    
	});

// EOF