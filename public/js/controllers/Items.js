/**
 *	Module for controller "Items"
 * 
 *  @doc	module
 * 	@name	controller.Items
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Items', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Items
	 * 
	 *	@doc	controller
	 * 	@name	Items
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Items', function($rootScope, $scope, $compile, $http, $attrs, Item) {
		
		$scope.saveItem = function(id, data) {
			
			console.log($scope.model.item);
			
			Item.save({
				
				id : [id],
				item : $scope.model.item,
				data : data,
				target : {
					auditory : $scope.model.auditory
				}
				
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
			
				dhtmlx.message({
					text: "Saved!",
					expire: 3000
				});	
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to save item : " + response.error,
					expire: 5000
				});					
				
			});
		
		};
		
		$scope.addItem = function() {
			
			Item.add({
				
				id : [$attrs.id],
				target : {
					auditory : $scope.model.auditory
				}
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.gridVersions.addRow(response.data.item.id,[response.data.item.name, response.data.item.updated]);
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to add item : " + response.error,
					expire: 5000
				});					
				
			});		
			
		};
		
		$scope.updateItem = function(id, name) {
			
			Item.update({
				
				id : [$attrs.id],
				item : id,
				name : name,
				target : {
					auditory : $scope.model.auditory
				}
				
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				try {
				
					$scope.gridVersions.changeRowId(id, response.data.itemid.new);
					
					dhtmlx.message({
						text: 'Item updated to "' + name + '".',
						expire: 5000
					});	
					
				} catch(error) {
					
					dhtmlx.message({
						type: "error",
						text: "Error to rename item : " + error,
						expire: 5000
					});	
					
				}
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to rename item : " + response.error,
					expire: 5000
				});					
				
			});	
			
		};
		
		$scope.deleteItem = function(id) {
		
			Item.delete({
				
				id : [$attrs.id],
				item : id,
				target : {
					auditory : $scope.model.auditory
				}

			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				var name = $scope.gridVersions.cells(id,0).getValue();
				
				$scope.gridVersions.deleteRow(id);
				
				dhtmlx.message({
					text: '"' + name + '" deleted.',
					expire: 10000
				});	
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to delete item : " + response.error,
					expire: 5000
				});					
				
			});
			
		};
		
		$scope.getItems = function(id) {
			
		/* ------: Variables :------ */
			
			var uniqid = "toolbar-" + chance.guid();
			
			if($scope.model.auditory === undefined) {
				$scope.model.auditory = "User";
			}
			
		/* ------: Create modal window :------ */
			
			$scope.windowModal = new dhtmlXWindows();
			$scope.windowModal.window = $scope.windowModal.createWindow($scope.winid, 0, 0, 600, 500);	
			$scope.windowModal.window.centerOnScreen();
			$scope.windowModal.window.hideHeader();
			$scope.windowModal.window.setModal(false);	
			$scope.windowModal.setSkin("material");
			
		/* ------: Create gird :------ */
			
			try {
				
				$scope.versionsLayout = $scope.windowModal.window.attachLayout("1C");	
				$scope.versionsLayout.setSkin("material");	
				$scope.versionsLayout.cells("a").setText("Items");
				
				$scope.gridVersions = $scope.versionsLayout.cells("a").attachGrid();
				$scope.gridVersions.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.gridVersions.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.gridVersions.setHeader("Name, Updated");
				$scope.gridVersions.setInitWidths("*");
				$scope.gridVersions.setColAlign("left,left");
				$scope.gridVersions.setColTypes("ed,ro");
				$scope.gridVersions.setColSorting("str,str");
				$scope.gridVersions.init();
				$scope.gridVersions.setSkin("material");
				
			} catch(error) {}
			
		/* ------: Create bottom toolbar :------ */
			
			var bottomToolbar = $scope.windowModal.window.attachStatusBar({text: "", height: 34});
			bottomToolbar.setText("<div id='"+ uniqid +"' style='width:100%;text-align:center;'></div>");
			
			$scope.toolbarRevertVersion = new dhtmlXToolbarObject({
				parent : uniqid,
				icons_path: "/resources/shared/imgs/",
				json: [
					{
						id: "close", type: "button", text: "Close", img: "close-18px.png"
					},
					{
						id: "load-default", type: "button", text: "Load default", img: "load-default-32px.ico"
					},
					{
						id: "use-selected", type: "button", text: "Use selected", img: "apply-32px.png"
					}
				]
			});
			
			$scope.toolbarRevertVersion.setAlign("right");	
			
			$scope.toolbarRevertVersion.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "use-selected":
						
						$scope.loadItem($attrs.id, $scope.gridVersions.getSelectedId());

					break;
					
					case "load-default":
						
						$scope.model.auditory = "User";
						$scope.loadItem($attrs.id);
						
					break;					
					
					case "close":
						
						$scope.windowModal.unload();
						
					break;
					
				}
				
			});
						
		/* ------: Attach top toolbar :------ */
			
			$scope.toolbarAvailableSubjects = $scope.versionsLayout.cells("a").attachToolbar({
				icons_path: "/resources/shared/imgs/",
				align: "left",
				json: [
					{
						id: "User", type: "buttonTwoState", mode: "select", text: "My", img: "user.png"
					},
					{
						id: "Role", type: "buttonTwoState", mode: "select", text: "Role", img: "role.png"
					},
					{
						id: "All", type: "buttonTwoState", mode: "select", text: "Shared", img: "default-18px.png"
					},
					{
						id:   "sep_id",
						type: "separator"
					},
					{
						id: "add-new", type: "button", mode: "select", text: "Add new", img: "add-18px.png"
					},
					{
						id: "delete-selected", type: "button", mode: "select", text: "Delete selected", img: "delete-18px.png"
					}
				]
			});	
			
			$scope.toolbarAvailableSubjects.addSpacer("sep_id");
			
			$scope.toolbarAvailableSubjects.attachEvent("onStateChange", function(id, state){
				
				if(state === false) {
					
					$scope.toolbarAvailableSubjects.setItemState(id, true);
					
				} else {
					
					$scope.toolbarAvailableSubjects.setItemState('User', false);
					$scope.toolbarAvailableSubjects.setItemState('Role', false);
					$scope.toolbarAvailableSubjects.setItemState('All', false);
					
				}
				
				$scope.toolbarAvailableSubjects.setItemState(id, true);
				
				$scope.model.auditory = id;
				$scope.index();
				
			});
			
			$scope.toolbarAvailableSubjects.attachEvent("onClick", function(id) {
				
				switch(id) {
					
					case "add-new":	
						
						$scope.addItem();
						
					break;
					
					case "delete-selected":	
						
						$scope.deleteItem($scope.gridVersions.getSelectedId());
						
					break;
					
				}
				
			});			
			
		/* ------: Attach edit event :------ */
			
			$scope.gridVersions.attachEvent("onEditCell", function(stage, rId, cInd, nValue, oValue){
				
				// Check action
				
				switch(stage) {
					
					case 0:
						
						var value = $scope.gridVersions.cellById(rId,cInd).getValue();
						
					break;
					
					case 1:
						
						// 
						
					break;
					
					case 2:
						
						if (oValue != nValue) {
							
							$scope.updateItem(rId, nValue);
							
						}
						
					break;
					
					default:
						
						alert("Unknown method");
						
					break;
				}
				
				return true;
				
			});
			
			$scope.toolbarAvailableSubjects.setItemState($scope.model.auditory, true);
			
			$scope.index();
			
		};
		
		$scope.index = function(){
			
			$scope.windowModal.window.progressOn();
			
		/* ------: Get all items :------ */
			
			Item.index({
				
				id : [$attrs.id],
				target : {
					auditory : $scope.model.auditory
				}
				
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.windowModal.window.progressOff();
				
				$scope.gridVersions.clearAll();
				
				if(response.length === 0){
					
					
				} else {
					
					var data = {};
					
					data.rows = $.map(response.data, function(item) {
						
						return {
							
							id : item.id,
							data:
							[
								item.name,
								moment.unix(item.updated).format("YYYY-MM-DD HH:mm:ss")
							]
							
						};
						
					});						
					
					$scope.gridVersions.parse(data, "json");
					
				}
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				$scope.windowModal.window.progressOff();
				
				dhtmlx.message({
					type: "error",
					text: "Error to get items : " + response.error,
					expire: 5000
				});	
				
			});
			
		};
		
		$scope.loadItem = function(id, item) {
			
			Item.get({
				
				id : [id],
				item : item,
				target : {
					auditory : $scope.model.auditory
				}
				
			})
			
			/* ------: On success :------ */
			
			.success(function(response) {
				
				$scope.model.item = item;
				
				if(item === undefined) {
					
					$scope.toolbar.setItemText("items", "Default");
					
				}
				else {
					
					$scope.toolbar.setItemText('items', $scope.gridVersions.cells(item,0).getValue());
					
				}
				
				$scope.$broadcast('loaditem', {
					
					data : response.data
					
				});
				
				try {$scope.windowModal.unload()} catch (error) {}
				
			})
			
			/* ------: On error :------ */
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: "Error to load item : " + response.error,
					expire: 5000
				});					
				
			});
			
		};
		
	});
		
// EOF