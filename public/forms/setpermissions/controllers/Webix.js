/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Webix
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix forms
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	https://docs.optibet.lv/admin/mounts.html
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $controller, View, User, Object) {
				
		$controller('Forms', {$scope: $scope, $attrs: $attrs, $element:$element});
				
		var controllers = [];
		
		$.each($scope.modal.data.controllers, function( key, value ) {
			
			var controller = {id:key, name:$scope.modal.data.controllers[key].name};
			controllers.push(controller);
			
		});
		
		function getDataForController(controller){
			var methods = [];
			$.each($scope.modal.data.controllers[controller].methods, function( key, value ) {
				
				var method = {id:key, name:$scope.modal.data.controllers[controller].methods[key].title};
				methods.push(method);
			});			
			
			return methods;
		}
		
		function getDataForMethod(controller, method){
			
			// if multicombo then combined data
			// else if combo then get for each
			
			// check if method is array and if yes then do the thing
			// if for this array storage is the same then process
			// else show something that these methods incompatible for items
			
			// also keep one storage for different factories!!!
			
			var source = $scope.modal.data.controllers[controller].methods[method];
			
			$$("formAdd").removeView("objects");
			
			if (source && source.objects && typeof source.objects.get === 'function') {
				
				if (source[source.objects.storage] === undefined) {
					
					source.objects.get()
					
					.success(function(response) {
						
						source[source.objects.storage] = response.data || [];
						source[source.objects.storage].unshift({id:"*", name:"Any"});
						
						$$("formAdd").addView(
							{
								view: "multicombo",
								id: 'objects',
								name: "objects",
								label: "Items",
								button: true,
								suggest: {
									body: {
										data: source[source.objects.storage],
										template: webix.template("#name#")
									}
								},
							}, 2
						);
						
					});	
					
				} else {
					
					$$("formAdd").addView(
						{
							view: "multicombo",
							id: 'objects',
							name: "objects",
							label: "Items",
							button: true,
							suggest: {
								body: {
									data: source[source.objects.storage],
									template: webix.template("#name#")
								}
							},
						}, 2
					);					
					
				}
			}
		}			
		
		webix.ui
		(
			{
				view: "form",
				id: "formAdd",
				container:"formAdd",
				elements: [
					{
						view: "combo",
						id: 'ctrl',
						name: "ctrl",
						label: "Controller",
						button: true,
						suggest: {
							body: {
								data: controllers,
								template: webix.template("#name#")
							}
						},
						on: { 'onChange' : function(controller) 
							{
								$$("formAdd").removeView("objects");
								$$("formAdd").removeView("methods");
								$$("formAdd").addView(
									{
										view: $scope.modal.data.controllers[controller].view,
										id: 'methods',
										name: "methods",
										label: $scope.modal.data.controllers[controller].label,
										button: true,
										suggest: {
											body: {
												data: getDataForController(controller),
												template: webix.template("#name#")
											}
										},
										on: { 'onChange' : function(method) 
											{
												getDataForMethod(controller, method);
											}
										}												
									}, 1
								);
							}
						}							
					}, 
					{
						view: "button",
						value: "Add",
						align: "center",
						type:"form",
						on: { 'onItemClick' : function() 
							
							{
								$scope.submit("add");
							}
							
						}			            
					}
				]
			}
		);		
		
		
		$scope.representPermissions = function(){
			
			// Remove existing
			
			$(".tree-multiselect").remove();
			$("#permissions").empty();
			
			// Convert permissions to presentation for treeMultiselect
			
			$.each($scope.modal.data.permissions, function( user, permissions ) {
				
				$.each(permissions, function( key ) {
					
					var permission = permissions[key].name.split('.');
					
					var route = "";
					
					$.each(permission, function( index, segment ) {
						
						if(index === (permission.length - 1)){
							
							$("#permissions").append(' \
								<option value="' + permissions[key].name + '" data-section="' + route.slice(0,-1) + '">'
									+ segment +
								'</option>'
							);
							
						} else {
							
							route += segment + "/";
							
						}
						
					});					
					
				});
				
			});
			
			// Initialize treeMultiselect
			
			$("#permissions").treeMultiselect({ sortable: true, startCollapsed:true });
			
		};
		
		
		$scope.representData = function(){
			
			$scope.representPermissions();
				
			webix.ui
			(
				{
					view: "form",
					id: "formRemove",
					container:"formRemove",
					borderless:true,
					elements: [
						{ 
							margin:5, 
							cols:
							[
								{ 
									view:"button", value:"Close",
									on: { 'onItemClick' : function() 
										
										{
											$scope.cancel();
										}
										
									}								
								},
								{ 
									view:"button", value:"Remove selected", type:"form",
									on: { 'onItemClick' : function() 
									
										{
											$scope.submit("remove");
										}
										
									}		            	
								}
							]
						}
					]
				}
			);	
		};			
		
		$scope.prepareData = function(method){
			
			var response = { method : method };
			
			switch (method) {
				
				case "add":
					
					var data = $$("formAdd").getValues();
					
					var permissions = [];
					var configuration = [];
					var segments, permission;
					
					$.each(data, function( key, value ) {
						if(data[key].length>0) {
							configuration.push(data[key].split(','));
						}
					});
					
					configuration = permutations(configuration);
					
					$.each(configuration, function( key, value ) {
						
						segments = configuration[key].split('.');
						permission = $scope.modal.data.controllers[segments[0]].methods[segments[1]].method;
						permission += $scope.modal.data.controllers[segments[0]].methods[segments[1]].url;
						if(segments[2] !== undefined) permission += "/" + segments[2];
						
						permission = permission.replace(/\//g, '.');
						permissions.push(permission);
						
					});
						
					response.data = permissions;
					
				break;
				
				case "remove":
					
					response.data = $(".selected").find( ".item" ).map(function(){return $(this).attr("data-value");}).get();
					
				break;					
				
			}
			
			return response;
			
		};
		
		
		// Request state
		
		$scope.$watch('modal.callback', function(callback) {
			
			switch(callback) {
				
				case "add":
					
					$scope.representPermissions();
					
				break;
				
				
				case "remove":
					
					// Remove items
					
					$.each($(".selected").find( ".item" ).map(function(){return $(this).attr("data-value");}).get(), function( key, value ) {
						
						$("[data-value='" + value + "']").remove();
						
					});
					
					// Remove empty
					
					$(".section").each(function () {
						
						if($(this).find(".item").length === 0) $(this).remove();
						
					});
					
				break;						
				
			}
			
		});
		
		
		// Inheritance (should be iherited from controller Form)
		
		$scope.submit = function(method){
			
			$scope.processForm($scope.prepareData(method));
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};				
		
		$scope.representData();
		
	});		
	


// EOF