
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		functions	:
						[
							"/js/functions/main.js"
						],
						
		controllers	:
						[
							"/js/controllers/Forms.js",
							"/forms/setpermissions/controllers/Webix.js"
						],
							    	
		directives	:	
						[
						
						],
						
		services	:
						[
							"/js/services/View.js",
							"/js/services/User.js",
							"/js/services/Object.js",
						],									
						
		form			:
						[
							//"/forms/setpermissions/app.js?" + Date.now()
							"/apps/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'chance',
		'jquery',
		'jqueryui',
		'treeselect',
		'webix',
		'angular',

		
		/* ---: Local :--- */
		
		'functions',
		'services',
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
