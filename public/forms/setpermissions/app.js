// Configure application

	$.getJSON( "/forms/setpermissions/modules.json?a=2", function( data ) {

		var app = angular.module('appFormSetPermissions', data.modules, 
			function($interpolateProvider) {
				$interpolateProvider.startSymbol('<%');
				$interpolateProvider.endSymbol('%>');
			}
		)
		
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
		}])
		
		.provider('$rootScope', function () {
			this.$get = ['$window', function ($window) {
				return $window.parent.angular.element($window.frameElement).scope().$new();
			}];
		});		
		
		// Run application
		
		app.run(function($rootScope) {
			
		})
		
	
	});
	
// EOF