/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Editview
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $controller, Model, Object, Source, Interpolators) {
		
		$controller('Forms', {$rootScope: $rootScope, $scope: $scope, $element: $element, $attrs: $attrs});
		
		var elements = [];
		
		$scope.renderForm = function(){
			
			webix.ui({
				view:"form", 
				id:"form",
				paddingX:25,
				elements: elements
			});	
			
		};
		
		$scope.getComponents = function(components){
			
			var items = [];
			
			$.each($scope.gridstack.widgets, function( index, value ) {
				
				if(components.indexOf($scope.gridstack.widgets[index].attr('data-component')) > -1) {
					
					if($scope.gridstack.fromview !== undefined) {
						
						items.push({
							
							id : $scope.gridstack.widgets[index].attr('id').replace("gs-", ""),
							name : $scope.gridstack.widgets[index].attr('data-label')
							
						});
						
						
					} else {
						
						items.push({
							
							id : $scope.gridstack.widgets[index].attr('id'),
							name : $scope.gridstack.widgets[index].attr('data-label')
								
						});
						
					}
					
				}
				
			});
			
			return items;
			
		};
		
		$scope.submit = function(){
			// datapoint-metrics.io/dashboard/db/metrics-example
			//console.log($$("form").getValues());
			
			//console.log($$("dataview1").getSelectedItem());
			
			switch($scope.formtype){
				
				case "graph":
					
					var form = $$("form").getValues();
					
					$scope.processForm({
						modelbrowser: form.modelbrowser,
						category : form.tabview,
						graph: {
							id : $$(form.tabview).getSelectedItem().id,
							title: $$(form.tabview).getSelectedItem().title
						}
					});
					
				break;
				
				
				default:
					
					var values = $$("form").getValues();
					
					$.each( values, function( key, value ) {
						
						if(value === undefined) {
							
							values[key] = $$(key).config.body.data;	
							
						}
						
					});
					
					if(typeof $scope.processForm == "undefined") {
						
						console.log(window.parent.controller.Views.processForm(values));
						
					} else {
					
						$scope.processForm(values);
					
					}
						
				break;
				
			}
			
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};			
		
		// This is when settings called from View not from Editview
		
		if(typeof $scope.modal === 'undefined') {
			
			$scope.modal = window.parent.controller.Views.modal;
			
		} 
		
		if(typeof $scope.gridstack === 'undefined') {
			
			$scope.gridstack = {
				
				widgets : $scope.modal.widgets,
				fromview : true
				
			};
			
		} 		
		
		
		var settings = JSON.parse($scope.gridstack.widgets[$scope.modal.widgetid].attr("data-settings"));
		
		
		switch($scope.modal.component) {
			
			case "graph":
				
				$scope.formtype = "graph";
				
				var cells = [];
				
				$.getJSON("/conf/charts.json?" + Date.now(), function(charts) {
					
					$.each(charts, function(category) {
						
						var categorydata = [];
						
						$.each(charts[category].data, function(chart) {
							
							charts[category].data[chart].id = chart;
							categorydata.push(charts[category].data[chart]);
							
						});
						
						cells.push(
							
							{
								header: charts[category].header,
								body: {
									view: "dataview", 
									id: category,
									name: category,
									select:true,
									type: {
										height: 160,
										width: 250
									},
									template:"<div class='webix_strong'>#title#</div> <img style='width:230px;height:120px;' src='#img#'/>",
									data: categorydata
								}
							}						
							
						);
						
					});
					
					elements.unshift(
						{
							view : "combo",
							id : 'modelbrowser',
							name : "modelbrowser",
							labelPosition : "top",
							label : "Link to model browser",
							button : true,
							value : settings.modelbrowser, 
							suggest : {
								body : {
									data : $scope.getComponents(["modelbrowser"]),
									template : webix.template("#name#")
								}
							}	
						},	
						{
							view : "combo",
							id : 'filter',
							name : "filter",
							labelPosition : "top",
							label : "Link to filter",
							button : true,
							value : settings.filter, 
							suggest : {
								body : {
									data : $scope.getComponents(["filter"]),
									template : webix.template("#name#")
								}
							}	
						},							
						{
							view: "tabview",
							id: "tabview",
							name: "tabview",
							padding:0,
							margin:5,
							multiview:{
							    animate:true
							},				
							cells: cells
						}
					);
						
					$scope.renderForm();
					
				})
				
				/* ------: On error :------ */
				
				.error(function(response, status) {
					
					dhtmlx.message({
						type: "error",
						text: "Get charts : " + status,
						expire: 5000
					});
					
				});
				
				
			break;
			
			
			case "modelbrowser":
				
				Model.index({})
				
				/* ------: On success :------ */
				
				.success(function(response) {
					
					elements.unshift(
						{
							view: "combo",
							id: 'model',
							name: "model",
							label: "Data from",
							button: true,
							value : settings.model,
							suggest: {
								body: {
									data: response.data,
									template: webix.template("#name#")
								}
							},
						}
					);
					
					$scope.renderForm();
					
				});
				
			break;
			
			case "singlestat":
				
				var grid = {};
				
				Model.index({})
				
				/* ------: On success :------ */
				
				.success(function(response) {
					
					elements.unshift(
						{
							view: "text",
							name: "label",
							label: "Label",
							value: settings.label
						},
						{
							view: "combo",
							id: 'model',
							name: "model",
							label: "Data from",
							button: true,
							value : settings.model,
							suggest: {
								body: {
									data: response.data,
									template: webix.template("#name#")
								}
							},
							on: { 
								'onChange' : function(id) 
								{
									
									$$("form").removeView("interpolators");
									
									Interpolators.pipe({id: id})
									
									/* ------: On success :------ */
									
									.success(function(response) {									
										
										console.log(response);
										
										var available = [];
										
										$.each(response, function( key, model ) {
											
											$.each(model.interpolators, function( index, interpolator ) {
												
												available.push(
													{
														model: model.model.name,
														key: interpolator,
														value : ""
													}
												);
												
											});
											
										});
										
										console.log(available);
										
										grid = {
											view: "treetable", 
											autoheight: true, 
											select: true,
											editable: true,
											editaction: "dblclick",  
											scrollX: false,
											columns: [
												{ id: "model",  header: {text: "Model", height: 30}, fillspace: 1},
												{ id: "key",  header: {text: "Parameter", height: 30}, fillspace: 1},
												{ id: "value", editor: "text", header: {text: "Value", height: 30}, fillspace: 1}
											],
											data: available
										};
										
										
										$$("form").addView({
											
											view: "forminput", id : "interpolators", name : "interpolators", body: grid
											
										}, 2);										
										
									});
									
									/*
									
									Object.get({
										
										id : id,
										type : "model"
										
									})
									
									.success(function(response) {
										
										var available = [];
										
										if(typeof response.data.data.interpolators == 'object') {
											
											$.each(response.data.data.interpolators, function( key, value ) {
												
												available.push(
													{
														key: key,
														value : value
													}
												);
												
											});
											
											grid = {
												view: "datatable", 
												autoheight: true, 
												select: true,
												editable: true,
												editaction: "dblclick",  
												scrollX: false,
												columns: [
													{ id: "key",  header: {text: "Parameter", height: 30}, fillspace: 1},
													{ id: "value", editor: "text", header: {text: "Value", height: 30}, fillspace: 1}
												],
												data: available
											};
											
											
											$$("form").addView({
												
												view: "forminput", id : "interpolators", name : "interpolators", body: grid
												
											}, 2);
											
										}
										
									});
									
									*/
								}
							}
						},
						{	view: "forminput",
							id : "interpolators",
							name : "interpolators",
							body: grid 
						},
						{
							view : "text",
							id : "reloadinterval",
							name : "reloadinterval",
							label : "Reload",
							value : settings.reloadinterval
						},
						{
							view : "checkbox",
							id : "showdiff",
							name : "showdiff",
							label : "Show diff",								
							labelWidth : 200,
							value : settings.showdiff
						},
						{
							view : "text",
							id : "backgroundcolor",
							name : "backgroundcolor",
							label : "Color",
							value : settings.backgroundcolor
						},
						{
							view : "text",
							id : "broadcast",
							name : "broadcast",
							label : "Broadcast",
							value : settings.broadcast
						},
						{
							view : "text",
							id : "subscribe",
							name : "subscribe",
							label : "Subscribe",
							value : settings.subscribe
						},
						{
							view : "text",
							id : "transform",
							name : "transform",
							label : "Transform",
							value : settings.transform
						}
					);
					
					$scope.renderForm();
					
					$$("form").removeView("interpolators");
					
					if(typeof settings.model == 'string') {
						
						Object.get({
							
							id : settings.model,
							type : "model"
							
						})
						
						.success(function(model) {
							
							console.log(model);
							
							var available = [];
							
							if(typeof model.data.data.interpolators == 'object') {
								
								$.each(model.data.data.interpolators, function( key, value ) {
									
									available.push(
										{
											key : key,
											value : value
										}
									);
									
								});
								
								grid = {
									view: "datatable", 
									autoheight: true, 
									select: true,
									editable: true,
									editaction: "dblclick",  
									scrollX: false,
									columns: [
										{ id: "key",  header: {text: "Parameter", height: 30}, fillspace: 1},
										{ id: "value", editor: "text", header: {text: "Value", height: 30}, fillspace: 1}
									],
									data: available
								};
								
								if(typeof settings.interpolators == 'object' ) {
									
									$.each(grid.data, function( index ) {
										
										$.each(settings.interpolators, function( index2 ) {
											
											if(grid.data[index].key == settings.interpolators[index2].key) {
												
												grid.data[index].value = settings.interpolators[index2].value;
												
											}
											
										});
										
									});
									
								}
								
							}
							
							if(typeof grid.data == 'object') {
								
								$$("form").addView({
									
									view: "forminput", id : "interpolators", name : "interpolators", body: grid
									
								}, 2);								
								
							}
							
						});
						
					}
					
				});
				
			break;
			
			case "url":
				
				elements.unshift(
					{
						view:"text",
						id:"url",
						name:"url",
						label:"URL",
						value: settings.url
					},
					{
						view : "text",
						id : "reloadinterval",
						name : "reloadinterval",
						label : "Reload",
						value : settings.reloadinterval
					}					
				);
				
				$scope.renderForm();
				
				
			break;
			
			case "datagrid":
				
				Model.index({})
				
				/* ------: On success :------ */
				
				.success(function(response) {
					
					elements.unshift(
						{
							view: "combo",
							id: 'model',
							name: "model",
							label: "Data from",
							button: true,
							value : settings.model,
							suggest: {
								body: {
									data: response.data,
									template: webix.template("#name#")
								}
							},
						},
						{
							view : "checkbox",
							id : "clientcacheenabled",
							name : "clientcacheenabled",
							label : "Client side cache enabled",								
							labelWidth : 200,
							value : settings.clientcacheenabled
						},
						{
							view : "checkbox",
							id : "autoloadenabled",
							name : "autoloadenabled",
							label : "Autoload",								
							labelWidth : 200,
							value : settings.autoloadenabled
						},
						{
							view : "checkbox",
							id : "hideheader",
							name : "hideheader",
							label : "Hide header",								
							labelWidth : 200,
							value : settings.hideheader
						},
						{
							view : "checkbox",
							id : "hiderownumbers",
							name : "hiderownumbers",
							label : "Hide row numbers",								
							labelWidth : 200,
							value : settings.hiderownumbers
						},
						{
							view : "checkbox",
							id : "hidestatusbar",
							name : "hidestatusbar",
							label : "Hide status bar",								
							labelWidth : 200,
							value : settings.hidestatusbar
						},
						{
							view : "checkbox",
							id : "hidescrollx",
							name : "hidescrollx",
							label : "Hide scroll X",								
							labelWidth : 200,
							value : settings.hidescrollx
						},
						{
							view : "checkbox",
							id : "hidescrolly",
							name : "hidescrolly",
							label : "Hide scroll Y",								
							labelWidth : 200,
							value : settings.hidescrolly
						},
						{ 
							view : "text", 
							id : "zoom",
							name : "zoom", 
							label : "Zoom", 
							value : settings.zoom
						},
						{ 
							view : "text", 
							id : "reloadinterval",
							name : "reloadinterval", 
							label : "Reload", 
							value : settings.reloadinterval
						},
						{
							view : "text",
							id : "broadcast",
							name : "broadcast",
							label : "Broadcast",
							value : settings.broadcast
						},	
						{
							view : "text",
							id : "subscribe",
							name : "subscribe",
							label : "Subscribe",
							value : settings.subscribe
						}						
					);
					
					$scope.renderForm();
					
				});
				
			break;
			
			
			case "filter":
				
				elements.unshift
				(
					{
						view : "combo",
						id : 'datagrid',
						name : "datagrid",
						labelPosition : "top",
						label : "Link to",
						button : true,
						value : settings.datagrid, 
						suggest : {
							body : {
								data : $scope.getComponents(["datagrid", "modelbrowser"]),
								template : webix.template("#name#")
							}
						}	
					},
					{
						view : "checkbox",
						id : "hideorderandpaging",
						name : "hideorderandpaging",
						label : "Hide order and paging",								
						labelWidth : 200,
						value : settings.hideorderandpaging
					},					
					{
						view : "checkbox",
						id : "ignoresamecond",
						name : "ignoresamecond",
						label : "Ignore the same conditions",								
						labelWidth : 200,
						value : settings.ignoresamecond
					}
				);
				
				$scope.renderForm();
				
			break;
			
			
			case "tableastarget":
				
				elements.unshift
				(
					{
						view : "combo",
						id : 'datasource',
						name : "datasource",
						labelPosition : "top",
						label : "Data source",
						button : true,
						value : settings.datasource, 
						suggest : {
							body : {
								data : $scope.getComponents(["modelbrowser"]),
								template : webix.template("#name#")
							}
						}	
					}
				);
				
				$scope.renderForm();
				
			break;	
			
			
			case "editor":
				
				Source.index({})
				
				/* ------: On success :------ */
				
				.success(function(response) {
					
					elements.unshift
					(
						{
							view : "combo",
							id : 'source',
							name : "source",
							labelPosition : "top",
							label : "Source",
							button : true,
							value : settings.source, 
							suggest: {
								body: {
									data: response.data,
									template: webix.template("#name#")
								}
							},
						},
						{
							view : "combo",
							id : 'target',
							name : "target",
							labelPosition : "top",
							label : "Target",
							button : true,
							value : settings.target, 
							suggest : {
								body : {
									data : $scope.getComponents(["tableastarget"]),
									template : webix.template("#name#")
								}
							}	
						}
					);
					
					$scope.renderForm();
				
				});
				
			break;			
			
			
			case "daterange" :
				
				elements.unshift(
					{
						view: "combo",
						id: 'datagrid',
						name: "datagrid",
						label: "Link to",
						button: true,
						value : settings.datagrid,
						suggest: {
							body: {
								data: $scope.getComponents(["datagrid","modelbrowser"]),
								template: webix.template("#name#")
							}
						},
						on: { 'onChange' : function(id) 
							{
								
								Object.get({
									id : JSON.parse($scope.gridstack.widgets[id].attr("data-settings")).model,
									type : "model",
								})
								
								/* ------: On success :------ */
								
								.success(function(response) {
									
									var fields = $.map(JSON.parse(response.data.data.settings), function(field) {
										
										if(field.Type == "date") {
											
											return {
												id : field["Field name"],
												title : field["Field title"].length > 0 ? field["Field title"] : field["Field name"]
											};
											
										}
									});									
									
									if(fields.length > 0) {
										
										$$("form").removeView("field");
										
										$$("form").addView(
											{
												view: "combo",
												id: 'field',
												name: "field",
												label: "Field",
												suggest: {
													body: {
														data: fields,
														template: webix.template("#title#")
													}
												},
											}, 1
										);
										
									} else {
										
										$$("form").removeView("field");
										
									}
									
								});								
								
							}
						}						
					},
					{
						view : "checkbox",
						id : "autoapply",
						name : "autoapply",
						label : "Auto apply",								
						labelWidth : 200,
						value : settings.autoapply
					}					
				);
				
				if(settings.field !== undefined && settings.datagrid !== undefined) {
					
					Object.get({
						id : JSON.parse($scope.gridstack.widgets[settings.datagrid].attr("data-settings")).model,
						type : "model",
					})	
					
					/* ------: On success :------ */
					
					.success(function(response) {
						
						var fields = $.map(JSON.parse(response.data.data.settings), function(field) {
							if(field.Type == "date") {
								
								return {
									id : field["Field name"],
									title : field["Field title"].length > 0 ? field["Field title"] : field["Field name"]
								};
								
							}
						});									
						
						if(fields.length > 0) {
							
							$$("form").removeView("field");
							
							$$("form").addView(
								{
									view: "combo",
									id: 'field',
									name: "field",
									label: "Field",
									value : settings.field,
									suggest: {
										body: {
											data: fields,
											template: webix.template("#title#")
										}
									},
								}, 1
							);
							
						} else {
							
							$$("form").removeView("field");
							
						}
						
					});						
					
				}
				
				$scope.renderForm();
				
			break;			
			
			
			case "paginator" :
				
				elements.unshift(
					{
						view: "combo",
						id: 'datagrid',
						name: "datagrid",
						label: "Link to",
						button: true,
						value : settings.datagrid,
						suggest: {
							body: {
								data: $scope.getComponents(["datagrid"]),
								template: webix.template("#name#")
							}
						},
					}
				);
				
				$scope.renderForm();				
				
			break;
			
			
			default:
				
				elements[0].cols.pop();
				elements.unshift({view:"label", label:"Component \"" + $scope.modal.component + "\" does not have settings."});
				$scope.renderForm();
				
			break;
			
		}		
		
	});		


// EOF