
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		controllers	:
						[
							"/js/controllers/Forms.js",
							"/forms/componentsettings/controllers/Webix.js"							
						],

		services	:
						[
							"/js/services/Model.js",
							"/js/services/Object.js",
							"/js/services/Source.js",
							"/js/services/Interpolators.js"
						],
							    	
		form :
						[
							"/forms/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'chance',
		'jquery',
		'webix',
		'dhtmlx',
		'angular',
		
		/* ---: Local :--- */
		
		'services',
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
