/**
 *	Module for form "setroles" and controller "Dhtmlx"
 * 
 *	@doc	module
 * 	@name	controller.Dhtmlx
 *	@duty	Rolands Strickis
*/

angular.module('controller.Dhtmlx', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Dhtmlx
	 * 
	 *	@doc	controller
	 * 	@name	Dhtmlx
	 *	@duty	Rolands Strickis
	*/

	.controller('Dhtmlx', function($rootScope, $parentScope, $scope, $element, $attrs, $controller, Role, User) {

	/* ------: Extend controller :------ */		
		
		$controller('Components', {$rootScope: $rootScope, $scope: $scope, $parentScope: $parentScope, $element: $element, $attrs: $attrs});
		$controller('Objects', {$scope: $scope, $attrs: $attrs, $parentScope: $parentScope, $element:$element});
		$controller('Forms', {$rootScope: $rootScope, $scope: $scope, $element: $element, $attrs: $attrs});
		
	/* ------: Attributes :------ */
		
		$attrs.window = {
			
			toolbar : true,
			conf: false,
			viewport : $attrs.id,
			guid : chance.guid()
			
		};	
		
	/* ------: Check modal data :------ */
		
		try {
			
			typeof $scope.modal.data.id;
			typeof $scope.modal.data.details;

		} catch (error){
			
			$scope.modal = {
				
				data : {
					
					id : [],
					details : []
					
				}
				
			};
			
		}
		
	/* ------: Model :------ */
		
		$scope.model = {
			
			id : $scope.modal.data.id,
			details : $scope.modal.data.details,
			
			roles : {
				
				assigned : {},
				available : {}
				
			}
			
		};
		
	/* ------: Create window :------ */
		
		$scope.construct($element, $attrs.window);
		$scope.layout = $scope.window.attachLayout("3T");
		
		$scope.layout.cells("a").hideHeader();
		$scope.layout.cells("b").hideHeader();
		$scope.layout.cells("c").hideHeader();
		
		$scope.layout.cells("a").setHeight(150);
		
	/* ------: Toolbar configuration :------ */
		
		// Define configuration
		
		$scope.conf.toolbar.private = [
			
			{ 
				id: "add", type: "button", img: "add-role-16px.png", text: "Add selected available"
			},					
			{ 
				id: "remove", type: "button", img: "remove-role-16px.png", text: "Remove selected assigned"
			},
			{ 
				id: "save", type: "button", img: "save-18px.png", text: "Save changes"
			},			
			{ 
				id: "close", type: "button", img: "close-18px.png", text: "Close"
			}
			
		];
		
		// Load configuration
		
		$scope.toolbar.loadStruct($scope.conf.toolbar.private);
		
		// Attach events
		
		$scope.toolbar.attachEvent("onClick", function(id) {
			
			switch(id) {
				
				case "save":
					
					$scope.submit();
					
				break;
				
				case "close":
					
					$scope.cancel();
					
				break;
				
			}
			
		});
		
	/* ------: User list :------ */
		
		var userList = {};
		
		userList.rows = $.map($scope.model.details, function(val,i) {
			
			return {
				
				id : val.id,
				data : [
					val.name + " (" + val.email + ")"
				]
				
			};
			
		});
		
		$scope.userList = $scope.layout.cells("a").attachGrid();
		$scope.userList.setImagePath("/include/dhtmlx/codebase/imgs/");
		$scope.userList.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
		$scope.userList.setHeader("Roles will be changed for the following users:");			
		$scope.userList.setStyle("font-size:15px;text-align:left;font-weight:bold;", "","", "");
		$scope.userList.setInitWidths("*,*");
		$scope.userList.setColAlign("left");
		$scope.userList.setColTypes("ro");
		$scope.userList.setColSorting("str");
		$scope.userList.init();
		$scope.userList.setSkin("material");
		$scope.userList.parse(userList, "json");
		
		// Disable selection
		
		$scope.userList.attachEvent("onBeforeSelect", function(row,old_row){
			
			return false;
			
		});		
		
	/* ------: Assigned roles :------ */
		
		if($scope.model.id.length > 0) {
			
			User.getRoles({
				
				objects : $scope.model.id
				
			})
			
			.success(function(response) {
				
				$.each(response, function( index, value ) {
					
					$scope.model.roles.assigned.rows = $.map(value, function(val, i) {
						
						return {
							
							id : val.id,
							data : [val.name]
							
						};
						
					});
					
				});
				
				$scope.assignedeRoles = $scope.layout.cells("c").attachGrid();
				$scope.assignedeRoles.setImagePath("/include/dhtmlx/codebase/imgs/");
				$scope.assignedeRoles.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
				$scope.assignedeRoles.setHeader("Assigned roles");			
				$scope.assignedeRoles.setStyle("font-size:19px;text-align:left;font-weight:bold;", "","", "");
				$scope.assignedeRoles.setInitWidths("*,*");
				$scope.assignedeRoles.setColAlign("left");
				$scope.assignedeRoles.setColTypes("ro");
				$scope.assignedeRoles.setColSorting("str");
				$scope.assignedeRoles.enableDragAndDrop(true);
				$scope.assignedeRoles.enableMultiselect(true);			
				$scope.assignedeRoles.init();
				$scope.assignedeRoles.setSkin("material");
				$scope.assignedeRoles.parse($scope.model.roles.assigned, "json");	
				
			})
			
			.error(function(response) {
				
				dhtmlx.message({
					type: "error",
					text: response.error,
					expire: 30000
				});
				
			});	
			
		}
		
	/* ------: Available roles :------ */					
		
		Role.index({})
		
		.success(function(response) {
			
			$scope.model.roles.available.rows = $.map( response.data, function(val,i) {
				return {
					id : response.data[i].id,
					data:
					[
						response.data[i].name,
						//response.data[i].created_at,
						//response.data[i].updated_at
					],										
				};
			});
			
			$scope.gridAvailableRoles = $scope.layout.cells("b").attachGrid();
			$scope.gridAvailableRoles.setImagePath("/include/dhtmlx/codebase/imgs/");
			$scope.gridAvailableRoles.setIconsPath("/include/dhtmlx/samples/dhtmlxTreeGrid/common/images/");
			$scope.gridAvailableRoles.setHeader("Available roles");
			$scope.gridAvailableRoles.setStyle("font-size:19px;text-align:left;font-weight:bold;", "","", "");
			$scope.gridAvailableRoles.setInitWidths("*,*");
			$scope.gridAvailableRoles.setColAlign("left");
			$scope.gridAvailableRoles.setColTypes("ro");
			$scope.gridAvailableRoles.setColSorting("str");
			$scope.gridAvailableRoles.enableDragAndDrop(true);
			$scope.gridAvailableRoles.enableMultiselect(true);				
			$scope.gridAvailableRoles.init();
			$scope.gridAvailableRoles.setSkin("material");
			$scope.gridAvailableRoles.parse($scope.model.roles.available, "json");	
			
		})
		
		.error(function(response) {
			
			dhtmlx.message({
				type: "error",
				text: response.error,
				expire: 30000
			});
			
		});	
		
		
		$scope.submit = function(){
			
			if(typeof $scope.assignedeRoles === "object") {
				
				var roles = [];
				
				$scope.assignedeRoles.forEachRow(function(id){
					
					roles.push(id);
					
				});			
				
				User.setRoles({
					
					objects : $scope.model.id,
					roles : roles
					
				})
				
				.success(function(response) {
					
					dhtmlx.message({
						text: "Done",
						expire: 30000
					});				
					
				})
				
				.error(function(response) {
					
					dhtmlx.message({
						type: "error",
						text: response.error,
						expire: 30000
					});
					
				});
			
			} else {
				
				dhtmlx.message({
					type: "error",
					text: "Nothing to save, because grid for assigned roles is not created",
					expire: 30000
				});				
				
			}
			
		};
		
		$scope.cancel = function(){
			
			try {
				
				$scope.cancelForm();
				
			} catch (error) {}
			
			try {
				
				$scope.close();
				
			} catch (error) {}
			
		};		
		
	});		

// EOF