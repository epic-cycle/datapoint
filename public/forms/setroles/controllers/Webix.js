/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Editview
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $controller) {
		
		$controller('Forms', {$rootScope: $rootScope, $scope: $scope, $element: $element, $attrs: $attrs});

		var small_film_set = [
			{ id:1, rank:"The Shawshank Redemption",},
			{ id:2, rank:"The Godfather",}
		];


		webix.ui({
		    container:"form",
		    id:"form",
		    type:"space",
		    rows: [
		        {
		        	cols:
		        	[
			            {

							view:"datatable",
							columns:[
								{ id:"rank",	header:"Name", css:"rank", width:250}
							],
							select:"row",
							autoheight:true,
							autowidth:true,
							multiselect:true,
			
							on:{
								onSelectChange:function(){
									var text = "Selected: "+grid.getSelectedId(true).join();
									document.getElementById('testB').innerHTML = text;
								}
							},
			
							data:small_film_set
			            	
			            },
			            {
							rows:
							[
								{ 
									view:"button", value:"Add >>",
									on: { 'onItemClick' : function() 
									
										{
											$scope.cancel();
										}
										
									}								
								},
								{ 
									view:"button", value:"<< Remove",
									on: { 'onItemClick' : function() 
									
										{
											$scope.submit();
										}
										
									}		            	
								}
							]
			            	
			            },
			            {

							view:"datatable",
							columns:[
								{ id:"rank",	header:"Name", css:"rank", width:250}
							],
							select:"row",
							autoheight:true,
							autowidth:true,
							multiselect:true,
			
							on:{
								onSelectChange:function(){
									var text = "Selected: "+grid.getSelectedId(true).join();
									document.getElementById('testB').innerHTML = text;
								}
							},
			
							data:small_film_set
			            	
			            }
		        	]
		        }
		    ]
		});

		/*

		webix.ready(function(){
			grid = webix.ui({
				container:"form",
				view:"datatable",
				columns:[
					{ id:"rank",	header:"", css:"rank",  		width:50},
					{ id:"title",	header:"Film title",width:200},
					{ id:"year",	header:"Released" , width:80},
					{ id:"votes",	header:"Votes", 	width:100}
				],
				select:"row",
				autoheight:true,
				autowidth:true,
				multiselect:true,

				on:{
					onSelectChange:function(){
						var text = "Selected: "+grid.getSelectedId(true).join();
						document.getElementById('testB').innerHTML = text;
					}
				},

				data:small_film_set
			});		
		});
		
		*/

		/*
		webix.ui({
			view:"form", 
			id:"form",
			elements:
			[
				{ view:"text", name:"name", label:"Name"},
				{ 
					margin:5, 
					cols:
					[
						{ 
							view:"button", value:"Cancel",
							on: { 'onItemClick' : function() 
							
								{
									$scope.cancel();
								}
								
							}								
						},
						{ 
							view:"button", value:"Save", type:"form",
							on: { 'onItemClick' : function() 
							
								{
									$scope.submit();
								}
								
							}		            	
						}
					]
				}
			]
			
		});	
		*/
		
		$scope.submit = function(){
			
			$scope.processForm($$("form").getValues());
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};			
			
	});		


// EOF