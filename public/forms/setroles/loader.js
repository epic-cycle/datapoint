
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		controllers	:
						[
					    	"/js/controllers/Objects.js",
							"/js/controllers/Components.js",							
							"/js/controllers/Forms.js",
							"/forms/setroles/controllers/Dhtmlx.js"							
						],
						
	    services	:
					    [
							"/js/services/Object.js",
							"/js/services/Role.js",
							"/js/services/User.js",
							"/js/services/Report.js"
					    ],						
							    	
		form :
						[
							"/apps/editview/app.css?" + Date.now(),
							"/forms/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'angular',
		'dhtmlx',
		'chance',
		
		/* ---: Local :--- */
		
		'controllers',
		'services',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
