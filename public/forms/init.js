/**
 *	Form initialization
 *
 *	@author	Rolands Strickis
 *	@duty	Rolands Strickis
*/	

	var app;

	$.getJSON($("html").attr('app')+"?" + new Date(), function(application) {
		
		app = angular.module(application.name, application.modules, 
			
			function($interpolateProvider) {
				
				$interpolateProvider.startSymbol(application.interpolators.start);
				$interpolateProvider.endSymbol(application.interpolators.end);
				
			}
			
		)
		
		.config(['$httpProvider', function($httpProvider) {
			
			var headers = $httpProvider.defaults.headers;
			headers.common["X-Requested-With"] = 'XMLHttpRequest';
			
		}])
		
		.run(function($rootScope) {
			
			console.log("Form " + application.name + " started.");
			if(typeof run === "function") run();
			
		});
		
		/* ------: Parent scope :------ */
			
		if(window.frameElement) {
			
			app.factory('$parentScope', function($window) {
				return $window.parent.angular.element($window.frameElement).scope();
			});
			
			app.provider('$rootScope', function () {
				this.$get = ['$window', function ($window) {
					return $window.parent.angular.element($window.frameElement).scope().$new();
				}];
			});				
			
		} else {
			
			app.factory('$parentScope', function($window) {
				return false;
			});		
			
		}
		
		angular.element(document).ready(function() {
			
			angular.bootstrap(document, [application.name]);
			
		});		
		
	});
	
// EOF