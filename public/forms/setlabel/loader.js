
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		controllers	:
						[
							"/js/controllers/Forms.js",
							"/forms/setlabel/controllers/Webix.js"							
						],
							    	
		form :
						[
							"/forms/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'webix',
		'angular',
		
		/* ---: Local :--- */
		
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
