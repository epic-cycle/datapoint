/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Editview
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $controller) {
		
		$controller('Forms', {$rootScope: $rootScope, $scope: $scope, $element: $element, $attrs: $attrs});
		
		webix.ui({
			view:"form", 
			id:"form",
			elements:
			[
				{ view:"text", name:"name", label:"Name"},
				{ 
					margin:5, 
					cols:
					[
						{ 
							view:"button", value:"Cancel",
							on: { 'onItemClick' : function() 
							
								{
									$scope.cancel();
								}
								
							}								
						},
						{ 
							view:"button", value:"Save", type:"form",
							on: { 'onItemClick' : function() 
							
								{
									$scope.submit();
								}
								
							}		            	
						}
					]
				}
			]
			
		});	
		
		$scope.submit = function(){
			
			$scope.processForm($$("form").getValues());
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};			
			
	});		


// EOF