// Configure application

	$.getJSON( "/forms/createuser/modules.json", function( data ) {
		
		var app = angular.module('appFormCreateUser', data.modules, 
			function($interpolateProvider) {
				$interpolateProvider.startSymbol('<%');
				$interpolateProvider.endSymbol('%>');
			}
		)
		
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
		}])
		
		.provider('$rootScope', function () {
			this.$get = ['$window', function ($window) {
				return $window.parent.angular.element($window.frameElement).scope().$new();
			}];
		});		
		
		// Run application
		
		app.run(function($rootScope) {
			
		})
		
	
	});
	
// EOF