/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Editview
 *	@duty	Rolands Strickis
 *	@link	
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $parentScope, $controller) {
		
		$controller('Forms', {$rootScope: $rootScope, $scope: $scope, $element: $element, $attrs: $attrs});
		
		$scope.parent = window.parent.angular.element('[ng-controller=Editmodel]').scope();
		
		webix.ui({
			view:"form", 
			id:"form",
			elements:
			[
				{
					view : "checkbox",
					id : "enabled",
					name : "enabled",
					value : $scope.parent.model.data.caching.enabled,
					label : "Caching enabled",								
					labelWidth : 200,
				},
				{
					view : "combo",
					id : 'storage',
					name : "storage",
					label : "Storage",
					button : true,
					value : $scope.parent.model.data.caching.storage,
					suggest : {
						body : {
							data : [
								{
									"id" : 1,
									"name" : "Redis"
								},
								{
									"id" : 2,
									"name" : "Filesystem"
								}								
							],
							template: webix.template("#name#")
						}
					},
				},				
				{ 
					view:"text", 
					name:"expiration", 
					//placeholder: "minutes", 
					label:"Expiration", 
					value : $scope.parent.model.data.caching.expiration,
					bottomPadding: 35,
					bottomLabel: "&nbsp;&nbsp;* minutes"
				},
				{ 
					margin:5, 
					cols:
					[
						{ 
							view:"button", value:"Cancel",
							on: { 'onItemClick' : function() 
							
								{
									$scope.cancel();
								}
								
							}								
						},
						{ 
							view:"button", value:"Done", type:"form",
							on: { 'onItemClick' : function() 
							
								{
									$scope.submit();
								}
								
							}		            	
						}
					]
				}
			]
			
		});	
		
		$scope.getData = function(){
			
			return $$("form").getValues();
			
		};
		
		// Inheritance
		/*
		$scope.submit = function(){
			
			$scope.processForm($$("form").getValues());
			
		};
		
		$scope.cancel = function(){
			
			$scope.cancelForm();
			
		};
		*/
		
	});		


// EOF