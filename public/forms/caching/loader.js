
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		controllers	:
						[
							"/js/controllers/Forms.js",
							"/forms/caching/controllers/Webix.js"							
						],
						
		services	:
						[
							"/js/services/Model.js"
						],
							    	
		form :
						[
							"/forms/init.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'chance',
		'jquery',
		'webix',
		'angular',
		
		/* ---: Local :--- */
		
		'services',
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
