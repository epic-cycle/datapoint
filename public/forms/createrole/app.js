// Configure application

	var app;

	$.getJSON( "/forms/createrole/app.json", function(application) {
		
		app = angular.module(application.name, application.modules, 
			
			function($interpolateProvider) {
				
				$interpolateProvider.startSymbol(application.interpolators.start);
				$interpolateProvider.endSymbol(application.interpolators.end);
				
			}
			
		)
		
		.config(['$httpProvider', function($httpProvider) {
			
			var headers = $httpProvider.defaults.headers;
			headers.common["X-Requested-With"] = 'XMLHttpRequest';
			
		}]);

		/* ------: Parent scope :------ */
			
		if(window.frameElement) {
			
			app.provider('$rootScope', function () {
				this.$get = ['$window', function ($window) {
					return $window.parent.angular.element($window.frameElement).scope().$new();
				}];
			});	
			
		}
		
		app.run(function($rootScope) {
			
			console.log("Application " + application.name + " started.");
			
		});
		
		angular.element(document).ready(function() {
			
			angular.bootstrap(document, [application.name]);
			
		});	
		
	});
	
// EOF