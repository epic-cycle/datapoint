
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		functions	:
						[
						
						],
						
		controllers	:
						[
							"/js/controllers/Forms.js",
							"/forms/createrole/controllers/Webix.js"
						],
							    	
		directives	:	
						[
						
						],
						
		services	:
						[
					
						],									
						
		form			:
						[
							
							"/forms/createrole/app.css?" + Date.now(),
							"/forms/createrole/app.js?" + Date.now()

						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'webix',
		'angular',
		
		/* ---: Local :--- */
		
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
