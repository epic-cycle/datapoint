/**
 *	Module for controller "Webix"
 * 
 *  @doc	module
 * 	@name	controller.Webix
 *	@duty	Rolands Strickis
 *	@link	https://docs.angularjs.org/guide/module
*/

angular.module('controller.Webix', [])

/* -------------------------------- */	

	/**
	 *	Controller to operate with Webix forms
	 * 
	 *	@doc	controller
	 * 	@name	Webix
	 *	@duty	Rolands Strickis
	 *	@link	https://docs.optibet.lv/admin/mounts.html
	*/

	.controller('Webix', function($rootScope, $scope, $element, $attrs, $controller) {
		
		$controller('Forms', {$scope: $scope, $attrs: $attrs, $element:$element});
		
		webix.ui({
			view:"form", 
			id: $attrs.id,
			elements:
			[
				{ view:"text", name:"name", label:"Name"},
				{ 
					margin:5, 
					cols:
					[
						{ 
							view:"button", value:"Cancel",
							on: { 'onItemClick' : function() 
								
								{
									$scope.cancel();
								}
								
							}								
						},
						{ 
							view:"button", value:"Create", type:"form",
							on: { 'onItemClick' : function() 
								
								{
									$scope.submit();
								}
								
							}		            	
						}
					]
				}
			]
			
		});	
		
		$scope.getData = function(){
			
			return $$("form").getValues();
			
		};	
		
	});		

// EOF