// Configure application

	$.getJSON( "/forms/createuser/modules.json", function( data ) {
		
		var app = angular.module('appFormCreateUser', data.modules, 
			function($interpolateProvider) {
				$interpolateProvider.startSymbol('<%');
				$interpolateProvider.endSymbol('%>');
			}
		)
		
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
		}])
		
		.provider('$rootScope', function () {
			this.$get = ['$window', function ($window) {
				return $window.parent.angular.element($window.frameElement).scope().$new();
			}];
		});		
		
		// Run application
		
		app.run(function($rootScope) {
			
		})
		
		.controller('Webix', function($rootScope, $scope, $element, $attrs) {
			
			webix.ui({
				view:"form", 
				id:"log_form",
				elements:
				[
					{ view:"text", name:"name", label:"Name"},
					{ view:"text", name:"email", label:"E-Mail"},
					{ view:"text", name:"password", type:"password", label:"Password"},
					{ 
						margin:5, 
						cols:
						[
							{ 
								view:"button", value:"Cancel",
								on: { 'onItemClick' : function() 
								
									{
										$scope.cancel();
									}
									
								}								
							},
							{ 
								view:"button", value:"Create", type:"form",
								on: { 'onItemClick' : function() 
								
									{
										$scope.submit();
									}
									
								}		            	
							}
						]
					}
				]
				
			});	
			
			
			
			// Inheritance
			
			$scope.submit = function(){
				
				$scope.processForm($$("log_form").getValues());
				
			};
			
			$scope.cancel = function(){
				
				$scope.cancelForm();
				
			};			
				
		});		
		
	});
	
// EOF