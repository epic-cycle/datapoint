
ljs

	.addAliases({

		vendors	:
						[
							"/deps/main.js"
						],
		
		functions	:
						[
						
						],
						
		controllers	:
						[
							"/js/controllers/Forms.js"
						],
							    	
		directives	:	
						[
						
						],
						
		services	:
						[
					
						],									
						
		form			:
						[
							"/forms/createuser/app.js?" + Date.now()
						],									
		
	})

    .load(
		
		/* ---: Vendors :--- */
		
		'vendors',
		'jquery',
		'webix',
		'angular',
		
		/* ---: Local :--- */
		
		'controllers',
		'form',
		
		/* ---: Callback :--- */		
		
		function(){
			
			$('body').fadeIn(200);
			
		}	
	)
;

// EOF
