
// Third party dependencies list

ljs
	
	.addAliases({
		
		jquery		:
						[
							"/vendor/jquery/dist/jquery.min.js",
						],
						
		angular		:	
						[
							"/vendor/angular/angular.min.js"
						],
						
		d3			:	
						[
							"/vendor/d3/d3.min.js"
						],
						
		jqdock		:	[
							"/vendor/jqdock/jquery.jqDock.min.js"
						],	
						
		chance		:	[
							"/vendor/chance/chance.js"
						],	
						    
	    dhtmlx		:
						[									
							"/vendor/proprietary/dhtmlx/codebase/dhtmlx.css",
							"/vendor/proprietary/dhtmlx/codebase/dhtmlx.js"
						],
						
	    bootstrap		:
						[									
							"/vendor/bootstrap/dist/js/bootstrap.min.js",
							//"/vendor/bootstrap/dist/css/bootstrap.min.css",
						],						
						
	    momentjs		:
						[									
							"/vendor/momentjs/min/moment.min.js",
						],	
						
		gridstack :		[
							"/vendor/gridstack/dist/gridstack.css",
							"/vendor/gridstack/dist/gridstack.min.js",
							"/apps/gridstack/gridstack-demo.css",					
						],
						
		jqueryui :		[
							"/vendor/jquery-ui/jquery-ui.min.js"
						],
						
		jquerytouchp :	[
							"/vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"
						],
							
		lodash :		[
							"/vendor/lodash/dist/lodash.min.js"
						],	
						
		datatables :	[
							"/vendor/datatables.net/js/jquery.dataTables.min.js",
							"/vendor/datatables.net-bs/js/dataTables.bootstrap.min.js",
							"/vendor/datatables.net-responsive/js/dataTables.responsive.min.js",
							"/vendor/datatables.net-responsive-bs/js/responsive.bootstrap.js",
							"/vendor/datatables.net-bs/css/dataTables.bootstrap.min.css",
							"/vendor/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
							"/vendor/datatables.net-dt/css/jquery.dataTables.min.css"
						],							

		webix :			[
							"/vendor/proprietary/webix/codebase/webix.js",
							"/vendor/proprietary/webix/codebase/webix.css"
						],	
						
		treeselect :	[
							"/vendor/tree-multiselect/dist/jquery.tree-multiselect.min.css",
							"/vendor/tree-multiselect/dist/jquery.tree-multiselect.min.js",
						],	
						
		daterange :		[
							"/vendor/bootstrap-daterangepicker/daterangepicker.js",
							"/vendor/bootstrap-daterangepicker/daterangepicker.css",
						],	
						
		ace :			[
							"/vendor/ace-builds/src-noconflict/ace.js",
							"/vendor/ace-builds/src-noconflict/ext-language_tools.js"
						],	
	    jqsqlbuilder :
					    [
							"/vendor/jqsqlbuilder/dist/css/query-builder.default.css",
							"/vendor/jqsqlbuilder/dist/js/query-builder.standalone.js",
							"/vendor/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css"
					    ],	
					    
	    spreadjs :
					    [
							"/vendor/proprietary/wijmo/jquery-wijmo.css",
							"/vendor/proprietary/wijmo/jquery.wijmo-pro.all.3.20143.59.min.css",
							"/vendor/proprietary/wijmo/jquery.wijmo.wijspread.3.20151.16.css",
							"/vendor/proprietary/wijmo/jquery.wijmo-open.all.3.20143.59.min.js",
							"/vendor/proprietary/wijmo/jquery.wijmo-pro.all.3.20143.59.min.js",
							"/vendor/proprietary/wijmo/jquery.wijmo.wijspread.all.3.20151.16.min.js"
					    ],	
					    
		timer :			[
							"/vendor/jqrunner/build/jquery.runner-min.js",
						],	
						
		paginator	:	[
							"/vendor/paginator/smartpaginator.js",
							"/vendor/paginator/smartpaginator.css"
						],
						
		md5	:			[
							"/vendor/jquery-md5/jquery.md5.js"
						],	
						
		angularbs :		[
							"/vendor/angular-bootstrap/ui-bootstrap-tpls.min.js"
						],	
						
		tooltipster :	[
							"/vendor/tooltipster/dist/js/tooltipster.bundle.min.js",
							"/vendor/tooltipster/dist/css/tooltipster.bundle.min.css",
							"/vendor/tooltipster/dist/css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-shadow.min.css",
						],
						
		jqtextfill :	[
							"/vendor/jqtextfill/source/jquery.textfill.min.js"
						],	
						
		flowtype :		[
							"/vendor/flowtype/flowtype.js"
						],	
						
		fittext :		[
							"/vendor/fittext/jquery.fittext.js"
						],	

		d3v3 :			[
							"https://d3js.org/d3.v3.min.js"
						],
						
		d3v4 :			[
							"https://d3js.org/d3.v4.min.js"
						],
						
		datetimepicker:	[
							"/vendor/pikaday/css/pikaday.css",
							"/vendor/pikaday/plugins/pikaday.jquery.js"
						],
						
		select2:		[
							"/vendor/select2/select2.css",
							"/vendor/select2/select2.min.js"
						],
						
		note:			[
							"/vendor/summernote/dist/summernote.css",
							"/vendor/summernote/dist/summernote.min.js"
						],
						
		loading:		[
							"https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js",
							"https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"
						],
						
		boxfit:			[
							 "/vendor/jquery.boxfit/dist/jquery.boxfit.min.js"
						],
		slidereveal:		[
			
						"/vendor/slidereveal/dist/jquery.slidereveal.min.js"
			
					]
		
	});

// EOF
