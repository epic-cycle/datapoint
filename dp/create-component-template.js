/**
 *	Short component description
 *
 *	@author	Firstname Lastname
 *	@duty	Firstname Lastname
*/	

/* ------: Settings :------ */

	id = "componentname";
	
	
/* ------: Module :------ */
	
	angular.module("directive." + id, [])
		
		
	/* ------: Directive :------ */
		
		.directive(id, function() {
			
			return {
				
			/* ------: Properties :------ */
				
				restrict: 'AE',
				replace: false,
				scope: true,
				
				
			/* ------: Template :------ */	       
				
				template: ' \
					<div class="component"> \
						<div class="viewport"> \
						</div> \
					</div> \
				',
				
				
			/* ------: Link function :------ */	  	    
				
				link: function($scope, $element, $attrs) {
					
				/* ------: Attributes :------ */		    	
					
					var attrs = {
						viewport : "viewport-" + $attrs.id,
						guid : $attrs.id,
					};					
					
					$element.find('.viewport').attr("id", "viewport-" + $attrs.id);
					
					
				/* ------: Create window :------ */
					
					$scope.construct($element, attrs);
					
				}
			};
		});
		
	/* ------: end of directive :------ */	
	
// EOF