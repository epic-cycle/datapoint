FROM alpine:3.5

# Install software

WORKDIR /tmp
RUN apk add --update curl php5-fpm php5-dom php5-mcrypt php5-curl php5-pgsql php5-pdo_pgsql php5-sqlite3 php5-pdo_sqlite php5-json php5-openssl php5-phar php5-ctype nodejs nginx supervisor git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer 


# Directories and permissions

RUN mkdir -p /app/log
RUN mkdir -p /app/www
RUN mkdir -p /run/nginx

RUN chmod 0777 /app/log
RUN chmod 0777 /run


# Get code

WORKDIR /app/www
RUN git clone https://bitbucket.org/epic-cycle/datapoint.git
WORKDIR /app/www/datapoint


# Install dependencies

RUN composer install

RUN npm install
RUN npm install yargs
RUN npm install -g bower
RUN npm install -g gulp@3.8.8

RUN npm install gulp@3.8.8
RUN npm install gulp-untar
RUN npm install gulp-gunzip
RUN npm install gulp-bower
RUN npm install --unsafe-perm gulp-sqlite3
RUN npm install gulp-shell
RUN npm install gulp-concat
RUN npm install gulp-replace
RUN npm install gulp-git
RUN npm install gulp-shell
RUN npm install gulp-clean

RUN gulp --docker true

RUN yes | php artisan migrate:refresh --force --seed


# Nginx

COPY nginx/ /etc/nginx/


# PHP FPM

RUN chown -R nobody:nobody /app/www/datapoint/
ADD php5/php-fpm.conf /etc/php5/


# Supervisord

COPY supervisord.conf /etc/


# Clean

RUN rm -rf /tmp/*
RUN rm -rf /var/cache/apk/*
RUN rm -rf /app/www/datapoint/node_modules/*


# Finish

WORKDIR /app

EXPOSE 80

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]
